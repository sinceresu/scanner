#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <QDialog>
#include <QVBoxLayout>
#include <QApplication>
#include <BlockShower/BlockShower.h>
#include "MainWindow.h"

#include <ros/ros.h>
#include "qnode.hpp"
#include <thread>
#include <QTimer>
#include <QtCore/QtCore>
#include <QtGui/QtGui>
#include <QtOpenGL/QtOpenGL>
#include <QThread>




int main( int argc, char *argv[] )
{

    QApplication app(argc, argv);

    MainWindow mw;
    mw.showMaximized();

    // 选择BlockDB存储目录
    mw.slotSetDBPath();
    // 添加一个工作区域, 调用函数中需要输入此区域的大小
    mw.slotAddArea();
    // 添加一个扫描区域
    mw.slotAddScanArea();
    // 开始扫描工作, 使用已经创建的扫描区域ID为参数
    mw.slotStartWork();

    // 实例qnode对象
    yd_scanner_ui::QNode qnode(argc, argv );
    // 启动ros订阅与调用block显示
    qnode.init(&mw);


    app.exec();
    return 0;
}
