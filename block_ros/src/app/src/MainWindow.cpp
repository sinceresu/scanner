#include "MainWindow.h"
#include <osg/Quat>
#include <osg/Vec3>
#include <ydUtil/log.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <ydUtil/matrix_math.h>


MainWindow::MainWindow()
    : QMainWindow( NULL )
{



    srand(time(NULL));
    QWidget *lpMainWidget = new QWidget;                                              //QWidget类是所有的用户接口对象的基类
    setCentralWidget( lpMainWidget );                                                 //通过使用一个作为所有这些其他窗口部件父对象的QWidget，以及通过使用布局管理器管理这些子窗口部件的大小和位置

    QVBoxLayout *lpMainLayout = new QVBoxLayout;                                      //用于构造垂直框布局对象
    lpMainLayout->setMargin(0);
    lpMainWidget->setLayout( lpMainLayout );                                          //将垂直框(主窗口) 加入 基类 【主从1】

    // 创建osg窗口时, 需要一个更新回调函数
    QWidget *lpWidget = _blockShower.createOsgWidget([this](){
                                                         this->emitUpdate();
                                                         //emit this->signalUpdate();
                                                     });

    // 更新信号绑定到刷新函数
    connect( this, SIGNAL( signalUpdate() ), lpWidget, SLOT( update() ) );
    // osg窗口添加到主窗口
    lpMainLayout->addWidget( lpWidget );                                              // osg窗口添加到（主窗口） 【主从2-1】

    QHBoxLayout *lpButtonLayout = new QHBoxLayout;                                    // 水平布局
    lpMainLayout->addLayout( lpButtonLayout );                                        // 水平布局 添加到垂直布局 【主从2-2】

    // {
    //     QPushButton *lpButton = new QPushButton("SetDBPath");
    //     lpButtonLayout->addWidget( lpButton );
    //     connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotSetDBPath() ) );
    // }
    // {
    //     QPushButton *lpButton = new QPushButton("AddArea");
    //     lpButtonLayout->addWidget( lpButton );
    //     connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotAddArea() ) );
    // }
    // {
    //     QPushButton *lpButton = new QPushButton("AddScanArea");
    //     lpButtonLayout->addWidget( lpButton );
    //     connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotAddScanArea() ) );
    // }

    // {
    //     QPushButton *lpButton = new QPushButton("StartWork");
    //     lpButtonLayout->addWidget( lpButton );
    //     connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotStartWork() ) );
    // }

    // {
    //     QPushButton *lpButton = new QPushButton("StopWork");
    //     lpButtonLayout->addWidget( lpButton );
    //     connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotStopWork() ) );
    // }

    // {
    //     QPushButton *lpButton = new QPushButton("^");
    //     lpButtonLayout->addWidget( lpButton );
    //     connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotUpTrack() ) );
    // }

    // {
    //     QPushButton *lpButton = new QPushButton("V");
    //     lpButtonLayout->addWidget( lpButton );
    //     connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotDownTrack() ) );
    // }

    // {
    //     QPushButton *lpButton = new QPushButton("<");
    //     lpButtonLayout->addWidget( lpButton );
    //     connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotLeftTrack() ) );
    // }

    // {
    //     QPushButton *lpButton = new QPushButton(">");
    //     lpButtonLayout->addWidget( lpButton );
    //     connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotRightTrack() ) );
    // }

    // {
    //     QPushButton *lpButton = new QPushButton("()");
    //     lpButtonLayout->addWidget( lpButton );
    //     connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotRotate() ) );
    // }


    connect( &_workTimer, SIGNAL( timeout() ), this, SLOT( slotMakeCloudTimeout() ) );

}




MainWindow::~MainWindow()
{

}

void MainWindow::emitUpdate()
{
    emit signalUpdate();
}
void MainWindow::slotSetDBPath()
{
    // 选择BlockDB存储目录
/*
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Project Directory"),
                                                    QString::fromLocal8Bit(getenv("HOME")),
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    if (dir.isEmpty())
    {
        return;
    }

    std::string sdir = std::string(dir.toLocal8Bit());

    _blockShower.setDBPath(sdir);
*/
    _blockShower.setDBPath("/home/ke/blockdb/");
    _blockShower.initScene();
}

void MainWindow::slotAddArea()
{
    ydBlockDB::BlockIndex minIndex = ydBlockDB::get( -500, -500, 0 );
    ydBlockDB::BlockIndex maxIndex = ydBlockDB::get( 500, 500, 10 );
    _areaID = _blockShower.createArea(minIndex, maxIndex, "area1", "");
    TRACE("areaID : %s\n", _areaID.c_str() );
}
void MainWindow::slotAddScanArea()
{
    osg::Vec3 pos;
    osg::Quat q;
    _scanAreaID = _blockShower.createScanArea( pos.x(), pos.y(), pos.z(), q.x(), q.y(), q.z(), q.w() );
    TRACE("scanAreaID : %s\n", _areaID.c_str() );
}
int  G_TimeCount = 0;
float G_StepX = 0.0f;




void MainWindow::slotStartWork()
{

    G_TimeCount = 0;
    _blockShower.startWorkScanArea( _scanAreaID );
    _workTimer.start( 1000 );

}


void MainWindow::slotStopWork()
{
    _workTimer.stop();
}


 void MainWindow::slotMakeCloudTimeout()
 {
    // 这里实现的随机生成点云, 生成一次后把定时器停止
    // int s = 500;
    // pcl::PointCloud<pcl::PointXYZI> cloud;
    // cloud.resize( s );
    // for ( int i = 0; i < s; i ++ )
    // {
    //     float x = ( rand() % 200 ) / 10.0f - 5 + G_StepX;
    //     float y = ( rand() % 200 ) / 20.0f - 5;
    //     float z = ( rand() % 40 ) / 10.0f;
    //     float k = ( rand () % 100 ) / 100.0f;

    //     cloud[i].x = x;
    //     cloud[i].y = y;
    //     cloud[i].z = z;
    //     cloud[i].intensity = k;
    // }

    // // G_StepX += 10.0f;

    // _blockShower.setCloudPoint( &cloud );

    // // if ( G_TimeCount >= 1 )
    // {
    //     _workTimer.stop();
    // }

    // G_TimeCount ++;

 }



void MainWindow::slotUpTrack()
{
    // 测试代码, 这里的相机参数是把原来的位置做个调整, 实际使用时请用真实的相机位姿代替
    osg::Matrix mat = _blockShower.getViewCone()->getMatrix();
    osg::Vec3 trans = mat.getTrans();
    trans -= osg::Vec3(0.0, 1.0, 0.0);
    osg::Quat q = mat.getRotate();
    
    // 测试
    updateCamera(trans, q);
}
void MainWindow::slotDownTrack()
{
    // 测试代码, 这里的相机参数是把原来的位置做个调整, 实际使用时请用真实的相机位姿代替
    osg::Matrix mat = _blockShower.getViewCone()->getMatrix();
    osg::Vec3 trans = mat.getTrans();
    trans += osg::Vec3(0.0, 1.0, 0.0);
    osg::Quat q = mat.getRotate();
    
    updateCamera(trans, q);
}
void MainWindow::slotLeftTrack()
{
    // 测试代码, 这里的相机参数是把原来的位置做个调整, 实际使用时请用真实的相机位姿代替
    osg::Matrix mat = _blockShower.getViewCone()->getMatrix();
    osg::Vec3 trans = mat.getTrans();
    trans += osg::Vec3(1.0, 0.0, 0.0);
    osg::Quat q = mat.getRotate();
    
    updateCamera(trans, q);
}
void MainWindow::slotRightTrack()
{
    // 测试代码, 这里的相机参数是把原来的位置做个调整, 实际使用时请用真实的相机位姿代替
    osg::Matrix mat = _blockShower.getViewCone()->getMatrix();
    osg::Vec3 trans = mat.getTrans();
    trans -= osg::Vec3(1.0, 0.0, 0.0);
    osg::Quat q = mat.getRotate();
    
    updateCamera(trans, q);
}

void MainWindow::slotRotate()
{
    // 测试代码, 这里的相机参数是把原来的位置做个调整, 实际使用时请用真实的相机位姿代替
    srand(time(NULL));
    osg::Matrixd mat = _blockShower.getViewCone()->getMatrix();
    osg::Vec3 trans = mat.getTrans();

    osg::Vec3d at, up;
    float yaw, pitch, roll;
    ydUtil::ComputePoseVecFromTransformd(mat, at, up);
    ydUtil::ConvertPoseVecToAngle(at, up, yaw, pitch, roll);

    int r = rand() % 3;
    if(r == 0)
    {
        yaw += 5;
    }
    if(r == 1)
    {
        pitch += 5;
    }
    if(r == 2)
    {
        roll += 5;
    }

    osg::Quat q(osg::inDegrees(roll), osg::Y_AXIS,
                osg::inDegrees(pitch), osg::X_AXIS,
                osg::inDegrees(-yaw), osg::Z_AXIS);

    updateCamera(trans, q);


}

void MainWindow::updateCamera(osg::Vec3 pos, osg::Quat q)
{
    // 测试代码, 使用使用时请用真实相机位姿代替
    _blockShower.updateCameraPose(pos.x(), pos.y(), 1.5, q.x(), q.y(), q.z(), q.w());
}
