#include "qnode.hpp"
// #include "device_error.hpp"

#include "boost/bind.hpp"

#include <ros/ros.h>
#include <ros/network.h>

#include <string>
#include <sstream>
#include <std_msgs/String.h>

#include "time.h"
// #include "utils.h"
#include "gflags/gflags.h"

//Qt5
#include <QtCore/QProcess>
#include <QtWidgets/QApplication>

// PORT
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/types.h>

#include <nav_msgs/Path.h>

DECLARE_string(topics);

using namespace std;

namespace yd_scanner_ui {
    QNode::QNode(int argc, char** argv ) : init_argc(argc), init_argv(argv) { }
    QNode::~QNode() {
        if(ros::isStarted()) {
            ros::shutdown();
            ros::waitForShutdown();
        }
        wait();
    }

    // DeviceError *deviceError;

    bool QNode::init( MainWindow* mw) {

        QNode::mw = mw;
        ROS_INFO("QNode -> init()");
        ros::init(init_argc, init_argv, "yd_scanner");

        if(!ros::master::check()) return false;

        // deviceError = new DeviceError();

        ros::start();
        ros::NodeHandle n;

        launchInit();  // 读取相关配置
        initMessages(n);

        start();

        return true;
    }

    // launch初始化
    void QNode::launchInit() {
        ROS_INFO("QNode -> launchInit()");
        // TODO
    }
    // 消息回调初始化
    void QNode::initMessages(ros::NodeHandle& n) {
        ROS_INFO("QNode -> initMessages()");



        // 订阅坐标变换后点云消息
       Subscriber[1] = n.subscribe<sensor_msgs::PointCloud2>("/yida_slam/mapping/cloud_registered_raw", 1, boost::bind(&QNode::ns1VelodyneCallback, this, _1));


        // 订阅位姿话题，消息类型nav_msgs::Odometry      
        Subscriber[3] = n.subscribe<nav_msgs::Odometry>("/yida_slam/mapping/odometry", 1, boost::bind(&QNode::ns1VelodyneCallback2, this, _1));
        
       

    }


    void QNode::run() {

        // ros::Rate loop_rate(ros::Duration(30));
        ros::Rate loop_rate(10);
        while(ros::ok()) {

            ros::spinOnce();
            loop_rate.sleep();
        }

        std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
        Q_EMIT rosShutdown();

        cout << "ROSNODE LIST:" << endl;
        system("rosnode list");

        //system("rosnode kill -a");
        // system("rosnode kill cam_capture_node");
    }
}
