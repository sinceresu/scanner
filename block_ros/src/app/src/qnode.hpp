#ifndef yd_scanner_ui_QNODE_HPP_
#define yd_scanner_ui_QNODE_HPP_

// To workaround boost/qt4 problems that won't be bugfixed. Refer to https://bugreports.qt.io/browse/QTBUG-22829
#ifndef Q_MOC_RUN
#include <ros/ros.h>
#include <stdio.h>
// Msg
#include <std_msgs/String.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/TransformStamped.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>

//#include <yidamsg/Image.h>
#endif

#include <string>
#include <vector>

// Qt5
#include <QtCore/QThread>
#include <QtCore/QStringListModel>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>

#include "MainWindow.h"
#include <nav_msgs/Path.h>


#include <pcl/io/pcd_io.h>
#include <iostream>




using namespace std;

namespace yd_scanner_ui
{
    class QNode : public QThread
    {
        Q_OBJECT
    public:
        // Constructor & Destructor
        QNode(int argc, char **argv);
        virtual ~QNode();

        // Fun
        bool init(MainWindow *mw);
        void launchInit();
        void initMessages(ros::NodeHandle &n);
        void run();
        
        pcl::PointCloud<pcl::PointXYZI> out_pointcloud;

        // 订阅sensor_msgs::Point消息，并block展示激光点云
        void ns1VelodyneCallback(const sensor_msgs::PointCloud2ConstPtr &ptr)
        {

            //  ROS_INFO_STREAM ("8888888888888888888" <<  *ptr);

            //消息转换成pcl数据存放
            pcl::fromROSMsg(*ptr, out_pointcloud);
            mw->_blockShower.setCloudPoint(&out_pointcloud);
        }


        //订阅nav_msgs::Odometry消息，并block展示位姿
        void ns1VelodyneCallback2(const nav_msgs::OdometryConstPtr &ptr2)
        {

            // ROS_INFO_STREAM("8888888888888888888" << *ptr2);

            mw->_blockShower.updateCameraPose(
                ptr2->pose.pose.position.x,
                ptr2->pose.pose.position.y,
                ptr2->pose.pose.position.z,
                ptr2->pose.pose.orientation.x,
                ptr2->pose.pose.orientation.y,
                ptr2->pose.pose.orientation.z,
                ptr2->pose.pose.orientation.w);
        }

    Q_SIGNALS:
        void rosShutdown();

    protected:
        int init_argc;
        char **init_argv;
        MainWindow *mw;

        ros::Subscriber Subscriber[4];
    };
}
#endif
