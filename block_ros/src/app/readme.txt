
注意：本代码是ROS功能包，放在ROS工作空间下即可。

运行前，请...
1.修改 CMakeLists.txt以下路径,分别对应三个库。

include_directories(
~/local/include/
~/project/3_/sdk/include/
~/blockshower/include/
）

link_directories(
~/local/lib/
~/project/3_/sdk/lib/
~/blockshower/lib/
）

2.修改 MainWindow.cpp 中BlockDB存储目录路径。
_blockShower.setDBPath("/home/ke/blockdb/");

3.添加.bashr环境
source .../devel/setup.bash
export LD_LIBRARY_PATH=~/local/lib:~/project/3_/sdk/lib:~/blockshower/lib:$LD_LIBRARY_PATH



运行命令：roslaunch app  app.launch

