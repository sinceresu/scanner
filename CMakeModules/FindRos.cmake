
FIND_PATH( ROS_INCLUDE_PATH  ros/ros.h
  /opt/ros/melodic/include
  /opt/ros/noetic/include
  #/usr/include
  #/usr/local/include
  /sw/include
  /opt/local/include
  $ENV{HOME}/local/include
  DOC "The directory where liblaszip.h resides")

FIND_PATH( ROS_LIB_PATH  libroscpp.so
  /opt/ros/melodic/lib
  /opt/ros/noetic/lib
  /usr/lib
  /usr/local/lib
  /opt/local/lib
  $ENV{HOME}/local/lib
  DOC "The directory where liblaszip.h resides")

IF (ROS_INCLUDE_PATH)
  SET( ROS_FOUND 1)# CACHE STRING "Set to 1 if ROS is found, 0 otherwise")
ELSE (ROS_INCLUDE_PATH)
  SET( ROS_FOUND 0)# CACHE STRING "Set to 1 if ROS is found, 0 otherwise")
ENDIF (ROS_INCLUDE_PATH)

MARK_AS_ADVANCED( ROS_FOUND )
