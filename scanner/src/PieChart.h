#ifndef PIECHART_H
#define PIECHART_H

#include <QtWidgets/QWidget>

struct PieItem
{
	double value;
	QColor color;
	QString label;
};


struct PieChartPrivate;

class PieChart : public QWidget
{
	Q_OBJECT

public:
	PieChart(QWidget * parent = 0);
	~PieChart();

public:
	void SetStartX(int x);
	void SetStartY(int y);
	void SetMinDiameter(int diameter);
	void SetStartAngle(int angle);
	void SetRingWidth(int width);
	void SetLegendVisible(bool visible);
    void SetRingLabel(const QString & label);

	void AddData(double value, const QColor & color, const QString & label);
	void AddData(const PieItem & item);

protected:
	virtual void paintEvent(QPaintEvent * event) override;
	virtual void resizeEvent(QResizeEvent * event) override;

private:
	void ConstructData();
	void ConstructRect(const QSize & size);
	void ConstructCornerLayout(const QSize & size);

private:
	QScopedPointer<PieChartPrivate> d_ptr;
};

#endif // PIECHART_H
