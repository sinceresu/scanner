#ifndef BOTTOMSCAN_VIEWER_H
#define BOTTOMSCAN_VIEWER_H
#include <memory>
#include <chrono>

#include <QWidget>
#include <QTimer>
#include <functional>


namespace Ui {
class BottomScan;
}
class Tip;
class BottomScan : public QWidget
{
    Q_OBJECT

public:
    using ActionCallback = std::function< void( ) >;
    explicit BottomScan(ActionCallback startCallback, ActionCallback anchorCallback, ActionCallback endCallback, ActionCallback screenShotCallback, QWidget *parent = nullptr);
    explicit BottomScan(QWidget *parent);
    ~BottomScan();

private slots:
    void on_screenshotButton_clicked();

    void on_startButton_clicked();

    void on_anchorButton_clicked();

    void on_endButton_clicked();

    void slot_calibrate_timeout();

private:
    Ui::BottomScan *ui;
    QWidget* _scan;

    QTimer  calibrate_timer_;
    std::chrono::system_clock::time_point start_time_;
    int calibrate_stationary_seconds_;
    Tip *tipWidget;

    ActionCallback _startCallback;
    ActionCallback _anchorCallback;
    ActionCallback _endCallback;
    ActionCallback _screenShotCallback;
};

#endif // BOTTOMSCAN_VIEWER_H
