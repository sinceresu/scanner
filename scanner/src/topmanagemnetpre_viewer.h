#ifndef TOPMANAGEMNETPRE_VIEWER_H
#define TOPMANAGEMNETPRE_VIEWER_H

#include <QWidget>
#include <QTimer>
#include "StatusUpdate.h"

namespace Ui {
class TopManagemnetPre;
}

class TopManagemnetPre : public QWidget
{
    Q_OBJECT

public:
    explicit TopManagemnetPre(QWidget *parent = nullptr);
    ~TopManagemnetPre();

signals:
    void back();

private slots:
    void on_backLabel_clicked();
    void slotTimeout();
public:
    void setTitle(QString title);


private:
    Ui::TopManagemnetPre *ui;

    QTimer _timer;

    StatusUpdate _statusUpdate;
};

#endif // TOPMANAGEMNETPRE_VIEWER_H
