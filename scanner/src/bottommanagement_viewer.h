#ifndef BOTTOMMANAGEMENT_VIEWER_H
#define BOTTOMMANAGEMENT_VIEWER_H

#include "managementdetail_viewer.h"
#include "managementinfo_viewer.h"
#include "managementpre_viewer.h"

#include <QWidget>

namespace Ui {
class BottomManagement;
}

class BottomManagement : public QWidget
{
    Q_OBJECT

public:
    explicit BottomManagement(QWidget *parent = nullptr);
    ~BottomManagement();

    void setSelectUUID( QString const& uuid ){ _selectUUID = uuid; };
private slots:
    void back(QObject *obj);

private slots:
    void on_previewButton_clicked();
    void on_renameButton_clicked();
    void on_delButton_clicked();
    void on_detailButton_clicked();
    void on_infoButton_clicked();
    void on_exportButton_clicked();

private:
    ManagementPre *managementPre;
    ManagementInfo *managementInfo;
    ManagementDetail *detail;


private:
    Ui::BottomManagement *ui;
    QString _selectUUID;
};

#endif // BOTTOMMANAGEMENT_VIEWER_H
