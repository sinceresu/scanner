#include "tip.h"
#include "ui_tip.h"


#include "common.h"
#include "QRoundProgressBar.h"

#include <QPixmap>

Tip::Tip(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Tip)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_TranslucentBackground);
    loadStyleSheet(this, "tip");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    progressLabel = new QLabel();

    progressBar = new QRoundProgressBar();
    progressBar->setRange(0, 100);
    progressBar->setDecimals(0);
    progressBar->setOutlinePenWidth(2);
    progressBar->setDataPenWidth(2);
    progressBar->setBarStyle(QRoundProgressBar::StyleDonut);

    QPalette p2(palette());
    p2.setBrush(QPalette::Highlight, QColor(48, 79, 254));
//    p2.setBrush(QPalette::Window, Qt::NoBrush);
    p2.setBrush(QPalette::Base, Qt::white);
    p2.setBrush(QPalette::AlternateBase, QColor(246, 248, 255));
    p2.setColor(QPalette::Text, QColor(68, 68, 68));
    p2.setColor(QPalette::Shadow, Qt::white);
    progressBar->setPalette(p2);

//    connect(this, SIGNAL(valueChanged(int)), progressBar, SLOT(setValue(int)));

    ui->stackedWidget->addWidget(progressLabel);
    ui->stackedWidget->addWidget(progressBar);

    ui->stackedWidget->setCurrentIndex(0);
}

Tip::~Tip()
{
    delete ui;
}

void Tip::on_closeButton_clicked()
{
    this->close();
}

void Tip::updateProgress(int val)
{

}

void Tip::setLabel(QString label)
{
    ui->label->setText(label);
}

void Tip::progress(QString label, int val)
{
    ui->label->setText(label);
    ui->stackedWidget->setCurrentIndex(1);
    progressBar->setValue(val);

    // emit valueChanged(val);
}

void Tip::finish(QString label)
{
    ui->stackedWidget->setCurrentIndex(0);
    progressLabel->setPixmap(QPixmap(":/scanner/images/finish.png"));
    ui->label->setText(label);
}

