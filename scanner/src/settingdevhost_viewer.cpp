#include "settingdevhost_viewer.h"
#include "ui_settingdevhost_viewer.h"

#include "common.h"

SettingDevHost::SettingDevHost(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingDevHost)
{
    ui->setupUi(this);

    loadStyleSheet(this, "settingDevHost");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    ui->operIcon->setPixmap(QPixmap(":/scanner/images/settings/oper.png"));

    ui->cpuLabel->setText("CPU");
    ui->memLabel->setText("内存");
    ui->ssdLabel->setText("SSD硬盘");
    ui->interfaceLabel->setText("接口");
    ui->wifiLabel->setText("WiFi");
    ui->blueLabel->setText("蓝牙");
    ui->clLabel->setText("磁力计");
    ui->imuLabel->setText("IMU");
    ui->disLabel->setText("显示屏");
    ui->sizeLabel->setText("尺寸");
    ui->pixLabel->setText("分辨率");
    ui->lightLabel->setText("亮度");
    ui->operLabel->setText("操作");


    ui->cpuValLabel->setText("Inter i7");
    ui->memValLabel->setText("32GB DDR4");
    ui->ssdValLabel->setText("1TB");
    ui->interfaceValLabel->setText("USB 3.0 以太网");
    ui->wifiValLabel->setText("802.11a/b/g/n/ac(2.4 & 5.0GHz)");
    ui->buleValLabel->setText("4.0 BLE");
    ui->clValLabel->setText("具备");
    ui->imuValLabel->setText("具备");
    ui->disValLabel->setText("LCD");
    ui->sizeValLabel->setText("10.1英寸");
    ui->pixValLabel->setText("1280 * 800");
    ui->lightValLabel->setText("350cd/m2");
    ui->operValLabel->setText("多点触控");
}

SettingDevHost::~SettingDevHost()
{
    delete ui;
}
