#include "managementpre_viewer.h"
#include "ui_managementpre_viewer.h"
#include "mainwindow.h"
#include "bottommanagementpre_viewer.h"
#include "common.h"
#include "topmanagemnetpre_viewer.h"
#include <BlockShower/BlockShower.h>
#include <ydUtil/DirUtils.h>
#include "Logger.h"

ManagementPre::ManagementPre(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ManagementPre),
    _blockShower(new BlockShower::BlockShower())
{
    ui->setupUi(this);
    loadStyleSheet(this, "managementPre");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    TopManagemnetPre *top = new TopManagemnetPre(this);
    top->move(0, 0);
    top->setTitle("三维扫描预览");
    connect(top, &TopManagemnetPre::back, this, &ManagementPre::backSlot);

    // 预览
    BottomManagementPre *bottom = new BottomManagementPre(this);
    bottom->move(0, 960);
    bottom->setWindowOpacity(0.7);

    QWidget *testWidget = _blockShower->createOsgWidget([this](){
                                                            //this->emitUpdate();
                                                            //emit this->signalUpdate();
                                                        });

    testWidget->setParent(ui->mainWidget);
    testWidget->setObjectName(QStringLiteral("scanWidget"));
    testWidget->setGeometry(this->geometry());
    testWidget->setSizePolicy(this->sizePolicy());
    testWidget->setMinimumSize(QSize(this->minimumWidth(), this->minimumHeight()));

    _uuid = MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject");
    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject(_uuid);
    if ( lpProject )
    {
        std::string saveMapDirectory = lpProject->getBlockDBPath();
        // _blockShower->reset();
        _blockShower->initScene();
        _blockShower->setDBPath(saveMapDirectory);

        float lp = atof( lpProject->getParam("lengthinput").c_str());
        float wd = atof( lpProject->getParam("width").c_str());
        float ht = atof( lpProject->getParam("height").c_str());
        ydBlockDB::BlockIndex minIndex = ydBlockDB::get( -lp/2, -wd/2, 0.0 );//atof( lpProject->getParam("lengthinput").c_str());
        ydBlockDB::BlockIndex maxIndex = ydBlockDB::get( lp/2, wd/2, ht);
        
        std::string name = lpProject->getName();
        _blockShower->setWorkArea(minIndex, maxIndex, name, lpProject->getID() );
        _blockShower->loadAllScan();
    }

    connect( this, SIGNAL( signalUpdate() ), testWidget, SLOT( update() ) );
    connect( bottom, SIGNAL( signalRemove() ), this, SLOT( slotRemoveProject() ) );
    connect( bottom, SIGNAL( signalDetail() ), this, SLOT( slotDetailProject() ) );
    connect( bottom, SIGNAL( signalBack() ), this, SLOT( slotScanBack() ) );

}

ManagementPre::~ManagementPre()
{
    delete ui;
}


void ManagementPre::backSlot()
{
    emit back(this);
}

void ManagementPre::backDetail()
{
    if(NULL != detail) {
        detail->close();
        detail = NULL;
    }
}

void ManagementPre::slotRemoveProject()
{
    // _blockShower->reset();
    emit MainWindow::GetMainWindow()->signalRemoveProject(QString(_uuid.c_str()));
    emit back(this);
}

void ManagementPre::slotDetailProject()
{
    detail = new ManagementDetail( QString(_uuid.c_str()) );
    detail->showFullScreen();
    detail->show();
    connect(detail, &ManagementDetail::back, this, &ManagementPre::backDetail);
}

void ManagementPre::slotScanBack()
{
    _blockShower->clearArea();
    _blockShower->loadAllScan();
}