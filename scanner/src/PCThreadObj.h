#ifndef __SCANNERUI_PROJECT_COPY_THREAD_H_FILE__
#define __SCANNERUI_PROJECT_COPY_THREAD_H_FILE__
#include <QObject>
#include <string>
using namespace std;
class  PCThreadObj : public QObject
{
    Q_OBJECT
public:
    PCThreadObj(std::string const& sourceDir, std::string const& targetDir);
    ~PCThreadObj();

   std::string  _sourceDir;
   std::string  _targetDir;
   int _rate;
   int _totalcount;
   
   bool CopyProjectDir(std::string const& sourceDir, std::string const& targetDir);
   size_t copy_files(const string& from_dir, const string& to_dir, string filename = "*");
   //std::vector<std::string> FindFilesInPath(std::string const& dir, bool withDir, bool findInChildDir );
   //unsigned int GetAllFilesCount(std::string dir);
   
Q_SIGNALS:
    void signalUploadProgress(int iprogress);
    void signalFinishCP();
public slots:
    void slotStartCP();
    void slotEndCP();
};    

#endif  //__SCANNERUI_PROJECT_COPY_THREAD_H_FILE__
