#ifndef __SCANNER_SRC_SCANNERCFG_MANAGER_H_FILE__
#define __SCANNER_SRC_SCANNERCFG_MANAGER_H_FILE__
#include <ydUtil/Param.h>
#include <osg/ref_ptr>
#include "Project.h"
#include <string>
#include <vector>
#include <map>

class ScannerCfgManager
{
public:
    ScannerCfgManager();

    void setBinPath( std::string const& binPath );
    std::string getBinPath(){ return _binPath; };


    void setSavePath( std::string const& savePath );
    std::string getSavePath();

    void loadProject();
    void save();

    void addProject( Project *lpProject );
    void removeProject( std::string const& uuid );

    Project *getProject( std::string const& uuid );

    std::vector<std::string> getProjectList();

    void setParam( std::string const& key, std::string const& value );
    std::string getParam( std::string const& key );

protected:
    std::vector< osg::ref_ptr<Project> > _projects;

    osg::ref_ptr<ydUtil::MapParam> _params;

    std::string _binPath;

};



#endif // __SCANNER_SRC_SCANNERCFG_MANAGER_H_FILE__
