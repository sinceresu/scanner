#ifndef SETTINGS_VIEWER_H
#define SETTINGS_VIEWER_H

#include "settingdevinfo_viewer.h"
#include "settingitem_viewer.h"
#include "settinglog_viewer.h"
#include "settingmodel_viewer.h"
#include "settingsoftversion_viewer.h"
#include "settingstorage_viewer.h"
#include "settingsys_viewer.h"

#include <QWidget>

namespace Ui {
class Settings;
}

class Settings : public QWidget
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = nullptr);
    ~Settings();

private slots:
    void onButtonClicked(bool checked);
    void back(QObject *obj);

private:
    SettingModel *modelWidget;
    SettingDevInfo *dev;
    SettingStorage *storageWidget;
    SettingSoftVersion *version;
    SettingSys *sys;
    SettingLog *logWidget;

private:
    SettingItem *model;
    SettingItem *devInfo;
    SettingItem *storage;
    SettingItem *softVersion;
    SettingItem *restore;
    SettingItem *log;
    SettingItem *sysSetting;
    SettingItem *removeSDCard;
    SettingItem *shutdown;
    SettingItem *screenSwitch;

private:
    Ui::Settings *ui;
};

#endif // SETTINGS_VIEWER_H
