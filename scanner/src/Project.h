#ifndef __SCANNER_SRC_PROJECT_H_FILE__
#define __SCANNER_SRC_PROJECT_H_FILE__
#include <osg/Referenced>
#include <string>
#include <vector>
#include <ydUtil/Param.h>

class Project : public osg::Referenced
{
public:
    Project();
    ~Project();

    void setID( std::string const& id );
    std::string getID();

    void setName( std::string const& name );
    std::string getName();

    void setParam( std::string const &key, std::string value );
    std::string getParam( std::string const& key );

    void setPath( std::string const& projectPath );
    std::string getPath();

    std::string getBlockDBPath();
    std::string getScanRootPath();

    std::string addScanArea( std::string const& scanAreaUUID );
    bool delScanArea( std::string const& scanAreaUUID );

    //std::vector<std::string> getScanAreaList(){ return _scanAreaUUID; };

    void load();
    void save();

protected:
    std::string getCfgFile();
protected:
    std::string _cfgPath;

    std::string _projectPath;

    osg::ref_ptr<ydUtil::MapParam> _projectParam;

};

#endif // __SCANNER_SRC_PROJECT_H_FILE__
