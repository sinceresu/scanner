#include "settingdevinfo_viewer.h"
#include "ui_settingdevinfo_viewer.h"

#include "common.h"
#include "topscanner_viewer.h"

SettingDevInfo::SettingDevInfo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingDevInfo)
{
    ui->setupUi(this);

    setAttribute(Qt::WA_TranslucentBackground);
    loadStyleSheet(this, "settingDevInfo");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);


    TopScanner *top = new TopScanner(this);
    top->setTitle("设备信息");
    top->move(0, 0);

    settingDevHost = new SettingDevHost(this);
    settingDevRadar = new SettingDevRadar(this);
    settingDevCamer = new SettingDevCamer(this);
    settingDevBattery = new SettingDevBattery(this);

    ui->stackedWidget->addWidget(settingDevHost);
    ui->stackedWidget->addWidget(settingDevRadar);
    ui->stackedWidget->addWidget(settingDevCamer);
    ui->stackedWidget->addWidget(settingDevBattery);

    ui->stackedWidget->setCurrentIndex(0);
    currentToolButton = ui->hostToolButton;

    connect(top, &TopScanner::back, this, &SettingDevInfo::backSlot);

    ui->hostLabel->setText("主机配置");
    ui->radarLabel->setText("激光雷达");
    ui->camerLabel->setText("全景相机");
    ui->batteryLabel->setText("电池");
}

SettingDevInfo::~SettingDevInfo()
{
    delete ui;
}

void SettingDevInfo::on_hostButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->hostToolButton->setIcon(QIcon(":/scanner/images/settings/host_icon_select.png"));
    ui->radarToolButton->setIcon(QIcon(":/scanner/images/settings/radar_icon.png"));
    ui->camerToolButton->setIcon(QIcon(":/scanner/images/settings/camer_icon.png"));
    ui->batteryToolButton->setIcon(QIcon(":/scanner/images/settings/battery_icon.png"));
}


void SettingDevInfo::on_radarButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    ui->hostToolButton->setIcon(QIcon(":/scanner/images/settings/host_icon.png"));
    ui->radarToolButton->setIcon(QIcon(":/scanner/images/settings/radar_icon_select.png"));
    ui->camerToolButton->setIcon(QIcon(":/scanner/images/settings/camer_icon.png"));
    ui->batteryToolButton->setIcon(QIcon(":/scanner/images/settings/battery_icon.png"));
}


void SettingDevInfo::on_camerButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
    ui->hostToolButton->setIcon(QIcon(":/scanner/images/settings/host_icon.png"));
    ui->radarToolButton->setIcon(QIcon(":/scanner/images/settings/radar_icon.png"));
    ui->camerToolButton->setIcon(QIcon(":/scanner/images/settings/camer_icon_select.png"));
    ui->batteryToolButton->setIcon(QIcon(":/scanner/images/settings/battery_icon.png"));
}


void SettingDevInfo::on_batteryButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(3);
    ui->hostToolButton->setIcon(QIcon(":/scanner/images/settings/host_icon.png"));
    ui->radarToolButton->setIcon(QIcon(":/scanner/images/settings/radar_icon.png"));
    ui->camerToolButton->setIcon(QIcon(":/scanner/images/settings/camer_icon.png"));
    ui->batteryToolButton->setIcon(QIcon(":/scanner/images/settings/battery_icon_select.png"));
}

void SettingDevInfo::backSlot()
{
    emit back(this);
}
