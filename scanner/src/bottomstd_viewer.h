#ifndef BOTTOMSTD_VIEWER_H
#define BOTTOMSTD_VIEWER_H

#include <QWidget>

namespace Ui {
class BottomSTD;
}

class BottomSTD : public QWidget
{
    Q_OBJECT

public:
    explicit BottomSTD(QWidget *parent = nullptr);
    ~BottomSTD();


signals:
    void sigCapture();
    void sigManagement();
    void sigSettings();

private slots:
    void on_captureButton_clicked();

    void on_managementButton_clicked();

    void on_settingsButton_clicked();

private:
    Ui::BottomSTD *ui;
};

#endif // BOTTOMSTD_VIEWER_H
