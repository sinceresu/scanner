#ifndef CONFIRMWIDGET_VIEWER_H
#define CONFIRMWIDGET_VIEWER_H

#include <QWidget>
#include <QDialog>
#include <functional>

namespace Ui {
class ConfirmWidget;
}

enum class ConfirmType{
    Delete,
    Export
};

enum class ConfirmHitType{
    OK = 0X01,
    Cancel,
};


class ConfirmWidget : public QDialog
{
    Q_OBJECT

public:
    using ConfirmCallback = std::function<void ( ConfirmHitType cht ) >;
    explicit ConfirmWidget(ConfirmCallback fun, QWidget *parent = nullptr);
    ~ConfirmWidget();

    void setConfirmData(QString title, QString content, QString confirm, bool tip = false, QString tipContent = QString(""));
    void setIconConfirmData(ConfirmType type, QString content, QString confirm,  bool tip = false, QString tipContent = QString(""));
    void setProgressBarValue(int value);
    void setProgressBarState(bool show,int minvalue,int maxvalue);

public:
    Q_SIGNALS:
    void signalStartCP();

public slots:
    void slotFinishCP();
    void slotUploadProgress(int iprogress);

private slots:
    void on_closeButton_clicked();
    void on_submit_clicked();

private:
    bool tip {false};
    QString tipContent;

private:
    Ui::ConfirmWidget *ui;

    ConfirmCallback _callback;
};

#endif // CONFIRMWIDGET_VIEWER_H
