#ifndef COMMON_H
#define COMMON_H

#include <QFile>
#include <QWidget>
#include <QDebug>
#include <QString>

void loadStyleSheet(QWidget* widget, const QString &sheetName);
void loadStyle(QWidget *widget, const QString &styleSheet);
const QString getStyleSheet(const QString &sheetName);

#endif // COMMON_H
