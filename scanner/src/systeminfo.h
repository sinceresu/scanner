#ifndef __SYSTEM_INFO_H_FILE__
#define __SYSTEM_INFO_H_FILE__

#include <string>


void getDisInfo( std::string const& binPath, unsigned long long &freeDisk, unsigned long long &availableDisk, unsigned long long &totalSize );
float getBATPercent( std::string &status );

std::string getFullTimeStr( time_t t );
std::string getSimpleTimeStr( time_t t );

std::string formatTime( int t );

#endif // __SYSTEM_INFO_H_FILE__
