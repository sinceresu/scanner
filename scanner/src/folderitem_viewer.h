#ifndef FOLDERITEM_VIEWER_H
#define FOLDERITEM_VIEWER_H

#include <QWidget>

namespace Ui {
class FolderItem;
}

class FolderItem : public QWidget
{
    Q_OBJECT

public:
    explicit FolderItem(QWidget *parent = nullptr);
    ~FolderItem();

protected:
    void paintEvent(QPaintEvent *e) override;

public:
    void setFolderName(QString name);

    void Init(QString text,QString iconPath);
    void onSelect();
    void onRelease();

    void onDoubleSelect();

private:
    Ui::FolderItem *ui;
};

#endif // FOLDERITEM_VIEWER_H
