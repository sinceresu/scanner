#ifndef SETTINGMODEL_VIEWER_H
#define SETTINGMODEL_VIEWER_H

#include "settingitem_viewer.h"

#include <QWidget>

namespace Ui {
class SettingModel;
}

class SettingModel : public QWidget
{
    Q_OBJECT

public:
    explicit SettingModel(QWidget *parent = nullptr);
    ~SettingModel();

signals:
    void back(QObject *obj);

private slots:
    void backSlot();

private slots:
    void onButtonClicked(bool checked);
    void authSuccess();

private:
    SettingItem *operationType;
    SettingItem *userType;

private:
    Ui::SettingModel *ui;
};

#endif // SETTINGMODEL_VIEWER_H
