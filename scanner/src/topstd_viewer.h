#ifndef TOPSTD_VIEWER_H
#define TOPSTD_VIEWER_H

#include <QTimer>
#include <QWidget>
#include "StatusUpdate.h"

namespace Ui {
class TopSTD;
}

class TopSTD : public QWidget
{
    Q_OBJECT

public:
    explicit TopSTD(QWidget *parent = nullptr);
    ~TopSTD();

protected slots:
    void slotTimeout();

public:
    void showLogo(bool show);
    void setTitle(QString title);

private:
    QTimer _timer;
    Ui::TopSTD *ui;

    StatusUpdate _statusUpdate;
};

#endif // TOPSTD_VIEWER_H
