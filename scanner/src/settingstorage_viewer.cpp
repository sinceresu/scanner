#include "settingstorage_viewer.h"
#include "ui_settingstorage_viewer.h"


#include "PieChart.h"
#include "common.h"
#include "confirmwidget_viewer.h"
#include "topscanner_viewer.h"

SettingStorage::SettingStorage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingStorage)
{
    ui->setupUi(this);
    loadStyleSheet(this, "settingStorage");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    TopScanner *top = new TopScanner(this);
    top->setTitle("存储空间");
    top->move(0, 0);

    connect(top, &TopScanner::back, this, &SettingStorage::backSlot);

    PieChart *pieChart = new PieChart(ui->chartWidget);
    pieChart->SetStartX(2);
    pieChart->SetStartY(2);

    pieChart->SetRingLabel(QStringLiteral(""));
    pieChart->AddData(35, QColor(48, 79, 254), "");
    pieChart->AddData(25, QColor(42, 187, 79), "");
    pieChart->AddData(23, QColor(170, 80, 217), "");
    pieChart->AddData(27, QColor(202, 205, 213), "");


    ui->label1->setText("文件类型1 35GB");
    ui->icon1->setStyleSheet("background-color: rgba(48, 79, 254, 1); border-radius: 50%;");

    ui->label2->setText("文件类型2 25GB");
    ui->icon2->setStyleSheet("background-color: rgba(42, 187, 79, 1); border-radius: 50%;");

    ui->label3->setText("文件类型3 23GB");
    ui->icon3->setStyleSheet("background-color: rgba(170, 80, 217, 1); border-radius: 50%;");

}

SettingStorage::~SettingStorage()
{
    delete ui;
}

void SettingStorage::backSlot()
{
    emit back(this);
}
