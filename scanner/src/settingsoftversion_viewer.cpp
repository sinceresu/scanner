#include "settingsoftversion_viewer.h"
#include "ui_settingsoftversion_viewer.h"

#include "common.h"
#include "confirmwidget_viewer.h"
#include "topscanner_viewer.h"

SettingSoftVersion::SettingSoftVersion(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingSoftVersion)
{
    ui->setupUi(this);
    loadStyleSheet(this, "settingSoftVersion");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    TopScanner *top = new TopScanner(this);
    top->move(0, 0);

    ui->logo->setPixmap(QPixmap(":/scanner/images/logo.png"));
    connect(top, &TopScanner::back, this, &SettingSoftVersion::backSlot);

    ui->verLabel->setText("版本");
    ui->updateLabel->setText("最近升级时间");
    ui->updateContentLabel->setText("升级内容");
    ui->buttomLabel->setText("Copyright © 1998-2022 北京易达图灵科技有限公司版权所有");

    ui->verValLabel->setText("13.21");
    ui->verDisLabel->setText("(已是最新版)");
    ui->updateValLabel->setText("2022-04-18 22:00:00");
    ui->updateContentValLabel->setText("1TB");
}

SettingSoftVersion::~SettingSoftVersion()
{
    delete ui;
}


void SettingSoftVersion::backSlot()
{
    emit back(this);
}
