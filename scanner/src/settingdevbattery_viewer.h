#ifndef SETTINGDEVBATTERY_VIEWER_H
#define SETTINGDEVBATTERY_VIEWER_H

#include <QWidget>

namespace Ui {
class SettingDevBattery;
}

class SettingDevBattery : public QWidget
{
    Q_OBJECT

public:
    explicit SettingDevBattery(QWidget *parent = nullptr);
    ~SettingDevBattery();

private:
    Ui::SettingDevBattery *ui;
};

#endif // SETTINGDEVBATTERY_VIEWER_H
