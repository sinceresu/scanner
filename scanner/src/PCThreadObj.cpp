#include "PCThreadObj.h"
#include <ydUtil/log.h>
#include <ydUtil/DirUtils.h>
#include <ydUtil/StringUtils.h>
#ifndef _WIN32
    #include <unistd.h>
    #include <sys/time.h>
    #include <sys/resource.h>
    #include <sys/stat.h>
    #include <dirent.h>
    #include <fcntl.h>
#else
    #include <direct.h>
    #include <corecrt_io.h>
    #define rmdir _rmdir
#endif // _WIN32

PCThreadObj::PCThreadObj(std::string const& sourceDir, std::string const& targetDir)
{
    _sourceDir = sourceDir;
    _targetDir = targetDir;
    _rate = 0;
    _totalcount = 100;
}

PCThreadObj::~PCThreadObj()
{

}

bool PCThreadObj::CopyProjectDir(std::string const& sourceDir, std::string const& targetDir)
{
    if ( sourceDir.length() == 0 )
    {
        return false;
    }
    if ( targetDir.length() == 0 )
    {
        return false;
    }
    if ( sourceDir == targetDir )
    {
        return false;
    }
    ydUtil::makeSureDirExist( targetDir );
    std::vector<std::string> fl = ydUtil::findFileInPath( sourceDir.c_str(), true, false );
    #pragma omp parallel for
    for ( int i=0;i<fl.size();i++ )
    {
        struct stat childStat;
        std::string childFile;
        childFile = fl[i];
        std::string targetFile = childFile;
        targetFile = ydUtil::ReplaceStr( targetFile, sourceDir, targetDir );
        if (stat(childFile.c_str(), &childStat) == -1)
        {
            continue;
        }
        if (!( childStat.st_mode & S_IFDIR ) )
        {
            ydUtil::copyFile( childFile, targetFile );
            int rate = ((_rate++)/(_totalcount*1.0))*100;
            emit signalUploadProgress(rate);
        }
        else
        {
            CopyProjectDir( childFile, targetFile );
        }
    }
}

void PCThreadObj::slotStartCP()
{ 
    _totalcount =  ydUtil::getFileCount(_sourceDir.c_str());;
    if(_totalcount>0)
    {
        CopyProjectDir(_sourceDir,_targetDir);
    }
    emit signalFinishCP(); 
}

void PCThreadObj::slotEndCP()
{

}
