#include "settings_viewer.h"
#include "ui_settings_viewer.h"

#include <QDebug>
#include <sys/mount.h>
#include "common.h"
#include "confirmwidget_viewer.h"
#include "settingdevinfo_viewer.h"
#include "settinglog_viewer.h"
#include "settingmodel_viewer.h"
#include "settingshutdown_viewer.h"
#include "settingsoftversion_viewer.h"
#include "settingstorage_viewer.h"
#include "settingsys_viewer.h"
#include <ydUtil/log.h>
#include "StatusUpdate.h"
#include "Logger.h"
#include "mainwindow.h"

bool    g_bFullWindowMask = true;  
Settings::Settings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);

    model = new SettingItem(this);
    model->setNameAndIcon("模式切换", "model");
    model->move(80, 117 - 64);

    devInfo = new SettingItem(this);
    devInfo->setNameAndIcon("设备信息", "devInfo");
    devInfo->move(384, 117 - 64);

    storage = new SettingItem(this);
    storage->setNameAndIcon("存储空间", "storage");
    storage->move(688, 117 - 64);

    softVersion = new SettingItem(this);
    softVersion->setNameAndIcon("软件版本", "softVersion");
    softVersion->move(992, 117 - 64);

    restore = new SettingItem(this);
    restore->setNameAndIcon("恢复出厂设置", "restore");
    restore->move(1296, 117 - 64);

    log = new SettingItem(this);
    log->setNameAndIcon("日志记录", "log");
    log->move(1600, 117 - 64);


    //row 2
    sysSetting = new SettingItem(this);
    sysSetting->setNameAndIcon("系统设置", "sysSetting");
    sysSetting->move(80, 421 - 64);

    removeSDCard = new SettingItem(this);
    removeSDCard->setNameAndIcon("弹出SD卡", "removeSDCard");
    removeSDCard->move(384, 421 - 64);

    shutdown = new SettingItem(this);
    shutdown->setNameAndIcon("关机", "shutdown");
    shutdown->move(688, 421 - 64);

    screenSwitch = new SettingItem(this);
    screenSwitch->setNameAndIcon("全屏切换", "shutdown");
    screenSwitch->move(992, 421 - 64);

    connect(model->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));
    connect(devInfo->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));
    connect(storage->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));
    connect(softVersion->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));
    connect(restore->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));
    connect(log->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));
    connect(sysSetting->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));
    connect(removeSDCard->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));
    connect(shutdown->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));
    connect(screenSwitch->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));

}

Settings::~Settings()
{
    delete ui;
}

void Settings::onButtonClicked(bool checked)
{
    Q_UNUSED(checked);
    QObject *btn = sender();

    if(btn == model->getPushButton()){

        modelWidget = new SettingModel();
        modelWidget->showFullScreen();
        modelWidget->show();
        connect(modelWidget, &SettingModel::back, this, &Settings::back);

    } else if(btn == devInfo->getPushButton()){

        dev = new SettingDevInfo();
        dev->showFullScreen();
        dev->show();
        connect(dev, &SettingDevInfo::back, this, &Settings::back);

    } else if(btn == storage->getPushButton()){

        storageWidget = new SettingStorage();
        storageWidget->showFullScreen();
        storageWidget->show();
        connect(storageWidget, &SettingStorage::back, this, &Settings::back);

    } else if(btn == softVersion->getPushButton()){

        version = new SettingSoftVersion();
        version->showFullScreen();
        version->show();
        connect(version, &SettingSoftVersion::back, this, &Settings::back);

    } else if(btn == restore->getPushButton()){

        ConfirmWidget restoreConfirm ([]( ConfirmHitType cht ){

                                                              TOLOG("系统", "恢复出厂设置");
                                                          });
        restoreConfirm.setConfirmData("提示", "功能开发中, 请耐心等待", "确认");
        //restoreConfirm.setConfirmData("恢复出厂", "确定是否恢复出厂设置？", "确认设置");
        restoreConfirm.showFullScreen();
        restoreConfirm.exec();

    } else if(btn == log->getPushButton()){

        logWidget = new SettingLog();
        logWidget->showFullScreen();
        logWidget->show();
        connect(logWidget, &SettingLog::back, this, &Settings::back);

    } else if(btn == sysSetting->getPushButton()){

        sys = new SettingSys();
        sys->showFullScreen();
        sys->show();
        connect(sys, &SettingSys::back, this, &Settings::back);

    } else if(btn == removeSDCard->getPushButton()){

        ConfirmWidget removeSDCardConfirm ([this]( ConfirmHitType cht ){
                                                                   if ( cht == ConfirmHitType::OK )
                                                                   {

                                                                       if ( StatusUpdate::getMediaPath() == StatusUpdate::getMediaRootPath() )
                                                                       {
                                                                           return;
                                                                       }
                                                                       system(( "umount " + StatusUpdate::getMediaRootPath() ).c_str() );
                                                                       //int r = umount(StatusUpdate::getMediaRootPath().c_str());
                                                                       // if ( r == -1 )
                                                                       // {
                                                                       //     TRACE("error : %s, %d\n", StatusUpdate::getMediaRootPath().c_str(), errno );
                                                                       //     return;
                                                                       // }
                                                                   }


                                                               });
        removeSDCardConfirm.setConfirmData("弹出SD卡", "确定是否弹出SD卡？", "确认", true, "已安全弹出，请拔出SD卡。");
        removeSDCardConfirm.showFullScreen();
        removeSDCardConfirm.exec();

    } else if(btn == shutdown->getPushButton()){

        ConfirmWidget shutdownConfirm ([](ConfirmHitType cht ){
                                                               if ( cht == ConfirmHitType::OK )
                                                               {
                                                                   TOLOG("系统", "关机");
                                                                   system("shutdown -h now");
                                                               }

                                                           });
        shutdownConfirm.setConfirmData("关机", "确定是否关机？", "确认关机");
        shutdownConfirm.showFullScreen();
        shutdownConfirm.exec();
    }else if ( btn == screenSwitch->getPushButton () )
    {
        MainWindow*mainWin=MainWindow::GetMainWindow();
        if(mainWin)
        {   
            mainWin->setWindowFlags(Qt::Widget);
            mainWin->showNormal();
        }
    }
}

void Settings::back(QObject *obj)
{
    Q_UNUSED(obj);
    QObject *btn = sender();

    if(btn == modelWidget) {
        modelWidget->close();
    } else if(btn == dev) {
        dev->close();
    } else if(btn == storageWidget) {
        storageWidget->close();
    } else if(btn == version) {
        version->close();
    } else if(btn == sys){
        sys->close();
    } else if(btn == logWidget){
        logWidget->close();
    }
}
