#ifndef SETTINGSOFTVERSION_VIEWER_H
#define SETTINGSOFTVERSION_VIEWER_H

#include <QWidget>

namespace Ui {
class SettingSoftVersion;
}

class SettingSoftVersion : public QWidget
{
    Q_OBJECT

public:
    explicit SettingSoftVersion(QWidget *parent = nullptr);
    ~SettingSoftVersion();

signals:
    void back(QObject *obj);

private slots:
    void backSlot();

private:
    Ui::SettingSoftVersion *ui;
};

#endif // SETTINGSOFTVERSION_VIEWER_H
