#include "settingmodel_viewer.h"
#include "ui_settingmodel_viewer.h"

#include "common.h"
#include "confirmwidget_viewer.h"
#include "settingauth_viewer.h"
#include "settingitem_viewer.h"
#include "topscanner_viewer.h"
#include "topstd_viewer.h"
#include "toscanner_viewer.h"

SettingModel::SettingModel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingModel)
{
    ui->setupUi(this);
    loadStyleSheet(this, "settingModel");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    TopScanner *top = new TopScanner(this);
    top->setTitle("模式切换");
    top->move(0, 0);

    operationType = new SettingItem(this);
    operationType->setNameAndIcon("运维模式", "model");
    operationType->move(80, 117);

    userType = new SettingItem(this);
    userType->setNameAndIcon("用户模式", "devInfo");
    userType->move(80, 117);

    userType->hide();

    connect(top, &TopScanner::back, this, &SettingModel::backSlot);
    connect(operationType->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));
    connect(userType->getPushButton(), SIGNAL(clicked(bool)), this, SLOT(onButtonClicked(bool)));

}

SettingModel::~SettingModel()
{
    delete ui;
}

void SettingModel::onButtonClicked(bool checked)
{
    QObject *btn = sender();

    if(btn == operationType->getPushButton()) {
        //TODO密码认证
        SettingAuth *auth = new SettingAuth(this);
        connect(auth, SIGNAL(authSuccess()), this, SLOT(authSuccess()));
        auth->move(585, 308);
        auth->show();
    } else if(btn == userType->getPushButton()) {
        ConfirmWidget *shutdownConfirm = new ConfirmWidget([this]( ConfirmHitType cht ){

                                                           });
        shutdownConfirm->setConfirmData("模式设置", "是否确认切换用户模式？", "确认切换");
        shutdownConfirm->showFullScreen();
        shutdownConfirm->show();
        userType->hide();
        operationType->show();
    }
}

void SettingModel::authSuccess()
{
    operationType->hide();
    userType->show();
}

void SettingModel::backSlot()
{
    emit back(this);
}
