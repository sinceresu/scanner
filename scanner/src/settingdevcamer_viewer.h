#ifndef SETTINGDEVCAMER_VIEWER_H
#define SETTINGDEVCAMER_VIEWER_H

#include <QWidget>

namespace Ui {
class SettingDevCamer;
}

class SettingDevCamer : public QWidget
{
    Q_OBJECT

public:
    explicit SettingDevCamer(QWidget *parent = nullptr);
    ~SettingDevCamer();

private:
    Ui::SettingDevCamer *ui;
};

#endif // SETTINGDEVCAMER_VIEWER_H
