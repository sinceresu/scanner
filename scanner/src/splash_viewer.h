#ifndef SPLASH_VIEWER_H
#define SPLASH_VIEWER_H

#include <QWidget>

namespace Ui {
class Splash;
}

class Splash : public QWidget
{
    Q_OBJECT

public:
    explicit Splash(QWidget *parent = nullptr);
    ~Splash();

public slots:
    void showMainWidget();

private:
    QTimer *updateTimer;
    Ui::Splash *ui;
};

#endif // SPLASH_VIEWER_H
