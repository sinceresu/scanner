#ifndef MANAGEMENTDETAIL_VIEWER_H
#define MANAGEMENTDETAIL_VIEWER_H

#include <QWidget>

namespace Ui {
class ManagementDetail;
}

class ManagementDetail : public QWidget
{
    Q_OBJECT

public:
    explicit ManagementDetail( QString selectUUID, QWidget *parent = nullptr);
    ~ManagementDetail();

signals:
    void back(QObject *obj);

private slots:
    void backSlot();

private:
    Ui::ManagementDetail *ui;
};

#endif // MANAGEMENTDETAIL_VIEWER_H
