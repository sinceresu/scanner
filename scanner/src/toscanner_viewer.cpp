#include "toscanner_viewer.h"
#include "ui_toscanner_viewer.h"
#include "mainwindow.h"
#include "common.h"
#include <ydUtil/log.h>
#include <QAction>
#include <QLineEdit>
#include <QListWidget>
#include <QScrollBar>
#include <ydUtil/console.h>
#include "Logger.h"
#include "confirmwidget_viewer.h"

ToScanner::ToScanner(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ToScanner)
{
    ui->setupUi(this);
    loadStyleSheet(this, "toscanner");

    ui->name->setPlaceholderText("请输入工程名称…");
    ui->deflection->setPlaceholderText("请输入北方向偏角…");
    ui->longitude->setPlaceholderText("经度：");
    ui->latitude->setPlaceholderText("纬度：");
    ui->altitude->setPlaceholderText("高程：");
    ui->interval->setPlaceholderText("区域间隔…");
    ui->area->setPlaceholderText("请输入区域面积…");
    ui->buildingName->setPlaceholderText("请输入楼栋名称…");
    ui->lengthInput->setPlaceholderText("长：");
    ui->width->setPlaceholderText("宽：");
    ui->height->setPlaceholderText("高：");
    ui->level->setPlaceholderText("请输入楼栋层级…");

    m_accountList = new QListWidget();
    m_accountList->installEventFilter(this);
    m_accountList->verticalScrollBar()->setContextMenuPolicy(Qt::NoContextMenu);

    // QListWidgetItem *listItem1 = new QListWidgetItem(QString("综合"));
    // m_accountList->addItem(listItem1);
    QListWidgetItem *listItem2 = new QListWidgetItem(QString("室内"));
    m_accountList->addItem(listItem2);
    QListWidgetItem *listItem3 = new QListWidgetItem(QString("室外"));
    m_accountList->addItem(listItem3);
    // QListWidgetItem *listItem4 = new QListWidgetItem(QString("地下"));
    // m_accountList->addItem(listItem4);

    ui->type->setModel(m_accountList->model());
    ui->type->setView(m_accountList);
    QWidget *parentx = m_accountList->parentWidget();
    if (parentx != nullptr) {
        parentx->setWindowFlags(m_accountList->parentWidget()->windowFlags() | Qt::FramelessWindowHint);
        parentx->setAttribute(Qt::WA_TranslucentBackground);
    }
    ui->type->setCurrentIndex(0);
    ui->type->lineEdit()->setPlaceholderText("请选择空间类型…");

    ui->startLabel->setAttribute(Qt::WA_TranslucentBackground);
//    ui->start->setAttribute(Qt::WA_TranslucentBackground);


    init();


    qApp->installEventFilter(this);

}

ToScanner::~ToScanner()
{
    delete ui;
}

void ToScanner::setTypeValue(int index)
{
    if(ui)
    {
        ui->type->setCurrentIndex(index);
    }
}
void ToScanner::init( )
{
    MainWindow *lpMainWindow = MainWindow::GetMainWindow();
    if ( !lpMainWindow )
    {
        return;
    }

    ScannerCfgManager *cfgManager = lpMainWindow->getCfgManager();


    std::string uuid = cfgManager->getParam("workingproject");

    // 恢复上一次未完成的扫描工程信息

    std::string name,deflection,longitude,latitude,altitude,interval,area,buildingname,lengthinput,width,height,level,stype;
    name = cfgManager->getParam("name");
    deflection = cfgManager->getParam("deflection");
    longitude = cfgManager->getParam( "longitude" );
    latitude = cfgManager->getParam("latitude");
    altitude = cfgManager->getParam("altitude");
    interval = cfgManager->getParam("interval");
    area = cfgManager->getParam("area");
    buildingname = cfgManager->getParam("buildingname");
    lengthinput = cfgManager->getParam("lengthinput");
    width = cfgManager->getParam("width");
    height = cfgManager->getParam("height");
    level = cfgManager->getParam("level");
    stype = cfgManager->getParam("type");

    Project *lpProject = cfgManager->getProject( uuid );
    if ( lpProject )
    {
        if ( name.length() == 0 ) {name = lpProject->getName();}
        if ( deflection.length() == 0 ) {deflection = lpProject->getParam("deflection");}
        if ( longitude.length() == 0 ) {longitude = lpProject->getParam("longitude");}
        if ( latitude.length() == 0 ) {latitude = lpProject->getParam("latitude");}
        if ( altitude.length() == 0 ) {altitude = lpProject->getParam("altitude");};
        if ( interval.length() == 0 ) {interval = lpProject->getParam("interval");};
        if ( area.length() == 0 ) {area = lpProject->getParam("area");};
        if ( buildingname.length() == 0 ) {buildingname = lpProject->getParam("buildingname");};
        if ( lengthinput.length() == 0 ) {lengthinput = lpProject->getParam("lengthinput");};
        if ( width.length() == 0 ) {width = lpProject->getParam("width");};
        if ( height.length() == 0 ) {height = lpProject->getParam("height");};
        if ( level.length() == 0 ) {level = lpProject->getParam("level");};
        if ( stype.length() == 0 ) {stype = lpProject->getParam("type");};
    }

    ui->name->setText( QString::fromLocal8Bit(name.c_str()) );
    ui->deflection->setText(QString::fromLocal8Bit(deflection.c_str()));
    ui->longitude->setText(QString::fromLocal8Bit(longitude.c_str()));
    ui->latitude->setText(QString::fromLocal8Bit(latitude.c_str()));;
    ui->altitude->setText(QString::fromLocal8Bit(altitude.c_str()));;
    ui->interval->setText(QString::fromLocal8Bit(interval.c_str()));
    ui->area->setText(QString::fromLocal8Bit(area.c_str()));
    ui->buildingName->setText(QString::fromLocal8Bit(buildingname.c_str()));
    ui->lengthInput->setText(QString::fromLocal8Bit(lengthinput.c_str()));
    ui->width->setText(QString::fromLocal8Bit(width.c_str()));
    ui->height->setText(QString::fromLocal8Bit(height.c_str()));
    ui->level->setText(QString::fromLocal8Bit(level.c_str()));
    ui->type->setCurrentIndex(atoi( stype.c_str() ));

    // ui->name->setText( QString::fromLocal8Bit(lpProject->getName().c_str()) );
    // ui->deflection->setText(QString::fromLocal8Bit(lpProject->getParam("deflection").c_str()));
    // ui->longitude->setText(QString::fromLocal8Bit(lpProject->getParam("longitude").c_str()));
    // ui->latitude->setText(QString::fromLocal8Bit(lpProject->getParam("latitude").c_str()));;
    // ui->altitude->setText(QString::fromLocal8Bit(lpProject->getParam("altitude").c_str()));;
    // ui->interval->setText(QString::fromLocal8Bit(lpProject->getParam("interval").c_str()));
    // ui->area->setText(QString::fromLocal8Bit(lpProject->getParam("area").c_str()));
    // ui->buildingName->setText(QString::fromLocal8Bit(lpProject->getParam("buildingname").c_str()));
    // ui->lengthInput->setText(QString::fromLocal8Bit(lpProject->getParam("lengthinput").c_str()));
    // ui->width->setText(QString::fromLocal8Bit(lpProject->getParam("width").c_str()));
    // ui->height->setText(QString::fromLocal8Bit(lpProject->getParam("height").c_str()));
    // ui->level->setText(QString::fromLocal8Bit(lpProject->getParam("level").c_str()));
    // ui->type->setCurrentIndex(atoi( lpProject->getParam("type").c_str() ) );

}

bool ToScanner::eventFilter(QObject *watched, QEvent *event)
{

    if(event->type() == QEvent::MouseButtonPress && watched == this)
    {
        if(ui->name->hasFocus())
        {
            ui->name->clearFocus();
        }
        if(ui->deflection->hasFocus())
        {
            ui->deflection->clearFocus();
        }
        if(ui->longitude->hasFocus())
        {
            ui->longitude->clearFocus();
        }
        if(ui->latitude->hasFocus())
        {
            ui->latitude->clearFocus();
        }
        if(ui->altitude->hasFocus())
        {
            ui->altitude->clearFocus();
        }
        if(ui->interval->hasFocus())
        {
            ui->interval->clearFocus();
        }
        if(ui->area->hasFocus())
        {
            ui->area->clearFocus();
        }
        if(ui->buildingName->hasFocus())
        {
            ui->buildingName->clearFocus();
        }
        if(ui->lengthInput->hasFocus())
        {
            ui->lengthInput->clearFocus();
        }
        if(ui->width->hasFocus())
        {
            ui->width->clearFocus();
        }
        if(ui->height->hasFocus())
        {
            ui->height->clearFocus();
        }
        if(ui->level->hasFocus())
        {
            ui->level->clearFocus();
        }
    }

    return false;
}

void ToScanner::on_type_activated(const QString &arg1)
{
    Q_UNUSED(arg1);
    //TODO
}

void ToScanner::on_startButton_clicked()
{
    TRACE("clicked\n");
    MainWindow *lpMainWindow = MainWindow::GetMainWindow();
    if ( !lpMainWindow )
    {
        TRACE("parent type error\n");
        return;
    }

    ScannerCfgManager *cfgManager = lpMainWindow->getCfgManager();

    if( ui->name->text().isEmpty()     || ui->deflection->text().isEmpty()   || ui->longitude->text().isEmpty()     ||
        ui->latitude->text().isEmpty() || ui->altitude->text().isEmpty()     || ui->interval->text().isEmpty()      ||
        ui->area->text().isEmpty()     || ui->buildingName->text().isEmpty() || ui->lengthInput->text().isEmpty()   ||
        ui->width->text().isEmpty()    || ui->height->text().isEmpty()       || ui->level->text().isEmpty()         || ui->type->currentText().isEmpty())
    {
        std::string error;
        if(ui->type->currentText().isEmpty())
            error = "空间类型选项无效!";
        else
            error = "参数列表不能有空值!";

        ConfirmWidget *cfw = new ConfirmWidget( [](ConfirmHitType cht){}, NULL );
        cfw->setConfirmData(QString::fromLocal8Bit("操作错误"), QString::fromLocal8Bit(error.c_str()), QString::fromLocal8Bit("确定"), false, "");
        cfw->showFullScreen();
        cfw->exec();
        TOLOG( "扫描", error.c_str());
        return;
    }
    else
    {
        std::vector<std::string> pl = cfgManager->getProjectList();
        for(auto uuid : pl)
        {
            Project *pro = cfgManager->getProject(uuid);
            if(pro->getName() == ui->name->text().toLocal8Bit().toStdString())
            {
                ConfirmWidget *cfw = new ConfirmWidget( [](ConfirmHitType cht){}, NULL );
                cfw->setConfirmData(QString::fromLocal8Bit("操作错误"), QString::fromLocal8Bit("该项目名称已存在!"), QString::fromLocal8Bit("确定"), false, "");
                cfw->showFullScreen();
                cfw->exec();
                TOLOG( "扫描", "该项目名称已存在");
                return;
            }
        }
    }

    //std::string uuid = cfgManager->getParam("workingproject");
    Project *lpProject = NULL;//cfgManager->getProject( uuid );
    //if ( !lpProject )
    {
        lpProject = new Project;
        cfgManager->addProject( lpProject );
        lpProject->setParam("createtime", std::to_string(time(NULL) ) );
        lpProject->setPath( cfgManager->getSavePath() + "/" + ydUtil::getTimeStr4Dir() );
        lpProject->setName( std::string(ui->name->text().toLocal8Bit() ) );// QString::fromLocal8Bit(lpProject->getName().c_str()) );
        emit lpMainWindow->signalNewProject( QString::fromLocal8Bit( lpProject->getID().c_str() ) );

        cfgManager->setParam("workingproject", lpProject->getID() );

    }

    cfgManager->setParam("workingproject", lpProject->getID() );

    lpProject->setParam("deflection", std::string(ui->deflection->text().toLocal8Bit() ) );//QString::fromLocal8Bit(lpProject->getParam("deflection").c_str()));
    lpProject->setParam("longitude",std::string( ui->longitude->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("longitude").c_str()));
    lpProject->setParam("latitude", std::string(ui->latitude->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("latitude").c_str()));;
    lpProject->setParam("altitude",std::string( ui->altitude->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("altitude").c_str()));;
    lpProject->setParam("interval",std::string( ui->interval->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("intervale").c_str()));
    lpProject->setParam("area",std::string( ui->area->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("area").c_str()));
    lpProject->setParam("buildingname",std::string( ui->buildingName->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("buildingname").c_str()));
    lpProject->setParam("lengthinput",std::string( ui->lengthInput->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("lengthinput").c_str()));
    lpProject->setParam("width",std::string( ui->width->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("width").c_str()));
    lpProject->setParam("height",std::string( ui->height->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("height").c_str()));
    lpProject->setParam("level",std::string( ui->level->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("level").c_str()));
    lpProject->setParam("type", std::to_string(ui->type->currentIndex()));//(atoi( lpProject->getParam("type").c_str() ) );
    G_Space_Type = ui->type->currentIndex();

    cfgManager->setParam("name", lpProject->getName() );//QString::fromLocal8Bit(lpProject->getParam("deflection").c_str()));
    cfgManager->setParam("deflection",lpProject->getParam("deflection") );//(QString::fromLocal8Bit(lpProject->getParam("longitude").c_str()));
    cfgManager->setParam("longitude",lpProject->getParam("longitude") );//(QString::fromLocal8Bit(lpProject->getParam("longitude").c_str()));
    cfgManager->setParam("latitude", lpProject->getParam("latitude") );//(QString::fromLocal8Bit(lpProject->getParam("latitude").c_str()));;
    cfgManager->setParam("altitude",lpProject->getParam("altitude") );//(QString::fromLocal8Bit(lpProject->getParam("altitude").c_str()));;
    cfgManager->setParam("interval",lpProject->getParam("interval") );//(QString::fromLocal8Bit(lpProject->getParam("intervale").c_str()));
    cfgManager->setParam("area",lpProject->getParam("area") );//(QString::fromLocal8Bit(lpProject->getParam("area").c_str()));
    cfgManager->setParam("buildingname",lpProject->getParam("buildingname") );//(QString::fromLocal8Bit(lpProject->getParam("buildingname").c_str()));
    cfgManager->setParam("lengthinput",lpProject->getParam("lengthinput") );//(QString::fromLocal8Bit(lpProject->getParam("lengthinput").c_str()));
    cfgManager->setParam("width",lpProject->getParam("width") );//(QString::fromLocal8Bit(lpProject->getParam("width").c_str()));
    cfgManager->setParam("height",lpProject->getParam("height") );//(QString::fromLocal8Bit(lpProject->getParam("height").c_str()));
    cfgManager->setParam("level",lpProject->getParam("level") );//(QString::fromLocal8Bit(lpProject->getParam("level").c_str()));
    cfgManager->setParam("type", lpProject->getParam("type"));//(atoi( lpProject->getParam("type").c_str() ) );
    qApp->removeEventFilter(this);

    lpProject->save();
    cfgManager->save();

    TOLOG( "工程管理", "新建工程:%s->%s", lpProject->getName().c_str(), lpProject->getID().c_str() );
    emit startScan();
}

