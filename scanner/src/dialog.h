#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

signals:
    void saveVal(QString val);
public:
    void setLabelText(QString labelText);
    QString getVal();

private slots:
    void on_closeButton_clicked();

    void on_save_clicked();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
