#include "managementinfo_viewer.h"
#include "ui_managementinfo_viewer.h"

#include "common.h"
#include "Project.h"
#include "mainwindow.h"
#include "ScannerCfgManager.h"
#include <ydUtil/DirUtils.h>

std::string getSimpleTimeStr( time_t t );
ManagementInfo::ManagementInfo( QString const& selectUUID, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ManagementInfo)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_TranslucentBackground);
    loadStyleSheet(this, "managementInfo");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    ui->nameVal->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    ui->typeVal->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    ui->contentVal->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    ui->localVal->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    ui->updateTimeVal->setAlignment(Qt::AlignRight | Qt::AlignVCenter);


    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( std::string( selectUUID.toLocal8Bit() ) );
    if ( lpProject )
    {
        ui->nameVal->setText( QString::fromLocal8Bit( lpProject->getName().c_str() ) );
        ui->typeVal->setText("文件夹");
        ui->contentVal->setText( QString("%1").arg( ydUtil::findFileInPath( lpProject->getPath().c_str(), true ).size() ) );
        ui->localVal->setText(QString::fromLocal8Bit( lpProject->getPath().c_str() ) );//"/temp/ER/001");
        ui->updateTimeVal->setText(QString::fromLocal8Bit( getSimpleTimeStr( atoll( lpProject->getParam( "createtime" ).c_str() ) ).c_str() ) );
    }
}

ManagementInfo::~ManagementInfo()
{
    delete ui;
}

void ManagementInfo::on_closeButton_clicked()
{
    this->close();
}

