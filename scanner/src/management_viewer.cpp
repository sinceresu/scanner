#include "management_viewer.h"
#include "ui_management_viewer.h"
#include <osg/ref_ptr>
#include "common.h"
#include "folderitem_viewer.h"
#include "mainwindow.h"
#include "Project.h"
#include "Logger.h"
#include "ScannerCfgManager.h"
#include <QString>
#include <ydUtil/DirUtils.h>
#include <ydUtil/StringUtils.h>
#include <ydUtil/log.h>
#include "StatusUpdate.h"

#include "confirmwidget_viewer.h"

#include "PCThreadObj.h"

Management::Management(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Management)
{
    ui->setupUi(this);
    loadStyleSheet(this, "management");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    ui->listWidget->setWrapping(true);
    ui->listWidget->setViewMode(QListView::IconMode);
    ui->listWidget->setFlow(QListView::LeftToRight);
    ui->listWidget->setSpacing(10);
    ui->listWidget->setResizeMode(QListWidget::Fixed);
    ui->listWidget->setMovement(QListWidget::Static);

    ui->listWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->listWidget->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded );

    ui->listWidget->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    ui->listWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
//        connect(ui->listWidget,SIGNAL(itemClicked(QListWidgetItem*)),this,SLOT(listWidgetClicked(QListWidgetItem*)));

    // for(int i = 0; i < 53; i++)
    // {
    //     FolderItem *item = new FolderItem();
    //     item->setFolderName(QString("文件夹%1").arg(i));
    //     QListWidgetItem *listItem = new QListWidgetItem("");
    //     listItem->setSizeHint(QSize(362, 283));
    //     ui->listWidget->addItem(listItem);
    //     ui->listWidget->setItemWidget(listItem, item);
    // }
    initProjectList();
    connect( MainWindow::GetMainWindow(), SIGNAL( signalNewProject( QString const) ), this, SLOT( slotNewProject( QString const ) ) );
    connect( MainWindow::GetMainWindow(), SIGNAL( signalRemoveProject( QString const) ), this, SLOT( slotRemoveProject( QString const ) ) );
    connect( MainWindow::GetMainWindow(), SIGNAL( signalExportProject( QString const) ), this, SLOT( slotExportProject( QString const ) ) );

    connect( MainWindow::GetMainWindow(), SIGNAL( signalModifyProject( QString const , QString const , QString const  ) ), this, SLOT( slotModifyProject( QString const, QString const, QString const ) ) );

}

Management::~Management()
{
    delete ui;
}


void Management::on_listWidget_itemClicked(QListWidgetItem *item)
{
    FolderItem *folderItem = static_cast<FolderItem*>(ui->listWidget->itemWidget(item));

    if(curLastItem != nullptr && curLastItem != folderItem)
    {
        curLastItem->onRelease();
    }

    if(curLastItem == folderItem && folderItem != selectLastItem)
    {
        return;
    }

    if(folderItem == selectLastItem){
        if(selectTime.elapsed() < 1000)
        {
            return;
        }
        else
        {
            if(selectLastItem != nullptr)
            {
                selectLastItem->onRelease();
            }
        }
    }

    QString selectUUID = item->data( Qt::UserRole ).toString();
    emit sigManagementClickSelect( selectUUID );
    folderItem->onSelect();
    curLastItem = folderItem;
}


void Management::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    FolderItem *folderItem = static_cast<FolderItem*>(ui->listWidget->itemWidget(item));

    if(selectLastItem != nullptr && selectLastItem != folderItem)
    {
        selectLastItem->onRelease();
    }

    QString selectUUID = item->data( Qt::UserRole ).toString();

    emit sigManagementDoubleSelect( selectUUID );
    folderItem->onDoubleSelect();
    selectLastItem = folderItem;

    selectTime.start();
}

void Management::addProject( Project *lpProject )
{
    if ( !lpProject )
    {
        return;
    }

    FolderItem *item = new FolderItem();
    item->setFolderName(QString("%1").arg(QString::fromLocal8Bit( lpProject->getName().c_str() ) ) );
    QListWidgetItem *listItem = new QListWidgetItem("");
    listItem->setSizeHint(QSize(362, 283));
    listItem->setData( Qt::UserRole, QString::fromLocal8Bit( lpProject->getID().c_str() ) );
    ui->listWidget->addItem(listItem);
    ui->listWidget->setItemWidget(listItem, item);
}
void Management::initProjectList()
{
    MainWindow *lpMainWindow = MainWindow::GetMainWindow();
    ScannerCfgManager *lpManager = lpMainWindow->getCfgManager();

    std::vector<std::string> projectList = lpManager->getProjectList();

    for(unsigned int i = 0; i < projectList.size(); i++)
    {
        Project *lpProject = lpManager->getProject( projectList[i] );
        addProject( lpProject );
    }
}

bool Management::CopyTheProject(std::string const& sourceDir, std::string const& targetDir)
{
    if ( sourceDir.length() == 0 || targetDir.length() == 0 || sourceDir == targetDir )
    {
        return false;
    }

    //进度条
    QThread*  pcThread = new QThread;
    PCThreadObj* pcThreadObj = new PCThreadObj(sourceDir,targetDir);
    pcThreadObj->moveToThread(pcThread);
    pcThread->start();
    
    ConfirmWidget shutdownConfirm ( [this](ConfirmHitType cht ){
                                    }  );
    connect(&shutdownConfirm, SIGNAL(signalStartCP()), pcThreadObj, SLOT(slotStartCP()));
    connect(pcThreadObj, SIGNAL(signalUploadProgress(int)),&shutdownConfirm, SLOT(slotUploadProgress(int)));
    connect(pcThreadObj, SIGNAL(signalFinishCP()),&shutdownConfirm, SLOT(slotFinishCP()));

    std::string strtext = "工程正在拷贝中, 请等待...";   // "拷贝完成, 请弹出U盘!"
    shutdownConfirm.setIconConfirmData(ConfirmType::Export, strtext.c_str(), "取消拷贝");
    shutdownConfirm.setProgressBarState(true,0,100);
    shutdownConfirm.showFullScreen();
    emit shutdownConfirm.signalStartCP();
    
    shutdownConfirm.exec();
    TOLOG( "工程管理", "正在拷贝中, 请等待..!");

    pcThread->requestInterruption();
    pcThread->terminate(); //quit
    pcThread->wait();
    return true;
}

void Management::slotNewProject( QString const uuid )
{
    MainWindow *lpMainWindow = MainWindow::GetMainWindow();
    ScannerCfgManager *lpManager = lpMainWindow->getCfgManager();

    Project *lpProject = lpManager->getProject( std::string(uuid.toLocal8Bit()) );
    if ( !lpProject )
    {
        return;
    }

    addProject( lpProject );
}

void Management::slotRemoveProject( QString const uuid )
{
    osg::ref_ptr<Project> lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( std::string( uuid.toLocal8Bit() ) );
    if ( !lpProject )
    {
        MainWindow::GetMainWindow()->getCfgManager()->removeProject( std::string( uuid.toLocal8Bit() ) );
        return;
    }

    TOLOG( "工程管理", "删除工程:%s", lpProject->getName().c_str() );
    for ( unsigned int i = 0; i < ui->listWidget->count(); i ++ )
    {
        QListWidgetItem *lpItem = ui->listWidget->item( i );
        if ( lpItem->data(Qt::UserRole).toString() == uuid )
        {
            //delete lpItem;
            //ui->listWidget->removeItemWidget( lpItem );
            delete lpItem;
            break;
        }
    }
    selectLastItem = NULL;
    curLastItem = NULL;
    ydUtil::delDir( lpProject->getPath() );
    MainWindow::GetMainWindow()->getCfgManager()->removeProject(uuid.toStdString());
    MainWindow::GetMainWindow()->slotChangeBottomWidget(0);
}
void Management::slotExportProject( QString const uuid )
{
    osg::ref_ptr<Project> lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( std::string( uuid.toLocal8Bit() ) );

    TRACE("export project : %s\n", lpProject->getPath().c_str() );
    if ( StatusUpdate::getMediaPath() == StatusUpdate::getMediaRootPath() )
    {
        ConfirmWidget shutdownConfirm ( [this](ConfirmHitType cht ){
                                                            }  );
        shutdownConfirm.setIconConfirmData(ConfirmType::Export, "U盘未准备好, 请插入U盘!", "操作错误");
        shutdownConfirm.showFullScreen();
        shutdownConfirm.exec();

        TOLOG( "工程管理", "U盘未准备好, 请插入U盘!");
        return;
    }

    std::string scanRootPath = lpProject->getScanRootPath();
    ydUtil::ReplaceStr( scanRootPath, "\\", "/" );

    std::vector<std::string> dl = ydUtil::split( scanRootPath, "/" );
    if ( dl.size() < 2 )
    {
        return;
    }

    //ydUtil::copyDir( lpProject->getScanRootPath(), "/home/fosky/" + dl[dl.size() - 1 - 1] );
    std::string targetDir = StatusUpdate::getMediaRootPath() + "/" + dl[dl.size() - 1];
    //if ( ydUtil::copyDir( lpProject->getPath(), targetDir ) )
    if(CopyTheProject(lpProject->getPath(), targetDir))
    {
        ConfirmWidget shutdownConfirm ( [this](ConfirmHitType cht ){
                                        }  );
        targetDir = "已拷贝到：" + targetDir;   // "拷贝完成, 请弹出U盘!"
        shutdownConfirm.setIconConfirmData(ConfirmType::Export, targetDir.c_str(), "操作成功");
        shutdownConfirm.showFullScreen();
        shutdownConfirm.exec();
        TOLOG( "工程管理", "拷贝完成, 请弹出U盘!");
    }else
    {
        ConfirmWidget shutdownConfirm ( [this](ConfirmHitType cht ){
                                        }  );
        shutdownConfirm.setIconConfirmData(ConfirmType::Export, "拷贝错误, 请检查U盘后重试!", "操作失败");
        shutdownConfirm.showFullScreen();
        shutdownConfirm.exec();
        TOLOG( "工程管理", "拷贝错误, 请检查U盘后重试!");
    }

}

void Management::slotModifyProject( QString const projectuuid, QString const key, QString const v )
{
    MainWindow *lpMainWindow = MainWindow::GetMainWindow();
    ScannerCfgManager *lpManager = lpMainWindow->getCfgManager();

    Project *lpProject = lpManager->getProject( std::string(projectuuid.toLocal8Bit()) );
    if ( !lpProject )
    {
        return;
    }
    TOLOG( "工程管理", "修改工程:%s->%s", lpProject->getParam( std::string( key.toLocal8Bit() ) ).c_str(), std::string( v.toLocal8Bit() ).c_str() );

    lpProject->setParam( std::string( key.toLocal8Bit() ), std::string( v.toLocal8Bit() ) );

    if ( key == QString("name"))
    {
        for ( unsigned int i = 0; i < ui->listWidget->count(); i ++ )
        {
            QListWidgetItem *lpItem = ui->listWidget->item( i );
            if ( lpItem->data( Qt::UserRole ).toString() == projectuuid )
            {
                FolderItem *folderItem = static_cast<FolderItem*>(ui->listWidget->itemWidget(lpItem));
                if ( folderItem )
                {
                    folderItem->setFolderName( v );
                }
            }
        }
    }


}
