#ifndef SETTINGDEVINFO_VIEWER_H
#define SETTINGDEVINFO_VIEWER_H

#include "settingdevbattery_viewer.h"
#include "settingdevcamer_viewer.h"
#include "settingdevhost_viewer.h"
#include "settingdevradar_viewer.h"

#include <QToolButton>
#include <QWidget>

namespace Ui {
class SettingDevInfo;
}

class SettingDevInfo : public QWidget
{
    Q_OBJECT

public:
    explicit SettingDevInfo(QWidget *parent = nullptr);
    ~SettingDevInfo();

signals:
    void back(QObject *obj);

private slots:
    void backSlot();

private slots:
    void on_hostButton_clicked();

    void on_radarButton_clicked();

    void on_camerButton_clicked();

    void on_batteryButton_clicked();

private:
    SettingDevHost *settingDevHost;
    SettingDevRadar *settingDevRadar;
    SettingDevCamer *settingDevCamer;
    SettingDevBattery *settingDevBattery;

    QToolButton *currentToolButton;

private:
    Ui::SettingDevInfo *ui;
};

#endif // SETTINGDEVINFO_VIEWER_H
