#ifndef SETTINGSTORAGE_VIEWER_H
#define SETTINGSTORAGE_VIEWER_H

#include <QWidget>

namespace Ui {
class SettingStorage;
}

class SettingStorage : public QWidget
{
    Q_OBJECT

public:
    explicit SettingStorage(QWidget *parent = nullptr);
    ~SettingStorage();

signals:
    void back(QObject *obj);

private slots:
    void backSlot();

private:
    Ui::SettingStorage *ui;
};

#endif // SETTINGSTORAGE_VIEWER_H
