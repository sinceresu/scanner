#include "settingsys_viewer.h"
#include "ui_settingsys_viewer.h"

#include "common.h"
#include "confirmwidget_viewer.h"
#include "topscanner_viewer.h"

SettingSys::SettingSys(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingSys)
{
    ui->setupUi(this);
    loadStyleSheet(this, "settingSys");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    TopScanner *top = new TopScanner(this);
    top->setTitle("系统设置");
    top->move(0, 0);

    ui->settingLabel->setText("手动设置");
    ui->grayLabel->setText("室外");
    ui->blackLabel->setText("室内");
    ui->label->setText("系统风格分为浅色和深色两种颜色模式，浅色适用于室外，深色适用于室内。");

    ui->grayIcon->setPixmap(QPixmap(":/scanner/images/settings/cur.png"));
    ui->blackIcon->setPixmap(QPixmap(":/scanner/images/settings/cur.png"));

    ui->blackIcon->hide();

    connect(top, &TopScanner::back, this, &SettingSys::backSlot);
}

SettingSys::~SettingSys()
{
    delete ui;
}

void SettingSys::on_blackToolButton_clicked()
{
    ConfirmWidget *shutdownConfirm = new ConfirmWidget([]( ConfirmHitType cht ){});
    shutdownConfirm->setConfirmData("提示", "系统正在开发中...", "确认");
    shutdownConfirm->showFullScreen();
    shutdownConfirm->show();
}

void SettingSys::backSlot()
{
    emit back(this);
}

