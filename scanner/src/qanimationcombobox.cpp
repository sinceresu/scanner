﻿#include "qanimationcombobox.h"

#include <QPainter>
#include <QAbstractItemView>
#include <QEvent>
#include <QMouseEvent>
#include <QLineEdit>
#include <QClipboard>
#include <QApplication>
#include <QDebug>

#include <QAction>
#include <QLineEdit>
#include <QListWidget>
#include <QScrollBar>

QAnimationComboBox::QAnimationComboBox(QWidget *parent) :
    QComboBox(parent)
{
    m_isAnimating = false;
    m_isFocuse = false;
    m_currentValue = 0;
    m_lineColor = QColor(255, 0, 0);
    m_focusLineColor = QColor(0, 0, 0);

    setEditable(true);
    lineEdit()->setStyleSheet("color: rgba(73, 83, 102, 1);");

    QLineEdit *lineEdit = this->lineEdit();
    lineEdit->installEventFilter(this);
    lineEdit->setContextMenuPolicy(Qt::CustomContextMenu);
}

QAnimationComboBox::~QAnimationComboBox()
{

}

QColor QAnimationComboBox::lineColor() const
{
    return m_lineColor;
}

void QAnimationComboBox::setLineColor(const QColor &color)
{
    m_lineColor = color;
}

QColor QAnimationComboBox::focusLineColor() const
{
    return m_focusLineColor;
}

void QAnimationComboBox::setFocusLineColor(const QColor &color)
{
    m_focusLineColor = color;
}

void QAnimationComboBox::valueChanged(const QVariant &value)
{
    m_currentValue = value.toFloat();
    update();
}

void QAnimationComboBox::animationFinished()
{
    m_currentValue = 0;
    m_isAnimating = false;
}


void QAnimationComboBox::focusInEvent(QFocusEvent *event)
{
    m_isAnimating = true;
    m_isFocuse = true;
    QComboBox::focusInEvent(event);
}

void QAnimationComboBox::focusOutEvent(QFocusEvent *event)
{
    QComboBox::focusOutEvent(event);
    m_isAnimating = true;
    m_isFocuse = false;
}

bool QAnimationComboBox::eventFilter(QObject *obj, QEvent *ev)
{
    Q_UNUSED(obj);
    if (ev->type() == QEvent::MouseButtonRelease) {
        if (static_cast<QMouseEvent*>(ev)->button() == Qt::RightButton) {
            return true;
        }
    }
    return false;
}
