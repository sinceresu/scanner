#ifndef SETTINGDEVRADAR_VIEWER_H
#define SETTINGDEVRADAR_VIEWER_H

#include <QWidget>

namespace Ui {
class SettingDevRadar;
}

class SettingDevRadar : public QWidget
{
    Q_OBJECT

public:
    explicit SettingDevRadar(QWidget *parent = nullptr);
    ~SettingDevRadar();

private:
    Ui::SettingDevRadar *ui;
};

#endif // SETTINGDEVRADAR_VIEWER_H
