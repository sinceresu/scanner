#include "topstd_viewer.h"
#include "ui_topstd_viewer.h"

#include "common.h"
#include <ydUtil/log.h>
#include "systeminfo.h"

TopSTD::TopSTD(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TopSTD)
{
    ui->setupUi(this);
    loadStyleSheet(this, "topstd");

    ui->dateLabel->setText( QString::fromLocal8Bit( getFullTimeStr(time(NULL)).c_str() ) );
    ui->logoLabel->hide();
    ui->udiskLabel->setPixmap(QPixmap(":/scanner/images/top/udisk.svg"));
    ui->diskLabel->setPixmap(QPixmap(":/scanner/images/top/disk.svg"));
    ui->cameraLabel->setPixmap(QPixmap(":/scanner/images/top/camera.svg"));
    ui->radarLabel->setPixmap(QPixmap(":/scanner/images/top/radar.svg"));
    ui->batteryLabel->setPixmap(QPixmap(":/scanner/images/top/batterying.svg"));

    _statusUpdate.setup( ui->dateLabel, ui->udiskLabel, ui->diskLabel, ui->cameraLabel, ui->radarLabel, ui->batteryLabel);
//    SD卡
//    ui->udiskLabel->setPixmap(QPixmap(":/scanner/images/top/udiskunmout.png"));       //无SD卡

//    磁盘
//    ui->diskLabel->setPixmap(QPixmap(":/scanner/images/top/disk.png"));               //磁盘不足
//    ui->diskLabel->setPixmap(QPixmap(":/scanner/images/top/disk.png"));               //磁盘写入数据中

//    雷达
//    ui->radarLabel->setPixmap(QPixmap(":/scanner/images/top/radar.png"));             //雷达连接异常
//    ui->radarLabel->setPixmap(QPixmap(":/scanner/images/top/radar.png"));             //雷达扫描工作中

//    相机
//    ui->cameraLabel->setPixmap(QPixmap(":/scanner/images/top/camera.png"));           //相机连接异常

//    电池
//    ui->batteryLabel->setPixmap(QPixmap(":/scanner/images/top/battery.png"));         //电量低

    connect( &_timer, SIGNAL( timeout() ), this, SLOT( slotTimeout() ) );
    _timer.start( 1000 );

}

TopSTD::~TopSTD()
{
    delete ui;
}

void TopSTD::showLogo(bool show)
{
    if(show)
        ui->logoLabel->show();
    else
        ui->logoLabel->hide();

}

void TopSTD::setTitle(QString title)
{
    ui->logoLabel->setText(title);
}
void TopSTD::slotTimeout()
{
    _statusUpdate.updateWidget();
}
