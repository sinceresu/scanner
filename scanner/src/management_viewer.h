#ifndef MANAGEMENT_VIEWER_H
#define MANAGEMENT_VIEWER_H

#include "folderitem_viewer.h"

#include <QListWidgetItem>
#include <QStyleOptionViewItem>
#include <QTime>
#include <QWidget>

namespace Ui {
class Management;
}

class Project;
class Management : public QWidget
{
    Q_OBJECT

public:
    explicit Management(QWidget *parent = nullptr);
    ~Management();

protected:
    void initProjectList();

    void addProject( Project *lpProject );

    bool CopyTheProject(std::string const& sourceDir, std::string const& targetDir);

signals:
    void sigManagementDoubleSelect( QString const uuid );
    void sigManagementClickSelect( QString const uuid );

private slots:
    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);
    void slotNewProject( QString const uuid );
    void slotRemoveProject( QString const uuid );
    void slotExportProject( QString const uuid );
    void slotModifyProject( QString const projectuuid, QString const key, QString const v );

private:
    FolderItem *curLastItem {nullptr};
    FolderItem *selectLastItem {nullptr};
    QTime selectTime;


private:
    Ui::Management *ui;
};

#endif // MANAGEMENT_VIEWER_H
