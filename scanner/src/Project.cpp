#include "Project.h"
#include <sole.hpp>
#include <fstream>
#include <ydUtil/log.h>
#include <ydUtil/DirUtils.h>

const std::string DBPATH = "/blockdb/";
const std::string SCANROOTPATH = "/scandata/";
const std::string PROJECTCFGFILE="project.json";

Project::Project()
{
    _projectParam = new ydUtil::MapParam;

    setID(sole::uuid0().str() );
}
Project::~Project()
{

}

void Project::setPath( std::string const& projectPath )
{
    _projectPath = projectPath;

    ydUtil::makeSureDirExist( getBlockDBPath() );
    ydUtil::makeSureDirExist( getScanRootPath() );

    load();
}

std::string Project::getPath()
{
    return _projectPath;
}

std::string Project::getBlockDBPath()
{
    return _projectPath + DBPATH;
}
std::string Project::getScanRootPath()
{
    return _projectPath + SCANROOTPATH;
}

std::string Project::addScanArea( std::string const& scanAreaUUID )
{
    ydUtil::VectorParam *lpScanArea = dynamic_cast<ydUtil::VectorParam*>(_projectParam->get("scanarea"));
    if ( !lpScanArea )
    {
        lpScanArea = new ydUtil::VectorParam;
        _projectParam->set("scanarea", lpScanArea );
    }

    lpScanArea->add( scanAreaUUID );

    //_scanAreaUUID.push_back( scanAreaUUID );

    ydUtil::makeSureDirExist( getScanRootPath() + scanAreaUUID );

    return getScanRootPath() + scanAreaUUID + "/";
}

bool Project::delScanArea( std::string const& scanAreaUUID )
{
    return ydUtil::delDir( getScanRootPath() + scanAreaUUID );
}
std::string Project::getCfgFile()
{
    return getPath() + "/" + PROJECTCFGFILE;
}

void Project::load()
{
    osg::ref_ptr<ydUtil::Param> lpParam = ydUtil::Param::fromFile( getCfgFile() );
    if ( dynamic_cast<ydUtil::MapParam*>( lpParam.get() ) )
    {
        _projectParam = dynamic_cast<ydUtil::MapParam*>( lpParam.get() );
    }
}

void Project::save()
{
    std::string paramStr = _projectParam->toString();
    std::ofstream ofs(getCfgFile());
    ofs << paramStr;
}

void Project::setID( std::string const& id )
{
    _projectParam->set("id", id );
    save();
}
std::string Project::getID()
{
    if ( _projectParam->getValue("id").length() == 0 )
    {
        setID( sole::uuid0().str() );
    }
    return _projectParam->getValue("id");
}

void Project::setName( std::string const& name )
{
    _projectParam->set( "name", name );
    save();
}
std::string Project::getName()
{
    return _projectParam->getValue("name");
}

void Project::setParam( std::string const &key, std::string value )
{
    _projectParam->set( key, value );
    save();
}
std::string Project::getParam( std::string const& key )
{
    return _projectParam->getValue( key );
}
