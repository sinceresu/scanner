#include "systeminfo.h"
#include <memory.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <ydUtil/StringUtils.h>
#include <vector>

void getDisInfo( std::string const& binPath, unsigned long long &freeDisk, unsigned long long &availableDisk, unsigned long long &totalSize )
{
/// 用于获取磁盘剩余空间
    struct statfs diskInfo;
    statfs(binPath.c_str(), &diskInfo);

    unsigned long long blocksize                = diskInfo.f_bsize;	//每个block里包含的字节数
    totalSize                = blocksize * diskInfo.f_blocks; 	//总的字节数，f_blocks为block的数目


    //printf("Total_size = %llu B                 = %llu KB = %llu MB = %llu GB\n",
    //       totalsize, totalsize>>10, totalsize>>20, totalsize>>30);

    totalSize = totalSize >> 30;

    freeDisk                 = diskInfo.f_bfree * blocksize;	//剩余空间的大小
    availableDisk            = diskInfo.f_bavail * blocksize; 	//可用空间大小

    //printf("Disk_free = %llu MB                 = %llu GB\nDisk_available = %llu MB = %llu GB\n",
    //       freeDisk>>20, freeDisk>>30, availableDisk>>20, availableDisk>>30);

    freeDisk = freeDisk >> 30;
    availableDisk = availableDisk >> 30;
}

int mysystem(const char* cmdstring, char* buf, int len)
{
    int   fd[2];
    pid_t pid;
    int   n, count;
    memset(buf, 0, len);
    if (pipe(fd) < 0)
        return -1;
    if ((pid = fork()) < 0)
        return -1;
    else if (pid > 0)
    {
        close(fd[1]);
        count = 0;
        while ((n = read(fd[0], buf + count, len)) > 0 && count > len)
            count += n;
        close(fd[0]);
        if (waitpid(pid, NULL, 0) > 0)
            return -1;
    }
    else
    {
        close(fd[0]);
        if (fd[1] != STDOUT_FILENO)
        {
            if (dup2(fd[1], STDOUT_FILENO) != STDOUT_FILENO)
            {
                return -1;
            }
            close(fd[1]);
        }
        if (execl("/bin/sh", "sh", "-c", cmdstring, (char*)0) == -1)
            return -1;
    }
    return 0;
}

/*
status1:
    state:               fully-charged
    percentage:          100%
status2:
    state:               discharging
    percentage:          94%

status3:
    state:               charging
    time to full:        11.2 minutes
    percentage:          89%
*/


float getBATPercent( std::string & status )
{
    std::string cmd = "upower -i $(upower -e | grep 'BAT') | grep -E \"state|to\\ full|percentage\"";

    char buff[1024];
    mysystem( cmd.c_str(), buff, sizeof( buff ) - 1 );
    std::string result = buff;

    std::vector<std::string> rl = ydUtil::split( buff, "\n" );
    if ( rl.size() < 2 )
    {
        return 0.0f;
    }
    {
        std::vector<std::string> detail = ydUtil::split( rl[0], " " );
        if ( detail.size() >= 2 )
        {
            status = detail[1];
        }
        // for ( auto d : detail )
        // {
        //     printf("%s ", d.c_str() );
        // }
    }
    {
        std::vector<std::string> detail = ydUtil::split( rl[rl.size() - 1], " " );
        if ( detail.size() >= 2 )
        {
            return atof( detail[1].c_str() );
        }
        // for ( auto d : detail )
        // {
        //     printf("%s ", d.c_str() );
        // }
    }

    return 0.0f;
}

std::string getFullTimeStr( time_t t )
{
    char ch[64] = { 0 };
    char result[100] = { 0 };
    strftime(ch, sizeof(ch) - 1, "%y年%m月%d日 %H:%M:%S", localtime(&t));
    sprintf(result, "%s", ch);
    return std::string(result);
}
std::string getSimpleTimeStr( time_t t )
{
    char ch[64] = { 0 };
    char result[100] = { 0 };
    strftime(ch, sizeof(ch) - 1, "%m/%d %H:%M:%S", localtime(&t));
    sprintf(result, "%s", ch);
    return std::string(result);
}

std::string formatTime( int t )
{
    int second = t % 60;
    int min = t / 60 % 60;
    int hour = t / 3600;

    char buff[1024];
    sprintf(buff, "%02d:%02d:%02d", hour, min, second);

    return std::string( buff );
}
