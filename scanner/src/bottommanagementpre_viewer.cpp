#include "bottommanagementpre_viewer.h"
#include "ui_bottommanagementpre_viewer.h"

#include "common.h"
#include "confirmwidget_viewer.h"

BottomManagementPre::BottomManagementPre(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BottomManagementPre)
{
    ui->setupUi(this);
//    setAttribute(Qt::WA_TranslucentBackground);
    setAttribute(Qt::WA_TranslucentBackground, true);

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    loadStyleSheet(this, "bottomManagementPre");

}

BottomManagementPre::~BottomManagementPre()
{
    delete ui;
}

// 补扫览项目
void BottomManagementPre::on_doScanPushButton_clicked()
{
    scan = new Scan();
    connect(scan, &Scan::signalBack, this, &BottomManagementPre::back);
    scan->showFullScreen();
    scan->show();
}

// 删除预览项目
void BottomManagementPre::on_doDelPushButton_clicked()
{
    ConfirmWidget shutdownConfirm ([](ConfirmHitType cht ){});
    shutdownConfirm.setIconConfirmData(ConfirmType::Delete, "确定删除扫描文件34-98 ？", "确认删除");
    shutdownConfirm.showFullScreen();
    shutdownConfirm.exec();
    emit signalRemove();
}

// 预览项目详情
void BottomManagementPre::on_detailPushButton_clicked()
{
    // detail = new ManagementDetail(_selectUUID);
    // detail->showFullScreen();
    // detail->show();
    // connect(detail, &ManagementDetail::back, this, &BottomManagementPre::back);
    emit signalDetail();
}


void BottomManagementPre::back(QObject *obj)
{
    Q_UNUSED(obj);
    QObject *btn = sender();

    if(btn == detail) {
        detail->close();
    } else if(btn == scan) {
        scan->close();
    }
    emit signalBack();
}
