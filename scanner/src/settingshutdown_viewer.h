#ifndef SETTINGSHUTDOWN_VIEWER_H
#define SETTINGSHUTDOWN_VIEWER_H

#include <QWidget>

namespace Ui {
class SettingShutdown;
}

class SettingShutdown : public QWidget
{
    Q_OBJECT

public:
    explicit SettingShutdown(QWidget *parent = nullptr);
    ~SettingShutdown();

private:
    Ui::SettingShutdown *ui;
};

#endif // SETTINGSHUTDOWN_VIEWER_H
