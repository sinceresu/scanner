#include "scan_viewer.h"
#include <stdlib.h>
#include <stdio.h>
#include "mainwindow.h"
#include "Project.h"
#include "ScannerCfgManager.h"
#include "ydUtil/log.h"
#include "ui_scan_viewer.h"
#include "systeminfo.h"
#include <sys/statfs.h>
#include <string>
#include <iostream>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <QScrollBar>
#include "confirmwidget_viewer.h"
#include "common.h"
#include "PieChart.h"
#include "topscanner_viewer.h"
#include "bottomscan_viewer.h"
#include "dialog.h"
#include "systeminfo.h"
#include "BlockShower/BlockShower.h"
#include "StatusUpdate.h"
#include "Logger.h"
#include <yaml-cpp/yaml.h>
#include <iostream>
#include <ydUtil/console.h>
#include <ydUtil/DirUtils.h>

using namespace std;
using namespace BlockShower;

Scan::Scan(QWidget *parent,  string project_path) :
    QWidget(parent),
    ui(new Ui::Scan),
    _blockShower(new BlockShower::BlockShower()),
    project_path_(project_path)
{
    ui->setupUi(this);
    _start.set( 0.0, 0.0, 0.0 );
    _end.set( 0.0, 0.0, 0.0 );

    // 创建osg窗口时, 需要一个更新回调函数
    _rosNode = new QNode();
    _rosNode->init(
        [&](  pcl::PointCloud<pcl::PointXYZI> *pointcloud )
            {
                if(_blockShower != nullptr && _working)
                {
                    _blockShower->setCloudPoint(pointcloud);
                }
                    
            },
        [&]( float x, float y, float z, float qx, float qy, float qz, float qw )
            {
                if(_blockShower != nullptr && _working)
                {
                    _blockShower->updateCameraPose(x, y, z, qx, qy, qz, qw);
                }
            }
        );

    QWidget *testWidget = _blockShower->createOsgWidget([this](){
                                                            this->emitUpdate();
                                                        });
    //  QWidget *testWidget = new QWidget(ui->scanMainWidget);
    testWidget->setParent( ui->scanMainWidget );
    testWidget->setObjectName(QStringLiteral("scanWidget"));
    testWidget->setGeometry(ui->scanWidget->geometry());
    testWidget->setSizePolicy(ui->scanWidget->sizePolicy());
    testWidget->setMinimumSize(QSize(ui->scanWidget->minimumWidth(), ui->scanWidget->minimumHeight()));
    ui->toolButton->setParent(testWidget);// = new QToolButton(scanWidget);
    ui->toolButton_2->setParent(testWidget);// = new QToolButton(scanWidget);
    ui->toolButton_3->setParent(testWidget);// = new QToolButton(scanWidget);
    ui->toolButton_4->setParent(testWidget);// = new QToolButton(scanWidget);
    ui->toolButton_5->setParent(testWidget);// = new QToolButton(scanWidget);
    ui->comboBox->setParent(testWidget);// = new QAnimationComboBox(scanWidget);
    ui->comboBox_2->setParent(testWidget);// = new QAnimationComboBox(scanWidget);
    ui->comboBox_3->setParent( testWidget );// = new QAnimationComboBox(scanWidget);
    ui->comboBox_4->setParent( testWidget );// = new QAnimationComboBox(scanWidget);
    ui->comboBox_5->setParent( testWidget );// = new QAnimationComboBox(scanWidget);
    ui->comboBox_6->setParent( testWidget );// = new QAnimationComboBox(scanWidget);
    ui->comboBox_7->setParent( testWidget );// = new QAnimationComboBox(scanWidget);
    delete ui->scanWidget;    // 可以用删除, 建议用删除
    ui->scanWidget = testWidget; // 如果是删除, 需要给它这个变量重新赋值

    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
    if ( lpProject )
    {
        //ui->scanTimeSum->setText("01:22:36");
        std::string saveMapDirectory = lpProject->getBlockDBPath();
        // _blockShower->reset();
        _blockShower->initScene();
        _blockShower->setDBPath(saveMapDirectory);

        // lpProject->setParam("lengthinput",std::string( ui->lengthInput->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("lengthinput").c_str()));
        // lpProject->setParam("width",std::string( ui->width->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("width").c_str()));
        // lpProject->setParam("height",std::string( ui->height->text().toLocal8Bit() ) );//(QString::fromLocal8Bit(lpProject->getParam("height").c_str()));

        float lp = atof( lpProject->getParam("lengthinput").c_str());
        float wd = atof( lpProject->getParam("width").c_str());
        float ht = atof( lpProject->getParam("height").c_str());
        ydBlockDB::BlockIndex minIndex = ydBlockDB::get( -lp/2, -wd/2, 0.0 );//atof( lpProject->getParam("lengthinput").c_str());
        ydBlockDB::BlockIndex maxIndex = ydBlockDB::get( lp/2, wd/2, ht);

        std::string name = lpProject->getName();

        _blockShower->createArea(minIndex,
                   maxIndex,
                   name,
                   lpProject->getID() );
        _blockShower->setWorkArea( lpProject->getID() );
        _blockShower->loadAllScan();

        _blockShower->setArrowCallBack(bind(&Scan::arrowFeedBack, this,  placeholders::_1, placeholders::_2, placeholders::_3, placeholders::_4, placeholders::_5, placeholders::_6));


        std::string createTime = lpProject->getParam("createtime");


        ui->scanTime->setText(QString::fromLocal8Bit( getSimpleTimeStr( atoll( createTime.c_str() ) ).c_str() ) );

        float setacc = atof ( MainWindow::GetMainWindow()->getCfgManager()->getParam("accuracy").c_str() );
        if ( setacc == 0.0f )
        {
            setacc = ydBlockDB::getAccuracy();
        }

        ui->scanAccuracy->setText(QString("%1mm").arg( setacc * 1000 ) );
        _workTimeInSecond = atoi ( lpProject->getParam("worktimeinsecond").c_str() );


        ui->scanDuration->setText("00:00:00");
        ui->scanTimeSum->setText( QString::fromLocal8Bit( formatTime( _workTimeInSecond ).c_str() ) );


        // std::string isoval = MainWindow::GetMainWindow()->getCfgManager()->getParam("isoval");
        // if ( lpProject->getParam("isoval").length() > 0 )
        // {
        //     isoval = lpProject->getParam("isoval");
        // }

        // ui->scanISOVal->setText(QString::fromLocal8Bit( isoval.c_str() ) );

        // std::string isospeedval = MainWindow::GetMainWindow()->getCfgManager()->getParam("isospeedval");
        // if ( lpProject->getParam("isospeedval").length() > 0 )
        // {
        //     isospeedval =  lpProject->getParam("isospeedval");
        // }
        // ui->scanISOSpeedVal->setText(QString::fromLocal8Bit( ( isospeedval + "s" ).c_str() ) );

        // std::string imuval = MainWindow::GetMainWindow()->getCfgManager()->getParam("imuval");
        // if ( lpProject->getParam("imuval").length() > 0 )
        // {
        //     imuval = lpProject->getParam("imuval");
        // }

        // ui->scanIMUVal->setText( QString::fromLocal8Bit( (imuval + "h").c_str() ) );

        // if ( lpProject->getParam("scanimu") == "true" )
        // {
        //     ui->scanIMUOp->setIcon((QPixmap(":/scanner/images/scan/on.png")));
        //     scanIMU = true;
        // }else
        // {
        //     ui->scanIMUOp->setIcon((QPixmap(":/scanner/images/scan/off.png")));
        //     scanIMU = false;
        // }

        // if(lpProject->getParam("scansettingauto") == "true")
        // {
        //     ui->scanSettingAutoOp->setIcon((QPixmap(":/scanner/images/scan/on.png")));
        //     scanSettingAuto = true;
        // }
        // else
        // {
        //     ui->scanSettingAutoOp->setIcon((QPixmap(":/scanner/images/scan/off.png")));
        //     scanSettingAuto = false;
        // }

        // if(lpProject->getParam("scansettingpreview") == "true")
        // {
        //     ui->scanSettingPreviewOp->setIcon((QPixmap(":/scanner/images/scan/on.png")));
        //     scanSettingPreview = true;
        // }
        // else
        // {
        //     ui->scanSettingPreviewOp->setIcon((QPixmap(":/scanner/images/scan/off.png")));
        //     scanSettingPreview = false;
        // }

    }

    connect( this, SIGNAL( signalUpdate() ), testWidget, SLOT( update() ) );

    loadStyleSheet(this, "scan");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    TopScanner *top = new TopScanner(this);
    top->setTitle("三维扫描仪");
    top->move(0, 0);

    ui->scanDuration->setAlignment(Qt::AlignRight);
    ui->scanTime->setAlignment(Qt::AlignRight);
    ui->scanAccuracy->setAlignment(Qt::AlignRight);
    ui->scanTimeSum->setAlignment(Qt::AlignRight);

    unsigned long long freeDisk = 0;
    unsigned long long availableDisk = 0;
    unsigned long long totalSize = 0;
    getDisInfo(MainWindow::GetMainWindow()->getCfgManager()->getBinPath(), freeDisk, availableDisk, totalSize );

    ui->diskInfoUsed->setText(QString("%1 GB").arg( totalSize - availableDisk ));
    ui->diskInfoFree->setText(QString("%1 GB").arg( availableDisk ) );


    _pieChart = new PieChart(ui->diskInfoChart);
    _pieChart->SetStartX(2);
    _pieChart->SetStartY(2);

    int usePercent = int(( 1 - float(availableDisk) / totalSize)  * 100 );
    _pieChart->SetRingLabel(QStringLiteral("%1%").arg( usePercent ));
    _pieChart->AddData(usePercent, QColor(31, 120, 255), "red");
    _pieChart->AddData(100 - usePercent, QColor(202, 205, 213), "blue");

    _drawing = false;
    _working = false;
    // BottomScan *buttom  = new BottomScan( [this](){
    //                                           this->start();
    //                                       }
    //     , [this](){
    //           this->anchor();
    //       }
    //     , [this](){
    //           this->stop();
    //       }
    //     , [this](){
    //           this->screenShot();
    //       }, this);
    BottomScan *buttom  = new BottomScan(this);
    buttom->move(0, 960);

    connect(top, &TopScanner::back, this, &Scan::back);


    m_List1 = new QListWidget();
    m_List1->installEventFilter(this);
    m_List1->verticalScrollBar()->setContextMenuPolicy(Qt::NoContextMenu);

    QListWidgetItem *listItem1 = new QListWidgetItem(QString("室内"));
    m_List1->addItem(listItem1);
    QListWidgetItem *listItem2 = new QListWidgetItem(QString("室外"));
    m_List1->addItem(listItem2);
    // QListWidgetItem *listItem3 = new QListWidgetItem(QString("室外"));
    // m_List1->addItem(listItem3);
    // QListWidgetItem *listItem4 = new QListWidgetItem(QString("地下"));
    // m_List1->addItem(listItem4);

    ui->comboBox->setModel(m_List1->model());
    ui->comboBox->setView(m_List1);
    QWidget *parentx = m_List1->parentWidget();
    if (parentx != nullptr) {
        parentx->setWindowFlags(m_List1->parentWidget()->windowFlags() | Qt::FramelessWindowHint);
        parentx->setAttribute(Qt::WA_TranslucentBackground);
    }
    ui->comboBox->lineEdit()->setPlaceholderText("选择区域…");
    ui->comboBox->setCurrentIndex(G_Space_Type);
    if(G_Space_Type==0) //室内
    {
        parseYaml(true);
    }
    else    //室外
    {
        parseYaml(false);
    }
    

    m_List2 = new QListWidget();
    m_List2->installEventFilter(this);
    m_List2->verticalScrollBar()->setContextMenuPolicy(Qt::NoContextMenu);

    QListWidgetItem *listItem11 = new QListWidgetItem(QString("¼"));
    m_List2->addItem(listItem11);
    QListWidgetItem *listItem21 = new QListWidgetItem(QString("½"));
    m_List2->addItem(listItem21);
    QListWidgetItem *listItem31 = new QListWidgetItem(QString("1"));
    m_List2->addItem(listItem31);
    QListWidgetItem *listItem41 = new QListWidgetItem(QString("2"));
    m_List2->addItem(listItem41);
    QListWidgetItem *listItem51 = new QListWidgetItem(QString("4"));
    m_List2->addItem(listItem51);
    QListWidgetItem *listItem61 = new QListWidgetItem(QString("8"));
    m_List2->addItem(listItem61);

    ui->comboBox_2->setModel(m_List2->model());
    ui->comboBox_2->setView(m_List2);
    QWidget *parent2x = m_List2->parentWidget();
    if (parent2x != nullptr) {
        parent2x->setWindowFlags(m_List2->parentWidget()->windowFlags() | Qt::FramelessWindowHint);
        parent2x->setAttribute(Qt::WA_TranslucentBackground);
    }
    ui->comboBox_2->setCurrentIndex(2);
    ui->comboBox_2->lineEdit()->setPlaceholderText("选择分辨率…");



    m_List3 = new QListWidget();
    m_List3->installEventFilter(this);
    m_List3->verticalScrollBar()->setContextMenuPolicy(Qt::NoContextMenu);

    QListWidgetItem *listItem01 = new QListWidgetItem(QString("亚透明"));   // 0.3
    m_List3->addItem(listItem01);
    QListWidgetItem *listItem32 = new QListWidgetItem(QString("半透明"));   // 0.5
    m_List3->addItem(listItem32);
    QListWidgetItem *listItem33 = new QListWidgetItem(QString("微透明"));   // 0.8
    m_List3->addItem(listItem33);
    QListWidgetItem *listItem34 = new QListWidgetItem(QString("不透明"));   // 1.0
    m_List3->addItem(listItem34);

    ui->comboBox_3->setModel(m_List3->model());
    ui->comboBox_3->setView(m_List3);
    QWidget *parent3x = m_List1->parentWidget();
    if (parent3x != nullptr) {
        parent3x->setWindowFlags(m_List3->parentWidget()->windowFlags() | Qt::FramelessWindowHint);
        parent3x->setAttribute(Qt::WA_TranslucentBackground);
    }
    ui->comboBox_3->setCurrentIndex(1);
    ui->comboBox_3->lineEdit()->setPlaceholderText("选择透明度…");

    //颜色模式
    m_ListColorMode = new QListWidget();
    m_ListColorMode->installEventFilter(this);
    m_ListColorMode->verticalScrollBar()->setContextMenuPolicy(Qt::NoContextMenu);

    QListWidgetItem *colorModelistItem00 = new QListWidgetItem(QString("默认颜色"));
    m_ListColorMode->addItem(colorModelistItem00);

    QListWidgetItem *colorModelistItem01 = new QListWidgetItem(QString("高度显示"));
    m_ListColorMode->addItem(colorModelistItem01);

    QListWidgetItem *colorModelistItem02 = new QListWidgetItem(QString("强度显示"));
    m_ListColorMode->addItem(colorModelistItem02);

    QListWidgetItem *colorModelistItem03 = new QListWidgetItem(QString("指定颜色"));
    m_ListColorMode->addItem(colorModelistItem03);

    ui->comboBox_4->setModel(m_ListColorMode->model());
    ui->comboBox_4->setView(m_ListColorMode);
    QWidget *parent4x = m_ListColorMode->parentWidget();
    if (parent4x != nullptr) {
        parent4x->setWindowFlags(m_ListColorMode->parentWidget()->windowFlags() | Qt::FramelessWindowHint);
        parent4x->setAttribute(Qt::WA_TranslucentBackground);
    }
    ui->comboBox_4->setCurrentIndex(0);
    ui->comboBox_4->lineEdit()->setPlaceholderText("颜色模式…");

    //点云模式
    m_ListPointMode = new QListWidget();
    m_ListPointMode->installEventFilter(this);
    m_ListPointMode->verticalScrollBar()->setContextMenuPolicy(Qt::NoContextMenu);

    QListWidgetItem *pointModelistItem01 = new QListWidgetItem(QString("Block"));
    m_ListPointMode->addItem(pointModelistItem01);

    QListWidgetItem *pointModelistItem02 = new QListWidgetItem(QString("Point"));
    m_ListPointMode->addItem(pointModelistItem02);

    ui->comboBox_5->setModel(m_ListPointMode->model());
    ui->comboBox_5->setView(m_ListPointMode);
    QWidget *parent5x = m_ListColorMode->parentWidget();
    if (parent5x != nullptr) {
        parent5x->setWindowFlags(m_ListPointMode->parentWidget()->windowFlags() | Qt::FramelessWindowHint);
        parent5x->setAttribute(Qt::WA_TranslucentBackground);
    }
    ui->comboBox_5->setCurrentIndex(0);
    ui->comboBox_5->lineEdit()->setPlaceholderText("点云模式…");

    //扫描跟踪
    m_ListScanTracking = new QListWidget();
    m_ListScanTracking->installEventFilter(this);
    m_ListScanTracking->verticalScrollBar()->setContextMenuPolicy(Qt::NoContextMenu);

    QListWidgetItem *scanTrackinglistItem01 = new QListWidgetItem(QString("跟随模式"));
    m_ListScanTracking->addItem(scanTrackinglistItem01);

    QListWidgetItem *scanTrackinglistItem02 = new QListWidgetItem(QString("自由模式"));
    m_ListScanTracking->addItem(scanTrackinglistItem02);

    QListWidgetItem *scanTrackinglistItem03 = new QListWidgetItem(QString("扫描车视角"));
    m_ListScanTracking->addItem(scanTrackinglistItem03);

    ui->comboBox_6->setModel(m_ListScanTracking->model());
    ui->comboBox_6->setView(m_ListScanTracking);
    QWidget *parent6x = m_ListColorMode->parentWidget();
    if (parent6x != nullptr) {
        parent6x->setWindowFlags(m_ListScanTracking->parentWidget()->windowFlags() | Qt::FramelessWindowHint);
        parent6x->setAttribute(Qt::WA_TranslucentBackground);
    }
    ui->comboBox_6->setCurrentIndex(1);
    ui->comboBox_6->lineEdit()->setPlaceholderText("扫描跟踪…");

    //轨迹线
    m_ListTrajectories = new QListWidget();
    m_ListTrajectories->installEventFilter(this);
    m_ListTrajectories->verticalScrollBar()->setContextMenuPolicy(Qt::NoContextMenu);

    QListWidgetItem *trajectorieslistItem01 = new QListWidgetItem(QString("显示"));
    m_ListTrajectories->addItem(trajectorieslistItem01);

    QListWidgetItem *trajectorieslistItem02 = new QListWidgetItem(QString("隐藏"));
    m_ListTrajectories->addItem(trajectorieslistItem02);


    ui->comboBox_7->setModel(m_ListTrajectories->model());
    ui->comboBox_7->setView(m_ListTrajectories);
    QWidget *parent7x = m_ListColorMode->parentWidget();
    if (parent7x != nullptr) {
        parent7x->setWindowFlags(m_ListTrajectories->parentWidget()->windowFlags() | Qt::FramelessWindowHint);
        parent7x->setAttribute(Qt::WA_TranslucentBackground);
    }
    ui->comboBox_7->setCurrentIndex(0);
    ui->comboBox_7->lineEdit()->setPlaceholderText("轨迹线…");

    // scan list
    // ui->scanListWidget->setSpacing(10);
    // ui->scanListWidget->setContentsMargins(5, 5, 5, 5);
    flushScanList();

    connect(ui->comboBox, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(slot_comboBox_clicked(const QString &)));
    connect(ui->comboBox_2, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(slot_comboBox_clicked(const QString &)));
    connect(ui->comboBox_3, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(slot_comboBox_clicked(const QString &)));
    connect(ui->comboBox_4, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(slot_comboBox_clicked(const QString &)));
    connect(ui->comboBox_5, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(slot_comboBox_clicked(const QString &)));
    connect(ui->comboBox_6, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(slot_comboBox_clicked(const QString &)));
    connect(ui->comboBox_7, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(slot_comboBox_clicked(const QString &)));

    connect(ui->toolButton, SIGNAL(clicked()), this, SLOT(slot_addScanArea_clicked()));
    connect( &_scanTimer, SIGNAL( timeout() ), this, SLOT( slotScanTimeout() ) );
    // connect(&calibrate_timer_, SIGNAL(timeout()), this, SLOT(on_calibrate_timeout()));
    connect(ui->deleteButton, SIGNAL(clicked()), this, SLOT(slotScanDelete()));
    connect(ui->loadButton, SIGNAL(clicked()), this, SLOT(slotScanLoad()));
}

Scan::~Scan()
{
    delete _rosNode;
    delete ui;
}

void Scan::emitUpdate()
{
    //
    emit signalUpdate();
}

/*
void Scan::on_scanISOEdit_clicked()
{
    Dialog w;// = new Dialog();
    //connect(w, &Dialog::saveVal, this, &Scan::saveScanISOVal);
    w.setLabelText(QString("修改ISO"));
    w.showFullScreen();
    if ( w.exec() == QDialog::Accepted )
    {
        ui->scanISOVal->setText(w.getVal());

        Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
        if ( lpProject )
        {
            lpProject->setParam("isoval", std::string( w.getVal().toLocal8Bit() ) );
        }
    }
}


void Scan::on_scanISOSpeedEdit_clicked()
{
    Dialog w;// = new Dialog();
    //connect(w, &Dialog::saveVal, this, &Scan::saveScanISOSpeedVal);
    w.setLabelText(QString("修改快慢速度"));
    w.showFullScreen();
    if ( w.exec() == QDialog::Accepted )
    {
        ui->scanISOSpeedVal->setText(w.getVal() + "s");

        Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
        if ( lpProject )
        {
            lpProject->setParam("isospeedval", std::string( w.getVal().toLocal8Bit() ) );
        }
    }
}


void Scan::on_scanIMUEdit_clicked()
{
    Dialog w;// = new Dialog();
    //connect(w, &Dialog::saveVal, this, &Scan::saveScanIMUVal);
    w.setLabelText(QString("修改校正时长"));
    w.showFullScreen();
    if ( w.exec() == QDialog::Accepted )
    {
        ui->scanIMUVal->setText(w.getVal() + "h");

        Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
        if ( lpProject )
        {
            lpProject->setParam("imuval", std::string( w.getVal().toLocal8Bit() ) );
        }
    }
}


void Scan::on_scanSettingPreviewOp_clicked()
{
    if(scanSettingPreview){
        ui->scanSettingPreviewOp->setIcon((QPixmap(":/scanner/images/scan/off.png")));
        scanSettingPreview = false;
    }
    else
    {
        ui->scanSettingPreviewOp->setIcon((QPixmap(":/scanner/images/scan/on.png")));
        scanSettingPreview = true;
    }

    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
    if ( lpProject )
    {
        lpProject->setParam("scansettingpreview", scanSettingPreview ? "true":"false" );
    }
}


void Scan::on_scanSettingAutoOp_clicked()
{
    if(scanSettingAuto){
        ui->scanSettingAutoOp->setIcon((QPixmap(":/scanner/images/scan/off.png")));
        scanSettingAuto = false;
    }
    else
    {
        ui->scanSettingAutoOp->setIcon((QPixmap(":/scanner/images/scan/on.png")));
        scanSettingAuto = true;
    }

    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
    if ( lpProject )
    {
        lpProject->setParam("scansettingauto", scanSettingAuto ? "true":"false" );
    }


}


void Scan::on_scanIMUOp_clicked()
{
    if(scanIMU){
        ui->scanIMUOp->setIcon((QPixmap(":/scanner/images/scan/off.png")));
        scanIMU = false;
    }
    else
    {
        ui->scanIMUOp->setIcon((QPixmap(":/scanner/images/scan/on.png")));
        scanIMU = true;
    }
    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
    if ( lpProject )
    {
        lpProject->setParam("scanimu", scanIMU ? "true":"false" );
    }
}

void Scan::saveScanISOVal(QString val)
{
    ui->scanISOVal->setText(val);
}
void Scan::saveScanISOSpeedVal(QString val)
{
    ui->scanISOSpeedVal->setText(val + "s");
}
void Scan::saveScanIMUVal(QString val)
{
    ui->scanIMUVal->setText(val + "h");
}
*/

// void Scan::on_calibrate_timeout()
// {
//     double calibrate_duration = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::system_clock::now() - start_time_).count();
//     double percentage = calibrate_duration  / calibrate_stationary_seconds_  * 100;
    
//     percentage = (1.0 - (double)_blockShower->getChildIndex() / (double)calibrate_stationary_seconds_) * 100.0;//(1.0 - (double)_blockShower->getChildIndex() / (double)calibrate_stationary_seconds_) * 100.0;

//     if (tipWidget)
//     {
//         if (percentage <= 100.0) {
//             tipWidget->progress( QString("正在卸载场景，请保持静止状态！"), percentage);
//         }

//         if (percentage >= 100.0) {
//             tipWidget->finish("卸载完毕。");
//         }
//         if (percentage >= 130.0) {
//             calibrate_timer_.stop();
            
//             tipWidget->close();
//             delete tipWidget;
//             tipWidget = nullptr;
//         }
//     }
// }

void Scan::back()
{
    // //TODO
    // tipWidget = new Tip();
    // tipWidget->progress(QString("正在卸载场景，请保持静止状态！"), 0);
    // tipWidget->showFullScreen();

    // start_time_ = std::chrono::system_clock::now();
    // calibrate_timer_.start(1 * 1000);

    // tipWidget->show();
    // calibrate_stationary_seconds_ = _blockShower->getChildIndex();

    // emit _rosNode.rosShutdown();
    stop();
    if (_blockShower)
    {
        // _blockShower->reset();
        _blockShower->clearArea();
        delete _blockShower.release();
        _blockShower = NULL;
    }
    emit signalBack(this);
}


void Scan::slot_addScanArea_clicked()
{
    if ( _working )
    {
        return;
    }
    // _blockShower->setViewPosition(CameraView::CV_MAIN);
    if (_drawing)
    {
        _drawing = false;
        _blockShower->setDrawArrow(false);
        ui->toolButton->setStyleSheet("background-color: rgba(0,0,0,0);");
        ui->toolButton->setFixedSize(QSize(48,48));
    }
    else
    {
        _drawing = true;
        _blockShower->setDrawArrow(true);
        ui->toolButton->setStyleSheet("background-color: rgba(176,196,222,255);");
        ui->toolButton->setFixedSize(QSize(52,52));
    }
}

void Scan::arrowFeedBack(float sx, float sy, float sz, float ex, float ey, float ez)
{
    if ( _working )
    {
        ConfirmWidget cfw( [](ConfirmHitType cht){}, this );
        cfw.setConfirmData(QString::fromLocal8Bit("操作错误"), QString::fromLocal8Bit("工作中, 请先停止当前扫描!"), QString::fromLocal8Bit("确定"), false, "");
        cfw.exec();
        return;
    }

    _start.set( sx, sy, sz);
    _end.set( ex, ey, ez );
}

void Scan::slotScanTimeout()
{
    _workTimeInSecond ++;
    _scanAreaTimeInSecond ++;
    ui->scanDuration->setText( QString::fromLocal8Bit(formatTime( _scanAreaTimeInSecond ).c_str() ) );
    ui->scanTimeSum->setText( QString::fromLocal8Bit( formatTime ( _workTimeInSecond ).c_str() ) );

    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
    if(lpProject)
    {
        lpProject->setParam("worktimeinsecond", std::to_string( _workTimeInSecond ) );
    }

    unsigned long long freeDisk = 0;
    unsigned long long availableDisk = 0;
    unsigned long long totalSize = 0;
    getDisInfo(MainWindow::GetMainWindow()->getCfgManager()->getBinPath(), freeDisk, availableDisk, totalSize );

    ui->diskInfoUsed->setText(QString("%1 GB").arg( totalSize - availableDisk ));
    ui->diskInfoFree->setText(QString("%1 GB").arg( availableDisk ) );

    int usePercent = int(( 1 - float(availableDisk) / totalSize)  * 100 );
    _pieChart->SetRingLabel(QStringLiteral("%1%").arg( usePercent ) );
    _pieChart->AddData(usePercent, QColor(31, 120, 255), "red");
    _pieChart->AddData(100 - usePercent, QColor(202, 205, 213), "blue");
}

void Scan::start()
{
    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
    if ( !lpProject )
    {
        return;
    }
    optionEnable(false);

    osg::Quat q;
    osg::Vec3 vec = _end - _start;
    vec.normalize();

    q.makeRotate(osg::Vec3( 1, 0, 0 ), vec);
    slot_addScanArea_clicked();
    printf("扫描起点_start=,%f,%f,%f\n",_start.x(), _start.y(), _start.z());
    printf("扫描终点_end=,%f,%f,%f\n",_end.x(), _end.y(), _end.z());
    printf("扫描方向=,%f,%f,%f,%f\n",q.x(), q.y(), q.z(), q.w());

    std::string scanid = _blockShower->createScanArea( _start.x(), _start.y(), _start.z(), q.x(), q.y(), q.z(), q.w());

    std::string scanAreaPath = lpProject->addScanArea( scanid );
    // flushScanList();
    addScanArea( scanid );
    _scanAreaTimeInSecond = 0;
    
    _blockShower->setDrawArrow(false);
    _blockShower->setWorkScanArea(scanid);
    _scanTimer.start( 1000 );
    _working = true;
    emitUpdate();

    QString roslaunch;
    roslaunch.append("roslaunch yida_slam run.launch savePCDDirectory:=");
    roslaunch.append(QString::fromLocal8Bit( scanAreaPath.c_str()));

    _scanProcess.start(roslaunch);

    std::string recordBag = MainWindow::GetMainWindow()->getCfgManager()->getParam("recordbag");
    if ( recordBag == "true" )
    {
        QString rosbag_record = "rosbag record /imu/data /ns1/velodyne_points /ns2/velodyne_points /usb_cam/image_raw/compressed /usb_cam_node1/image_raw_new/compressed /usb_cam_node2/image_raw_new_2/compressed /usb_cam_node3/image_raw_new_3/compressed /usb_cam_node4/image_raw_new_4/compressed -O ";
        std::string dir = scanAreaPath + ydUtil::getTimeStr4Dir() + ".bag";
        QString bag_path = QString::fromLocal8Bit( dir.c_str() );
        rosbag_record.append(bag_path);
        ydUtil::sleep( 1000 );
        _bagRecord.start(rosbag_record);
        TRACE("%s\n", rosbag_record.toStdString().c_str());
    }

    _rosNode->reinit();

    emit MainWindow::GetMainWindow()->signalStartScan();

    StatusUpdate::setDiskStatus( StatusUpdate::DISK_WRITING );

    TOLOG( "扫描", "开始扫描%s子区域", lpProject->getName().c_str() );
}

bool Scan::isReady()
{
    if ( _working )
    {
        ConfirmWidget *cfw = new ConfirmWidget ( [this](ConfirmHitType cht){
            if(cht == ConfirmHitType::OK)
            {
                stop();
            }
        }, NULL );
        cfw->setConfirmData(QString::fromLocal8Bit("操作错误"), QString::fromLocal8Bit("工作中, 请先停止当前扫描!"), QString::fromLocal8Bit("确定"), false, "");
        cfw->showFullScreen();
        cfw->exec();
        TOLOG( "扫描", "正在扫描中,请不要重复开始");
        return false;
    }
    if ( this->_start == this->_end )
    {
        ConfirmWidget *cfw = new ConfirmWidget( [](ConfirmHitType cht){}, NULL );
        cfw->setConfirmData(QString::fromLocal8Bit("操作错误"), QString::fromLocal8Bit("未设置子区域角度!"), QString::fromLocal8Bit("确定"), false, "");
        cfw->showFullScreen();
        cfw->exec();
        TOLOG( "扫描", "没有设置子区域位姿");

        return false;
    }

    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
    if ( !lpProject )
    {
        return false;
    }

    _blockShower->clearArea("");

    return true;
}

void Scan::stop()
{
    if ( !_working )
    {
        return;
    }
    optionEnable(true);

    this->_scanTimer.stop();
    this->_working = false;

    _start.set( 0.0, 0.0, 0.0 );
    _end.set( 0.0, 0.0, 0.0 );

    std::string recordBag = MainWindow::GetMainWindow()->getCfgManager()->getParam("recordbag");
    if ( recordBag == "true" )
    {
        _bagRecord.terminate();
    }
    _scanProcess.terminate();

    emit MainWindow::GetMainWindow()->signalStopScan();

    StatusUpdate::setDiskStatus( StatusUpdate::DISK_NOMAL );
    //_blockShower->cameraTrakMode(BlockShower::CTM_FREE);
    _blockShower->stopWorkScanArea();
    // ydBlockDB::flushDB();

    TOLOG( "扫描", "停止扫描子区域" );

}

void Scan::screenShot()
{
    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
    if ( lpProject )
    {
        std::string screenshotPath = lpProject->getScanRootPath() /*+ lpProject->getID()*/ + "/screenshot.jpg";
        _blockShower->screenshot(screenshotPath);
        TOLOG( "保存截图", screenshotPath.c_str() );
    }
}
void Scan::anchor()
{

}

void Scan::slot_comboBox_clicked(const QString &qstr)
{
    std::string key = qstr.toStdString();
    if(key.size()>0)
    {
        if(key == "高度显示") _blockShower->setScanAreaColorMode(BlockShower::CM_HEIGHT);
        else if(key == "强度显示") _blockShower->setScanAreaColorMode(BlockShower::CM_INTENSITY);
        else if(key == "指定颜色") _blockShower->setScanAreaColorMode(BlockShower::CM_SPECIFY);
        else if(key == "默认颜色") _blockShower->setScanAreaColorMode(BlockShower::CM_ORIGIN);

        else if(key == "Block") _blockShower->setShapeMode(key);
        else if(key == "Point") _blockShower->setShapeMode(key);

        else if(key == "跟随模式") _blockShower->cameraTrakMode(BlockShower::CMT_TRACK);
        else if(key == "自由模式") _blockShower->cameraTrakMode(BlockShower::CTM_FREE);
        else if(key == "扫描车视角") _blockShower->cameraTrakMode(BlockShower::CMT_SELF);

        else if(key == "显示") _blockShower->setShowTrack(true);
        else if(key == "隐藏") _blockShower->setShowTrack(false);

        // else if(key == "¼") _blockShower->setRatio(0.25);
        // else if(key == "½") _blockShower->setRatio(0.5);
        // else if(key == "1") _blockShower->setRatio(1.0);
        // else if(key == "2") _blockShower->setRatio(2.0);
        // else if(key == "4") _blockShower->setRatio(4.0);
        // else if(key == "8") _blockShower->setRatio(8.0);

        else if(key == "亚透明") _blockShower->setClarity(0.3);
        else if(key == "半透明") _blockShower->setClarity(0.5);
        else if(key == "微透明") _blockShower->setClarity(0.8);
        else if(key == "不透明") _blockShower->setClarity(1.0);

        else if(key == "室内") {
            G_Space_Type = 0;
            parseYaml(true);
        }
        else if(key == "室外")
        {
            G_Space_Type = 1;
            parseYaml(false);
        } 
    }
    MainWindow::GetMainWindow()->centralWidget()->update();

}

void Scan::parseYaml(bool indoor)
{
    // float odometrySurfLeafSize;
    // float mappingCornerLeafSize;
    // float mappingSurfLeafSize;
    // if(indoor)
    // {
    //     odometrySurfLeafSize = 0.2;
    //     mappingCornerLeafSize = 0.1;
    //     mappingSurfLeafSize = 0.2;
    // }else
    // {
    //     odometrySurfLeafSize = 0.4;
    //     mappingCornerLeafSize = 0.2;
    //     mappingSurfLeafSize = 0.4;
    // }

    std::string yaml = MainWindow::GetMainWindow()->getCfgManager()->getParam("yamlpath");
    if(yaml.empty())
        yaml = "./params.yaml";
    else
        yaml += "/params.yaml";

    YAML::Node params;
    try{
        params = YAML::LoadFile(yaml);
    } catch(YAML::BadFile &e) {
        TRACE("read error: %s\n", yaml.c_str());
        return;
    }

    params["yida_slam"]["inDoor"] = indoor ? "true" : "false";
    // params["yida_slam"]["SC_DIST_THRES"] = indoor ? 0.15 : 0.30;
    // params["yida_slam"]["matchdist"] = indoor ? 0.2 : 1.0;
    // params["yida_slam"]["odometrySurfLeafSize"] = indoor ? 0.2 : 0.4;
    // params["yida_slam"]["mappingCornerLeafSize"] = indoor ? 0.1 : 0.2;
    // params["yida_slam"]["mappingSurfLeafSize"] = indoor ? 0.2 : 0.4;

    std::ofstream fout(yaml);
    fout << params;
    fout.close();

}

void Scan::optionEnable(bool able)
{
    ui->comboBox->setEnabled(able);
    ui->comboBox_2->setEnabled(able);
    // ui->comboBox_3->setEnabled(able);    // 设置场景Block颜色
    ui->comboBox_4->setEnabled(able);       // 颜色模式
    ui->comboBox_5->setEnabled(able);
    // ui->comboBox_6->setEnabled(able);    // 跟踪模型
    // ui->comboBox_7->setEnabled(able);    // 扫描轨迹
}

void Scan::flushScanList()
{
    if (!_blockShower)
        return;

    ui->scanListWidget->clear();
    for (auto str : _blockShower->getScanAreaList())
    {
        QListWidgetItem* item = new QListWidgetItem(QString(str.c_str()));
        ui->scanListWidget->addItem(item);
    }
    ui->scanListWidget->setCurrentRow(ui->scanListWidget->count()-1);
}

void Scan::addScanArea(const std::string& scanid)
{
    QListWidgetItem* item = new QListWidgetItem(QString(scanid.c_str()));
    ui->scanListWidget->addItem(item);
    ui->scanListWidget->setCurrentItem(item);
}

void Scan::slotScanDelete()
{
    if ( _working )
    {
        ConfirmWidget cfw( [](ConfirmHitType cht){}, NULL );
        cfw.setConfirmData(QString::fromLocal8Bit("操作错误"), QString::fromLocal8Bit("工作中, 请先停止当前扫描!"), QString::fromLocal8Bit("确定"), false, "");
        cfw.exec();
        return;
    }
        
    auto item = ui->scanListWidget->currentItem();
    if (!item)
    {
        return; 
    }

    std::string scanid = item->text().toStdString();
    _blockShower->deleteScanArea(scanid);

    int index = ui->scanListWidget->currentRow();
    ui->scanListWidget->takeItem(index);

    _blockShower->stopWorkScanArea();
    _blockShower->clearArea();

    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( MainWindow::GetMainWindow()->getCfgManager()->getParam("workingproject"));
    if ( !lpProject )
    {
        return;
    }

    lpProject->delScanArea( scanid );
}

void Scan::slotScanLoad()
{
    if ( _working )
    {
        ConfirmWidget cfw( [](ConfirmHitType cht){}, NULL );
        cfw.setConfirmData(QString::fromLocal8Bit("操作错误"), QString::fromLocal8Bit("工作中, 请先停止当前扫描!"), QString::fromLocal8Bit("确定"), false, "");
        cfw.exec();
        return;
    }

    auto item = ui->scanListWidget->currentItem();
    if (item)
    {
        std::string scanid = item->text().toStdString();
        _blockShower->clearArea();

        _blockShower->loadScan(scanid);
        _blockShower->setWorkScanArea(scanid);
        emitUpdate();
    }

}
