#include "splash_viewer.h"
#include "ui_splash_viewer.h"

#include "common.h"
#include "mainwindow.h"

#include <QTimer>

extern QMainWindow *gMainWindow;

Splash::Splash(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Splash)
{
    ui->setupUi(this);
    loadStyleSheet(this, "splash");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    updateTimer = new QTimer(this);
    connect(updateTimer, &QTimer::timeout, this, &Splash::showMainWidget);
    updateTimer->start(1250);
    updateTimer->setInterval(1250);
    updateTimer->setSingleShot(true);
}

Splash::~Splash()
{
    delete ui;
}

void Splash::showMainWidget()
{
    updateTimer->stop();
    this->close();
    gMainWindow->showFullScreen();
    gMainWindow->show();
}
