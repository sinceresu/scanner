#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QPainter>
#include <QStyleOption>
#include <QToolButton>

#include "common.h"
#include "Logger.h"
#include <ydUtil/log.h>
#include "toscanner_viewer.h"
#include "scan_viewer.h"

#include "StatusUpdate.h"

int G_Space_Type=0;

MainWindow *MainWindow::G_Self = NULL;

MainWindow *MainWindow::GetMainWindow()
{
    return G_Self;
}

MainWindow::MainWindow(ScannerCfgManager *cfgManager, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , _scannerCfgManager( cfgManager )
{
    scan = NULL;
    G_Self = this;
    ui->setupUi(this);
    loadStyleSheet(this, "mainwindow");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    top = new TopSTD(this);
    top->move(0, 0);


    toScanner = new ToScanner(this);
    ui->stackedWidget->addWidget(toScanner);

    management = new Management(this);
    ui->stackedWidget->addWidget(management);

    settings = new Settings(this);
    ui->stackedWidget->addWidget(settings);

    bottomSTD = new BottomSTD(this);
//    bottomSTD->move(0, 894);
    bottomManagement = new BottomManagement();

    ui->buttomStackedWidget->addWidget(bottomSTD);
    ui->buttomStackedWidget->addWidget(bottomManagement);
    ui->buttomStackedWidget->setCurrentIndex(0);


    ui->stackedWidget->setCurrentIndex(0);

    top->showLogo(false);
    ui->logoNewLabel->show();

    connect(toScanner, &ToScanner::startScan, this, &MainWindow::startScan);

    connect(bottomSTD, &BottomSTD::sigCapture, this, &MainWindow::captureSlot);
    connect(bottomSTD, &BottomSTD::sigManagement, this, &MainWindow::managementSlot);
    connect(bottomSTD, &BottomSTD::sigSettings, this, &MainWindow::settingsSlot);

    connect(management, SIGNAL( sigManagementDoubleSelect( QString const ) ), this, SLOT( managementDoubleSelectSlot( QString const ) ) );
    connect(management, SIGNAL( sigManagementClickSelect (QString const ) ), this, SLOT( managementClickSelectSlot ( QString const ) ) );

    connect( this, SIGNAL( signalChangeBottomWidget( int ) ), this, SLOT( slotChangeBottomWidget( int ) ) );

}

MainWindow::~MainWindow()
{
    StatusUpdate::stopUpdate();

    TRACE("扫描程序关机\n");
    TOLOG( "系统", "扫描程序关机" );

    delete ui;
}

void MainWindow::startScan()
{
    if ( scan )
    {
        delete scan;
    }

    scan = new Scan();
    connect(scan, &Scan::signalBack, this, &MainWindow::backScan);
    scan->showFullScreen();
    scan->show();
    this->hide();

}

void MainWindow::backScan(QObject *obj)
{   
    this->showFullScreen();
    this->show();
    toScanner->setTypeValue(G_Space_Type);
    scan->hide();
    scan->close();
    if ( scan )
    {
        delete scan;
        scan = nullptr;
    }
}

void MainWindow::onButtonClicked()
{
    QObject *btn = sender();

}

void MainWindow::captureSlot()
{
    top->showLogo(false);
    ui->logoNewLabel->show();
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::managementSlot()
{
    top->showLogo(true);
    ui->logoNewLabel->hide();
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::settingsSlot()
{
    top->showLogo(true);
    ui->logoNewLabel->hide();
    ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::managementClickSelectSlot( QString const selectUUID )
{
    ui->buttomStackedWidget->setCurrentIndex(0);
}

void MainWindow::managementDoubleSelectSlot( QString const selectUUID )
{
    MainWindow::GetMainWindow()->getCfgManager()->setParam("workingproject", selectUUID.toStdString());
    bottomManagement->setSelectUUID( selectUUID );
    ui->buttomStackedWidget->setCurrentIndex(1);
}

void MainWindow::slotChangeBottomWidget( int index )
{
    if ( index > 1 )
    {
        return;
    }
    ui->buttomStackedWidget->setCurrentIndex(index);
}
