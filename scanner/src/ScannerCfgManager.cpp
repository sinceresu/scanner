#include "ScannerCfgManager.h"
#include <ydUtil/console.h>
#include <ydUtil/log.h>
#include <fstream>
#include <ydUtil/DirUtils.h>
#include <fstream>

ScannerCfgManager::ScannerCfgManager()
{
    _params = new ydUtil::MapParam;
}

void ScannerCfgManager::setBinPath( std::string const& binPath )
{
    _binPath = binPath;

    osg::ref_ptr<ydUtil::Param> param = ydUtil::Param::fromFile( binPath + "/scanner.json" );
    TRACE("%s\n", binPath.c_str() );
    if ( !dynamic_cast<ydUtil::MapParam*>(param.get()) )
    {
        return;
    }

    _params = dynamic_cast<ydUtil::MapParam*>(param.get());
    if ( _params->getValue("recordbag").length() == 0 )
    {
        _params->set("recordbag", "false");
    }

    std::string savePath = _params->getValue("savepath");
    if ( savePath.length() > 0 )
    {
        ydUtil::makeSureDirExist( savePath );
        loadProject();
    }
}

void ScannerCfgManager::setSavePath(std::string const& savePath)
{
    ydUtil::makeSureDirExist( savePath );
    _params->set("savepath", savePath);
    //_savePath = savePath;
    loadProject();

    std::string paramStr = _params->toString();
    std::ofstream ofs( _binPath +  "/scanner.json" );
    ofs << paramStr;

}

void ScannerCfgManager::loadProject()
{
    std::vector<std::string> dirList = ydUtil::findDirInPath(getSavePath().c_str());
    if ( dirList.size() == 0 )
    {
        return;
    }

    _projects.clear();

    for ( auto a : dirList )
    {
        if ( ydUtil::fileExist( ( getSavePath() + "/" + a + "/project.json" ).c_str() ) )
        {
            Project *lpProject = new Project;
            lpProject->setPath( getSavePath() + "/" + a );
            lpProject->load();
            addProject( lpProject );
        }
    }

}
std::string ScannerCfgManager::getSavePath()
{
    return _params->getValue( "savepath" );
}

void ScannerCfgManager::addProject( Project *lpProject )
{
    if ( !lpProject )
    {
        return;
    }
    _projects.push_back( lpProject );
}

void ScannerCfgManager::removeProject( std::string const& uuid )
{
    for ( std::vector< osg::ref_ptr<Project> >::iterator it = _projects.begin(); it != _projects.end(); it ++ )
    {
        if ( (*it)->getID() == uuid )
        {
            _projects.erase( it );
            break;
        }
    }
}

Project *ScannerCfgManager::getProject( std::string const& uuid )
{
    for ( std::vector< osg::ref_ptr<Project> >::iterator it = _projects.begin(); it != _projects.end(); it ++ )
    {
        if ( (*it)->getID() == uuid )
        {
            return (*it).get();
        }
    }

    return NULL;
}

std::vector<std::string> ScannerCfgManager::getProjectList()
{
    std::vector<std::string> pl;
    for ( std::vector< osg::ref_ptr<Project> >::iterator it = _projects.begin(); it != _projects.end(); it ++ )
    {
        pl.push_back( (*it)->getID() );
    }
    return pl;
}

void ScannerCfgManager::setParam( std::string const& key, std::string const& value )
{
    _params->set( key, value );
    save();
}
std::string ScannerCfgManager::getParam( std::string const& key )
{
    return _params->getValue(key);
}

void ScannerCfgManager::save()
{
    std::string cfgFile = _binPath + "/scanner.json";
    std::string paramStr = _params->toString();
    std::ofstream ofs( cfgFile );
    ofs << paramStr;
}
