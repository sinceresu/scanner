#include "managementdetail_viewer.h"
#include <QFileInfo>
#include "ui_managementdetail_viewer.h"
#include "mainwindow.h"
#include "ScannerCfgManager.h"
#include "Project.h"
#include "common.h"
#include "topscanner_viewer.h"
#include <ydUtil/DirUtils.h>
#include <ydUtil/StringUtils.h>
#include <osgDB/FileNameUtils>
#include <ydUtil/log.h>
#include <dirent.h>

long long int GetDirectorySize(const char *dir)
{
    DIR *dp;
    struct dirent *entry;
    struct stat statbuf;
    long long int totalSize=0;

    if ((dp = opendir(dir)) == NULL)
    {
        fprintf(stderr, "Cannot open dir: %s\n", dir);
        return -1; //可能是个文件，或者目录不存在
    }
    
    //先加上自身目录的大小
    lstat(dir, &statbuf);
    totalSize+=statbuf.st_size;

    while ((entry = readdir(dp)) != NULL)
    {
        char subdir[256];
        sprintf(subdir, "%s/%s", dir, entry->d_name);
        lstat(subdir, &statbuf);
        
        if (S_ISDIR(statbuf.st_mode))
        {
            if (strcmp(".", entry->d_name) == 0 ||
                strcmp("..", entry->d_name) == 0)
            {
                continue;
            }

            long long int subDirSize = GetDirectorySize(subdir);
            totalSize+=subDirSize;
        }
        else
        {
            totalSize+=statbuf.st_size;
        }
    }

    closedir(dp);    
    return totalSize;
}

ManagementDetail::ManagementDetail( QString selectUUID, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ManagementDetail)
{
    ui->setupUi(this);
    loadStyleSheet(this, "managementDetail");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    TopScanner *top = new TopScanner(this);
    top->move(0, 0);
    connect(top, &TopScanner::back, this, &ManagementDetail::backSlot);

    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->verticalHeader()->setVisible(false);
    ui->tableWidget->setAlternatingRowColors(true);
    ui->tableWidget->horizontalHeader()->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    ui->tableWidget->verticalHeader()->setDefaultSectionSize(88);
    ui->tableWidget->horizontalHeader()->setDefaultAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->tableWidget->setFrameShape(QFrame::VLine);
    ui->tableWidget->setShowGrid(false);
    //1760 88
    ui->tableWidget->horizontalHeader()->setFixedHeight(118);
    ui->tableWidget->verticalHeader()->setStyleSheet(
                "QScrollBar{background:transparent; height:10px; }"
                "QScrollBar::handle{background:lightgray; border:8px solid transparent; border-radius:15px; }"
                "QScrollBar::handle:hover{background: rgba(227,230,233,1); }"
                "QScrollBar::handle:pressed{background:black;}"
                "QScrollBar::sub-line{background:transparent;}"
                "QScrollBar::add-line{background:transparent;}"
                );
    ui->tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->tableWidget->setVerticalScrollMode(QAbstractItemView::ScrollPerItem);
    ui->tableWidget->setAutoScroll(true);


    Project *lpProject = MainWindow::GetMainWindow()->getCfgManager()->getProject( std::string( selectUUID.toLocal8Bit() ) );
    if ( lpProject )
    {
        // ui->nameVal->setText( QString::fromLocal8Bit( lpProject->getName().c_str() ) );
        // ui->typeVal->setText("文件夹");
        // ui->contentVal->setText( QString("%1").arg( ydUtil::findFileInPath( lpProject->getPath().c_str(), true ).size() ) );
        // ui->localVal->setText(QString::fromLocal8Bit( lpProject->getPath().c_str() ) );//"/temp/ER/001");
        // ui->updateTimeVal->setText(QString::fromLocal8Bit( getSimpleTimeStr( atoll( lpProject->getParam( "createtime" ) ).c_str() ) ) );

        top->setTitle(QString::fromLocal8Bit( lpProject->getName().c_str() ));
        ui->tableWidget->setColumnCount(4);

        QStringList header;
        header << "文件名称" // <<"扫描类型"
               <<"修改时间"<<"类型" << "大小";
        ui->tableWidget->setHorizontalHeaderLabels(header);
        //1760
        ui->tableWidget->setColumnWidth(0, 500);
        //ui->tableWidget->setColumnWidth(1, 350);
        ui->tableWidget->setColumnWidth(1, 410);
        ui->tableWidget->setColumnWidth(2, 400);
        ui->tableWidget->setColumnWidth(3, 440);

        //std::vector<std::string> fileList = ydUtil::findFileInPath( lpProject->getScanRootPath().c_str(), true );
        std::vector<std::string> fileList = ydUtil::findFileInPath( lpProject->getPath().c_str(), true );

        for (unsigned int i = 0; i < fileList.size(); i++) {

            ui->tableWidget->insertRow(i);

            QString fileName= QString::fromLocal8Bit( ydUtil::ReplaceStr( fileList[i], lpProject->getPath(), "" ).c_str() );

            QTableWidgetItem *item1 = new QTableWidgetItem( fileName );
            item1->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);

            //QTableWidgetItem *item2 = new QTableWidgetItem(QString("新建"));
            //item2->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            QFileInfo info(QString::fromLocal8Bit( fileList[i].c_str() ) );
            QString modifyStr = info.lastModified().toString("yyyy-MM-dd hh:mm:ss");

            QTableWidgetItem *item3 = new QTableWidgetItem(modifyStr);
            item3->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

            std::string dotExt = osgDB::getFileExtension( fileList[i] );
            if ( dotExt == "" )
            {
                dotExt = "文件夹";
            }
            QTableWidgetItem *item4 = new QTableWidgetItem(QString::fromLocal8Bit( dotExt.c_str() ) );
            item4->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

            long long totalSize = GetDirectorySize(fileList[i].c_str());
            if(totalSize == -1)
            {
                totalSize = info.size();
            }

            QString fileSize;
            if ( totalSize >= 1024 * 1024 * 1024 )
            {
                fileSize = QString("%1Gb").arg( totalSize / 1024.0 / 1024.0 / 1024.0, 0, 'f', 2 );
            }
            else if ( totalSize >= 1024 * 1024 && totalSize < 1024 * 1024 * 1024)
            {
                fileSize = QString("%1Mb").arg( totalSize / 1024.0 / 1024.0, 0, 'f', 2 );
            }else if ( totalSize >= 1024 && totalSize < 1024 * 1024 )
            {
                fileSize = QString("%1Kb").arg( totalSize / 1024.0, 0, 'f', 2);
            }else
            {
                fileSize = QString("%1b").arg( totalSize);
            }

            QTableWidgetItem *item5 = new QTableWidgetItem(fileSize);
            item5->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

            ui->tableWidget->setItem(i, 0, item1);
            //ui->tableWidget->setItem(i, 1, item2);
            ui->tableWidget->setItem(i, 1, item3);
            ui->tableWidget->setItem(i, 2, item4);
            ui->tableWidget->setItem(i, 3, item5);
        }
    }

}

ManagementDetail::~ManagementDetail()
{
    delete ui;
}

void ManagementDetail::backSlot()
{
    emit back(this);
}
