#include "StatusUpdate.h"
#include <ydUtil/DirUtils.h>
#include "systeminfo.h"
#include "mainwindow.h"
#include "ScannerCfgManager.h"
#include <ydUtil/console.h>
#include <ydUtil/StringUtils.h>
#include <ydUtil/log.h>
#include "Logger.h"

float       StatusUpdate::s_batteryPercent = 100.0f;
bool        StatusUpdate::s_batteryStateChange = false;
std::string StatusUpdate::s_batteryStates = "";
unsigned int StatusUpdate::_camera = StatusUpdate::CAMERA_ERROR;
unsigned int StatusUpdate::_radar = StatusUpdate::RADAR_ERROR;
unsigned int StatusUpdate::_udisk = StatusUpdate::UDISK_UMOUNT;
unsigned int StatusUpdate::_disk = StatusUpdate::DISK_NOMAL;
std::string StatusUpdate::_mediaPath = "";

std::string StatusUpdate::_mediaRootPath = "";
std::thread *StatusUpdate::_updateThread;
bool StatusUpdate::_work = false;

StatusUpdate::StatusUpdate( )
{
    _timeLabel = NULL;//( timeLabel )
    _udiskStatus = NULL;//( udiskStatus )
    _cameraStatus = NULL;//( cameraStatus )
    _radarStatus = NULL;//( radarStatus )
    _batteryStatus = NULL;//( batteryStatus )

    _lcamera = 0;
    _lradar = 0;
    _ludisk = 0;
    _ldisk = 0;

}
void StatusUpdate::setup( QLabel *timeLabel, QLabel *udiskStatus, QLabel *diskStatus, QLabel *cameraStatus, QLabel *radarStatus, QLabel *batteryStatus )
{
    _timeLabel = timeLabel;
    _diskStatus = diskStatus;
    _udiskStatus = udiskStatus;
    _cameraStatus = cameraStatus;
    _radarStatus = radarStatus;
    _batteryStatus = batteryStatus;

    updateWidget();
}

void StatusUpdate::startUpdate()
{
    _updateThread = new std::thread([](){
                                    StatusUpdate::_work = true;
                                    TOLOG( "设备状态", "状态监测线程启动");
                                    while( StatusUpdate::_work )
                                    {
                                        StatusUpdate::update();
                                        ydUtil::sleep( 1000 );
                                    }
                                });
}
void StatusUpdate::stopUpdate()
{
    if (_updateThread && _updateThread->joinable() )
    {
        _work = false;
        _updateThread->join();
        TOLOG( "设备状态", "状态监测线程退出");
    }
    delete _updateThread;
}
StatusUpdate::~StatusUpdate()
{

}
void StatusUpdate::setCameraStatus( unsigned int v )
{
    _camera = v;
}
void StatusUpdate::setDiskStatus( unsigned int v )
{
    _disk = v;
}
void StatusUpdate::setUDiskStatus( unsigned int v )
{
    _udisk = v;
}
void StatusUpdate::setRadarStatus( unsigned int v )
{
    _radar = v;
}
void StatusUpdate::setBatteryStatus( float v )
{
    s_batteryPercent = v;
}
void StatusUpdate::update()
{
    std::string s;
    float p = getBATPercent( s );
    if((s_batteryStates!=s) || (p!=s_batteryPercent))
    {
        s_batteryStateChange = true;
        s_batteryStates = s;
        s_batteryPercent = p;
    }

    // udisk
    std::vector<std::string> fileList = ydUtil::findFileInPath( _mediaPath.c_str(), true );
    if ( fileList.size() == 0 )
    {
        if ( _udisk != UDISK_UMOUNT )
        {
            TOLOG( "设备状态", "U盘弹出");
        }
        _mediaRootPath = _mediaPath;
        _udisk = UDISK_UMOUNT;
    }else
    {
        if ( _udisk != UDISK_NOMAL )
        {
            TOLOG( "设备状态", "U盘接入");
        }


        _udisk = UDISK_NOMAL;
        _mediaRootPath = fileList[0];
    }

    // disk
    unsigned long long freeDisk = 0;
    unsigned long long availableDisk = 0;
    unsigned long long totalSize = 0;
    getDisInfo(MainWindow::GetMainWindow()->getCfgManager()->getBinPath(), freeDisk, availableDisk, totalSize );
    if ( _disk == DISK_WRITING )
    {
        // 小于10G, 就认为空间不够
        if ( freeDisk < 10 )
        {
            if ( _disk != DISK_SHORTAGE )
            {
                _disk = DISK_SHORTAGE;
                TOLOG( "设备状态", "硬盘空间不足 : %d", freeDisk );
            }
        }

    }else
    {

        if ( totalSize == 0 )
        {
            if ( _disk != DISK_UMOUNT )
            {
                TOLOG( "设备状态", "硬盘弹出");
                _disk = DISK_UMOUNT;
            }
        }else
        {
            if ( _disk != DISK_NOMAL )
            {
                TOLOG( "设备状态", "硬盘正常工作 : %d", freeDisk );
                _disk = DISK_NOMAL;
            }
        }

    }

}

std::string StatusUpdate::getMediaPath()
{
    return _mediaPath;
}

std::string StatusUpdate::getMediaRootPath()
{
    return _mediaRootPath;
}

void StatusUpdate::setMediaPath( std::string const& p )
{
    char *user = getenv("USER");
    if ( user )
    {
        _mediaPath = p + "/" + user;
    }else
    {
        std::vector<std::string> fileList = ydUtil::findFileInPath( p.c_str(), true );
        if ( fileList.size() == 0 )
        {
            _mediaPath = p;
        }else
        {
            for ( auto f : fileList)
            {
                if ( f.size() == 0 )
                {
                    continue;
                }
                std::vector<std::string> pathList = ydUtil::split( f, "/");
                if ( pathList.size() == 0 )
                {
                    continue;
                }

                bool valid = true;
                for ( auto pl : pathList )
                {
                    if ( pl.at(0) == '.' )
                    {
                        valid = false;
                        break;
                    }
                }
                if ( !valid )
                {
                    continue;
                }
                _mediaPath = f;
                break;
            }
        }
    }
    _mediaRootPath = _mediaPath;
}

void StatusUpdate::updateWidget()
{
    if ( _timeLabel )
    {
        _timeLabel->setText( QString::fromLocal8Bit( getFullTimeStr(time(NULL)).c_str() ) );
    }

    if ( _batteryStatus && s_batteryStateChange )
    {
        if ( s_batteryStates == "charging" )
        {
            _batteryStatus->setPixmap( QPixmap(":/scanner/images/top/batterying.svg"));
        }
        else if ( s_batteryStates == "fully-charged" )
        {
            _batteryStatus->setPixmap( QPixmap(":/scanner/images/top/batteryfull.svg"));
        }
        else if ( s_batteryStates == "discharging")
        {   
            float p = s_batteryPercent;
            if ( p < 10 )
            {
                _batteryStatus->setPixmap( QPixmap(":/scanner/images/top/batterylow.png"));
            }else if ( p < 20 )
            {
                _batteryStatus->setPixmap( QPixmap(":/scanner/images/top/battery20.svg"));
            }else if ( p < 40 )
            {
                _batteryStatus->setPixmap( QPixmap(":/scanner/images/top/battery40.svg"));
            }else if ( p < 60 )
            {
                _batteryStatus->setPixmap( QPixmap(":/scanner/images/top/battery60.svg"));
            }else if ( p < 80 )
            {
                _batteryStatus->setPixmap( QPixmap(":/scanner/images/top/battery80.svg"));
            }else
            {
                _batteryStatus->setPixmap( QPixmap(":/scanner/images/top/batteryfull.svg"));
            }   
        }
    }

    if ( _udiskStatus )
    {
        if ( _ludisk != _udisk )
        {
            switch ( _udisk )
            {
            case UDISK_NOMAL:
                _udiskStatus->setPixmap( QPixmap(":/scanner/images/top/udisk.png") );
                break;
            case UDISK_UMOUNT:
                _udiskStatus->setPixmap( QPixmap(":/scanner/images/top/udiskunmout.png") );
                break;
            }

            _ludisk = _udisk;
        }

    }

    if ( _diskStatus )
    {
        if ( _ldisk != _disk )
        {
            _ldisk = _disk;
            switch ( _disk )
            {
            case DISK_NOMAL:
                _diskStatus->setPixmap( QPixmap(":/scanner/images/top/disk.svg"));
                break;
            case DISK_SHORTAGE:
                _diskStatus->setPixmap( QPixmap(":/scanner/images/top/diskshortage.svg"));
                break;
            case DISK_WRITING:
                _diskStatus->setPixmap( QPixmap(":/scanner/images/top/diskwriting.svg"));
                break;
            case DISK_UMOUNT:
                _diskStatus->setPixmap( QPixmap(":/scanner/images/top/diskunmout.svg"));
                break;

            }
        }

    }

    if ( _cameraStatus )
    {
        if ( _lcamera != _camera )
        {
            _lcamera = _camera;
            switch ( _camera )
            {
            case CAMERA_ERROR:
                _cameraStatus->setPixmap( QPixmap(":/scanner/images/top/cameraerror.svg"));
                break;
            case CAMERA_WORK:
                _cameraStatus->setPixmap( QPixmap(":/scanner/images/top/camera.svg"));
                break;
            }
        }
    }

    if ( _radarStatus )
    {
        if ( _lradar != _radar )
        {
            _lradar = _radar;
            switch( _radar )
            {
            case RADAR_NORMAL:
                _radarStatus->setPixmap(QPixmap(":/scanner/images/top/radar.svg"));
                break;
            case RADAR_ERROR:
                _radarStatus->setPixmap(QPixmap(":/scanner/images/top/radarerror.svg"));
                break;
            case RADAR_WORK:
                _radarStatus->setPixmap(QPixmap(":/scanner/images/top/radarworking.svg"));
                break;
            }
        }
    }

}
