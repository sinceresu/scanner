﻿#ifndef QANIMATIONCOMBOBOX_H
#define QANIMATIONCOMBOBOX_H

#include <QWidget>
#include <QComboBox>
#include <QPropertyAnimation>
#include <QLineEdit>
#include <QMenu>
#include <QListWidget>

class Q_DECL_EXPORT QAnimationComboBox : public QComboBox
{
    Q_OBJECT

    Q_PROPERTY(QColor lineColor READ lineColor WRITE setLineColor DESIGNABLE true)
    Q_PROPERTY(QColor focusLineColor READ focusLineColor WRITE setFocusLineColor DESIGNABLE true)

public:
    explicit QAnimationComboBox(QWidget *parent = nullptr);
    ~QAnimationComboBox();

    QColor lineColor() const;
    void setLineColor(const QColor &color);
    QColor focusLineColor() const;
    void setFocusLineColor(const QColor &color);

protected:
    void focusInEvent(QFocusEvent *event) override;
    void focusOutEvent(QFocusEvent *event) override;
    bool eventFilter(QObject *obj, QEvent *ev) override;

private Q_SLOTS:
    void valueChanged(const QVariant &value);
    void animationFinished();


private:
    bool m_isAnimating;
    bool m_isFocuse;
    float m_currentValue;
    QColor m_lineColor;
    QColor m_focusLineColor;
};

#endif // QANIMATIONCOMBOBOX_H
