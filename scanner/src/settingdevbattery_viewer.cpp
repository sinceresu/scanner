#include "settingdevbattery_viewer.h"
#include "ui_settingdevbattery_viewer.h"

#include "common.h"

SettingDevBattery::SettingDevBattery(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingDevBattery)
{
    ui->setupUi(this);

    loadStyleSheet(this, "settingDevBattery");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);


    ui->typeLabel->setText("电池型号");
    ui->durationLabel->setText("待机时长");
    ui->acLabel->setText("AC输入");
    ui->dcLabel->setText("DC输出");
    ui->channelLabel->setText("充电通道");

    ui->typeValLabel->setText("01");
    ui->durationValLabel->setText("4小时");
    ui->acValLabel->setText("90-264V / 47-63Hz");
    ui->dcValLabel->setText("26.5 V / 4A");
    ui->channelValLabel->setText("2");

}

SettingDevBattery::~SettingDevBattery()
{
    delete ui;
}
