#ifndef BOTTOMMANAGEMENTPRE_VIEWER_H
#define BOTTOMMANAGEMENTPRE_VIEWER_H

#include "managementdetail_viewer.h"
#include "scan_viewer.h"

#include <QWidget>

namespace Ui {
class BottomManagementPre;
}

class BottomManagementPre : public QWidget
{
    Q_OBJECT

public:
    explicit BottomManagementPre(QWidget *parent = nullptr);
    ~BottomManagementPre();

signals:
    void signalRemove();
    void signalDetail();
    void signalBack();

private slots:
    void back(QObject *obj);

private slots:
    void on_doScanPushButton_clicked();

    void on_doDelPushButton_clicked();

    void on_detailPushButton_clicked();

private:
    Scan *scan;
    ManagementDetail *detail;

private:
    Ui::BottomManagementPre *ui;
};

#endif // BOTTOMMANAGEMENTPRE_VIEWER_H
