#include "Logger.h"
#include "systeminfo.h"
#include <time.h>
#include <fstream>


Logger::Logger()
{
    
}
Logger::~Logger()
{
    
}

std::string Logger::getCurFilePath()
{
    time_t t = time(NULL);

    char ch[64] = { 0 };
    strftime(ch, sizeof(ch) - 1, "%Y-%m.log", localtime(&t));

    return _savePath + "/" + ch;
}

// time format
//"%Y年%m月%d日 %H:%M:%S", localtime(&t);

void Logger::log( char *operType, char *buff )
{
    std::string fileName = getCurFilePath();
    std::ofstream ofs ( fileName, std::ios::app );

    time_t t = time(NULL);
    char ch[64] = { 0 };
    strftime(ch, sizeof(ch) - 1, "%Y/%m/%d %H:%M:%S", localtime(&t));
    ofs << operType << "\t" << buff << "\t" << ch << std::endl;
}
std::vector<std::string> Logger::getLog( int count )
{
    std::vector<std::string> logInfo;

    std::ifstream ifs( getCurFilePath() );

    std::string line;

    while (std::getline(ifs, line))
    {
        logInfo.push_back( line );
    }
    if ( logInfo.size() > 100 )
    {
        logInfo.erase( logInfo.begin(), logInfo.begin() + ( logInfo.size() - 100 ) );
    }

    return logInfo;
}
void Logger::setSavePath( std::string const& f )
{
    _savePath = f;
}
