#ifndef MANAGEMENTPRE_VIEWER_H
#define MANAGEMENTPRE_VIEWER_H

#include <QWidget>
#include <memory>
#include "managementdetail_viewer.h"

namespace BlockShower {
    class BlockShower;
}
namespace Ui {
class ManagementPre;
}

class ManagementPre : public QWidget
{
    Q_OBJECT

public:
    explicit ManagementPre(QWidget *parent = nullptr);
    ~ManagementPre();

signals:
    void back(QObject *obj);

private slots:
    void backSlot();
    void backDetail();
    void slotRemoveProject();
    void slotDetailProject();
    void slotScanBack();


private:
    std::string _uuid;
    Ui::ManagementPre *ui;
    ManagementDetail *detail;
    std::unique_ptr<BlockShower::BlockShower> _blockShower;
};

#endif // MANAGEMENTPRE_VIEWER_H
