#ifndef TOSCANNER_VIEWER_H
#define TOSCANNER_VIEWER_H

#include <QLabel>
#include <QListWidget>
#include <QWidget>

namespace Ui {
class ToScanner;
}

class ToScanner : public QWidget
{
    Q_OBJECT

public:
    explicit ToScanner(QWidget *parent = nullptr);
    ~ToScanner();

    void setTypeValue(int index);

protected:
    void init();

signals:
    void startScan();

public:
    virtual bool eventFilter(QObject *watched, QEvent *event) override;

private slots:
    void on_type_activated(const QString &arg1);

    //void on_start_clicked();

    void on_startButton_clicked();

private:
    QListWidget *m_accountList;
    Ui::ToScanner *ui;
};

#endif // TOSCANNER_VIEWER_H
