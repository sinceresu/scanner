#include "topmanagemnetpre_viewer.h"
#include "ui_topmanagemnetpre_viewer.h"
#include "systeminfo.h"
#include "common.h"

TopManagemnetPre::TopManagemnetPre(QWidget *parent) :
    QWidget(parent)
    ,ui(new Ui::TopManagemnetPre)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_TranslucentBackground);
    loadStyleSheet(this, "topManagemnetPre");

    ui->dateLabel->setText( QString::fromLocal8Bit( getFullTimeStr(time(NULL)).c_str() ) );
    ui->udiskLabel->setPixmap(QPixmap(":/scanner/images/top/udisk.svg"));
    ui->diskLabel->setPixmap(QPixmap(":/scanner/images/top/disk.svg"));
    ui->cameraLabel->setPixmap(QPixmap(":/scanner/images/top/camera.svg"));
    ui->radarLabel->setPixmap(QPixmap(":/scanner/images/top/radar.svg"));
    ui->batteryLabel->setPixmap(QPixmap(":/scanner/images/top/batterying.svg"));

    ui->titleLabel->hide();

    connect( &_timer, SIGNAL( timeout() ), this, SLOT( slotTimeout() ) );
    _timer.start( 1000 );


    //void setup( QLabel *timeLabel, QLabel *udiskStatus, QLabel *diskStatus, QLabel *cameraStatus, QLabel *radarStatus, QLabel *batteryStatus );
    _statusUpdate.setup( ui->dateLabel, ui->udiskLabel, ui->diskLabel, ui->cameraLabel, ui->radarLabel, ui->batteryLabel);

}

TopManagemnetPre::~TopManagemnetPre()
{
    delete ui;
}


void TopManagemnetPre::on_backLabel_clicked()
{
    emit back();
}

void TopManagemnetPre::setTitle(QString title)
{
    ui->titleLabel->setText(title);
    ui->titleLabel->show();
}

void TopManagemnetPre::slotTimeout()
{
    _statusUpdate.updateWidget();
}
