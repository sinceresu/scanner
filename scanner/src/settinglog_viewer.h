#ifndef SETTINGLOG_VIEWER_H
#define SETTINGLOG_VIEWER_H

#include <QWidget>

namespace Ui {
class SettingLog;
}

class SettingLog : public QWidget
{
    Q_OBJECT

public:
    explicit SettingLog(QWidget *parent = nullptr);
    ~SettingLog();
signals:
    void back(QObject *obj);

private slots:
    void backSlot();

private:
    Ui::SettingLog *ui;
};

#endif // SETTINGLOG_VIEWER_H
