#include "bottomscan_viewer.h"
#include "ui_bottomscan_viewer.h"

#include <QDebug>

#include "common.h"
#include "tip.h"
#include "scan_viewer.h"

BottomScan::BottomScan(ActionCallback startCallback, ActionCallback anchorCallback, ActionCallback endCallback, ActionCallback screenShotCallback, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BottomScan),
    calibrate_stationary_seconds_(5),
    tipWidget(nullptr)
{
    _startCallback = startCallback;
    _anchorCallback = anchorCallback;
    _endCallback = endCallback;
    _screenShotCallback = screenShotCallback;
    ui->setupUi(this);
    loadStyleSheet(this, "bottomscan");

    ui->startButton->setStyleSheet("border: none; background-color:transparent;");
    ui->anchorButton->setStyleSheet("border: none; background-color:transparent;");
    ui->endButton->setStyleSheet("border: none; background-color:transparent;");


  connect(&calibrate_timer_, SIGNAL(timeout()), this, SLOT(slot_calibrate_timeout()));


}

BottomScan::BottomScan(QWidget *parent)
    : QWidget(parent)
    , _scan(parent)
    , ui(new Ui::BottomScan)
    , calibrate_stationary_seconds_(5)
    , tipWidget(nullptr)
{
    ui->setupUi(this);
    loadStyleSheet(this, "bottomscan");

    ui->startButton->setStyleSheet("border: none; background-color:transparent;");
    ui->anchorButton->setStyleSheet("border: none; background-color:transparent;");
    ui->endButton->setStyleSheet("border: none; background-color:transparent;");


    connect(&calibrate_timer_, SIGNAL(timeout()), this, SLOT(slot_calibrate_timeout()));
}

BottomScan::~BottomScan()
{
    delete ui;
}

void BottomScan::on_screenshotButton_clicked()
{
    ui->screenshotButton->setStyleSheet("border: none; background-color: rgba(73, 83, 102, 0.06);");
    ui->startButton->setStyleSheet("border: none; background-color:transparent;");
    ui->anchorButton->setStyleSheet("border: none; background-color:transparent;");
    ui->endButton->setStyleSheet("border: none; background-color:transparent;");

    ui->screenshot->setIcon(QPixmap(":/scanner/images/buttom/screenshotactive.png"));
    ui->start->setIcon(QPixmap(":/scanner/images/buttom/start.png"));
    ui->anchor->setIcon(QPixmap(":/scanner/images/buttom/anchor.png"));
    ui->end->setIcon(QPixmap(":/scanner/images/buttom/end.png"));

    //TODO
    // if ( _screenShotCallback )
    // {
    //     _screenShotCallback();
    // }

    Scan* lpScan = dynamic_cast<Scan*>(_scan);
    if(lpScan != NULL)
    {
        lpScan->screenShot();
    }
    
    //TODO
    Tip *tipWidget = new Tip();
    tipWidget->finish(QString("截图已保存screenshot.jpg!"));
    tipWidget->showFullScreen();
    tipWidget->show();

}


void BottomScan::on_startButton_clicked()
{
    Scan* lpScan = dynamic_cast<Scan*>(_scan);
    if(lpScan == NULL || !lpScan->isReady())
    {
        return;
    }

    ui->startButton->setStyleSheet("border: none; background-color: rgba(73, 83, 102, 0.06);");
    ui->screenshotButton->setStyleSheet("border: none; background-color:transparent;");
    ui->anchorButton->setStyleSheet("border: none; background-color:transparent;");
    ui->endButton->setStyleSheet("border: none; background-color:transparent;");

    ui->screenshot->setIcon(QPixmap(":/scanner/images/buttom/screenshot.png"));
    ui->start->setIcon(QPixmap(":/scanner/images/buttom/startactive.png"));
    ui->anchor->setIcon(QPixmap(":/scanner/images/buttom/anchor.png"));
    ui->end->setIcon(QPixmap(":/scanner/images/buttom/end.png"));

    //TODO
    tipWidget = new Tip();
    tipWidget->progress(QString("正在IMU校准，请保持静止状态！"), 0);
    tipWidget->showFullScreen();

    start_time_ = std::chrono::system_clock::now();
    calibrate_timer_.start(1 * 1000);

    tipWidget->show();
}


void BottomScan::on_anchorButton_clicked()
{
    ui->anchorButton->setStyleSheet("border: none; background-color: rgba(73, 83, 102, 0.06);");
    ui->screenshotButton->setStyleSheet("border: none; background-color:transparent;");
    ui->startButton->setStyleSheet("border: none; background-color:transparent;");
    ui->endButton->setStyleSheet("border: none; background-color:transparent;");

    ui->screenshot->setIcon(QPixmap(":/scanner/images/buttom/screenshot.png"));
    ui->start->setIcon(QPixmap(":/scanner/images/buttom/start.png"));
    ui->anchor->setIcon(QPixmap(":/scanner/images/buttom/anchoractive.png"));
    ui->end->setIcon(QPixmap(":/scanner/images/buttom/end.png"));


    Scan* lpScan = dynamic_cast<Scan*>(_scan);
    if(lpScan != NULL)
    {
        lpScan->anchor();
    }

    //TODO
    Tip *tipWidget = new Tip();
    tipWidget->finish(QString("已经结束，正在下载视频！"));
    tipWidget->showFullScreen();
    tipWidget->show();

//     if ( _anchorCallback )
//     {
//         _anchorCallback();
//     }
}


void BottomScan::on_endButton_clicked()
{
    ui->endButton->setStyleSheet("border: none; background-color: rgba(73, 83, 102, 0.06);");
    ui->screenshotButton->setStyleSheet("border: none; background-color:transparent;");
    ui->startButton->setStyleSheet("border: none; background-color:transparent;");
    ui->anchorButton->setStyleSheet("border: none; background-color:transparent;");

    ui->screenshot->setIcon(QPixmap(":/scanner/images/buttom/screenshot.png"));
    ui->start->setIcon(QPixmap(":/scanner/images/buttom/start.png"));
    ui->anchor->setIcon(QPixmap(":/scanner/images/buttom/anchor.png"));
    ui->end->setIcon(QPixmap(":/scanner/images/buttom/endactive.png"));

    //TODO
    //scan_controller_->stopRecord();

    Scan* lpScan = dynamic_cast<Scan*>(_scan);
    if(lpScan != NULL)
    {
        lpScan->stop();
    }

    // if ( _endCallback )
    // {
    //     _endCallback();
    // }
}

void BottomScan::slot_calibrate_timeout() {
  double calibrate_duration = std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::system_clock::now() - start_time_).count();
  double percentage = calibrate_duration  / calibrate_stationary_seconds_  * 100;

  if (tipWidget) {
    if (percentage <= 100.0) {
      tipWidget->progress( QString("正在IMU校准，请保持静止状态！"), percentage);
    }

    if (percentage >= 100.0) {
      tipWidget->finish("校准结束，可以开始移动扫描。");
      
    }
    if (percentage >= 130.0) {
        calibrate_timer_.stop();
      
        tipWidget->close();
        delete tipWidget;
        tipWidget = nullptr;
        // if ( _startCallback )
        // {
        //     _startCallback();
        // }
        //scan_controller_->startRecord();
        Scan* lpScan = dynamic_cast<Scan*>(_scan);
        if(lpScan != NULL)
        {
            lpScan->start();
        }
    }
  }
}


