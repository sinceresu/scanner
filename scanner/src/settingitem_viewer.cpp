#include "settingitem_viewer.h"
#include "ui_settingitem_viewer.h"

#include "common.h"

SettingItem::SettingItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingItem)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_TranslucentBackground);
    loadStyleSheet(this, "settingItem");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);
}

SettingItem::~SettingItem()
{
    delete ui;
}

void SettingItem::setNameAndIcon(QString name, QString iconName)
{
    ui->label->setText(name);
    ui->label->setAlignment(Qt::AlignHCenter);
    ui->toolButton->setIcon(QIcon(":/scanner/images/settings/" + iconName + ".png"));
}

QPushButton *SettingItem::getPushButton()
{
    return ui->button;
}


