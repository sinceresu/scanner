#include "settingdevcamer_viewer.h"
#include "ui_settingdevcamer_viewer.h"

#include "common.h"

SettingDevCamer::SettingDevCamer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingDevCamer)
{
    ui->setupUi(this);

    loadStyleSheet(this, "settingDevCamer");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    ui->captureValLabel->setPixmap(QPixmap(":/scanner/images/scan/on.png"));

    ui->typeLabel->setText("全景相机型号");
    ui->pixLabel->setText("传感器分辨率");
    ui->foucLabel->setText("焦点");
    ui->lensLabel->setText("透镜");
    ui->captureLabel->setText("视频采集设置");

    ui->typeValLabel->setText("01");
    ui->pixValLabel->setText("1600*1200");
    ui->foucValLabel->setText("已矫正");
    ui->lensValLabel->setText("鱼眼");

}

SettingDevCamer::~SettingDevCamer()
{
    delete ui;
}
