#ifndef SETTINGSYS_VIEWER_H
#define SETTINGSYS_VIEWER_H

#include <QWidget>

namespace Ui {
class SettingSys;
}

class SettingSys : public QWidget
{
    Q_OBJECT

public:
    explicit SettingSys(QWidget *parent = nullptr);
    ~SettingSys();

signals:
    void back(QObject *obj);

private slots:
    void on_blackToolButton_clicked();
    void backSlot();

private:
    Ui::SettingSys *ui;
};

#endif // SETTINGSYS_VIEWER_H
