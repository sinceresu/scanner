#ifndef SCAN_VIEWER_H
#define SCAN_VIEWER_H

#include <memory>
#include <osg/Vec3>
#include <QListWidget>
#include <QWidget>
#include <QTimer>
#include "qnode.hpp"
#include "PieChart.h"
#include <QProcess>
#include "tip.h"

namespace scanner{
    class ScanController;
}

namespace BlockShower {
    class BlockShower;
}

namespace Ui {
class Scan;
}

class Scan : public QWidget
{
    Q_OBJECT

public:
    explicit Scan(QWidget *parent = nullptr,  std::string project_path = "");
    ~Scan();
    
    bool isReady();
    void start();
    void stop();
    void screenShot();
    void anchor();

signals:
    void signalBack(QObject *obj);

private slots:
    /* void on_scanISOEdit_clicked(); */
    /* void on_scanISOSpeedEdit_clicked(); */
    /* void on_scanIMUEdit_clicked(); */
    /* void on_scanSettingPreviewOp_clicked(); */
    /* void on_scanSettingAutoOp_clicked(); */
    /* void on_scanIMUOp_clicked(); */

    /* void saveScanISOVal(QString val); */
    /* void saveScanISOSpeedVal(QString val); */
    /* void saveScanIMUVal(QString val); */

    void back();
    // void on_calibrate_timeout();

    void slot_addScanArea_clicked();


    void slotScanLoad();
    void slotScanDelete();

    void slotScanTimeout();
    void slot_comboBox_clicked(const QString &qstr);

private:
    // 告诉界面刷新三维窗口, 此版本实现的是被动刷新
    void emitUpdate();
    void arrowFeedBack(float sx, float sy, float sz, float ex, float ey, float ez);

    // ture: 室内 spacetype=0  false: 室外 spacetype=1
    void parseYaml(bool indoor);
    void optionEnable(bool able);
    void flushScanList();
    void addScanArea(const std::string& scanid);

Q_SIGNALS:
    void signalUpdate();

private:
    bool scanSettingPreview;
    bool scanSettingAuto;
    bool scanIMU;

private:
    QListWidget *m_List1;
    QListWidget *m_List2;
    QListWidget *m_List3;
    Ui::Scan *ui;
    std::unique_ptr<BlockShower::BlockShower> _blockShower;

    std::string project_path_;

    QTimer _scanTimer;

    int _workTimeInSecond;
    int _scanAreaTimeInSecond;

    bool _drawing;
    osg::Vec3 _start;
    osg::Vec3 _end;
    bool _working;
    PieChart *_pieChart;

    QNode* _rosNode;

    QProcess _bagRecord;
    QProcess _scanProcess;

    // Tip *tipWidget;
    // QTimer  calibrate_timer_;
    // int calibrate_stationary_seconds_;
    // std::chrono::system_clock::time_point start_time_;
    
    //add by 20221008
    QListWidget *m_ListColorMode;
    QListWidget *m_ListPointMode;
    QListWidget *m_ListScanTracking;
    QListWidget *m_ListTrajectories;
};

#endif // SCAN_VIEWER_H
