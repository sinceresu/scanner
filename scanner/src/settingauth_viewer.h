#ifndef SETTINGAUTH_VIEWER_H
#define SETTINGAUTH_VIEWER_H

#include <QWidget>

namespace Ui {
class SettingAuth;
}

class SettingAuth : public QWidget
{
    Q_OBJECT

public:
    explicit SettingAuth(QWidget *parent = nullptr);
    ~SettingAuth();

signals:
    void authSuccess();

private slots:
    void on_closeButton_clicked();

    void on_submit_clicked();

private:
    Ui::SettingAuth *ui;
};

#endif // SETTINGAUTH_VIEWER_H
