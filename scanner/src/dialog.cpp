#include "dialog.h"
#include "ui_dialog.h"


#include "common.h"

#include <QString>


Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_TranslucentBackground);
    loadStyleSheet(this, "dialog");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    ui->lineEdit->setPlaceholderText("请输入信息");
    ui->save->setText(QString("保存"));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::setLabelText(QString labelText)
{
    ui->label->setText(labelText);
}

QString Dialog::getVal()
{
    return ui->lineEdit->text();
}

void Dialog::on_closeButton_clicked()
{
    //this->close();
    QDialog::done( QDialog::Rejected );
}

void Dialog::on_save_clicked()
{
    //emit saveVal(ui->lineEdit->text());
    //this->close();
    QDialog::done( QDialog::Accepted );
}

