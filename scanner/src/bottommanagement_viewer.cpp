#include "bottommanagement_viewer.h"
#include "confirmwidget_viewer.h"
#include "dialog.h"
#include "managementdetail_viewer.h"
#include "managementinfo_viewer.h"
#include "tip.h"
#include "ui_bottommanagement_viewer.h"
#include "mainwindow.h"
#include "Project.h"
#include "ScannerCfgManager.h"
#include "common.h"
#include "ydUtil/log.h"
#include <QDebug>

BottomManagement::BottomManagement(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BottomManagement)
{
    ui->setupUi(this);
    loadStyleSheet(this, "bottomManagement");
}

BottomManagement::~BottomManagement()
{
    delete ui;
}

void BottomManagement::on_previewButton_clicked()
{
    this->setDisabled(true);
    managementPre = new ManagementPre();
    managementPre->showFullScreen();
    managementPre->show();
    connect(managementPre, &ManagementPre::back, this, &BottomManagement::back);
}


void BottomManagement::on_renameButton_clicked()
{
    Dialog *w = new Dialog();
    w->setLabelText(QString("修改工程名称"));
    w->showFullScreen();
    w->exec();

    QString v = w->getVal();
    if ( v.isEmpty() )
    {
        return;
    }
    emit MainWindow::GetMainWindow()->signalModifyProject( _selectUUID, QString("name"), v );
}


void BottomManagement::on_delButton_clicked()
{
    MainWindow *lpMainWindow = MainWindow::GetMainWindow();
    ScannerCfgManager *lpManager = lpMainWindow->getCfgManager();

    Project *lpProject = lpManager->getProject( std::string( _selectUUID.toLocal8Bit() ) );
    if ( !lpProject )
    {
        return;
    }
    ConfirmWidget delConfirm ( [this](ConfirmHitType cht ){
                                                            if ( cht == ConfirmHitType::Cancel )
                                                            {
                                                                return;
                                                            }
                                                            emit MainWindow::GetMainWindow()->signalRemoveProject( this->_selectUUID );
                                                            emit MainWindow::GetMainWindow()->signalChangeBottomWidget( 0 );
                                                        } );

    QString info = QString::fromLocal8Bit("确定删除扫描文件") + QString::fromLocal8Bit( lpProject->getName().c_str() ) + "?";
    delConfirm.setIconConfirmData(ConfirmType::Delete, info, "确认删除");
    delConfirm.showFullScreen();
    delConfirm.exec();
}


void BottomManagement::on_detailButton_clicked()
{
    detail = new ManagementDetail( _selectUUID );
    detail->showFullScreen();
    detail->show();
    connect(detail, &ManagementDetail::back, this, &BottomManagement::back);
}


void BottomManagement::on_infoButton_clicked()
{
    managementInfo = new ManagementInfo( _selectUUID );
    managementInfo->showFullScreen();
    managementInfo->show();
}


void BottomManagement::on_exportButton_clicked()
{
    //TODO
    ConfirmWidget *shutdownConfirm = new ConfirmWidget( [this](ConfirmHitType cht ){
                                                            if ( cht == ConfirmHitType::Cancel )
                                                            {
                                                                return;
                                                            }
                                                            emit MainWindow::GetMainWindow()->signalExportProject( this->_selectUUID );
                                                        }  );
    shutdownConfirm->setIconConfirmData(ConfirmType::Export, "确定导出扫描文件34-98 ？", "确认导出");
    shutdownConfirm->showFullScreen();
    shutdownConfirm->exec();

}


void BottomManagement::back(QObject *obj)
{
    Q_UNUSED(obj);
    QObject *btn = sender();

    if(btn == detail) {
        detail->close();
    } else if(btn == managementPre) {
         managementPre->close();
    }
    this->setDisabled(false);

}

