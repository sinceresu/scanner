#include "confirmwidget_viewer.h"
#include "ui_confirmwidget_viewer.h"
#include "mainwindow.h"
#include "Project.h"
#include "ScannerCfgManager.h"

#include "common.h"

ConfirmWidget::ConfirmWidget(ConfirmCallback fun, QWidget *parent) :
    QDialog(parent)
    , ui(new Ui::ConfirmWidget)
    , _callback( fun )
{
    ui->setupUi(this);

    setAttribute(Qt::WA_TranslucentBackground);
    loadStyleSheet(this, "comfirmwidget");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    ui->titleLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->contentLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->secendContentLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

}

ConfirmWidget::~ConfirmWidget()
{
    delete ui;
}

void ConfirmWidget::setConfirmData(QString title, QString content, QString confirm, bool tip, QString tipContent)
{
    ui->titleLabel->setText(title);
    ui->contentLabel->setText(content);
    ui->submit->setText(confirm);

    this->tip = tip;
    this->tipContent = tipContent;
}

void ConfirmWidget::setIconConfirmData(ConfirmType type, QString content, QString confirm, bool tip, QString tipContent)
{
    this->tip = tip;
    this->tipContent = tipContent;

    ui->titleLabel->hide();
    ui->contentLabel->hide();

    if(type == ConfirmType::Delete){
        ui->icon->setPixmap(QPixmap(":/scanner/images/delete.png"));
    }
    if(type == ConfirmType::Export){
        ui->icon->setPixmap(QPixmap(":/scanner/images/export.png"));
    }

    ui->iconLabel->setText(content);
    ui->submit->setText(confirm);
}

void ConfirmWidget::setProgressBarValue(int value)
{
    ui->progressBar->setValue(value);
}
void ConfirmWidget::setProgressBarState(bool show,int minvalue,int maxvalue)
{
    if(show)
    {
        ui->progressBar->show();
    }
    else
    {
        ui->progressBar->hide();
    }
    ui->progressBar->setRange(minvalue,maxvalue);
}

void ConfirmWidget::slotFinishCP()
{
    on_closeButton_clicked();
}
void ConfirmWidget::slotUploadProgress(int iprogress)
{
    ui->progressBar->setValue(iprogress);
}

void ConfirmWidget::on_closeButton_clicked()
{
    _callback( ConfirmHitType::Cancel );
    this->close();
}


void ConfirmWidget::on_submit_clicked()
{
    //TODO 编写业务代码
    if(tip){
        ui->secendContentLabel->setText(tipContent);
    }
    _callback( ConfirmHitType::OK );
    this->close();

}

