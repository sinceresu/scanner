#include "settingshutdown_viewer.h"
#include "ui_settingshutdown_viewer.h"

SettingShutdown::SettingShutdown(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingShutdown)
{
    ui->setupUi(this);
}

SettingShutdown::~SettingShutdown()
{
    delete ui;
}
