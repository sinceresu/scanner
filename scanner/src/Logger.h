#ifndef __SCANNER_LOGGER_H_FILE__
#define __SCANNER_LOGGER_H_FILE__

#include <ydUtil/singleton.h>
#include <vector>
#include <string>

class Logger : public Singleton<Logger>
{
protected:
    Logger();
    ~Logger();
    friend class Singleton<Logger>;

public:
    void log( char *operType, char *buff );
    void setSavePath( std::string const& f );

    std::vector<std::string> getLog( int count = 100 );

protected:
    std::string getCurFilePath();

protected:
    std::string _savePath;
};

#define TOLOG(operType, ...) { char buff[1024]; sprintf(buff, __VA_ARGS__); Logger::Instance()->log( operType, buff ); }

#endif // __SCANNER_LOGGER_H_FILE__
