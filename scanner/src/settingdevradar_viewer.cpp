#include "settingdevradar_viewer.h"
#include "ui_settingdevradar_viewer.h"

#include "common.h"

SettingDevRadar::SettingDevRadar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingDevRadar)
{
    ui->setupUi(this);

    loadStyleSheet(this, "settingDevRadar");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    ui->type1Label->setText("激光雷达型号");
    ui->len1Label->setText("波长");
    ui->rate1Label->setText("频率");
    ui->fov1Label->setText("视场角");
    ui->dis1Label->setText("测距");
    ui->speed1Label->setText("测点速度");
    ui->level1Label->setText("激光安全等级");

    ui->type2Label->setText("激光雷达型号");
    ui->len2Label->setText("波长");
    ui->rate2Label->setText("频率");
    ui->fov2Label->setText("视场角");
    ui->dis2Label->setText("测距");
    ui->speed2Label->setText("测点速度");
    ui->level2Label->setText("激光安全等级");

    ui->type1ValLabel->setText("001");
    ui->len1ValLabel->setText("905 nm");
    ui->rate1ValLabel->setText("最高15HZ");
    ui->fov1ValLabel->setText("水平 360度，垂直 35度");
    ui->dis1ValLabel->setText("150米");
    ui->speed1ValLabel->setText("43200点/秒");
    ui->level1ValLabel->setText("I级");

    ui->type2ValLabel->setText("002");
    ui->len2ValLabel->setText("905 nm");
    ui->rate2ValLabel->setText("最高15HZ");
    ui->fov2ValLabel->setText("水平 360度，垂直 35度");
    ui->dis2ValLabel->setText("150米");
    ui->speed2ValLabel->setText("43200点/秒");
    ui->level2ValLabel->setText("I级");
}

SettingDevRadar::~SettingDevRadar()
{
    delete ui;
}
