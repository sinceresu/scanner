#include "folderitem_viewer.h"
#include "ui_folderitem_viewer.h"

#include "common.h"

#include <QStyle>
#include <QStyleOption>
#include <QStylePainter>
#include <QPalette>

FolderItem::FolderItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FolderItem)
{
    ui->setupUi(this);
    loadStyleSheet(this, "folderItem");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    ui->selectLabel->setPixmap(QPixmap(":/scanner/images/management/folder.png"));
    ui->selectIcon->setPixmap(QPixmap(":/scanner/images/management/select.png"));
    ui->selectIcon->hide();

}

FolderItem::~FolderItem()
{
    delete ui;
}

void FolderItem::setFolderName(QString name)
{
    ui->discribe->setText(name);
}

void FolderItem::Init(QString text, QString iconPath)
{
    Q_UNUSED(text);
    QPixmap pixmapPic(iconPath);
    QPixmap pixmapPicFit = pixmapPic.scaled(45, 45, Qt::IgnoreAspectRatio);
}

void FolderItem::onSelect()
{
    ui->selectLabel->setPixmap(QPixmap(":/scanner/images/management/folderselect.png"));
}

void FolderItem::onRelease()
{
    ui->selectLabel->setPixmap(QPixmap(":/scanner/images/management/folder.png"));
    ui->selectIcon->hide();
}

void FolderItem::onDoubleSelect()
{
    ui->selectIcon->show();
}

void FolderItem::paintEvent(QPaintEvent *e)
{
    QStylePainter painter(this);
    QStyleOption opt;
    opt.initFrom(this);
    opt.rect = rect();
//    painter.drawPrimitive(QStyle::PE_Widget, opt);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
    QWidget::paintEvent(e);
}
