#include "settinglog_viewer.h"
#include "ui_settinglog_viewer.h"
#include "Logger.h"
#include "common.h"
#include "topscanner_viewer.h"
#include <ydUtil/StringUtils.h>

SettingLog::SettingLog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingLog)
{
    ui->setupUi(this);
    loadStyleSheet(this, "settingLog");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);

    TopScanner *top = new TopScanner(this);
    top->setTitle("日志记录");
    top->move(0, 0);

    connect(top, &TopScanner::back, this, &SettingLog::backSlot);

    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->verticalHeader()->setVisible(false);
    ui->tableWidget->setAlternatingRowColors(true);

    ui->tableWidget->horizontalHeader()->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Fixed);

    ui->tableWidget->verticalHeader()->setDefaultSectionSize(88);
    ui->tableWidget->horizontalHeader()->setDefaultAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    ui->tableWidget->setFrameShape(QFrame::VLine);
    ui->tableWidget->setShowGrid(false);
    ui->tableWidget->verticalHeader()->setStyleSheet(
            "QScrollBar{background:transparent; height:10px; }"
            "QScrollBar::handle{background:lightgray; border:8px solid transparent; border-radius:15px; }"
            "QScrollBar::handle:hover{background: rgba(227,230,233,1); }"
            "QScrollBar::handle:pressed{background:black;}"
            "QScrollBar::sub-line{background:transparent;}"
            "QScrollBar::add-line{background:transparent;}"
            );
    ui->tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->tableWidget->setVerticalScrollMode(QAbstractItemView::ScrollPerItem);
    ui->tableWidget->setAutoScroll(true);

    //1760 88
    ui->tableWidget->horizontalHeader()->setFixedHeight(118);


    ui->tableWidget->setColumnCount(3);
    QStringList header;
    header<<"操作类型"<<"操作内容"<<"操作时间";
    ui->tableWidget->setHorizontalHeaderLabels(header);

    ui->tableWidget->setColumnWidth(0, 300);
    ui->tableWidget->setColumnWidth(1, 850);
    ui->tableWidget->setColumnWidth(2, 610);

    int logCount = 100;
    std::vector<std::string> logInfo = Logger::Instance()->getLog( logCount );
    for ( auto l : logInfo )
    {
        std::vector<std::string> ll = ydUtil::split( l, "\t");
        if ( ll.size() != 3 )
        {
            continue;
        }
        int row = ui->tableWidget->rowCount();
        ui->tableWidget->insertRow( row );
        QTableWidgetItem *item1 = new QTableWidgetItem(QString::fromLocal8Bit( ll[0].c_str() ) );
        item1->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

        QTableWidgetItem *item2 = new QTableWidgetItem(QString::fromLocal8Bit( ll[1].c_str() ) );
        item2->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

        QTableWidgetItem *item3 = new QTableWidgetItem(QString::fromLocal8Bit( ll[2].c_str() ) );
        item3->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

        ui->tableWidget->setItem(row, 0, item1);
        ui->tableWidget->setItem(row, 1, item2);
        ui->tableWidget->setItem(row, 2, item3);
    }
}

SettingLog::~SettingLog()
{
    delete ui;
}


void SettingLog::backSlot()
{
    qDebug() << "SettingSys::backSlot";
    emit back(this);
}
