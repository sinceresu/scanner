#ifndef SETTINGITEM_VIEWER_H
#define SETTINGITEM_VIEWER_H

#include <QPushButton>
#include <QWidget>

namespace Ui {
class SettingItem;
}

class SettingItem : public QWidget
{
    Q_OBJECT

public:
    explicit SettingItem(QWidget *parent = nullptr);
    ~SettingItem();

public:
    void setNameAndIcon(QString name, QString iconName);
    QPushButton* getPushButton();

private:
    Ui::SettingItem *ui;
};

#endif // SETTINGITEM_VIEWER_H
