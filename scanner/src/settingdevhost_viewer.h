#ifndef SETTINGDEVHOST_VIEWER_H
#define SETTINGDEVHOST_VIEWER_H

#include <QWidget>

namespace Ui {
class SettingDevHost;
}

class SettingDevHost : public QWidget
{
    Q_OBJECT

public:
    explicit SettingDevHost(QWidget *parent = nullptr);
    ~SettingDevHost();

private:
    Ui::SettingDevHost *ui;
};

#endif // SETTINGDEVHOST_VIEWER_H
