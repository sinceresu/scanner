#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <memory>
#include "ScannerCfgManager.h"
#include "bottommanagement_viewer.h"
#include "bottomstd_viewer.h"
#include "management_viewer.h"
#include "scan_viewer.h"
#include "settings_viewer.h"
#include "topstd_viewer.h"
#include "toscanner_viewer.h"

extern int G_Space_Type;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
namespace scanner {
    class QNode;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(ScannerCfgManager *cfgManager, QWidget *parent = nullptr);
    ~MainWindow();

    ScannerCfgManager *getCfgManager(){ return _scannerCfgManager; };

    static MainWindow *GetMainWindow();
Q_SIGNALS:
    void signalNewProject( QString const uuid );
    void signalRemoveProject( QString const uuid );
    void signalExportProject( QString const uuid );
    void signalChangeBottomWidget( int index );
    void signalModifyProject( QString const selectUUID, QString const paramName, QString const v );

    void signalStartScan();
    void signalStopScan();
public slots:
    void startScan();
    void backScan(QObject *obj);

    void onButtonClicked();
    void captureSlot();
    void managementSlot();
    void settingsSlot();

    void managementClickSelectSlot( QString const selectUUID );
    void managementDoubleSelectSlot( QString const selectUUID );
    void slotChangeBottomWidget( int index );

private:
    TopSTD *top;
    ToScanner *toScanner;
    Scan *scan;
    Management *management;
    Settings *settings;

    BottomSTD *bottomSTD;
    BottomManagement *bottomManagement;


    static MainWindow *G_Self;
private:
    Ui::MainWindow *ui;

    ScannerCfgManager *_scannerCfgManager;

};
#endif // MAINWINDOW_H
