#include "topscanner_viewer.h"
#include "ui_topscanner_viewer.h"
//#include "controller/scan_controller.hpp"
#include "systeminfo.h"
#include "common.h"

TopScanner::TopScanner(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TopScanner)
{
    ui->setupUi(this);
    loadStyleSheet(this, "topscanner");

    ui->dateLabel->setText( QString::fromLocal8Bit( getFullTimeStr(time(NULL)).c_str() ) );
    ui->udiskLabel->setPixmap(QPixmap(":/scanner/images/top/udisk.svg"));
    ui->diskLabel->setPixmap(QPixmap(":/scanner/images/top/disk.svg"));
    ui->cameraLabel->setPixmap(QPixmap(":/scanner/images/top/camera.svg"));
    ui->radarLabel->setPixmap(QPixmap(":/scanner/images/top/radar.svg"));
    ui->batteryLabel->setPixmap(QPixmap(":/scanner/images/top/batterying.svg"));
    ui->titleLabel->hide();
    //ui->radarLabel->hide();
    _statusUpdate.setup( ui->dateLabel, ui->udiskLabel, ui->diskLabel, ui->cameraLabel, ui->radarLabel, ui->batteryLabel);

    connect( &_timer, SIGNAL( timeout() ), this, SLOT( slotTimeout() ) );
    _timer.start( 1000 );
}

TopScanner::~TopScanner()
{
    delete ui;
}

void TopScanner::on_backLabel_clicked()
{
    emit back();
}

void TopScanner::setTitle(QString title)
{
    ui->titleLabel->setText(title);
    ui->titleLabel->show();
}


void TopScanner::slotTimeout()
{
    _statusUpdate.updateWidget();
}
