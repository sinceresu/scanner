#ifndef MANAGEMENTINFO_VIEWER_H
#define MANAGEMENTINFO_VIEWER_H

#include <QWidget>

namespace Ui {
class ManagementInfo;
}

class ManagementInfo : public QWidget
{
    Q_OBJECT

public:
    explicit ManagementInfo( QString const& selectUUID, QWidget *parent = nullptr);
    ~ManagementInfo();

private slots:
    void on_closeButton_clicked();

private:
    Ui::ManagementInfo *ui;
};

#endif // MANAGEMENTINFO_VIEWER_H
