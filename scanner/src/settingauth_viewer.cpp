#include "settingauth_viewer.h"
#include "ui_settingauth_viewer.h"

#include "common.h"

SettingAuth::SettingAuth(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingAuth)
{
    ui->setupUi(this);
    loadStyleSheet(this, "settingAuth");

    Qt::WindowFlags flags;
    flags |= Qt::FramelessWindowHint;
    setWindowFlags(flags);
}

SettingAuth::~SettingAuth()
{
    delete ui;
}

void SettingAuth::on_closeButton_clicked()
{
    this->close();
}


void SettingAuth::on_submit_clicked()
{
    if(ui->password->text() == "123456"){
        emit authSuccess();
        this->close();
    }
    else {
        ui->display->setText("密码认证失败。");
    }
}

