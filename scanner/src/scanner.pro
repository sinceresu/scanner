QT      += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
CONFIG += resources_big

SOURCES += \
    PieChart.cpp \
    QRoundProgressBar.cpp \
    bottommanagement_viewer.cpp \
    bottommanagementpre_viewer.cpp \
    bottomscan_viewer.cpp \
    bottomstd_viewer.cpp \
    confirmwidget_viewer.cpp \
    dialog.cpp \
    folderitem_viewer.cpp \
    main.cpp \
    mainwindow.cpp \
    management_viewer.cpp \
    managementdetail_viewer.cpp \
    managementinfo_viewer.cpp \
    managementpre_viewer.cpp \
    qanimationcombobox.cpp \
    scan_viewer.cpp \
    settingauth_viewer.cpp \
    settingdevbattery_viewer.cpp \
    settingdevcamer_viewer.cpp \
    settingdevhost_viewer.cpp \
    settingdevinfo_viewer.cpp \
    settingdevradar_viewer.cpp \
    settingitem_viewer.cpp \
    settinglog_viewer.cpp \
    settingmodel_viewer.cpp \
    settings_viewer.cpp \
    settingshutdown_viewer.cpp \
    settingsoftversion_viewer.cpp \
    settingstorage_viewer.cpp \
    settingsys_viewer.cpp \
    splash_viewer.cpp \
    tip.cpp \
    topmanagemnetpre_viewer.cpp \
    topscanner_viewer.cpp \
    topstd_viewer.cpp \
    toscanner_viewer.cpp

HEADERS += \
    PieChart.h \
    QRoundProgressBar.h \
    bottommanagement_viewer.h \
    bottommanagementpre_viewer.h \
    bottomscan_viewer.h \
    bottomstd_viewer.h \
    common.h \
    confirmwidget_viewer.h \
    dialog.h \
    folderitem_viewer.h \
    mainwindow.h \
    management_viewer.h \
    managementdetail_viewer.h \
    managementinfo_viewer.h \
    managementpre_viewer.h \
    qanimationcombobox.h \
    scan_viewer.h \
    settingauth_viewer.h \
    settingdevbattery_viewer.h \
    settingdevcamer_viewer.h \
    settingdevhost_viewer.h \
    settingdevinfo_viewer.h \
    settingdevradar_viewer.h \
    settingitem_viewer.h \
    settinglog_viewer.h \
    settingmodel_viewer.h \
    settings_viewer.h \
    settingshutdown_viewer.h \
    settingsoftversion_viewer.h \
    settingstorage_viewer.h \
    settingsys_viewer.h \
    splash_viewer.h \
    tip.h \
    topmanagemnetpre_viewer.h \
    topscanner_viewer.h \
    topstd_viewer.h \
    toscanner_viewer.h

FORMS += \
    bottommanagement_viewer.ui \
    bottommanagementpre_viewer.ui \
    bottomscan_viewer.ui \
    bottomstd_viewer.ui \
    confirmwidget_viewer.ui \
    dialog.ui \
    folderitem_viewer.ui \
    mainwindow.ui \
    management_viewer.ui \
    managementdetail_viewer.ui \
    managementinfo_viewer.ui \
    managementpre_viewer.ui \
    scan_viewer.ui \
    settingauth_viewer.ui \
    settingdevbattery_viewer.ui \
    settingdevcamer_viewer.ui \
    settingdevhost_viewer.ui \
    settingdevinfo_viewer.ui \
    settingdevradar_viewer.ui \
    settingitem_viewer.ui \
    settinglog_viewer.ui \
    settingmodel_viewer.ui \
    settings_viewer.ui \
    settingshutdown_viewer.ui \
    settingsoftversion_viewer.ui \
    settingstorage_viewer.ui \
    settingsys_viewer.ui \
    splash_viewer.ui \
    tip.ui \
    topmanagemnetpre_viewer.ui \
    topscanner_viewer.ui \
    topstd_viewer.ui \
    toscanner_viewer.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target



DISTFILES +=

RESOURCES += \
    resources/resource.qrc
