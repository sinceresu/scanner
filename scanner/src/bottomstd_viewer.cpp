#include "bottomstd_viewer.h"
#include "ui_bottomstd_viewer.h"


#include "common.h"

BottomSTD::BottomSTD(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BottomSTD)
{
    ui->setupUi(this);
    loadStyleSheet(this, "bottomstd");
}

BottomSTD::~BottomSTD()
{
    delete ui;
}


void BottomSTD::on_captureButton_clicked()
{
    ui->capture->setIcon(QPixmap(":/scanner/images/buttom/captureactive.png"));
    ui->management->setIcon(QPixmap(":/scanner/images/buttom/management.png"));
    ui->settings->setIcon(QPixmap(":/scanner/images/buttom/settings.png"));

    emit sigCapture();
}


void BottomSTD::on_managementButton_clicked()
{
    ui->capture->setIcon(QPixmap(":/scanner/images/buttom/capture.png"));
    ui->management->setIcon(QPixmap(":/scanner/images/buttom/managementactive.png"));
    ui->settings->setIcon(QPixmap(":/scanner/images/buttom/settings.png"));

    emit sigManagement();
}


void BottomSTD::on_settingsButton_clicked()
{
    ui->capture->setIcon(QPixmap(":/scanner/images/buttom/capture.png"));
    ui->management->setIcon(QPixmap(":/scanner/images/buttom/management.png"));
    ui->settings->setIcon(QPixmap(":/scanner/images/buttom/settingsactive.png"));

    emit sigSettings();
}
