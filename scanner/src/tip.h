#ifndef TIP_H
#define TIP_H

#include "QRoundProgressBar.h"

#include <QLabel>
#include <QWidget>

namespace Ui {
class Tip;
}

class Tip : public QWidget
{
    Q_OBJECT

public:
    explicit Tip(QWidget *parent = nullptr);
    ~Tip();

signals:
    void valueChanged(int val);

private slots:
    void on_closeButton_clicked();
    void updateProgress(int val);

public:
    void setLabel(QString label);
    void progress(QString label, int val);
    void finish(QString label);

private:
    QLabel *progressLabel;
    QRoundProgressBar *progressBar;

private:
    Ui::Tip *ui;
};

#endif // TIP_H
