
#include "mainwindow.h"
#include "ScannerCfgManager.h"
#include "splash_viewer.h"
#include <ydUtil/console.h>
#include <QApplication>
#include <QPixmap>
#include <QSplashScreen>
#include <QThread>
#include "systeminfo.h"
#include <QFile>
#include <QWidget>
#include <QDebug>
#include <QString>
#include <QString>
#include <QFontDatabase>
#include <QtWidgets/QMessageBox>
#include "StatusUpdate.h"
#include "Logger.h"
#include "gflags/gflags.h"
#include "glog/logging.h"
#include <ydUtil/DirUtils.h>

//#include "qnode.hpp"


DEFINE_string(workspace_directory, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");

DEFINE_string(topics, "",
              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");
QMainWindow *gMainWindow;

void loadStyleSheet(QWidget* widget, const QString &sheetName)
{
    QFile file(":/scanner/qss/" + sheetName + ".css");
    file.open(QFile::ReadOnly);
    if (file.isOpen())
    {
        QString qsstyleSheet = QLatin1String(file.readAll());
        widget->setStyleSheet(qsstyleSheet);
    }
    file.close();
}

void loadStyle(QWidget *widget, const QString &styleSheet)
{
    if (!styleSheet.isEmpty()) {
        widget->setStyleSheet("");
        widget->setStyleSheet(styleSheet);
    }
}

const QString getStyleSheet(const QString &sheetName)
{
    QString qsstyleSheet;
    QFile file(":/scanner/qss/" + sheetName + ".css");
    file.open(QFile::ReadOnly);
    if (file.isOpen())
    {
        qsstyleSheet = QLatin1String(file.readAll());
    }
    file.close();
    return qsstyleSheet;
}

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    std::string binPath =  ydUtil::getBinPath();

    //TODO 需根据工程文件路径， 修改成从文件读取加载，不然导致编译资源文件过慢。
    std::string fontFile = binPath + "/resources/font/SF-Pro-Text-Medium.otf";
    //int ret = QFontDatabase::addApplicationFont(":/scanner/font/SF-Pro-Text-Medium.otf");
    int ret = QFontDatabase::addApplicationFont( QString::fromLocal8Bit(fontFile.c_str()) );
    if (ret != -1)
        qInfo() << "成功安装字体" << QFontDatabase::applicationFontFamilies(ret);
    // else
    //     qInfo() << "安装字体失败" << "SF-Pro.ttf";

    fontFile = binPath + "/resources/font/PingFang.ttc";
    ret = QFontDatabase::addApplicationFont(QString::fromLocal8Bit( fontFile.c_str() ) );
    //ret = QFontDatabase::addApplicationFont(":/scanner/font/PingFang.ttc");
    if (ret != -1)
        qInfo() << "成功安装字体" << QFontDatabase::applicationFontFamilies(ret);
    // else
    //     qInfo() << "安装字体失败" << "SF-Pro.ttf";


    StatusUpdate::setMediaPath("/media");

    ScannerCfgManager cfgManager;
    cfgManager.setBinPath( binPath );

    std::string logPath = cfgManager.getParam("logpath");
    if ( logPath == "" )
    {
        logPath = binPath + "/logger";
        ydUtil::makeSureDirExist( logPath );
        cfgManager.setParam("logpath", logPath);
    }
    Logger::Instance()->setSavePath( logPath );

    //
    std::string driverPath = cfgManager.getParam("driverPath");
    //start Laser scanning drive
    QProcess LaserStartProcess;
    QString cmdlaster = QString("roslaunch ") + QString(driverPath.c_str()) + QString("start_devices.launch");
    LaserStartProcess.start(cmdlaster);
    LaserStartProcess.waitForStarted();       //阻塞，等待bash启动完毕
    qInfo() << "启动激光驱动...";
    ydUtil::sleep(1000);
    
    //start Camera drive
    QProcess camStartProcess;
    camStartProcess.start("roslaunch usb_cam usb_cam-total.launch");
    camStartProcess.waitForStarted();
     qInfo() << "启动相机驱动...";
    ydUtil::sleep(10000);

    TOLOG( "系统", "开机成功" );

    MainWindow *mainWindow = new MainWindow( &cfgManager );
    gMainWindow = mainWindow;
    mainWindow->hide();

    Splash splash;
    splash.showFullScreen();
    splash.show();

    StatusUpdate::startUpdate();
    a.exec();
    
    delete mainWindow;

    camStartProcess.terminate();
    camStartProcess.waitForFinished();
    LaserStartProcess.terminate();
    LaserStartProcess.waitForFinished();

    return 0;
}
