#ifndef __STATUS_UPDATE_H_FILE__
#define __STATUS_UPDATE_H_FILE__
#include <QLabel>
#include <string>
#include <thread>

class StatusUpdate
{
public:
    enum DistStatus
    {
        DISK_NOMAL = 0X01,
        DISK_WRITING,
        DISK_SHORTAGE,
        DISK_UMOUNT
    };
    enum UDiskStatus
    {
        UDISK_NOMAL = 0X01,
//        UDISK_WRITING,
//        UDISK_SHORTAGE,
        UDISK_UMOUNT
    };
    enum CameraStatus
    {
        CAMERA_WORK = 0X01,
        CAMERA_ERROR
    };
    enum RadarStatus
    {
        RADAR_NORMAL = 0X01,
        RADAR_WORK,
        RADAR_ERROR
    };

    StatusUpdate();
    virtual ~StatusUpdate();

    void setup( QLabel *timeLabel, QLabel *udiskStatus, QLabel *diskStatus, QLabel *cameraStatus, QLabel *radarStatus, QLabel *batteryStatus );

    static void startUpdate();
    static void stopUpdate();
    static void update();
    static void setMediaPath( std::string const& p );
    static void setCameraStatus( unsigned int v );
    static void setDiskStatus( unsigned int v );
    static void setUDiskStatus( unsigned int v );
    static void setRadarStatus( unsigned int v );
    static void setBatteryStatus( float v );

    static std::string getMediaPath();
    static std::string getMediaRootPath();

    void updateWidget();

protected:
    static float s_batteryPercent;
    static std::string s_batteryStates;
    static bool   s_batteryStateChange;
    static unsigned int _camera;
    static unsigned int _radar;
    static unsigned int _udisk;
    static unsigned int _disk;
    static std::string _mediaPath;
    static std::string _mediaRootPath;

    QLabel *_timeLabel;
    QLabel *_udiskStatus;
    QLabel *_diskStatus;
    QLabel *_cameraStatus;
    QLabel *_radarStatus;
    QLabel *_batteryStatus;

    unsigned int _lcamera;
    unsigned int _lradar;
    unsigned int _ludisk;
    unsigned int _ldisk;


    static std::thread *_updateThread;
    static bool _work;

};

#endif // __STATUS_UPDATE_H_FILE__
