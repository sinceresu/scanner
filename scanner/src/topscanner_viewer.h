#ifndef TOPSCANNER_VIEWER_H
#define TOPSCANNER_VIEWER_H
#include <memory>
#include <QTimer>
#include <QWidget>

#include "StatusUpdate.h"

namespace scanner{
    class ScanController;
}

namespace Ui {
class TopScanner;
}

class TopScanner : public QWidget
{
    Q_OBJECT

public:
    explicit TopScanner(QWidget *parent = nullptr);
    ~TopScanner();

signals:
    void back();

private slots:
    void on_backLabel_clicked();
    void slotTimeout();
public:
    void setTitle(QString title);

private:
    Ui::TopScanner *ui;
    QTimer _timer;

    StatusUpdate _statusUpdate;
};

#endif // TOPSCANNER_VIEWER_H
