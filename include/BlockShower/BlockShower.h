#ifndef __BLOCK_SHOWER_BLOCK_SHOWER_H_FILE__
#define __BLOCK_SHOWER_BLOCK_SHOWER_H_FILE__
#include <BlockShower/export.h>
#include <ydBlockDB/BlockDB.h>
#include <ydRender/SceneRender.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <osg/ref_ptr>
#include <vector>
#include <functional>
#include <QWidget>
#include <mutex>
#include <condition_variable>
#include "BlockShower/ScanAreaElement.h"
#include <functional>
#include <osgGA/KeySwitchMatrixManipulator>

namespace BlockShower
{
    /*
    相机视图
        1.主视图
        2.左视图
        3.右视图
        4.前视图
        5.后视图
    */
    enum CameraView
    {
        CV_MAIN,
        CV_LEFT,
        CV_RIGHT,
        CV_FRONT,
        CV_BACK
    };
    /*
    实时显示扫描车的视锥，用以标识当前扫描车的位置和方向，有三种工作模式：
        1.扫描车视角模式：仿照机器人视角实现
        2.自由模式：可以随意拖动三维视角
        3.跟踪模型：以上帝视角展示视锥，视锥始终在三维中心
    */
    enum CameraTrackMod
    {
        CTM_FREE,   //自由模式
        CMT_SELF,   //扫描车视角
        CMT_TRACK   //跟随模式
    };
    /*
    支持三种显示模式：
        1.默认颜色显示
        2.指定颜色显示
        3.按高度显示
        4.按强度显示
    */
    enum ChildAreaColorMod
    {
        CM_ORIGIN=0, 
        CM_HEIGHT,
        CM_INTENSITY,
        CM_SPECIFY
    };
    /*
    鼠标点击模式：
        1.左键单击
        2.右键单击
        3.中键单击
        1.左键双击
        2.右键双击
        3.中键双击
    */
    enum MouseClickType
    {
        MCT_LEFT,
        MCT_RIGHT,
        MCT_MIDDLE,
        MCT_LEFT_DOUBLE,
        MCT_RIGHT_DOUBLE,
        MCT_MIDDLE_DOUBLE
    };

    class MouseEventHandler;
    class CameraTrackElement;
    
    class BLOCKSHOWER_API BlockShower// : public Singleton<BlockShower>
    {
    public:
        BlockShower();
        ~BlockShower();

        //friend class Singleton<BlockShower>;
        using UpdateCallback = std::function<void()>;
    public:
        // 初始化OSG显示场景, 这个窗口需要放到某个对话框中嵌入展示
        // 传进来的函数在添加一次点云后主动调用, 用于更新三维的显示
        QWidget *createOsgWidget( UpdateCallback fun );
        UpdateCallback getUpdateCallback(){ return _updateCallback; };

        // 用于创建一个新的工作区域，需提供工作区域的包围盒大小,区域名称和区域的唯一标识符字符串，
        // 如果提供的标识符为空，则在创建区域时自动生成一个，并由此函数返回，每个扫描任务只有一个区域
        std::string createArea(ydBlockDB::BlockIndex const& minIndex,
                               ydBlockDB::BlockIndex const& maxIndex,
                               std::string const& name,
                               std::string const& id = "");


        // 清空某个区域内的所有数据
        void clearArea( std::string const& areaUUID = "" );

        // 在运行过程中如果重新开始一个新的扫描时, 需要把三维里面的所有内容清空
        // 需要和ResetDB配合使用, 如果要切换数据的存储, 使用者也要在后面调用resetDB函数
        void reset();
        // 重置数据库为空
        void resetDB();

        // 从数据库中加载上一次创建的结果
        void loadArea();
        // 把当前的改变存储到数据库
        void save();

        // 做任何区域方面的改变前, 都要重新设置, 在同一个工程内，应该只有一个区域
        void setWorkArea( std::string const& areaUUID );
        void setWorkArea(ydBlockDB::BlockIndex const& minIndex,
                        ydBlockDB::BlockIndex const& maxIndex,
                        std::string const& name,
                        std::string const& areaUUID );
        // 开始一个新扫描时调用此函数，以一个IUUID为输入参数
        void setWorkScanArea( std::string const& uuid );

        // 配置参数中相机的Fovy值，分辨率(aspect)，有效范围
        void setCameraParam(float fovy, float wide, float high, float zn, float zf);

        // 配置相机的显示颜色
        void setCameraColor(int r, int g, int b, int a);

        // 实时更新当前相机的位姿：x，y，z，qx，qy，qz，qw
        void updateCameraPose(float x, float y, float z, float qx, float qy, float qz, float qw);

        // 设置相机的跟踪模式，输入参数为相机模式CameraTrackMod（枚举类型）
        void cameraTrakMode(CameraTrackMod mod);

        // 删除一个区域，输入参数为区域的标识符,用于维护操作
        void deleteArea(std::string const& id);


        // 显示指定的子区域，输入参数为子区域ID，是一个字符串
        void showScanArea(std::string const& id);

        // 隐藏指定的子区域，子区域的ID，是一个字符串
        void hideScanArea(std::string const& id);

        // 给出空间坐标，返回当前空间坐标所属的子区域
        std::string getScanArea(float x, float y, float z);

        // 设置指定区域的显示颜色，设定完成后此区域内所有的显示内容按指定方式显示颜色。
        void setScanAreaColorMode(ChildAreaColorMod mod, std::string const& id = "");

        // 当开始新的扫描时，调用此函数
        void startWorkScanArea(std::string const& id);

        // 当一次扫描工作完成后，调用此函数
        void stopWorkScanArea(std::string const& id = "");

        // 此区域包含的子区域全部显示
        void showWorkArea();


        // 创建扫描区域
        // 输入参数为当前坐标点, 方向
        std::string createScanArea( float x, float y, float z, float qx, float qy, float qz, float qw );

        // 此区域包含的子区域全部隐藏，当某个子区域被多个扫描区域包含时，只要有一个扫描区域显示，此子区域也显示
        void hideWorkArea();

        // 给出空间坐标，返回当前空间坐标所属的工作区域
        std::string getWorkArea(float x, float y, float z);

        // 设置一个鼠标消息回调，当用户点击鼠标时，BlockShower调用此回调，
        // 把当前点击的空间坐标，屏幕坐标，点击类型（单击、双击）在回调函数中传出。
        // x y z 为空间坐标。 w，h为三维窗口中鼠标相对于左下角的坐标
        using CallBack = std::function<void(float x, float y, float z, float w, float h, MouseClickType type)>;
        void setMouseEventCallback(CallBack cb);

        // 三维场景跳进到点击区域，按照子区域的大小来决定当前相机到中心点的距离
        void switchToPosition(float x, float y, float z);

        // 扫描车扫描到新的点云后，调用此函数把点云放到BlockDB
        void setCloudPoint(pcl::PointCloud<pcl::PointXYZI>* cloud);

        // 参数为true时，存储轨迹，否则不存储
        void setRecordTrack(bool save);

        // 参数为true时，显示轨迹，否则不显示
        void setShowTrack(bool show);

        // 设置轨迹线颜色[0, 255]
        void setTrackLineColor(int r, int g, int b, int a);

        // 设置BlockDB数据库路径, 可以是空目录, 里面内容自动创建,
        // 最后是把每个扫描工程的BlockDB目录放到此工程文件夹下
        void setDBPath(std::string const& path);

        // 设置相机的视图
        void setViewPosition(CameraView cv);

        // 箭头绘制控制，true开启 false关闭
        void setDrawArrow(bool arrow);

        // 绘制箭头返回箭头的起始位置和结束位置，需要设置该反馈的回调函数
        using ArrowCallBack = std::function<void(float sx, float sy, float sz, float ex, float ey, float ez)>;
        void setArrowCallBack(ArrowCallBack acb);

        // 获取所有扫描区域ScanAreaElement列表
        std::vector<std::string> getScanAreaList();
        void deleteScanArea(std::string const& scanid);

        // 显示/隐藏扫描区域ScanAreaElement
        void setSceneAreaMask(std::string const& uuid, bool show);

        void emitUpdateNodeState();
        void initScene();

        // 设置工作扫描区域的透明度
        void setClarity(float a);
        // 截图
        void screenshot(std::string path);
        void setShapeMode(const std::string& mod);
        void setRatio(float ratio);
        void loadScan( std::string const& scanid );
        void loadAllScan();

        //
        void updateStates();

        // ============================test code==============================
        osg::MatrixTransform* getViewCone();
    protected:
        ydBase::Element *findElement( std::string const& id );
        ydBase::Element *findElement( osg::Vec3 const& pos );
        void addElement( ydBase::Element *lpElement );
        void removeElement( ydBase::Element *lpElement );
        void initManipulator();


    public:
        ydRender::SceneRender *getSceneRender(){ return _sceneRender.get(); };


    protected:
        osg::ref_ptr<ydRender::SceneRender> _sceneRender;
        osg::ref_ptr<ydBase::Element> _workScanArea;
        osg::ref_ptr<ydBase::Element> _workArea;
        osg::ref_ptr<ydBase::Element> _planeElement;
        osg::BoundingBox _workScanAreaValidBoundBox;

        UpdateCallback _updateCallback;

        // CameraTrackMod _cameraTrackMode;
        // ChildAreaColorMod _childAreaColorMod;

        MouseEventHandler* _handler;
        osg::ref_ptr<osg::Program> _shaderProg;
        osg::ref_ptr<CameraTrackElement> _cameraElement;
        osg::ref_ptr<osg::Camera::DrawCallback> _cameraDrawCallback;
        osg::ref_ptr<osgGA::KeySwitchMatrixManipulator> _switchManipulator;

        osg::Matrixd _freeInvMatrix;
        osg::Matrixd _traceInvMatrix;

        bool _record;
        osg::Vec3 _pos;
        osg::Matrix _offset;

        std::atomic_int _threadCount;

        std::string _dbPath;

        //states
        float _clarity;
    };

}

#endif // __BLOCK_SHOWER_BLOCK_SHOWER_H_FILE__
