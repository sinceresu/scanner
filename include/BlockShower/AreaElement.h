#ifndef __BLOCKSHOWER_AREA_ELEMENT_H_FILE__
#define __BLOCKSHOWER_AREA_ELEMENT_H_FILE__
#include <BlockShower/export.h>
#include <ydBase/Element.h>
#include <osg/ref_ptr>

namespace BlockShower
{
    class BLOCKSHOWER_API  AreaElement : public ydBase::Element
    {
    public:

        META_Info(AreaElement);
        AreaElement();
        ~AreaElement();

        virtual void load(bool inThread);
        virtual void load(std::istream& in);
        virtual void save(std::ostream& out);
        virtual void initFromParam( ydUtil::Param *lpParam );

        void setPosition( float x, float y, float z );
        osg::Vec3 getPosition()const{ return osg::Vec3( _px, _py, _pz ); };

        void setRange( std::vector<osg::Vec3> const& pointLine );
        std::vector<osg::Vec3> getRange()const{ return _range; };

        void setVec2Range( osg::Vec2 const& s );
        osg::Vec2 getVec2Range()const;
        osg::Vec3 getBasePoint();

        void setFaceColor( osg::Vec4 const& c = osg::Vec4( 1.0, 1.0, 1.0, 1.0 ) );
        osg::Vec4 getFaceColor()const{ return _faceColor; };

        void setLineColor( osg::Vec4 const& c = osg::Vec4( 0.772f, 0.3529f, 0.067f, 1.0f ) );
        osg::Vec4 getLineColor()const{ return _lineColor; };

        void setHeight( float v );
        float getHeight()const{ return _height; };

    protected:
        // compute
        osg::BoundingBox _rangeBB;

        // set
        float _px;
        float _py;
        float _pz;

        float _height;

        std::vector<osg::Vec3> _range;

        osg::Vec4 _lineColor;
        osg::Vec4 _faceColor;

        // std::map<ydUtil::BlockIndex, osg::ref_ptr<ChildAreaElement> > _childElement;
    };

}


#endif // __BLOCKSHOWER_AREA_ELEMENT_H_FILE__
