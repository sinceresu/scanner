#ifndef __BLOCKSHOWER_CAMERA_TRACK_ELEMENT_H_FILE__
#define __BLOCKSHOWER_CAMERA_TRACK_ELEMENT_H_FILE__
#include <BlockShower/export.h>
#include <ydBase/Element.h>
#include <osg/Switch>
#include <osg/ref_ptr>
#include <osg/observer_ptr>
#include <osg/Geode>
#include <osg/Geometry>

namespace BlockShower
{
    class BLOCKSHOWER_API  CameraTrackElement : public ydBase::Element
    {
    public:
        CameraTrackElement();
        virtual void load(bool inThread) {};
        virtual void load(std::istream& in) {};
        virtual void save(std::ostream& out) {};
        void updateCameraPose(const osg::Matrix& mat);
        void updateCameraPose(osg::Vec3 pos, osg::Quat q);
        void setCameraColor(int r, int g, int b, int a);
        void setCameraParam(float fovy, float wide, float high, float zn, float zf);

        // test codeing
        osg::MatrixTransform* getViewCone() { return _viewCone; };

        void setTrackLineColor(osg::Vec4 color);
        void setShowTrack(bool show);
        void clearTrack();

        osg::Node* getCameraNode() { return _camera; }
        osg::Vec3 getPos() { return _viewCone->getMatrix().getTrans(); }
        osg::Quat getRotate() { return _viewCone->getMatrix().getRotate(); }
        bool updated() { return _updated; }

    protected:
        void initCamera(bool hasFace = false);
        osg::Geode* createGeode( osg::Drawable* drawable, bool transparent=false );
        osg::Geometry* createGeometry(osg::Vec3Array* va,
                                      osg::Vec3Array* na,
                                      osg::Vec2Array* ta,
                                      std::vector<osg::PrimitiveSet*> p,
                                      std::vector<osg::Vec4>const& cv,
                                      bool autoNormals=true, bool useVBO=false );

        void frushCamera();
        void initTrack();
        void updateTrack(osg::Vec3 pos);

    protected:
        osg::ref_ptr<osg::Node> _camera;
        osg::observer_ptr<osg::Geometry> _coneGeom;
        osg::ref_ptr<osg::MatrixTransform> _viewCone;
        osg::Vec4 _cameraColor;

        float _zn;
        float _zf;
        float _fovy;
        float _width;
        float _height;
        float _aspect;
        
        osg::Vec4 _trackLineColor;
        osg::ref_ptr<osg::Geometry> _trackGeome;

        bool _updated;
    };

}


#endif // __BLOCKSHOWER_CAMERA_TRACK_ELEMENT_H_FILE__
