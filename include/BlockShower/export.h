#ifndef __BLOCK_SHOWER_EXPORT_H_FILE__
#define __BLOCK_SHOWER_EXPORT_H_FILE__

#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#  ifdef BLOCKSHOWER_LIBRARY
#    define BLOCKSHOWER_API __declspec(dllexport)
#  else
#    define BLOCKSHOWER_API __declspec(dllimport)
#  endif
#else
#  ifdef LBUELIGHT_LIBRARY
#    define BLOCKSHOWER_API  __attribute__ ((visibility("default")))
#  else
#    define BLOCKSHOWER_API
#  endif
#endif


#endif // _BLOCK_SHOWER_EXPORT_H_FILE__
