#ifndef __BLOCK_SHOWER_SCAN_AREA_ELEMENT_H_FILE__
#define __BLOCK_SHOWER_SCAN_AREA_ELEMENT_H_FILE__
#include "export.h"
#include <ydBase/Element.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <map>
#include <mutex>
#include <ydUtil/Block.h>

namespace BlockShower
{
    class ChildAreaElement;
    class BLOCKSHOWER_API ScanAreaElement : public ydBase::Element
    {
    public:
        ScanAreaElement( osg::Vec3 const& childSize );
        ~ScanAreaElement();

        virtual void load(bool inThread);
        virtual void load(std::istream& in) {};
        virtual void save(std::ostream& out) {};

        void setStartPose( float x, float y, float z, float qx, float qy, float qz, float qw );

        void setCloudPoint(pcl::PointCloud<pcl::PointXYZI>const* cloud);

        osg::Vec3 mapToWorld( osg::Vec3 const& pos );

        void checkChild( int width, int height, float scope, osg::Polytope & frustum, osg::Vec3 const& cameraPos );

        void setBox(osg::Vec3 min, osg::Vec3 max);
        osg::BoundingBox getBox(){ return _validBoundBox; };

        /* virtual osg::BoundingSphere computeBound() const; */

        /* void initFromBlockDB(); */
        bool isDirty() { return _childAreaElementDirty; };
        float getHeight()const{ return _height; };

        static std::string getFilePath( std::string const& cachePath, ydUtil::BlockIndex const& index, int level );

        void flush();
    protected:
        osg::Matrix _localToWorld;

        osg::BoundingBox _validBoundBox;

        bool _childAreaElementDirty;

        // std::mutex _childMapMutex;
        std::map<ydUtil::BlockIndex, osg::ref_ptr<ydBase::Element> > _childMap;

        osg::Vec3 _childSize;

        float _height;

        osg::ref_ptr<osg::Referenced> _tileFileManager;
    };
}


#endif // __BLOCK_SHOWER_SCAN_AREA_ELEMENT_H_FILE__
