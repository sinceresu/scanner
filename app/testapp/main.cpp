#include <stdlib.h>
#include <stdio.h>
#include <QDialog>
#include <QVBoxLayout>
#include <QApplication>
#include <BlockShower/BlockShower.h>
#include "MainWindow.h"

int main( int argc, char *argv[] )
{
    QApplication app(argc, argv);

    MainWindow mw;
    mw.showMaximized();

    app.exec();
    return 0;
}
