#ifndef __MAINWINDOW_H_FILE__
#define __MAINWINDOW_H_FILE__
#include <QMainWindow>
#include <QTimer>
#include <BlockShower/BlockShower.h>
#include <QProcess>

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow();
    ~MainWindow();

    void updateCamera(osg::Vec3 pos, osg::Quat q);

Q_SIGNALS:
    void signalUpdate();

protected slots:
    // 初始化时设置BlockDB的目录
    void slotSetDBPath();
    // 添加一个工作区域, 调用函数中需要输入此区域的大小
    void slotAddArea();
    // 添加一个扫描区域
    void slotAddScanArea();
    // 开始扫描工作, 使用已经创建的扫描区域ID为参数
    void slotStartWork();
    // 停止工作, 暂时还没有实质性的内容
    void slotStopWork();

    // 测试用的定时器
    void slotMakeCloudTimeout();

    // test coding
    void slotUpTrack();
    void slotDownTrack();
    void slotLeftTrack();
    void slotRightTrack();

    void slotRotate();

    void slotShowScanAreaElement();
    void slotHideScanAreaElement();

    void slotSwitchTrackMod();
    void slotSwitchViewkMod();
    void slotDrawArrrow(bool arrow);
    void slotRecord();
    void slotTerminate();
protected:
    // 告诉界面刷新三维窗口, 此版本实现的是被动刷新
    void emitUpdate();
protected:
    QTimer _workTimer;
    // 要有这样一个变量来调用相应的功能函数
    BlockShower::BlockShower _blockShower;


    std::string _areaID;
    std::string _scanAreaID;
    QProcess _bagRecord;
};

#endif // __MAINWINDOW_H_FILE__
