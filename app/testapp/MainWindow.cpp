#include "MainWindow.h"
#include <osg/Quat>
#include <osg/Vec3>
#include <ydUtil/log.h>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <ydUtil/matrix_math.h>
#include <QDebug>

MainWindow::MainWindow()
    : QMainWindow( NULL )
{
    srand(time(NULL));

    QWidget *lpMainWidget = new QWidget;
    setCentralWidget( lpMainWidget );

    QVBoxLayout *lpMainLayout = new QVBoxLayout;
    lpMainLayout->setMargin(0);
    lpMainWidget->setLayout( lpMainLayout );

    // 创建osg窗口时, 需要一个更新回调函数
    QWidget *lpWidget = _blockShower.createOsgWidget([this](){
                                                         this->emitUpdate();
                                                         //emit this->signalUpdate();
                                                     });

    // 更新信号绑定到刷新函数
    connect( this, SIGNAL( signalUpdate() ), lpWidget, SLOT( update() ) );
    // osg窗口添加到主窗口
    lpMainLayout->addWidget( lpWidget );

    QHBoxLayout *lpButtonLayout = new QHBoxLayout;
    lpMainLayout->addLayout( lpButtonLayout );

    {
        QPushButton *lpButton = new QPushButton("SetDBPath");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotSetDBPath() ) );
    }
    {
        QPushButton *lpButton = new QPushButton("AddArea");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotAddArea() ) );
    }
    {
        QPushButton *lpButton = new QPushButton("AddScanArea");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotAddScanArea() ) );
    }

    {
        QPushButton *lpButton = new QPushButton("StartWork");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotStartWork() ) );
    }

    {
        QPushButton *lpButton = new QPushButton("StopWork");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotStopWork() ) );
    }

    {
        QPushButton *lpButton = new QPushButton("^");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotUpTrack() ) );
    }

    {
        QPushButton *lpButton = new QPushButton("V");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotDownTrack() ) );
    }

    {
        QPushButton *lpButton = new QPushButton("<");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotLeftTrack() ) );
    }

    {
        QPushButton *lpButton = new QPushButton(">");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotRightTrack() ) );
    }

    {
        QPushButton *lpButton = new QPushButton("()");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotRotate() ) );
    }

    {
        QPushButton *lpButton = new QPushButton("TrackMod");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotSwitchTrackMod() ) );
    }

    {
        QPushButton *lpButton = new QPushButton("ViewMod");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotSwitchViewkMod() ) );
    }

    {
        QPushButton *lpButton = new QPushButton("Arrow");
        lpButtonLayout->addWidget( lpButton );
        lpButton->setCheckable(true);
        connect( lpButton, SIGNAL( clicked(bool) ), this, SLOT( slotDrawArrrow(bool) ) );
    }

    {
        QPushButton *lpButton = new QPushButton("record");
        lpButtonLayout->addWidget( lpButton );
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotRecord() ) );
    }

    {
        QPushButton *lpButton = new QPushButton("terminate");
        lpButtonLayout->addWidget( lpButton );
        lpButton->setCheckable(true);
        connect( lpButton, SIGNAL( clicked() ), this, SLOT( slotTerminate() ) );
    }

    connect( &_workTimer, SIGNAL( timeout() ), this, SLOT( slotMakeCloudTimeout() ) );
}
MainWindow::~MainWindow()
{

}

void MainWindow::emitUpdate()
{
    emit signalUpdate();
}

void arrowFeedback(float sx, float sy, float sz, float ex, float ey, float ez)
{
    std::cout << "start: " << sx << " " << sy << " " << sz << std::endl;
    std::cout << "  end: " << ex << " " << ey << " " << ez << std::endl;
}
void MainWindow::slotRecord()
{
    QString rosbag_record = "rosbag record /imu/data /ns1/velodyne_points /ns2/velodyne_points /usb_cam/image_raw/compressed /usb_cam_node1/image_raw_new/compressed /usb_cam_node2/image_raw_new_2/compressed /usb_cam_node3/image_raw_new_3/compressed /usb_cam_node4/image_raw_new_4/compressed -O ";
    QString bag_path = QString::fromLocal8Bit( "/home/fosky/test.bag" );
    rosbag_record.append(bag_path);
    qDebug() << rosbag_record << "\n";
    _bagRecord.start(rosbag_record);
    return;
}

void MainWindow::slotTerminate()
{
    _bagRecord.terminate();
    return;
}

void MainWindow::slotSetDBPath()
{
    // 选择BlockDB存储目录
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Project Directory"),
                                                    QString::fromLocal8Bit(getenv("HOME")),
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    if (dir.isEmpty())
    {
        return;
    }

    std::string sdir = std::string(dir.toLocal8Bit());

    _blockShower.setDBPath(sdir);
    //_blockShower.setDBPath("/home/fosky/data/blocktest2/");
    _blockShower.initScene();
    _blockShower.setArrowCallBack(arrowFeedback);
}

void MainWindow::slotAddArea()
{
    ydBlockDB::BlockIndex minIndex = ydBlockDB::get( -500, -500, 0 );
    ydBlockDB::BlockIndex maxIndex = ydBlockDB::get( 500, 500, 10 );
    _areaID = _blockShower.createArea(minIndex, maxIndex, "area1", "");
    TRACE("areaID : %s\n", _areaID.c_str() );
}
void MainWindow::slotAddScanArea()
{
    osg::Vec3 pos;
    osg::Quat q;
    _scanAreaID = _blockShower.createScanArea( pos.x(), pos.y(), pos.z(), q.x(), q.y(), q.z(), q.w() );
    TRACE("scanAreaID : %s\n", _areaID.c_str() );
}
int  G_TimeCount = 0;
float G_StepX = 0.0f;
void MainWindow::slotStartWork()
{
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr>* laserFrames = new std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr>;
    for ( int i = 0; i < 500; i ++ )
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr ptr( new  pcl::PointCloud<pcl::PointXYZ> );
        ptr->resize( 50000 );
        laserFrames->push_back(ptr);
    }

    laserFrames->clear();
    delete laserFrames;
    // G_TimeCount = 0;
    // _blockShower.startWorkScanArea( _scanAreaID );
    // _workTimer.start( 100 );
}
void MainWindow::slotStopWork()
{
    _workTimer.stop();
}
void MainWindow::slotMakeCloudTimeout()
{
    // 这里实现的随机生成点云, 生成一次后把定时器停止
    //for(int i=0.....)
        //laserFrames[i]->~PointCloud();   然后laserFrames->clear(); <pcl::PointCloud<PointType>::ptr> laserswapcloud;
        //laserswapcloud.swap(laserFrames);
    int s = 5000;
    pcl::PointCloud<pcl::PointXYZI> cloud;
    cloud.resize( s );
    for ( int i = 0; i < s; i ++ )
    {
        float x = ( rand() % 200 ) / 10.0f - 5 + G_StepX;
        float y = ( rand() % 200 ) / 20.0f - 5;
        float z = ( rand() % 40 ) / 10.0f;
        float k = ( rand () % 100 ) / 100.0f;

        cloud[i].x = x;
        cloud[i].y = y;
        cloud[i].z = z;
        cloud[i].intensity = k;
    }

    G_StepX += 10.0f;

    _blockShower.setCloudPoint( &cloud );

    //if ( G_TimeCount >= 1 )
    {
        _workTimer.stop();
    }

    G_TimeCount ++;

}


void MainWindow::slotUpTrack()
{
    // 测试代码, 这里的相机参数是把原来的位置做个调整, 实际使用时请用真实的相机位姿代替
    osg::Matrix mat = _blockShower.getViewCone()->getMatrix();
    osg::Vec3 trans = mat.getTrans();
    trans -= osg::Vec3(0.0, 1.0, 0.0);
    osg::Quat q = mat.getRotate();
    
    // 测试
    updateCamera(trans, q);
}
void MainWindow::slotDownTrack()
{
    // 测试代码, 这里的相机参数是把原来的位置做个调整, 实际使用时请用真实的相机位姿代替
    osg::Matrix mat = _blockShower.getViewCone()->getMatrix();
    osg::Vec3 trans = mat.getTrans();
    trans += osg::Vec3(0.0, 1.0, 0.0);
    osg::Quat q = mat.getRotate();
    
    updateCamera(trans, q);
}
void MainWindow::slotLeftTrack()
{
    // 测试代码, 这里的相机参数是把原来的位置做个调整, 实际使用时请用真实的相机位姿代替
    osg::Matrix mat = _blockShower.getViewCone()->getMatrix();
    osg::Vec3 trans = mat.getTrans();
    trans += osg::Vec3(1.0, 0.0, 0.0);
    osg::Quat q = mat.getRotate();
    
    updateCamera(trans, q);
}
void MainWindow::slotRightTrack()
{
    // 测试代码, 这里的相机参数是把原来的位置做个调整, 实际使用时请用真实的相机位姿代替
    osg::Matrix mat = _blockShower.getViewCone()->getMatrix();
    osg::Vec3 trans = mat.getTrans();
    trans -= osg::Vec3(1.0, 0.0, 0.0);
    osg::Quat q = mat.getRotate();
    
    updateCamera(trans, q);
}

void MainWindow::slotRotate()
{
    // 测试代码, 这里的相机参数是把原来的位置做个调整, 实际使用时请用真实的相机位姿代替
    srand(time(NULL));
    osg::Matrixd mat = _blockShower.getViewCone()->getMatrix();
    osg::Vec3 trans = mat.getTrans();

    osg::Vec3d at, up;
    float yaw, pitch, roll;
    ydUtil::ComputePoseVecFromTransformd(mat, at, up);
    ydUtil::ConvertPoseVecToAngle(at, up, yaw, pitch, roll);

    int r = rand() % 3;
    if(r == 0)
    {
        yaw += 5;
    }
    if(r == 1)
    {
        pitch += 5;
    }
    if(r == 2)
    {
        roll += 5;
    }

    osg::Quat q(osg::inDegrees(roll), osg::Y_AXIS,
                osg::inDegrees(pitch), osg::X_AXIS,
                osg::inDegrees(-yaw), osg::Z_AXIS);

    updateCamera(trans, q);


}
void MainWindow::slotShowScanAreaElement()
{
    // 在这里应该是首先获取所有的ScanArea, 获取后由用户选择要显示哪个区域
    // 把区域ID传给BlockShower, 最后显示出来.
    _blockShower.showScanArea("testid");
}
void MainWindow::slotHideScanAreaElement()
{
    // 在这里应该是首先获取所有的ScanArea, 获取后由用户选择要显示哪个区域
    // 把区域ID传给BlockShower, 最后隐藏出来.
    _blockShower.hideScanArea("testid");
}

void MainWindow::updateCamera(osg::Vec3 pos, osg::Quat q)
{
    // 测试代码, 使用使用时请用真实相机位姿代替
    _blockShower.updateCameraPose(pos.x(), pos.y(), 1.5, q.x(), q.y(), q.z(), q.w());
}

void MainWindow::slotSwitchTrackMod()
{
    // 用于测试切换跟踪模式，自由视角、跟踪视角和第一人称视角
    static unsigned mod = 0;
    _blockShower.cameraTrakMode((BlockShower::CameraTrackMod)(mod%3));
    mod++;
}
void MainWindow::slotSwitchViewkMod()
{
    // 用于测试视图切换，包括主视图、左右实体图和前后视图
    static unsigned mod = 0;
    _blockShower.setViewPosition((BlockShower::CameraView)(mod%5));
    mod++;
}
void MainWindow::slotDrawArrrow(bool arrow)
{
    // 开启、关闭箭头绘制功能
    _blockShower.setDrawArrow(arrow);
}
