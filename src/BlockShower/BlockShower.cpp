#include <BlockShower/BlockShower.h>
#include <ydUI/OSGWidget.h>
#include <ydBase/Layer.h>
#include <ydBase/PlaneElement.h>
#include <ydBase/Scene.h>
#include <ydBase/SceneManager.h>
#include <ydUI/osgqt.h>
#include <ydUI/SceneDockWidget.h>
#include <ydRender/EnvParam.h>
#include "MouseEventHandler.h"
#include <ydBlockDB/BlockDB.h>
#include <BlockShower/AreaElement.h>
#include "ChildAreaElement.h"
#include <ydUtil/log.h>
#include <ydUtil/DirUtils.h>
#include <ydUtil/console.h>
#include <osgViewer/ViewerEventHandlers>
#include "ThreadPool.h"
#include "UpdateChildElementHandler.h"
#include <BlockShower/CameraTrackElement.h>
#include <ydUtil/StringUtils.h>
#include "CustomModelManipulator.h"
#include "CameraTrackerManipulator.h"
#include <osgGA/TrackballManipulator>
#include <osgDB/ReadFile>
#include <osg/io_utils>
#include <ydUtil/ImageChange.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/impl/io.hpp>
#include <osg/BlendColor>
#include <osg/BlendFunc>
#include <osg/Material>
#include <ydUtil/matrix_math.h>
#include "ChildElementCache.h"
#include "Config.h"
//#include <automic>

class WindowCaptureCallback : public osg::Camera::DrawCallback
{
public:
    WindowCaptureCallback()
    {
        _image = new osg::Image;
        _isOutOfRtt = 0;
        _screenshot = "";
		//_timer = new osg::Timer;
		//_timer->setStartTick();
    }

    virtual void operator () (osg::RenderInfo& renderInfo) const
    {
        if(_screenshot.empty())
            return;
        osg::GraphicsContext* gc = renderInfo.getState()->getGraphicsContext();
        if (gc->getTraits())
        {
            OpenThreads::Mutex& m = const_cast<OpenThreads::Mutex&>(_mutex);
            OpenThreads::ScopedLock<OpenThreads::Mutex> l(m);
            int width = gc->getTraits()->width;
            int height = gc->getTraits()->height;
            if(_isOutOfRtt)
            {
                if (_image.get())
                {
                    int s = _image->s();
                    int t = _image->t();
                    if (width != s || height != t)
                    {
                        _image->allocateImage(width, height, 1, GL_RGBA, GL_UNSIGNED_BYTE);
                    }
                }
            }
            else
            {
                _image->readPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE);
                cv::Mat colorMat = ydUtil::osgImageToMat2(_image.get());
                cv::imwrite( _screenshot.c_str(), colorMat );
                _screenshot = "";
            }
        }
    }

    osg::Image* getImage()
    {
        OpenThreads::ScopedLock<OpenThreads::Mutex> l(_mutex);
        if (_isOutOfRtt)
        {
            if (_image.get())
            {
                return _image->asImage();
            }
        }
        else 
        {
            osg::Image* lpImage = (osg::Image*)_image->clone(osg::CopyOp::DEEP_COPY_IMAGES);
            //TRACE("clone use : %f\n", tt.getUseTime() );
            return lpImage;
        }
        return NULL;
    }
    osg::Image* getBaseImage()
    {
        return _image->asImage();
    }

    void AllocateImage(int s, int t, int r, GLenum pixelFormat, GLenum type)
    {
        if (_image)
        {
            _image->allocateImage(s, t, r, pixelFormat, type);
        }
    }
    void SetOutType(bool bOutOfRtt)
    {
        _isOutOfRtt = bOutOfRtt;
    }

    void screenshot(std::string path) { _screenshot = path; }

protected:
    OpenThreads::Mutex _mutex;
    osg::ref_ptr<osg::Image>    _image;
    bool _isOutOfRtt; //0 = window , 1 = rtt
	//osg::Timer* _timer;
    mutable std::string _screenshot;
};

namespace BlockShower
{
    extern float G_Ratio;
 
    BlockShower::BlockShower()
        : _threadCount(0)
    {
        _shaderProg = NULL;
        _sceneRender = new ydRender::SceneRender;
        // osg::Camera* camera = _sceneRender->getViewer()->getCamera();
        // camera->setCullingMode(camera->getCullingMode() & ~osg::CullStack::SMALL_FEATURE_CULLING);

        std::string curPath = ydUtil::getBinPath();
        int cr = chdir(curPath.c_str());
        _clarity = 1.0;
        _workScanAreaValidBoundBox.expandBy( osg::Vec3( 1, 1, 1 ));
    }
    BlockShower::~BlockShower()
    {
        _sceneRender = NULL;
        _workScanArea = NULL;
        _workArea = NULL;
        _planeElement = NULL;

        _handler = NULL;
        _shaderProg = NULL;
        _cameraElement = NULL;
        _cameraDrawCallback = NULL;
        _switchManipulator = NULL;
    }
    void BlockShower::initScene( )
    {
        ydBase::Scene *lpScene = new ydBase::Scene( false );
        ydBase::Layer *lpLayer = new ydBase::Layer;
        lpScene->addLayer( lpLayer );

        ydBase::Element *planeElement = ydBase::Element::createElement("PlaneElement");

        //lpState->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
        planeElement->getOrCreateStateSet()->setRenderBinDetails(100, "RenderBin");
        //planeElement->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
        {
            ydBase::PlaneElement *lpPlaneElement = dynamic_cast<ydBase::PlaneElement *>( planeElement );
            lpPlaneElement->setPosition( 0.0, 0.0, Z_OFFSET );
        }

        planeElement->load(false);
        _planeElement = planeElement;

        _cameraElement = new CameraTrackElement;
        lpLayer->addElement(_planeElement);
        lpLayer->addElement(_cameraElement);

        MouseEventHandler* handler = new MouseEventHandler;
        _sceneRender->addHandler("MouseEventHandler", handler);
        handler->setPlaneElement(_planeElement);
        _handler = handler;

        ydBase::SceneManager::Instance()->addScene( lpScene );
        _sceneRender->setScene( lpScene );
        initManipulator();

    }

    void BlockShower::initManipulator()
    {
        _switchManipulator = new osgGA::KeySwitchMatrixManipulator;

        // CustomCameraManipulator* free = new CustomCameraManipulator;
        // _switchManipulator->addMatrixManipulator(CTM_FREE, "CustomCameraManipulator", free);
        osgGA::TrackballManipulator* free = new osgGA::TrackballManipulator;
        _switchManipulator->addMatrixManipulator(CTM_FREE, "TrackballManipulator", free);
        free->setAllowThrow(false);

        CustomModelManipulator* self = new CustomModelManipulator;
        _switchManipulator->addMatrixManipulator(CMT_SELF, "CustomModelManipulator", self);
        self->setTrackNode(_cameraElement);

        CameraTrackerManipulator * track = new CameraTrackerManipulator;
        _switchManipulator->addMatrixManipulator(CMT_TRACK, "CameraTrackerManipulator", track);
        track->setTrackNode(_cameraElement->getCameraNode());

        _sceneRender->getViewer()->setCameraManipulator(_switchManipulator);
        _switchManipulator->selectMatrixManipulator(CTM_FREE);
        _switchManipulator->setAutoComputeHomePosition(true);
    }

    QWidget *BlockShower::createOsgWidget( UpdateCallback fun )
    {
        _updateCallback = fun;
        // 目前只能使用单线程模式
        QWidget *lpWidget = ydUI::osgWidget( _sceneRender->getViewer(), false );

        _sceneRender->getViewer()->addEventHandler(new osgViewer::StatsHandler);
        _sceneRender->getViewer()->addEventHandler(new UpdateChildElementHandler(this));
        //initScene();
        //initCamera();
        //initTrack();

        return lpWidget;
    }


    std::string BlockShower::createArea( ydBlockDB::BlockIndex const& minIndex, ydBlockDB::BlockIndex const& maxIndex, std::string const& name, std::string const& id )
    {
        if ( !_planeElement )
        {
            TRACELL(LL_WARNING, "no init scene\n");
            return "";
        }
        osg::Vec3d minPos;
        osg::Vec3d maxPos;
        ydBlockDB::getPos( minIndex, &minPos[0], &minPos[1], &minPos[2] );
        ydBlockDB::getPos( maxIndex, &maxPos[0], &maxPos[1], &maxPos[2] );
        if ( ( maxPos - minPos ).length() <= 0.01 )
        {
            minPos.set(-100, -100, 0);
            maxPos.set(100, 100, 10);
        }

        ydBase::PlaneElement *lpPlaneElement = dynamic_cast<ydBase::PlaneElement *>( _planeElement.get() );
        if ( lpPlaneElement )
        {
            lpPlaneElement->setRange( (  maxPos.x() - minPos.x() ) * 1.2, ( maxPos.y() - minPos.y() ) * 1.2 );
            osg::Vec3 center = ( minPos + maxPos ) / 2;
            lpPlaneElement->setPosition( center.x(), center.y(), minPos.z() + Z_OFFSET );

            lpPlaneElement->setGridSize( ( maxPos.x() - minPos.x() ) / 20, ( maxPos.y() - minPos.y() ) / 20 );

        }

        AreaElement *lpElement = new AreaElement;
        osg::Vec3 s = maxPos - minPos;
        osg::Vec3 center = ( minPos + maxPos ) / 2;
        lpElement->setVec2Range( osg::Vec2( s.x(), s.y() ) );
        lpElement->setHeight( s.z() );
        lpElement->setPosition( center.x(), center.y(), minPos.z() );
        lpElement->load( false );

        lpElement->setName( name );
        lpElement->set("name", name);

        std::string areaID = ydBlockDB::createArea( name, minPos[0], minPos[1], minPos[2], maxPos[0], maxPos[1], maxPos[2], id );
        lpElement->set("id", areaID);
        addElement( lpElement );

        //lpElement->set("id", areaID );
        ydBlockDB::setAreaData(areaID, "areaheight", std::to_string( s.z() ) );
        ydBlockDB::setAreaData(areaID, "position", std::to_string( center.x() ) + " " + std::to_string( center.y() ) + " " + std::to_string( center.z() ) );

        // addElement( lpElement );

        _workArea = lpElement;
        return areaID;
    }

    void BlockShower::loadArea()
    {
        std::vector< std::string > rl = ydBlockDB::queryAreaList();
        for ( unsigned int i = 0; i < rl.size(); i ++ )
        {
            AreaElement *lpArea = new AreaElement;
            std::vector<ydBlockDB::BlockIndex> rangeList = ydBlockDB::getAreaRange( rl[i] );
            std::vector<osg::Vec3> al;
            osg::BoundingBox bb;
            for ( unsigned int r = 0; r < rangeList.size(); r ++ )
            {
                double x, y, z;
                ydBlockDB::getPos( rangeList[r], &x, &y, &z );
                al.push_back( osg::Vec3( x, y, z ) );
                bb.expandBy( osg::Vec3( x, y, z ) );
            }
            lpArea->set("id", rl[i] );
            lpArea->setName( rl[i] );
            lpArea->setRange( al );
            lpArea->load( false );

        }

    }

    void BlockShower::save()
    {
        ydBlockDB::flushDB();
    }

    // 清空某个区域内的所有数据
    void BlockShower::clearArea(std::string const& areaUUID)
    {
        while (_threadCount)
        {
            ydUtil::sleep(100);
        }
        ydBase::Scene *lpScene = _sceneRender->getScene();
        if ( !lpScene )
        {
            TRACELL(LL_ERROR, "no scene\n" );
            return;
        }
        ydBase::Layer *lpLayer = lpScene->getLayer( 0 );
        if ( !lpLayer )
        {
            TRACELL(LL_ERROR, "no layer\n" );
            return;
        }

        //_workArea = NULL;
        _workScanArea = NULL;
        _cameraElement->clearTrack();
        
        lpLayer->removeElement(0, lpLayer->elementCount());
        addElement( _cameraElement );
        addElement( _planeElement );
        addElement( _workArea );

        // 在这里添加清除，可以更快的释放内存占用
        ChildElementCache::Instance()->clear();
    }
    void BlockShower::reset()
    {
        while (_threadCount)
        {
            ydUtil::sleep(100);
        }
        
        ydBase::Layer *lpLayer;
        ydBase::Scene *lpScene;
        lpScene = _sceneRender->getScene();
        if(lpScene != NULL)
        {
            lpLayer = lpScene->getLayer( 0 );
            if(lpLayer != NULL)
                lpLayer->removeElement(0, lpLayer->elementCount());

        }
        //BlockDBUpdater::Destory();
        resetDB();
        // clearArea();
    }
    // 重置数据库为空
    void BlockShower::resetDB()
    {
        ydBlockDB::unloadDB();
    }
    void BlockShower::addElement( ydBase::Element *lpElement )
    {
        ydBase::Scene *lpScene = _sceneRender->getScene();
        if ( !lpScene )
        {
            TRACELL(LL_ERROR, "no scene\n" );
            return;
        }
        ydBase::Layer *lpLayer = lpScene->getLayer( 0 );
        if ( !lpLayer )
        {
            TRACELL(LL_ERROR, "no layer\n" );
            return;
        }

        lpLayer->addElement( lpElement );
    }
    void BlockShower::removeElement( ydBase::Element *lpElement )
    {
        ydBase::Scene *lpScene = _sceneRender->getScene();
        ydBase::Layer *lpLayer = lpScene->getLayer( 0 );
        lpLayer->removeElement( lpElement );
    }
    void BlockShower::setWorkScanArea( std::string const& scanAreaUUID )
    {
        if(_workScanArea != NULL)
        {
            _cameraElement->clearTrack();
        }

        _workScanArea = findElement( scanAreaUUID );
        startWorkScanArea(scanAreaUUID);


    }
    void BlockShower::setWorkArea( std::string const& areaUUID )
    {
        _workArea = findElement( areaUUID );
        //loadAllScan( _dbPath );
        // loadAllScan();
    }
    void BlockShower::setWorkArea( ydBlockDB::BlockIndex const& minIndex, ydBlockDB::BlockIndex const& maxIndex, std::string const& name, std::string const& areaUUID )
    {
        ydBase::Element *lpElement = findElement(areaUUID);
        if(lpElement == NULL)
        {
            osg::Vec3d minPos;
            osg::Vec3d maxPos;
            ydBlockDB::getPos( minIndex, &minPos[0], &minPos[1], &minPos[2] );
            ydBlockDB::getPos( maxIndex, &maxPos[0], &maxPos[1], &maxPos[2] );
            if ( ( maxPos - minPos ).length() <= 0.01 )
            {
                minPos.set(-100, -100, 0);
                maxPos.set(100, 100, 10);
            }

            ydBase::PlaneElement *lpPlaneElement = dynamic_cast<ydBase::PlaneElement *>( _planeElement.get() );
            if ( lpPlaneElement )
            {
                lpPlaneElement->setRange( (  maxPos.x() - minPos.x() ) * 1.2, ( maxPos.y() - minPos.y() ) * 1.2 );
                osg::Vec3 center = ( minPos + maxPos ) / 2;
                lpPlaneElement->setPosition( center.x(), center.y(), minPos.z() + Z_OFFSET );
            }

            AreaElement *element = new AreaElement;
            osg::Vec3 s = maxPos - minPos;
            osg::Vec3 center = ( minPos + maxPos ) / 2;
            element->setVec2Range( osg::Vec2( s.x(), s.y() ) );
            element->setHeight( s.z() );
            element->setPosition( center.x(), center.y(), minPos.z() );
            element->load( false );

            element->setName( name );
            element->set("name", name);

            element->set("id", areaUUID);
            lpElement = element;
            addElement( element );
        }
        // addElement( lpElement );

        _workArea = lpElement;
        // loadAllScan();
    }

    class PrintNodeTreeVisitor : public osg::NodeVisitor
    {
    public:
        PrintNodeTreeVisitor():
            osg::NodeVisitor (TRAVERSE_ALL_CHILDREN)
            {
                TRACE("==============================================\n");
            }
        ~PrintNodeTreeVisitor()
            {
                TRACE("----------------------------------------------\n");
            }

        void apply(osg::MatrixTransform &node)
            {
                ScanAreaElement *lpScanElement = dynamic_cast<ScanAreaElement *>(&node);
                if ( lpScanElement )
                {
                    TRACE("cache path : %s\n", lpScanElement->get("cachepath").c_str() );
                }


                // ChildAreaElement* child = dynamic_cast<ChildAreaElement*>(&node);
                // if(child)
                // {
                //     child->checkChild( _width, _height, _slope, _frustum, _cameraPos, _lpTileFileManager );
                // }


                traverse( node );
            }

    };
    void printElement( osg::Node *lpRoot )
    {
        PrintNodeTreeVisitor ptv;
        lpRoot->accept( ptv );

        // lpScene->for_each_layer([]( ydBase::Layer *lpLayer ){
        //                             if ( !lpLayer )
        //                             {
        //                                 return false;
        //                             }

        //                             lpLayer->for_each([](ydBase::Element *lpE ){
        //                                                   ScanAreaElement *lpScanElement = dynamic_cast<ScanAreaElement *>( lpE );
        //                                                   if ( lpScanElement )
        //                                                   {
        //                                                       TRACE("%s\n", lpScanElement->get("id").c_str());
        //                                                   }
        //                                                   return false;
        //                                               });

        //                             return false;

        //                         });
    }

    ydBase::Element* BlockShower::findElement( std::string const& id )
    {
        ydBase::Scene *lpScene = _sceneRender->getScene();

        osg::ref_ptr<ydBase::Element> findElement;
        lpScene->for_each_layer([id, &findElement]( ydBase::Layer *lpLayer ){
                                    if ( !lpLayer )
                                    {
                                        return false;
                                    }


                                    findElement = lpLayer->getElement( id );

                                    if ( findElement.valid() )
                                    {
                                        return true;
                                    }

                                    return false;

                                });


        return findElement.release();
    }

    ydBase::Element* BlockShower::findElement( osg::Vec3 const& pos )
    {
        ydBase::Scene *lpScene = _sceneRender->getScene();

        osg::ref_ptr<ydBase::Element> findElement;
        lpScene->for_each_layer([pos, &findElement]( ydBase::Layer *lpLayer )
            {
                if ( !lpLayer )
                {
                    return false;
                }

                lpLayer->for_each([pos, &findElement ](ydBase::Element* lpElement )
                    {
                        AreaElement* area = dynamic_cast<AreaElement*>(lpElement);
                        if (area && area->typeName() != "Layer" && area->getBound().contains(pos) )
                        {
                            findElement = area;
                            return true;
                        }
                        ScanAreaElement* scanArea = dynamic_cast<ScanAreaElement*>(lpElement);
                        if (scanArea && scanArea->typeName() != "Layer" && scanArea->getBound().contains(pos) )
                        {
                            findElement = scanArea;
                            return true;
                        }
                        return false;
                    }
                );

                if ( findElement.valid() )
                {
                    return true;
                }

                return false;

            }
        );


        return findElement.release();

    }

    void BlockShower::setCameraParam(float fovy, float wide, float high, float zn, float zf)
    {
        _cameraElement->setCameraParam(fovy, wide, high, zn, zf);
    }

    void BlockShower::setCameraColor(int r, int g, int b, int a)
    {
        // _cameraColor.set(r/255.f, g/255.f, b/255.f, a/255.f);
        _cameraElement->setCameraColor(r, g, b, a);
    }

    void BlockShower::updateCameraPose(float x, float y, float z, float qx, float qy, float qz, float qw)
    {   
        if(_cameraElement == nullptr)// || !_offset.valid())
           return;
        osg::Vec3 pos(x, y, z);
        osg::Quat q(qx, qy, qz, qw);
        osg::Matrix cameraMat = osg::Matrix::rotate(q) * osg::Matrix::translate(pos) * _offset;
        _cameraElement->updateCameraPose(cameraMat);

        // if ( _updateCallback )
        // {
        //     _updateCallback();
        // }

        if(_record && _workScanArea)
        {
            std::string sceneId = _workScanArea->get("id");
            std::string trackRecord = ydBlockDB::getAreaData(sceneId, "track");
            if(!trackRecord.empty())
            {
                trackRecord += ",";
            }
            std::stringstream ss;
            ss << cameraMat.getTrans();
            trackRecord += ss.str();
            ydBlockDB::setAreaData(sceneId, "track", trackRecord);

        }
    }

    void BlockShower::cameraTrakMode(CameraTrackMod mod)
    {
        if(!_switchManipulator){
            return;
        }

        if(mod != CTM_FREE && !_cameraElement->updated()){
            return;
        }

        // printf("%d\n", mod);
        // _cameraTrackMode = mod;
        
        osgGA::CameraManipulator*pOrientManipulator = _switchManipulator->getCurrentMatrixManipulator();
        if(dynamic_cast<osgGA::TrackballManipulator*>(pOrientManipulator)) //free
        {
            _freeInvMatrix = _switchManipulator->getInverseMatrix();
        }
        else if(dynamic_cast<CameraTrackerManipulator*>(pOrientManipulator))//trace
        {
            _traceInvMatrix=_switchManipulator->getInverseMatrix();
        }

        _switchManipulator->selectMatrixManipulator(mod);

        if (mod == CMT_SELF) {
            _cameraElement->getCameraNode()->setNodeMask(0);
        }
        else if(mod == CMT_TRACK)
        {
            _cameraElement->getCameraNode()->setNodeMask(0xFFFFFFFF);
            _switchManipulator->setByInverseMatrix(_traceInvMatrix);
        }
        else if(mod == CTM_FREE)
        {
            _cameraElement->getCameraNode()->setNodeMask(0xFFFFFFFF);
            _switchManipulator->setByInverseMatrix(_freeInvMatrix);
        }

        if ( _updateCallback )
        {
            _updateCallback();
        }
    }

    void BlockShower::deleteArea(std::string const& id)
    {
        ydBase::Element* element = findElement(id);
        if ( element )
        {
            removeElement(element);
            ydBlockDB::removeArea(id);
        }
    }

    void BlockShower::showScanArea(std::string const& id)
    {
        ydBase::Element* element = findElement(id);
        if ( element )
        {
            element->setNodeMask(0XFFFFFFFF);
        }else
        {
            // float s = 320;
            // ScanAreaElement *childArea = new ScanAreaElement( osg::Vec3( s, s, s ));
            // childArea->set( "id", id );
            // childArea->initFromBlockDB();
            // // ScanAreaElement *childArea = new ScanAreaElement;
            // // childArea->set( "id", id );
            // // childArea->initFromBlockDB();
        }
    }

    void BlockShower::hideScanArea(std::string const& id)
    {
        ydBase::Element* element = findElement(id);
        if ( element )
        {
            element->setNodeMask(0);
        }
    }

    std::string BlockShower::getScanArea(float x, float y, float z)
    {
        osg::Vec3 pos(x, y, z);
        ydBase::Element* element = findElement(pos);
        ScanAreaElement* childArea = dynamic_cast<ScanAreaElement*>(element);
        if(childArea)
        {
            return childArea->get("id");
        }
        return "";
    }

    void BlockShower::setScanAreaColorMode(ChildAreaColorMod mod, std::string const& id /*= ""*/)
    {
        ydBase::Element* lpScanArea = NULL;
        if (id.empty())
        {
            lpScanArea = _workScanArea;
        }
        else
        {
            lpScanArea = findElement(id);
        }
        if (!lpScanArea)
        {
            return;
        }
        
        // clearArea();
        // loadAllScan();

        if (_shaderProg == NULL)
        {
            _shaderProg = new osg::Program;
            _shaderProg->addShader(osgDB::readShaderFile(osg::Shader::VERTEX, "res/shader/cloudcolor.vert"));
            _shaderProg->addShader(osgDB::readShaderFile(osg::Shader::FRAGMENT, "res/shader/cloudcolor.frag"));
        }

        if (ChildAreaElement::CM_HEIGHT == ChildAreaElement::getColorMode() && 
            ChildAreaElement::CM_HEIGHT != mod)
        {
            osg::StateSet* lpState = lpScanArea->getOrCreateStateSet();
            lpState->setAttributeAndModes(_shaderProg, osg::StateAttribute::OFF);
            lpState->getUniformList().clear();
            lpState->removeAttribute(_shaderProg);
        }

        switch (mod)
        {
        case ChildAreaColorMod::CM_ORIGIN:
        {
            ChildAreaElement::setColorMode(ChildAreaElement::CM_ORIGIN);
            
            break;
        }

        case ChildAreaColorMod::CM_HEIGHT:
        {
            ChildAreaElement::setColorMode(ChildAreaElement::CM_HEIGHT);

            //osg::BoundingBox bb = ydUtil::computeWorldBoundingBox(lpScanArea);
            // osg::BoundingSphere bs = dynamic_cast<ScanAreaElement*>(lpScanArea)->getBound();
            float fminHeight = _workScanAreaValidBoundBox.zMin();
            float fmaxHeight = _workScanAreaValidBoundBox.zMax();
            osg::StateSet* lpState = lpScanArea->getOrCreateStateSet();
            lpState->setAttributeAndModes(_shaderProg, osg::StateAttribute::ON | osg::StateAttribute::PROTECTED);
            lpState->addUniform(new osg::Uniform("startColor", osg::Vec4(0.80, 0.80, 0.0, 1.0)));
            lpState->addUniform(new osg::Uniform("endColor", osg::Vec4(0.80, 0.0, 0.80, 1.0)));
            lpState->addUniform(new osg::Uniform("showType", 1));
            lpState->addUniform(new osg::Uniform("minHeight", fminHeight));
            lpState->addUniform(new osg::Uniform("maxHeight", fmaxHeight));
            lpState->setMode(GL_BLEND, osg::StateAttribute::ON);
            lpState->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
            break;
        }
        case ChildAreaColorMod::CM_SPECIFY:
        {
            /* code */
            break;
        }
        case ChildAreaColorMod::CM_INTENSITY :
        {
            ChildAreaElement::setColorMode(ChildAreaElement::CM_INTENSITY);
            break;
        }
        default:
            break;
        }
    }

    void BlockShower::startWorkScanArea(std::string const& id)
    {
        ydBase::Element* element = findElement(id);
        if ( !element )
        {
            TRACELL(LL_WARNING, "find %s ScanAreaElement failed\n", id.c_str() );
            return;
        }
        ScanAreaElement *scanElement = dynamic_cast<ScanAreaElement *>( element );
        if ( !scanElement )
        {
            TRACELL(LL_WARNING, "change %s from element to ScanAreaElement failed\n", id.c_str() );
            return;
        }

        _workScanArea = scanElement;
    }

    void BlockShower::stopWorkScanArea(std::string const& id)
    {
        ScanAreaElement *lpScanArea = dynamic_cast<ScanAreaElement*>( _workScanArea.get() );
        if ( !lpScanArea )
            return;
            
        lpScanArea->flush();
        _workScanAreaValidBoundBox = lpScanArea->getBox();
        ydBlockDB::flushDB();
    }

    void BlockShower::showWorkArea()
    {
        _workArea->setNodeMask(0XFFFFFFFF);
    }

    void BlockShower::hideWorkArea()
    {
        _workArea->setNodeMask(0);
    }

    std::string BlockShower::getWorkArea(float x, float y, float z)
    {
        osg::Vec3 pos(x, y, z);
        ydBase::Element* element = findElement(pos);
        AreaElement* childArea = dynamic_cast<AreaElement*>(element);
        if(childArea)
        {
            return childArea->get("id");
        }
        return "";
    }
    std::string BlockShower::createScanArea( float x, float y, float z, float qx, float qy, float qz, float qw )
    {
        //_workScanArea = lpScanArea;

        if ( !_workArea )
        {
            TRACELL(LL_ERROR, "no work area\n" );
            return "";
        }
        _pos.set(x, y, z);
        _offset = osg::Matrix::rotate(osg::Quat(qx, qy, qz, qw)) * osg::Matrix::translate(osg::Vec3(x, y, z));
        // _offset.setRotate(osg::Quat(qx,qy,qz,qw));
        // _offset.setTrans(osg::Vec3(x,y,z));

        float s = G_ChildAreaSize / G_Ratio;
        ScanAreaElement *lpScanArea = new ScanAreaElement(osg::Vec3( s, s, s ));
        lpScanArea->setStartPose(x, y, z, qx, qy, qz, qw );
        addElement( lpScanArea );
        std::string areaId = _workArea->get("id");
        std::string scanAreaList = ydBlockDB::getAreaData(areaId, "ScanAreaList");
        if(!scanAreaList.empty())
        {
            scanAreaList += " ";
        }
        scanAreaList += lpScanArea->get("id");
        ydBlockDB::setAreaData(areaId, "ScanAreaList", scanAreaList);

        // "x y z;qx qy qz qw;x y z,x y z"
        std::stringstream ss;
        ss.precision(8);
        osg::BoundingBox bb = lpScanArea->getBox();
        ss << osg::Vec3(x,y,z) << ";" << osg::Quat(qx,qy,qz,qw) << ";" << bb._min << "," << bb._max;
        ydBlockDB::setAreaData(areaId, lpScanArea->get("id"), ss.str());
        // 这个路径用于存储缓存数据，也就是生成的这些BlockBody数据
        std::string cachePath =  _dbPath + "/cache/" + lpScanArea->get("id");
        ydUtil::makeSureDirExist( cachePath.c_str() );
        lpScanArea->set("cachepath", cachePath );
        return lpScanArea->get("id");
    }

    void BlockShower::setMouseEventCallback(CallBack cb)
    {
        if(_handler)
        {
            _handler->setMouseEventCallback(cb);
        }
    }

    void BlockShower::switchToPosition(float x, float y, float z)
    {
        osg::Matrix viewMat = _sceneRender->getViewer()->getCamera()->getViewMatrix();
        viewMat.setTrans(x, y, z);
        _sceneRender->getViewer()->getCamera()->setViewMatrix(viewMat);
    }

    void BlockShower::setCloudPoint(pcl::PointCloud<pcl::PointXYZI>* cloud)
    {
        if ( cloud->size() == 0 )
        {
            return;
        }
 
        if ( _threadCount >= 1 )
            return;

        //printElement( _sceneRender->getViewer()->getSceneData() );

        //printElement( _sceneRender->getScene() );
        // _threadCount ++;
        pcl::PointCloud<pcl::PointXYZI> tmpCloud;// = *cloud;
        pcl::copyPointCloud(*cloud, tmpCloud);

        std::thread th([this, tmpCloud](){

            this->_threadCount ++;
            ScanAreaElement *lpScanArea = dynamic_cast<ScanAreaElement*>( this->_workScanArea.get() );
            if(lpScanArea)
            {
                lpScanArea->setCloudPoint( &tmpCloud );

                // 更新扫描区域的包围盒
                // if(lpScanArea->isDirty())
                // if(this->_workArea != nullptr && lpScanArea != nullptr)
                {
                    std::string data = ydBlockDB::getAreaData(this->_workArea->get("id"), lpScanArea->get("id"));
                    std::vector<std::string> vec = ydUtil::split(data, ";");
                    if(vec.size() == 3)
                    {
                        std::stringstream ss;
                        ss << lpScanArea->getBox()._min << "," << lpScanArea->getBox()._max;
                        data = vec[0] + ";" + vec[1] + ";" + ss.str();
                        ydBlockDB::setAreaData(_workArea->get("id"), lpScanArea->get("id"), data);
                    }
                }
            }
            

            this->_threadCount --;

            if ( this->_updateCallback )
            {
                this->_updateCallback();
            }

        });
        th.detach();
    }

    void BlockShower::setRecordTrack(bool save)
    {
        _record = save;
    }

    void BlockShower::setShowTrack(bool show)
    {
        _cameraElement->setShowTrack(show);
    }

    void BlockShower::setTrackLineColor(int r, int g, int b, int a)
    {
        osg::Vec4 color(r/255.f, g/255.f, b/255.f, a/255.f);
        _cameraElement->setTrackLineColor(color);
    }

    void BlockShower::setDBPath(std::string const& path)
    {
        _dbPath = path;
        ydBlockDB::unloadDB();
        ydBlockDB::setDBPath(path);
        // todo : set Accuracy from ui
        //ydBlockDB::setAccuracy( 0.06 );
        ydBlockDB::setAccuracy( 0.1 );
        // loadAllScan();
    }


    void BlockShower::emitUpdateNodeState()
    {
    }

    void BlockShower::setViewPosition(CameraView cv)
    {
        cameraTrakMode(CTM_FREE);

        osg::Vec3 eye, center, up;
        ydBase::Scene *lpScene = _sceneRender->getScene();
        if(!lpScene){
            return;
        }
        osg::BoundingSphere bs = lpScene->getRoot()->getBound();
        eye = center = bs.center();
        float radius = bs.radius();

        // 正
        if(cv == CV_MAIN){
            eye.z() += radius;
            up = osg::Y_AXIS;
        }
        else if(cv == CV_LEFT){
            eye.x() += radius;
            up = osg::Z_AXIS;
        }
        else if(cv == CV_RIGHT){
            eye.x() -= radius;
            up = osg::Z_AXIS;
        }
        else if(cv == CV_FRONT){
            eye.y() -= radius;
            up = osg::Z_AXIS;
        }
        else if(cv == CV_BACK){
            eye.y() += radius;
            up = osg::Z_AXIS;
        }

        osg::Matrixd mat;
        mat.makeLookAt(eye, center, up);

        osgGA::TrackballManipulator* cm = dynamic_cast<osgGA::TrackballManipulator*>(_switchManipulator->getCurrentMatrixManipulator());
        if(cm) {
            cm->setByInverseMatrix(mat);
            cm->setDistance(radius*1.2);
            cm->setCenter(center);
        }

        if ( _updateCallback )
        {
            _updateCallback();
        }
    }

    void BlockShower::setDrawArrow(bool arrow)
    {
        if(_handler) {
            _handler->setDrawArrow(arrow);
        }
    }

    void BlockShower::setArrowCallBack(ArrowCallBack acb)
    {
        if(_handler) {
            _handler->setArrowCallBack(acb);
        }
    }

    osg::MatrixTransform* BlockShower::getViewCone()
    {
        if(_cameraElement.valid())
            return _cameraElement->getViewCone();
        else
            return NULL;
    };

    std::vector<std::string> BlockShower::getScanAreaList()
    {
        // "id1 id2..."
        std::vector<std::string> list;
        std::string str = ydBlockDB::getAreaData(_workArea->get("id"), "ScanAreaList");
        std::vector<std::string> scanList = ydUtil::split(str, " ");
        for (auto id : scanList)
        {
            list.push_back(id);
        }
        return list;
    }

    void BlockShower::deleteScanArea(std::string const& scanid)
    {
        // "id1 id2..."
        std::string str = ydBlockDB::getAreaData(_workArea->get("id"), "ScanAreaList");
        std::string dst;
        std::vector<std::string> scanList = ydUtil::split(str, " ");
        for (auto id : scanList)
        {
            if (scanid == id)
                continue;
        
            if (!dst.empty())
                dst += " ";
            dst += id;
        }
        ydBlockDB::setAreaData(_workArea->get("id"), "ScanAreaList", dst);
    }

    void BlockShower::setSceneAreaMask(std::string const& uuid, bool show)
    {
        unsigned mask = show ? 0xFFFFFFFF : 0x0;
        ydBase::Element* element = findElement(uuid);
        if(element)
        {
            element->setNodeMask(mask);
        }
    }

    void BlockShower::loadScan( std::string const& scanid )
    {
        if(!_workArea.valid() || _dbPath.empty())
            return;

        // "x y z;qx qy qz qw;x y z,x y z"
        std::string info = ydBlockDB::getAreaData(_workArea->get("id"), scanid);
        std::vector<std::string> params = ydUtil::split(info, ";");

        std::stringstream ss;
        osg::Quat rotate;
        osg::Vec3 position, min, max;
        if(params.size() == 3)
        {
            ss.clear(); ss << params[0]; ss >> position;
            ss.clear(); ss << params[1]; ss >> rotate;

            std::vector<std::string> boxstr = ydUtil::split(params[2], ",");
            if(boxstr.size() == 2)
            {
                ss.clear(); ss << boxstr[0]; ss >> min;
                ss.clear(); ss << boxstr[1]; ss >> max;
            }

            float s = G_ChildAreaSize / G_Ratio;
            ScanAreaElement *lpScanArea = new ScanAreaElement(osg::Vec3(s, s, s));
            lpScanArea->set("cachepath", _dbPath + "/cache/" + scanid );
            ydUtil::makeSureDirExist( ( _dbPath + "/cache/" + scanid).c_str() );
            lpScanArea->setStartPose(position[0], position[1], position[2],
                                    rotate[0], rotate[1], rotate[2], rotate[3]);
            lpScanArea->set("id", scanid);
            lpScanArea->setBox(min, max);
            addElement( lpScanArea );
            _workScanArea = lpScanArea;
            _workScanAreaValidBoundBox = lpScanArea->getBox();
        }

        // 加载扫描路径
        if(_cameraElement)
        {
            std::string trackRecord = ydBlockDB::getAreaData(scanid, "track");
            std::vector<std::string> track = ydUtil::split(trackRecord, ",");
            for (auto it : track)
            {
                osg::Vec3 pos;
                std::stringstream ss;
                ss << it;
                ss >> pos;
                _cameraElement->updateCameraPose(pos, osg::Quat());
            }

        }

    }

    void BlockShower::loadAllScan()
    {
        if(!_workArea.valid())
            return;
        std::string str = ydBlockDB::getAreaData(_workArea->get("id"), "ScanAreaList");
        std::vector<std::string> scanList = ydUtil::split(str, " ");
        for (auto scanid : scanList)
        {
            if(findElement(scanid) != NULL)
                continue;
            loadScan(scanid);
        }

    }

    void BlockShower::updateStates()
    {
        // setScanAreaColorMode(BlockShower::CM_HEIGHT);
        // setShapeMode(key);
        // cameraTrakMode(BlockShower::CMT_TRACK);
        // setShowTrack(true);
        // setRatio(0.25);
        setClarity(_clarity);

    }

    void BlockShower::setClarity(float a)
    {
        ScanAreaElement* lpSacnArea = dynamic_cast<ScanAreaElement*>(_workScanArea.get());
        if(!lpSacnArea)
        {
            return;
        }

        osg::StateSet* state = lpSacnArea->getOrCreateStateSet();
        //关闭灯光
        state->setMode(GL_LIGHTING, osg::StateAttribute::OFF | osg::StateAttribute::PROTECTED);
        //打开混合融合模式
        state->setMode(GL_BLEND, osg::StateAttribute::ON);
        state->setMode(GL_DEPTH_TEST, osg::StateAttribute::ON);
        state->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
        //使用BlendFunc实现透明效果
        static osg::ref_ptr<osg::BlendColor> bc = new osg::BlendColor(osg::Vec4(1.0, 1.0, 1.0, 0.0));
        static osg::ref_ptr<osg::BlendFunc> bf = new osg::BlendFunc();
        state->setAttributeAndModes(bf, osg::StateAttribute::ON);
        state->setAttributeAndModes(bc, osg::StateAttribute::ON);
        bf->setSource(osg::BlendFunc::CONSTANT_ALPHA);
        bf->setDestination(osg::BlendFunc::ONE_MINUS_CONSTANT_ALPHA);
        bc->setConstantColor(osg::Vec4(1, 1, 1, a));
        _clarity = a;

        // state->setMode(GL_BLEND,osg::StateAttribute::ON);
        // osg::ref_ptr<osg::Material> mat = new osg::Material;
        // //漫发射光
        // mat->setDiffuse(osg::Material::FRONT_AND_BACK,osg::Vec4(1.0, 1.0, 1.0, 1.0));
        // //环境光
        // mat->setAmbient(osg::Material::FRONT_AND_BACK,osg::Vec4(1.0, 1.0, 1.0, 1.0));
        // //设置材质的混合光颜色
        // mat->setTransparency(osg::Material::FRONT_AND_BACK, a);
        // state->setAttributeAndModes(mat, osg::StateAttribute::OVERRIDE | osg::StateAttribute::ON);
        // state->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
    }

    void BlockShower::screenshot(std::string path)
    {
        if(!_cameraDrawCallback.valid())
        {
            _cameraDrawCallback = new WindowCaptureCallback;
            _sceneRender->getViewer()->getCamera()->setFinalDrawCallback(_cameraDrawCallback);
        }

        WindowCaptureCallback* lpCallBack = dynamic_cast<WindowCaptureCallback*>(_cameraDrawCallback.get());
        if(lpCallBack != NULL)
        {
            lpCallBack->screenshot(path);
        }
    }

    void BlockShower::setShapeMode(const std::string& mod)
    {
        ChildAreaElement::ShapeMode md;
        if(mod == "Block")
            md = ChildAreaElement::SM_BLOCK;
        else if( mod == "Point")
            md = ChildAreaElement::SM_POINT;

        if(md != ChildAreaElement::getShapeMode())
        {
            ChildAreaElement::setShapeMode(md);
            clearArea();
            loadAllScan();
        }
    }

    void BlockShower::setRatio(float ratio)
    {
        if(G_Ratio == ratio)
            return;

        G_Ratio = ratio;
        //ydBlockDB::setAccuracy(ratio);
        //ydBlockDB::flushDB();
        clearArea();
        loadAllScan();
    }
}
