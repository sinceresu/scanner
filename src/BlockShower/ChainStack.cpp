#include "ChainStack.h"

namespace BlockShower
{
    void ChainStack::push( BlockChain *lpChain, BlockChain *lpStart, BlockChain *lpRefer )
    {
        _items.push( Item( lpChain, lpStart, lpRefer ) );
    }
    ChainStack::Item ChainStack::pop()
    {
        Item item;
        if ( _items.empty() )
        {
            return item;
        }

        item = _items.top();
        _items.pop();
        return item;
    }
    bool ChainStack::empty()const
    {
        return _items.empty();
    }
}
