#include "LevelSize.h"
#include <assert.h>
#include <math.h>

namespace BlockShower
{
    LevelSize::LevelSize( int maxLevel )
    {
        _levelSize.resize( maxLevel );
        for ( int i = 0; i < maxLevel; i ++ )
        {
            float levelToLodRangeMax = 300.0f / powf((float)(i + 1), 0.8f);

            _levelSize[i] = levelToLodRangeMax;
        }
    }

    float LevelSize::get( int level )
    {
        assert( level < _levelSize.size() );
        return _levelSize[level];
    }
}
