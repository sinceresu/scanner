#ifndef __CBD_CUSTOM_MODLE_MANIPULATOR_H_FILE__
#define __CBD_CUSTOM_MODLE_MANIPULATOR_H_FILE__
#include <BlockShower/export.h>
#include <osgGA/OrbitManipulator>


namespace BlockShower
{
    class BLOCKSHOWER_API CustomModelManipulator : public osgGA::OrbitManipulator
    {
    public:
        CustomModelManipulator();

        virtual void setByMatrix(const osg::Matrixd& matrix);
        virtual osg::Matrixd getMatrix() const;
        virtual osg::Matrixd getInverseMatrix() const;

        void setTrackNode(osg::Node* node);


    protected:
        osg::ref_ptr<osg::Node> _node;
    };
}


#endif // __CBD_CUSTOM_MODLE_MANIPULATOR_H_FILE__
