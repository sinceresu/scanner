#ifndef __SCANNER_SRC_LEVEL_SIZE_H_FILE__
#define __SCANNER_SRC_LEVEL_SIZE_H_FILE__
#include <vector>

namespace BlockShower
{
    class LevelSize
    {
    public:
        LevelSize( int maxLevel );

        float get( int level );

    protected:
        std::vector<float> _levelSize;

    };
}


#endif // __SCANNER_SRC_LEVEL_SIZE_H_FILE__
