#ifndef NET_FRAME_THREAD_POOL_H
#define NET_FRAME_THREAD_POOL_H

#include "MessageQueue.h"

#include <vector>
#include <memory>
#include <thread>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>

namespace BlockShower
{
    #define MIN_THREADS 1

    template<class Type>
    class ThreadPool
    {
        ThreadPool& operator=(const ThreadPool&) = delete;
        ThreadPool(const ThreadPool& other) = delete;
    public:
        ThreadPool(int32_t threads, std::function<void(Type& msg)> handler);
        virtual ~ThreadPool();
        void Submit(Type msg);

    private:
        bool _shutdown;
        int32_t _threads;
        std::function<void(Type&)> _handler;
        std::vector<std::thread> _workers;
        MessageQueue<Type> _tasks;

    };

    template<class Type>
    ThreadPool<Type>::ThreadPool(int32_t threads, std::function<void(Type &msg)> handler)
            : _shutdown(false)
            , _threads(threads)
            , _handler(handler)
            , _workers()
            , _tasks()
    {
        if (_threads < MIN_THREADS)
            _threads = MIN_THREADS;


        for (int32_t i = 0; i < _threads; ++i)
        {
            _workers.emplace_back(
            [this] {
                while (!_shutdown)
                {
                    Type msg;
                    this->_tasks.Pop(msg, true);
                    this->_handler(msg);
                }
            });
        }
    }


    template<class Type>
    ThreadPool<Type>::~ThreadPool()
    {
        _shutdown = true;
        for (std::thread &worker : _workers)
            worker.join();
    }

    template<class Type>
    void ThreadPool<Type>::Submit(Type msg)
    {
        _tasks.Push(msg);
    }
}

#endif //NET_FRAME_THREAD_POOL_H
