#ifndef __SRC_BLOCK_SHOWER_BOX_ELEMENT_H_FILE__
#define __SRC_BLOCK_SHOWER_BOX_ELEMENT_H_FILE__

#include <ydBase/Element.h>
#include <ydUtil/Block.h>

namespace BlockShower
{
    class BoxElement : public ydBase::Element
    {
    public:

        META_Info(BoxElement);
        BoxElement(int level, int step);
        ~BoxElement();

        int getLevel()const{ return _level; };
        virtual void load( bool inThread );

        void setIndex(ydUtil::BlockIndex const& bi ){ _index = bi; };
        ydUtil::BlockIndex getIndex()const{ return _index; };

        osg::BoundingBox getBox(){ return _rangeBB;};
        void setRange( osg::BoundingBox const& bb );

        int getStep()const{ return _step; };
    protected:
        virtual void initGridBoxInfo( ydUtil::BlockIndex const &bi, BoxElement *lpInfo );

        void updateChildPartRange( osg::BoundingBox const& bb );
        /* ydUtil::BlockIndex getGridIndex( osg::Vec3 const& pos ); */
        osg::Vec3 getChildPartRange();
        ydUtil::BlockIndex getChildIndex( osg::Vec3 const& pos );
        osg::Vec3 getChildPos(ydUtil::BlockIndex const& bi );
    protected:
        osg::Vec3 _childPartRange;

        osg::BoundingBox _rangeBB;
        ydUtil::BlockIndex _index;

        int _level;
        int _step;
    };
}



#endif // __SRC_BLOCK_SHOWER_BOX_ELEMENT_H_FILE__
