#ifndef __CHECK_CHILD_GRID_THREAD_H_FILE__
#define __CHECK_CHILD_GRID_THREAD_H_FILE__
#include <thread>
#include <mutex>
#include <condition_variable>
#include <osg/observer_ptr>
#include <osg/Node>
#include <vector>
#include <functional>
#include <atomic>

namespace BlockShower
{
    class CheckChildGridThread
    {
    public:
        using UpdateCallback = std::function<void()>;
        CheckChildGridThread( UpdateCallback fun );
        ~CheckChildGridThread();

        void addCheckNode( osg::Node *lpNode );
        // 弱指针, 不用显示移除
        // void removeCheckNode( osg::Node *lpNode );

        void stop();

        void clear();
        void workOnce();

    protected:
        void work(std::vector<osg::observer_ptr<osg::Node> > checkNodes );

    protected:
        std::thread _thread;
        //bool _down;
        std::atomic<bool> _down;

        UpdateCallback _fun;

        std::vector<osg::observer_ptr<osg::Node> > _checkNodes;
    };
}


#endif // __CHECK_CHILD_GRID_THREAD_H_FILE__
