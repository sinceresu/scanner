#ifndef __SCANNER_BLOCK_CHAIN_BLOCK_BODY__
#define __SCANNER_BLOCK_CHAIN_BLOCK_BODY__
#include <osg/Geode>
#include <functional>
#include <ydUtil/Block.h>
#include <map>
#include <mutex>
#include <osg/observer_ptr>

namespace BlockShower
{
    struct ChainPos
    {
        double _x;
        double _y;
        double _z;

    ChainPos()
        :_x(0.0),_y(0.0),_z(0.0)
            {
            }
    };

    class BlockChain : public osg::Referenced
    {
    public:
        friend class BlockBody;
        using EachFun = std::function<bool ( BlockChain *lpbc )>;
        ydUtil::BlockIndex getIndex()const
        {
            return _index;
        }
        // first is min index, second is max index
        inline std::pair<ChainPos, ChainPos> getRange( int gridSize )
        {
            ChainPos n, p;

            p._x = _index._x - gridSize * 0.5;
            p._y = _index._y - gridSize * 0.5;
            p._z = _index._z - gridSize * 0.5;

            n._x = _index._x + gridSize * 0.5;
            n._y = _index._y +  gridSize * 0.5;
            n._z = _index._z +  gridSize * 0.5;

            return std::make_pair(p,n);
        }

        void destory( BlockChain  *lpBC );

        //void setColor( osg::Vec4 const& color ){ _color = color;};
        //osg::Vec4 getColor()const{ return _color; };
    protected:

        BlockChain(ydUtil::BlockIndex const& bi);
        ~BlockChain();

        bool insert(ydUtil::BlockIndex const& bi, int gridSize );
        void remove(ydUtil::BlockIndex const& bi);

        BlockChain* find(ydUtil::BlockIndex const& bi);
        // EachFun return true to break
        void for_each(EachFun fun, int step );

        inline bool removeX( BlockChain *lpBC )
        {
            if ( lpBC->_xp )
            {
                lpBC->_xp->_xn = lpBC->_xn;
            }
            if ( lpBC->_xn )
            {
                lpBC->_xn->_xp = lpBC->_xp;
            }
            return true;
        }
        inline bool removeY( BlockChain *lpBC )
        {
            if ( lpBC->_yp )
            {
                lpBC->_yp->_yn = lpBC->_yn;
            }
            if ( lpBC->_yn )
            {
                lpBC->_yn->_yp = lpBC->_yp;
            }
            return true;
        }
        inline bool removeZ( BlockChain *lpBC )
        {
            if ( lpBC->_zp )
            {
                lpBC->_zp->_zn = lpBC->_zn;
            }
            if ( lpBC->_zn )
            {
                lpBC->_zn->_zp = lpBC->_zp;
            }
            return true;
        }

        enum FLAG
        {
            X = 0X01,
            Y = 0X02,
            Z = 0X03
        };
        inline void link( BlockChain *pre, BlockChain *next, BlockChain *cur, FLAG f )
        {
            _lastChain = cur;
            switch( f )
            {
            case X:
            {
                cur->_xn = next;
                if ( pre )
                {
                    pre->_xn = cur;
                }
                cur->_xp = pre;
                if ( next )
                {
                    next->_xp = cur;
                }


            }

            break;
            case Y:
            {
                cur->_yn = next;
                if ( pre )
                {
                    pre->_yn = cur;
                }
                cur->_yp = pre;
                if ( next )
                {
                    next->_yp = cur;
                }

            }
            break;
            case Z:
            {
                cur->_zn = next;
                if ( pre )
                {
                    pre->_zn = cur;
                }
                cur->_zp = pre;
                if ( next )
                {
                    next->_zp = cur;
                }

            }
            break;
            default:
                break;
            }
        }

        inline bool checkLink(BlockChain *pre, BlockChain *next, BlockChain *cur,  int gridSize )
        {
            if ( pre && cur )
            {
                if ( cur->_index._x - pre->_index._x >= gridSize || cur->_index._y - pre->_index._y >= gridSize || cur->_index._z - pre->_index._z >= gridSize )
                {
                    return true;
                }
            }
            if ( cur && next )
            {
                if ( cur->_index._x - next->_index._x >= gridSize || cur->_index._y - next->_index._y >= gridSize || cur->_index._z - next->_index._z >= gridSize )
                {
                    return true;
                }
            }

            return false;

        }
        inline bool checkStep( BlockChain *cur, BlockChain *lpRefer, int step )
        {
            if ( cur && lpRefer )
            {
                if ( abs ( cur->_index._x - lpRefer->_index._x ) >= step
                     || abs ( cur->_index._y - lpRefer->_index._y ) >= step
                     || abs ( cur->_index._z - lpRefer->_index._z ) >= step )
                {
                    return true;
                }
            }


            return false;
        }

        inline bool inRange( ydUtil::BlockIndex const& minv, ydUtil::BlockIndex const& maxv )
        {
            return minv._x <= _index._x && minv._y <= _index._y && minv._z <= _index._z
                && maxv._x >= _index._x && maxv._y >= _index._y && maxv._z >= _index._z;
        }
    protected:
        osg::ref_ptr<BlockChain> _xp;
        osg::ref_ptr<BlockChain> _xn;
        osg::ref_ptr<BlockChain> _yp;
        osg::ref_ptr<BlockChain> _yn;
        osg::ref_ptr<BlockChain> _zp;
        osg::ref_ptr<BlockChain> _zn;

        ydUtil::BlockIndex _index;
        float _intensity;

        osg::observer_ptr<BlockChain> _lastChain;
    };

    class BlockBody : public osg::Referenced
    {
    public:
        BlockBody();
        ~BlockBody();

        void setFilePath( std::string const& fp ){ _filePath = fp; };
        std::string getFilePath()const{ return _filePath; };

        osg::Node *makeNode( int step, osg::Vec4 const& color );

        bool addBlock( ydUtil::BlockIndex const& bi );

        void remove(ydUtil::BlockIndex const& bi);

        BlockChain* find(ydUtil::BlockIndex const& bi);


        inline void traverse(BlockChain::EachFun fun, int step )
        {
            _lpChain->for_each(fun, step);
        }

        inline void setUnitGridSize( float unitGridSize )
        {
            _unitGridSize = unitGridSize;
        }
        inline void setIntensity( float intensity )
        {
            if( _lpChain )
            {
                _lpChain->_intensity = intensity;
            }
        }
        inline void setGridSize( int gridSize )
        {
            _gridSize = gridSize;
        }
        void freeChain();
        void initChain();
    protected:
        void syncToFile();
        void loadFromFile();

        bool innerAddBlock( ydUtil::BlockIndex const& bi );
    protected:
        std::mutex _chainMutex;
        float _unitGridSize;
        int _gridSize;

        bool _initFromFile;
        bool _dirty;

        osg::ref_ptr<BlockChain> _lpChain;
        std::string _filePath;

        int _loadCount;
    };

}

#endif // __SCANNER_BLOCK_CHAIN_BLOCK_BODY__
