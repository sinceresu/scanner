#include "BoxElement.h"
#include "Config.h"
#include <ydUtil/log.h>

namespace BlockShower
{
    BoxElement::BoxElement(int level, int step)
        : _level( level )
        , _step( step )
    {
    }
    BoxElement::~BoxElement()
    {
        
    }

    void BoxElement::load( bool inThread )
    {
        
    }


    void BoxElement::setRange( osg::BoundingBox const& bb )
    {
        _rangeBB = bb;
        updateChildPartRange( bb );
    }

    void BoxElement::initGridBoxInfo( ydUtil::BlockIndex const &bi, BoxElement *lpInfo )
    {
        lpInfo->_index = bi;
        osg::Vec3 halfBBRange = getChildPartRange() / 2;

        // 本节点的网格大小
        // 本节点中本位置的中心点
        osg::Vec3d localCenter = getChildPos( bi );

        osg::Vec3 min = localCenter - halfBBRange;
        osg::Vec3 max = localCenter + halfBBRange;

        // 子节点的包围盒, 包含了中心点和大小
        osg::BoundingBox childBB( min, max );

        lpInfo->set("cachepath", get("cachepath"));
        lpInfo->set("scanid", get("scanid"));
        lpInfo->setRange(childBB);

    }

    void BoxElement::updateChildPartRange( osg::BoundingBox const& bb )
    {
        osg::Vec3 minPos = bb._min;
        osg::Vec3 maxPos = bb._max;
        // if ( _finish )
        // {
        //     _childPartRange  = ( maxPos - minPos );//osg::Vec3(ydBlockDB::getAccuracy(), ydBlockDB::getAccuracy(),ydBlockDB::getAccuracy() );//( maxPos - minPos ) / FINISH_GRID_COUNT;
        // }else
        _childPartRange  = ( maxPos - minPos ) / CHILD_LAYER_COUNT;
    }
    /* ydUtil::BlockIndex getGridIndex( osg::Vec3 const& pos ); */
    osg::Vec3 BoxElement::getChildPartRange()
    {
        return _childPartRange;
    }
    ydUtil::BlockIndex BoxElement::getChildIndex( osg::Vec3 const& pos )
    {
        ydUtil::BlockIndex bi = ydUtil::getBlockIndex( osg::Vec3( 0.0, 0.0, 0.0 ), getChildPartRange(), pos );
        //ydUtil::BlockIndex bi = ydUtil::getBlockIndex( getChildPartRange() / 2, getChildPartRange(), pos );
        //ydUtil::BlockIndex bi = ydUtil::getBlockIndex( getBox()._min + getChildPartRange() / 2, getChildPartRange(), pos );
        return bi;
    }
    osg::Vec3 BoxElement::getChildPos(ydUtil::BlockIndex const& bi )
    {
        //osg::Vec3 pos = ydUtil::getPos( getChildPartRange() / 2, bi, getChildPartRange() );
        osg::Vec3 pos = ydUtil::getPos( osg::Vec3( 0.0, 0.0, 0.0 ), bi, getChildPartRange() );
        //osg::Vec3 pos = ydUtil::getPos( getBox()._min + getChildPartRange() / 2, bi, getChildPartRange() );
        return pos;
    }
}
