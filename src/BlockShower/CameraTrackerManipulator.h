#ifndef __CBD_CAMERA_TRACKER_MANIPULATOR_H_FILE__
#define __CBD_CAMERA_TRACKER_MANIPULATOR_H_FILE__
#include <BlockShower/export.h>
#include <osgGA/NodeTrackerManipulator>


namespace BlockShower
{
    class BLOCKSHOWER_API CameraTrackerManipulator : public osgGA::NodeTrackerManipulator
    {
    public:
        CameraTrackerManipulator();
        
        virtual void setByMatrix(const osg::Matrixd& matrix);
        virtual osg::Matrixd getMatrix() const;
        virtual osg::Matrixd getInverseMatrix() const;
        virtual void setByInverseMatrix( const osg::Matrixd& matrix );
    };
}


#endif // __CBD_CAMERA_TRACKER_MANIPULATOR_H_FILE__
