#include "MouseEventHandler.h"
#include <osgDB/ReadFile>
#include <ydUtil/matrix_math.h>
// #include <osg/io_utils>
#include "Config.h"

namespace BlockShower
{
    MouseEventHandler::MouseEventHandler()
        :_drawArrow(false)
    {
    }

    MouseEventHandler::~MouseEventHandler()
    {

    }
    bool MouseEventHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
    {
        if ( _mouseOperator )
        {
            bool flag =  _mouseOperator->handle( ea, aa );
            if ( _mouseOperator->over() )
            {
                _mouseOperator = NULL;
            }

            return flag;

        }
        osgViewer::Viewer* view = dynamic_cast<osgViewer::Viewer*>(&aa);

        osg::Vec3 pos;
        bool isHit = testHitModel(view, ea.getXnormalized(), ea.getYnormalized(), pos);
        if(!isHit)
            return false;

        // _drawArrow = (ea.getModKeyMask() == ea.MODKEY_ALT);
        if(_drawArrow && _arrow.valid())
        {
            if(ea.getEventType() == ea.RELEASE)
            {
                if(_arrowCallback) {
                    _arrowCallback(_startPos.x(), _startPos.y(), _startPos.z(), pos.x(), pos.y(), pos.z());
                }
            }
            else if(ea.getEventType() == ea.PUSH)
            {
                if ( !_hitElement->containsNode( _arrow ) )
                {
                    _hitElement->addChild(_arrow);
                }
                if(isHit) {
                    _arrow->setMatrix(osg::Matrix::scale(0.0001, 0.0001, 0.0001) * osg::Matrix::translate(pos));
                    _startPos = pos;
                }
            }
            else if(ea.getEventType() == ea.DRAG)
            {
                if(isHit)
                {
                    osg::Matrix mat = _arrow->getMatrix();
                    osg::Vec3 src = mat.getTrans();
                    pos.z() = src.z();
                    pos = pos - src;

                    float r = _round->getBound().radius() * 2;
                    float l = pos.length();

                    osg::Quat rotate;
                    pos.normalize();
                    rotate.makeRotate(osg::X_AXIS, pos);
                    mat.setRotate(rotate);

                    _arrow->setMatrix(osg::Matrix::scale(l/r, 1.0, 1.0) * mat);
                }
            }
            return true;
        }

        // if(ea.getEventType() != ea.KEYB)
        static float dbclickTime = 0.3;
        if (_timer.time_s() > dbclickTime && _hasRelease)
        {
            _hasRelease = false;
            _timer.setStartTick();
            callBack(_prePos, _preX, _preY, _mouseMask, false);
            return false;
        }

        if(_timer.time_s() < dbclickTime && ea.getEventType() == ea.PUSH && _hasRelease)
        {
            _hasRelease = false;
            _timer.setStartTick();
            callBack(pos, ea.getXnormalized(), ea.getYnormalized(), _mouseMask, true);
            return false;
        }

        if(ea.getEventType() == ea.PUSH)
        {
            _hasRelease = true;
            _timer.setStartTick();
            _mouseMask = ea.getButton();
            _preX = ea.getXnormalized();
            _preY = ea.getYnormalized();
            _prePos = pos;
        }

        return false;
    }
    void MouseEventHandler::setMouseOperator( unsigned int t,  MouseOperator fun )
    {
        /*
        switch( t )
        {
        case MO_PICK:
            _mouseOperator = new PickOperator( fun );
            break;
        case MO_LINE:
            _mouseOperator = new LineOperator( fun );
            break;
        case MO_TRIANGEL:
            _mouseOperator = new TriangleOperator( fun );
            break;
        case MO_QUAD:
            _mouseOperator = new PolygonOperator( fun );
        }
        */
        _mouseOperator = NULL;
    }
    void MouseEventHandler::setPlaneElement(osg::MatrixTransform* element)
    {
        _hitElement = element;

        if(!_arrow.valid())
        {
            osg::ref_ptr<osg::Node> arrow = osgDB::readNodeFile("res/arrow.osgb");
            if(!arrow.valid())
            {
                return;
            }

            _arrow = new osg::MatrixTransform;
            osg::MatrixTransform* ms = new osg::MatrixTransform;
            _arrow->addChild(ms);

            ms->setMatrix(osg::Matrix::scale(0.01, 0.01, 0.01) * osg::Matrix::rotate(osg::Quat(osg::PI_2, osg::X_AXIS)));

            ms->addChild(arrow);
            _round = ms;
        }
    }

    bool MouseEventHandler::testHitModel(osgViewer::View* view, float nx, float ny, osg::Vec3& pos)
    {
        if (!_hitElement)
        {
            return false;
        }

        osg::Vec3d eye, ray;
        if (!ydUtil::getRay(view->getCamera(), nx, ny, eye, ray))
        {
            return false;
        }

        osg::NodePath np;
        osg::NodePathList nodePaths = _hitElement->getParentalNodePaths();
        if (nodePaths.size() == 0)
        {
            np.push_back(_hitElement);
        }
        else
        {
            np = nodePaths[0];
        }

        osg::Matrixd localToWorldMatrix = osg::computeLocalToWorld(np);

        osg::ref_ptr<osg::MatrixTransform> xform = new osg::MatrixTransform;
        // for (unsigned int i = 0; i < _hitElement->getNumChildren(); i++)
        // {
        //     xform->addChild(_hitElement->getChild(i));
        // }
        xform->addChild(_hitElement);
        xform->setMatrix(localToWorldMatrix);

        osg::ref_ptr<osgUtil::LineSegmentIntersector> intersector = new osgUtil::LineSegmentIntersector(eye, eye + ray * 100000 /* _hitElement->getBound().radius() * 2 */);
        osgUtil::IntersectionVisitor iv(intersector.get());
        xform->accept(iv);

        if (intersector->containsIntersections())
        {
            pos = intersector->getFirstIntersection().getWorldIntersectPoint();
            return true;
        }

        return false;
    }

    void MouseEventHandler::callBack(osg::Vec3 pos, float x, float y, int mask, bool dbClick)
    {
        MouseClickType type;
        if(mask == osgGA::GUIEventAdapter::MouseButtonMask::LEFT_MOUSE_BUTTON)
        {
            if(dbClick)
                type = MouseClickType::MCT_LEFT_DOUBLE;
            else
                type = MouseClickType::MCT_LEFT;
        }
        else if(mask == osgGA::GUIEventAdapter::MouseButtonMask::MIDDLE_MOUSE_BUTTON)
        {
            if(dbClick)
                type = MouseClickType::MCT_MIDDLE_DOUBLE;
            else
                type = MouseClickType::MCT_MIDDLE;
        }
        else if(mask == osgGA::GUIEventAdapter::MouseButtonMask::RIGHT_MOUSE_BUTTON)
        {
            if(dbClick)
                type = MouseClickType::MCT_RIGHT_DOUBLE;
            else
                type = MouseClickType::MCT_RIGHT;
        }

        if(_callback)
        {
            _callback(pos.x(), pos.y(), pos.z() - Z_OFFSET, x, y, type);
        }
    }
    void MouseEventHandler::setDrawArrow(bool arrow)
    {
        _drawArrow = arrow;
        if ( !_drawArrow )
        {
            _hitElement->removeChild(_arrow);
        }
    }
}
