#include <BlockShower/CameraTrackElement.h>
#include <osg/LineWidth>
#include <osgUtil/SmoothingVisitor>
#include <osgDB/ReadFile>
#include <osg/ShapeDrawable>

namespace BlockShower
{
    CameraTrackElement::CameraTrackElement()
    {
        _zf = 15.0;
        _zn = 9.9999997473787516e-05;
        _fovy = 81.599998474121094;
        _width = 1920.0;
        _height = 1080.0;
        _aspect = _width / _height;

        initTrack();
        initCamera(false);
        frushCamera();
    };

    void CameraTrackElement::setCameraParam(float fovy, float wide, float high, float zn, float zf)
    {
        _zn = zn;
        _zf = zf;
        _fovy = fovy;
        _width = wide;
        _height = high;
        _aspect = wide / high;

        frushCamera();
    }

    void CameraTrackElement::setCameraColor(int r, int g, int b, int a)
    {
        _cameraColor.set(r/255.f, g/255.f, b/255.f, a/255.f);
    }

    void CameraTrackElement::updateCameraPose(osg::Vec3 pos, osg::Quat q)
    {
        if (!_viewCone){
            return;
        }
        if (_viewCone->getNodeMask() == 0){
            _viewCone->setNodeMask(0XFFFFFFFF);
        }
        if(!_updated){
            _updated = true;
        }

        osg::Matrix cameraWorldMatrix = osg::Matrix::rotate(q) * osg::Matrix::translate(pos);

        osg::Matrix adjust = osg::Matrix::rotate(3.0*osg::PI_2, osg::Y_AXIS,
                                                 1.0*osg::PI_2, osg::X_AXIS,
                                                 0.0*osg::PI_2, osg::Z_AXIS);

        _viewCone->setMatrix(adjust * cameraWorldMatrix);

        updateTrack(pos);
    }

    void CameraTrackElement::updateCameraPose(const osg::Matrix& mat)
    {
        if (!_viewCone){
            return;
        }
        if (_viewCone->getNodeMask() == 0){
            _viewCone->setNodeMask(0XFFFFFFFF);
        }
        if(!_updated){
            _updated = true;
        }

        osg::Matrix adjust = osg::Matrix::rotate(3.0*osg::PI_2, osg::Y_AXIS,
                                                 1.0*osg::PI_2, osg::X_AXIS,
                                                 0.0*osg::PI_2, osg::Z_AXIS);
        adjust *= osg::Matrix::translate(osg::Vec3(0.0, 0.0, 1.0));
        _viewCone->setMatrix(adjust * mat);

        updateTrack(mat.getTrans());
    }

    void CameraTrackElement::frushCamera()
    {
        osg::Matrix proj = osg::Matrix::perspective(_fovy, _aspect, _zn, _zf);
        osg::Matrix mvp = proj;
        osg::Matrix invMVP = osg::Matrix::inverse(mvp);
        /*
            7_____6
            /|   /|
          4/_|_5/_|
           | /3 | /2
           |/___|/
           0    1
        */
        osg::Vec3Array* va = static_cast<osg::Vec3Array*>(_coneGeom->getVertexArray());

        (*va)[0] = osg::Vec3(-1.0f, -1.0f, -1.0f) * invMVP;
        (*va)[1] = osg::Vec3(1.0f, -1.0f, -1.0f) * invMVP;
        (*va)[2] = osg::Vec3(1.0f, 1.0f, -1.0f) * invMVP;
        (*va)[3] = osg::Vec3(-1.0f, 1.0f, -1.0f) * invMVP;
        (*va)[4] = osg::Vec3(-1.0f, -1.0f, 1.0f) * invMVP;
        (*va)[5] = osg::Vec3(1.0f, -1.0f, 1.0f) * invMVP;
        (*va)[6] = osg::Vec3(1.0f, 1.0f, 1.0f) * invMVP;
        (*va)[7] = osg::Vec3(-1.0f, 1.0f, 1.0f) * invMVP;
        (*va)[8] = osg::Vec3(0.0f, 0.0f, 0.0f) * (proj * invMVP);

        va->dirty();
        _coneGeom->dirtyBound();
    }

    void CameraTrackElement::initCamera(bool hasFace)
    {
        osg::ref_ptr<osg::Vec3Array> va = new osg::Vec3Array(9);
        osg::ref_ptr<osg::DrawElementsUShort> linede = new osg::DrawElementsUShort(GL_LINES);
        osg::ref_ptr<osg::DrawElementsUShort> de = new osg::DrawElementsUShort(GL_TRIANGLES);
        std::vector<osg::PrimitiveSet*> pv;
        pv.push_back(linede);
        if (hasFace)
        {
            pv.push_back(de);
        }
        std::vector<osg::Vec4> cv;
        cv.push_back(osg::Vec4(255.0f / 255.0f, 100.0f / 255.0f, 100.0f / 255.0f, 1.0f));
        cv.push_back(osg::Vec4(237.0f / 255.0f, 145.0f / 255.0f, 140.0f / 255.0f, 0.3f));
        osg::ref_ptr<osg::Geometry> geom = createGeometry(va.get(), NULL, NULL, pv,
                                                          cv, false, true);
        geom->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
        geom->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
        geom->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
        /*
            7_____6
            /|   /|
          4/_|_5/_|
           | /3 | /2
           |/___|/
           0    1
        */
        // lines
        linede->push_back(0); linede->push_back(1); linede->push_back(1); linede->push_back(2);
        linede->push_back(2); linede->push_back(3); linede->push_back(3); linede->push_back(0);
        linede->push_back(0); linede->push_back(4); linede->push_back(1); linede->push_back(5);
        linede->push_back(2); linede->push_back(6); linede->push_back(3); linede->push_back(7);
        linede->push_back(4); linede->push_back(5); linede->push_back(5); linede->push_back(6);
        linede->push_back(6); linede->push_back(7); linede->push_back(7); linede->push_back(4);
        linede->push_back(8); linede->push_back(0); linede->push_back(8); linede->push_back(1);
        linede->push_back(8); linede->push_back(2); linede->push_back(8); linede->push_back(3);
        //
        // traingles
        //      de->push_back(0); de->push_back(2); de->push_back(1);
        //      de->push_back(0); de->push_back(2); de->push_back(3);

        de->push_back(0); de->push_back(5); de->push_back(4);
        de->push_back(0); de->push_back(1); de->push_back(5);

        de->push_back(1); de->push_back(2); de->push_back(5);
        de->push_back(2); de->push_back(6); de->push_back(5);

        de->push_back(2); de->push_back(3); de->push_back(6);
        de->push_back(3); de->push_back(7); de->push_back(6);

        de->push_back(3); de->push_back(0); de->push_back(4);
        de->push_back(3); de->push_back(4); de->push_back(7);

        de->push_back(8); de->push_back(0); de->push_back(3);
        de->push_back(8); de->push_back(1); de->push_back(0);
        de->push_back(8); de->push_back(2); de->push_back(1);
        de->push_back(8); de->push_back(3); de->push_back(2);

        // top face
        //      de->push_back(4); de->push_back(5); de->push_back(6);
        //      de->push_back(4); de->push_back(6); de->push_back(7);

        osg::ref_ptr<osg::LineWidth> LineSize = new osg::LineWidth;
        LineSize->setWidth(3.0f);
        geom->getOrCreateStateSet()->setAttributeAndModes(LineSize.get(), osg::StateAttribute::ON);
        geom->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);

        osg::ref_ptr<osg::MatrixTransform> trans = new osg::MatrixTransform;

        trans->addChild( createGeode(geom.get()) );
        trans->setNodeMask( 0 );

        _viewCone = trans;
        _coneGeom = geom;

        // /home/eclipse/code/osg/OpenSceneGraph-Data/axes.osgt
        _camera = osgDB::readNodeFile("res/camera.ive");
        if(_camera)
        {
            _viewCone->addChild(_camera);
        }
        else
        {
            osg::Geode* geode = new osg::Geode;
            osg::Capsule* sphere = new osg::Capsule(osg::Vec3(0.0, 0.0, 0.0), 0.5, 1.5);
            geode->addDrawable(new osg::ShapeDrawable(sphere));
            _viewCone->addChild(geode);
            _camera = geode;
            // _viewCone->addChild(_camera);
        }
        addChild(_viewCone);
    }

    osg::Geometry* CameraTrackElement::createGeometry( osg::Vec3Array* va, osg::Vec3Array* na, osg::Vec2Array* ta,
                                    std::vector<osg::PrimitiveSet*> p,
                                    std::vector<osg::Vec4>const& cv,
                                    bool autoNormals, bool useVBO )
    {
        if ( !va )
        {
            OSG_NOTICE << "createGeometry: invalid parameters" << std::endl;
            return NULL;
        }

        osg::ref_ptr<osg::Vec4Array> ca = new osg::Vec4Array;
        for ( unsigned int i = 0; i < cv.size(); i ++ )
        {
            ca->push_back( cv[i] );
        }
        //(*ca)[0] = osg::Vec4(c[0], c[1], c[2], 1.0f);

        osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
        geom->setVertexArray( va );
        geom->setTexCoordArray( 0, ta );
        geom->setColorArray( ca.get() );
        geom->setColorBinding( osg::Geometry::BIND_PER_PRIMITIVE_SET );

        for ( std::vector<osg::PrimitiveSet*>::iterator it = p.begin(); it != p.end(); it ++ )
        {
            geom->addPrimitiveSet( (*it) );
        }

        if ( useVBO )
        {
            geom->setUseDisplayList( false );
            geom->setUseVertexBufferObjects( true );
        }

        if ( na )
        {
            unsigned int normalSize = na->size();
            if ( normalSize==va->size() )
            {
                geom->setNormalArray( na );
                geom->setNormalBinding( osg::Geometry::BIND_PER_VERTEX );
            }
            else if ( normalSize==1 )
            {
                geom->setNormalArray( na );
                geom->setNormalBinding( osg::Geometry::BIND_OVERALL );
            }
        }
        else if ( autoNormals )
            osgUtil::SmoothingVisitor::smooth( *geom );
        return geom.release();
    }

    osg::Geode* CameraTrackElement::createGeode( osg::Drawable* drawable, bool transparent )
    {
        osg::ref_ptr<osg::Geode> geode = new osg::Geode;
        geode->addDrawable( drawable );
        if ( transparent )
        {
            geode->getOrCreateStateSet()->setMode( GL_BLEND, osg::StateAttribute::ON );
            geode->getOrCreateStateSet()->setRenderingHint( osg::StateSet::TRANSPARENT_BIN );
        }
        return geode.release();
    }

    void CameraTrackElement::initTrack()
    {
        osg::ref_ptr<osg::Geode> geode = new osg::Geode;
        osg::ref_ptr<osg::Geometry> geome = new osg::Geometry;
        osg::ref_ptr<osg::Vec3Array> vertex = new osg::Vec3Array;
        osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;

        _trackLineColor.set(0.0, 1.0, 0.0, 1.0);
        colors->push_back(_trackLineColor);

        geome->setVertexArray(vertex);
        geome->setColorArray(colors);
        geome->addPrimitiveSet(new osg::DrawArrays(GL_LINE_STRIP, 0, 0));
        geome->setColorBinding(osg::Geometry::BIND_OVERALL);
        geome->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
        geome->getOrCreateStateSet()->setAttributeAndModes(new osg::LineWidth(3));

        geode->addDrawable(geome);

        _trackGeome = geome;
        addChild(geode);
        // _sceneRender->getScene()->getRoot()->addChild(geode);

    }

    void CameraTrackElement::updateTrack(osg::Vec3 pos)
    {
        osg::Vec3Array* vertex;
        vertex = dynamic_cast<osg::Vec3Array*>(_trackGeome->getVertexArray());
        float push = false;
        if(!vertex->empty())
        {
            push = (vertex->at(vertex->size()-1) - pos).length() > 0.05;
        }
        else
        {
            push = true;
        }

        if(push)
        {
            vertex->push_back(pos);

            vertex->dirty();
            _trackGeome->setPrimitiveSet(0, new osg::DrawArrays(GL_LINE_STRIP, 0, vertex->size()));
            _trackGeome->dirtyBound();
        }
    }

    void CameraTrackElement::setTrackLineColor(osg::Vec4 color)
    {
        _trackLineColor = color;
        osg::Vec4Array* ac = dynamic_cast<osg::Vec4Array*>(_trackGeome->getColorArray());
        if(ac)
        {
            ac->clear();
            ac->push_back(color);
            ac->dirty();
        }
    }

    void CameraTrackElement::setShowTrack(bool show)
    {
        if(show)
        {
            _trackGeome->setNodeMask(0xFFFFFFFF);
        }
        else
        {
            _trackGeome->setNodeMask(0);
        }
    }

    void CameraTrackElement::clearTrack()
    {
        // return;
        osg::Vec3Array* vertex;
        vertex = dynamic_cast<osg::Vec3Array*>(_trackGeome->getVertexArray());
        if(vertex != NULL)
        {
            _trackGeome->setPrimitiveSet(0, new osg::DrawArrays(GL_LINE_STRIP, 0, 0));
            vertex->clear();
            _trackGeome->dirtyBound();
            // vertex->dirty();
        }
    }
}
