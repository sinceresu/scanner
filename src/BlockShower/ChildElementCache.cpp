#include "ChildElementCache.h"
#include <thread>

namespace BlockShower
{
    ChildElementCache::ChildElementCache()
    {
        
    }
    ChildElementCache::~ChildElementCache()
    {
        
    }

    void ChildElementCache::add( std::string const& id, osg::Node *lpElement )
    {
        CacheItem *lpci = new CacheItem;
        lpci->_element = lpElement;
        // 检查300次后，如果没有再新加进来，就释放
        lpci->_count = 100;
        std::lock_guard<std::mutex> l(_cacheMutex);
        _cacheItems[id] = lpci;
    }
    osg::Node *ChildElementCache::get( std::string const& id )
    {
        std::lock_guard<std::mutex> l(_cacheMutex);
        auto it = _cacheItems.find( id );
        if ( it == _cacheItems.end() )
        {
            return NULL;
        }
        return it->second->_element.get();
    }

    void ChildElementCache::checkToFree()
    {
        if ( _running )
        {
            return;
        }
        _running = true;
        std::thread t([this](){

                          std::lock_guard<std::mutex> l(this->_cacheMutex);
                          for ( auto it = this->_cacheItems.begin(); it != this->_cacheItems.end(); )
                          {
                              if ( it->second->_count > 0 )
                              {
                                  it->second->_count --;
                              }else
                              {
                                  it = _cacheItems.erase( it );
                              }
                          }
                          this->_running = false;
                      });

        t.detach();
    }
    void ChildElementCache::clear()
    {
        _running = true;
        //std::thread t([this](){

                          std::lock_guard<std::mutex> l(this->_cacheMutex);
                          for ( auto it = this->_cacheItems.begin(); it != this->_cacheItems.end(); )
                          {
                              it = _cacheItems.erase( it );
                          }
                          //});

                          //t.detach();
    }

}
