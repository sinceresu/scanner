#ifndef __SCANNER_BLOCK_CHAIN_STACK_H_FILE__
#define __SCANNER_BLOCK_CHAIN_STACK_H_FILE__
#include "BlockBody.h"
#include <osg/ref_ptr>
#include <stack>

namespace BlockShower
{
    class ChainStack
    {
    public:

        struct Item
        {
            Item( BlockChain *lpChain = NULL, BlockChain *lpStart = NULL, BlockChain *lpRefer = NULL )
                {
                    _lpChain = lpChain;
                    _lpStart = lpStart;
                    _lpRefer = lpRefer;
                }
            osg::ref_ptr<BlockChain> _lpChain;
            osg::ref_ptr<BlockChain> _lpStart;
            osg::ref_ptr<BlockChain> _lpRefer;
        };

        void push( BlockChain *lpChain, BlockChain *lpStart, BlockChain *lpRefer );
        Item pop();
        bool empty()const;

    protected:
        typedef std::stack<Item> Items;
        Items _items;

    };

}
#endif // __SCANNER_BLOCK_CHAIN_STACE_STACK_H_FILE__
