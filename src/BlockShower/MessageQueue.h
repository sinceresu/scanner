
#ifndef NET_FRAME_MESSAGE_QUEUE_H
#define NET_FRAME_MESSAGE_QUEUE_H
 
#include <queue>
#include <mutex>
#include <condition_variable>
namespace BlockShower
{
    //消息队列实现
    template<class Type>
    class MessageQueue
    {
        MessageQueue& operator=(const MessageQueue&) = delete;
        MessageQueue(const MessageQueue& other) = delete;
    public:
        MessageQueue() : _queue(), _mutex(), _condition() { }

        virtual ~MessageQueue() { }

        void Push(Type msg)
        {
            std::lock_guard <std::mutex> lock(_mutex);
            _queue.push(msg);
            //每增加一个元素，用条件变量进行通知。
            _condition.notify_one();
        }

        bool Pop(Type& msg, bool isBlocked = true)
        {
            if (isBlocked)
            {
                std::unique_lock <std::mutex> lock(_mutex);
                while (_queue.empty())
                {
                    _condition.wait(lock);
                }
                //注意这一段必须放在if语句中，因为lock的生命域仅仅在if大括号内
                msg = std::move(_queue.front());
                _queue.pop();
                return true;
            }
            else
            {
                std::lock_guard<std::mutex> lock(_mutex);
                if (_queue.empty())
                {
                    return false;
                }

                msg = std::move(_queue.front());
                _queue.pop();
                return true;
            }

        }

        int32_t Size()
        {
            std::lock_guard <std::mutex> lock(_mutex);
            return _queue.size();
        }

        bool Empty()
        {
            std::lock_guard <std::mutex> lock(_mutex);
            return _queue.empty();
        }

    private:
        std::queue<Type> _queue;
        mutable std::mutex _mutex;
        std::condition_variable _condition;
    };
}
 
#endif //NET_FRAME_MESSAGE_QUEUE_H

