#include <BlockShower/AreaElement.h>
#include <ydUtil/DataReadWriter.h>
#include <osg/Geometry>
#include <osg/Geode>
#include <ydUtil/StringUtils.h>
#include "Config.h"

namespace BlockShower
{
    ELEMENT_FACTORY(AreaElement);
    AreaElement::AreaElement()
    {
        setName( "AreaElement" );

        _px = 0.0f;
        _py = 0.0f;
        _pz = 0.0f;

        _faceColor = osg::Vec4( 1.0, 1.0, 1.0, 0.5 );
        _lineColor = osg::Vec4(0.772f, 0.3529f, 0.067f, 1.0f);
        _height = 3;

        std::vector<osg::Vec3> range;
        range.push_back( osg::Vec3 (-40.0, -40.0, 0.0 ) );
        range.push_back( osg::Vec3 (40.0, -40.0, 0.0 ) );
        range.push_back( osg::Vec3 (40.0, 40.0, 0.0 ) );
        range.push_back( osg::Vec3 (-40.0, 40.0, 0.0 ) );
        range.push_back( osg::Vec3 (-40.0, -40.0, 0.0 ) );

        setRange( range );

        getOrCreateStateSet()->setMode( GL_LIGHTING, false );

    }

    AreaElement::~AreaElement()
    {

    }


    void AreaElement::load(bool inThread)
    {

        getOrCreateStateSet()->setRenderBinDetails(100, "RenderBin");
        osg::Geode *lpGeode = new osg::Geode;
        {
            osg::Geometry *lpGeometry = new osg::Geometry;

            osg::Vec3Array *lpArray = new osg::Vec3Array;
            osg::Vec4Array *lpColorArray = new osg::Vec4Array;
            lpColorArray->push_back( _lineColor );

            osg::Vec3 center( _px, _py, _pz + Z_OFFSET );
            for ( auto i : _range )
            {
                lpArray->push_back( i + center );
            }
            lpGeometry->addPrimitiveSet( new osg::DrawArrays( osg::PrimitiveSet::LINE_LOOP, 0, lpArray->size() ) );

            int cs = lpArray->size();
            for ( auto i : _range )
            {
                lpArray->push_back( i + osg::Vec3( 0.0, 0.0, _height ) + center );
            }
            lpGeometry->addPrimitiveSet( new osg::DrawArrays( osg::PrimitiveSet::LINE_LOOP, cs, lpArray->size() - cs ) );

            cs = lpArray->size();
            for ( auto i : _range )
            {
                lpArray->push_back( i + osg::Vec3( 0.0, 0.0, Z_OFFSET ) );
                i.z() += _height;
                lpArray->push_back( i + osg::Vec3( 0.0, 0.0, Z_OFFSET ) );

            }

            lpGeometry->addPrimitiveSet( new osg::DrawArrays( osg::PrimitiveSet::LINES, cs, lpArray->size() - cs ) );

            lpGeometry->setVertexArray( lpArray );
            lpGeometry->setColorArray( lpColorArray, osg::Array::BIND_OVERALL );

            lpGeode->addDrawable( lpGeometry );

        }

        {
            osg::Geometry *lpGeometry = new osg::Geometry;

            osg::Vec3Array *lpArray = new osg::Vec3Array;
            osg::Vec4Array *lpColorArray = new osg::Vec4Array;
            lpColorArray->push_back( _faceColor );

            for ( auto i : _range )
            {
                lpArray->push_back( i + osg::Vec3( 0.0, 0.0, Z_OFFSET ) );
                i.z() += _height;
                lpArray->push_back( i + osg::Vec3( 0.0, 0.0, Z_OFFSET ) );

            }

            if ( _faceColor.a() < 1.0 )
            {
                lpGeometry->getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
                lpGeometry->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
            }

            lpGeometry->addPrimitiveSet( new osg::DrawArrays( osg::PrimitiveSet::TRIANGLE_STRIP, 0, lpArray->size() ) );

            lpGeometry->setVertexArray( lpArray );
            lpGeometry->setColorArray( lpColorArray, osg::Array::BIND_OVERALL );

            lpGeode->addDrawable( lpGeometry );

        }



        if ( inThread )
        {
            osg::ref_ptr<osg::Node> refNode = lpGeode;
            _afterLoadFun = [this, refNode ](){
                                {
                                    this->removeChildren( 0, this->getNumChildren() );
                                    this->addChild( refNode );
                                }
                            };
        }else
        {
            this->removeChildren( 0, this->getNumChildren() );
            addChild( lpGeode );
        }

    }
    void AreaElement::load(std::istream& in)
    {
        ydBase::Element::load ( in );
        ydUtil::DataReadWriter drw;

        drw.read( in, _px );
        drw.read( in, _py );
        drw.read( in, _pz );

        int rc = 0;
        drw.read ( in, rc );
        _range.clear();
        for ( int i = 0; i < rc; i ++ )
        {
            osg::Vec3 p;
            drw.read( in, p.x() );
            drw.read( in, p.y() );
            drw.read( in, p.z() );
            _range.push_back( p );
        }

        drw.read( in, _lineColor.x() );
        drw.read( in, _lineColor.y() );
        drw.read( in, _lineColor.z() );
        drw.read( in, _lineColor.w() );

        drw.read( in, _height );

        load( _loadFun.valid() );

    }
    void AreaElement::save(std::ostream& out)
    {
        ydBase::Element::save ( out );
        ydUtil::DataReadWriter drw;

        drw.write( out, _px );
        drw.write( out, _py );
        drw.write( out, _pz );

        int rc = _range.size();
        drw.write ( out, rc );
        for ( int i = 0; i < rc; i ++ )
        {
            osg::Vec3 p = _range[i];
            drw.write( out, p.x() );
            drw.write( out, p.y() );
            drw.write( out, p.z() );
        }

        drw.write( out, _lineColor.x() );
        drw.write( out, _lineColor.y() );
        drw.write( out, _lineColor.z() );
        drw.write( out, _lineColor.w() );

        drw.write( out, _height );
    }

    void AreaElement::setPosition( float x, float y, float z )
    {
        _px = x;
        _py = y;
        _pz = z;
        //setMatrix( osg::Matrix::translate( osg::Vec3( _px, _py, _pz ) ) );
        if ( getNumChildren() > 0 )
        {
            load( false );
        }
    }

    void AreaElement::setRange( std::vector<osg::Vec3> const& pointLine )
    {
        _rangeBB = osg::BoundingBox();
        _range = pointLine;
        for ( auto i : pointLine )
        {
            _rangeBB.expandBy( i );
        }
        if ( getNumChildren() > 0 )
        {
            load( false );
        }
    }
    void AreaElement::setFaceColor( osg::Vec4 const& c )
    {
        _faceColor = c;
        if ( getNumChildren() > 0 )
        {
            load( false );
        }
    }
    void AreaElement::setLineColor( osg::Vec4 const& c )
    {
        _lineColor = c;
        if ( getNumChildren() > 0 )
        {
            load( false );
        }
    }
    void AreaElement::setHeight( float v )
    {
        _height = v;
        if ( getNumChildren() > 0 )
        {
            load( false );
        }
    }
    void AreaElement::setVec2Range( osg::Vec2 const& s )
    {
        std::vector<osg::Vec3> range;
        range.push_back( osg::Vec3 (-s.x() / 2, -s.y() / 2, 0.0 ) );
        range.push_back( osg::Vec3 (s.x() / 2, -s.y() / 2, 0.0 ) );
        range.push_back( osg::Vec3 (s.x() / 2, s.y() / 2, 0.0 ) );
        range.push_back( osg::Vec3 (-s.x() / 2, s.y() / 2, 0.0 ) );
        range.push_back( osg::Vec3 (-s.x() / 2, -s.y() / 2, 0.0 ) );

        setRange( range );
    }
    osg::Vec2 AreaElement::getVec2Range()const
    {
        osg::Vec2 s( _rangeBB.xMax() - _rangeBB.xMin(), _rangeBB.yMax() - _rangeBB.yMin() );
        return s;
    }
    osg::Vec3 AreaElement::getBasePoint()
    {
        return _rangeBB._min;
    }
    void AreaElement::initFromParam( ydUtil::Param *lpParam )
    {
        ydUtil::MapParam *lpMapParam = dynamic_cast<ydUtil::MapParam *>(lpParam);
        {
            std::vector<std::string> rl = ydUtil::split(lpMapParam->getValue("range"), ",");
            std::vector<osg::Vec3> pointList;
            for( auto r : rl )
            {
                std::vector<std::string> pl = ydUtil::split( r, " " );
                if ( pl.size() < 3 )
                {
                    continue;
                }
                osg::Vec3 p(atof( pl[0].c_str() ), atof( pl[1].c_str()), atof( pl[2].c_str() ) );
                pointList.push_back( p );
            }
            setRange( pointList );
        }

        {
            std::vector<std::string> vl = ydUtil::split( lpMapParam->getValue("position"), " ");
            if ( vl.size() >= 3 )
            {

                setPosition(atof( vl[0].c_str() ), atof( vl[1].c_str()), atof( vl[2].c_str() ));
            }
        }
        {
            std::vector<std::string> vl = ydUtil::split( lpMapParam->getValue("facecolor"), " ");
            if ( vl.size() >= 4 )
            {
                osg::Vec4 p(atof( vl[0].c_str() ), atof( vl[1].c_str()), atof( vl[2].c_str() ), atof( vl[3].c_str() ) );

                setFaceColor( p );
            }
        }
        {
            std::vector<std::string> vl = ydUtil::split( lpMapParam->getValue("linecolor"), " ");
            if ( vl.size() >= 4 )
            {
                osg::Vec4 p(atof( vl[0].c_str() ), atof( vl[1].c_str()), atof( vl[2].c_str() ), atof( vl[3].c_str() ) );

                setLineColor( p );
            }
        }
        {
            float h = atof( lpMapParam->getValue( "height" ).c_str() );
            if ( h > 0.01f )
            {
                setHeight( h );
            }
        }


    }
}
