#ifndef __BLOCK_SHOWER_MOUSE_EVENT_HANDLER_H_FILE__
#define __BLOCK_SHOWER_MOUSE_EVENT_HANDLER_H_FILE__
#include <osg/Referenced>
#include <osg/ref_ptr>
#include <osgGA/GUIEventHandler>
#include <functional>
#include <osgViewer/Viewer>
#include <osg/MatrixTransform>
#include <BlockShower/BlockShower.h>
#include <osg/Timer>

namespace BlockShower
{
    class MouseOperator : public osg::Referenced
    {
    public:
        using Callback = std::function<void( unsigned int mo, std::vector<osg::Vec3> const& )>;
        MouseOperator( Callback fun )
        {
            _fun = fun;
        }
        virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
        {
            return true;
        }

        bool over(){ return _over; };

        bool _over;

        Callback _fun;

    };

    class MouseEventHandler : public osgGA::GUIEventHandler
    {
    public:
        enum MO
        {
            MO_PICK = 1,
            MO_LINE = 2,
            MO_TRIANGLE = 3,
            MO_POLYGON = 4,
        };
        MouseEventHandler();
        ~MouseEventHandler();
        virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);

        void setMouseOperator( unsigned int t,  MouseOperator fun );
        void setPlaneElement(osg::MatrixTransform* element);

        void setDrawArrow(bool arrow);
        void setMouseEventCallback(BlockShower::CallBack cb) { _callback = cb; }
        void setArrowCallBack(BlockShower::ArrowCallBack acb) { _arrowCallback = acb; }

    protected:
        bool testHitModel(osgViewer::View* view, float nx, float ny, osg::Vec3& pos);
        void callBack(osg::Vec3 pos, float x, float y, int mask, bool dbClick);

    protected:
        osg::ref_ptr<MouseOperator> _mouseOperator;
        osg::ref_ptr<osg::MatrixTransform> _hitElement;
        osg::ref_ptr<osg::MatrixTransform> _arrow;
        osg::ref_ptr<osg::MatrixTransform> _round;

        float _preX;
        float _preY;
        int _mouseMask;
        
        bool _drawArrow;
        bool _hasRelease;

        osg::Vec3 _prePos;
        osg::Vec3 _startPos;
        osg::Timer _timer;
        BlockShower::CallBack _callback;
        BlockShower::ArrowCallBack _arrowCallback;
    };

}


#endif // __BLOCK_SHOWER_MOUSE_EVENT_HANDLER_H_FILE__
