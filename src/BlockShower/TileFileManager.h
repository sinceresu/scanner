#ifndef __SRC_BLOCK_SHOWER_TILE_FILE_MANAGER_H_FILE__
#define __SRC_BLOCK_SHOWER_TILE_FILE_MANAGER_H_FILE__
#include <ydUtil/singleton.h>
#include <osg/Referenced>
#include <osg/ref_ptr>
#include <vector>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <functional>
#include <map>
#include <osg/Node>
#include <ydUtil/Block.h>
#include "GridBoxInfo.h"
#include <ydBase/Element.h>
#include <functional>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace BlockShower
{

    class FileProxy : public osg::Referenced
    {
    public:
        friend class TileFileManager;
        using FreeCallback = std::function<void( osg::Referenced *lpFile )>;
        FileProxy( osg::Referenced *lpFile, FreeCallback callback );

        void active(){ _frameCount ++; };

        void operator()(){
            if ( _freeCallback )
            {
                _freeCallback( _lpFile.get() );
            }
        }

        bool check();
    protected:
        FreeCallback _freeCallback;
        osg::ref_ptr<osg::Referenced> _lpFile;
        int _frameCount;
    };

    class TileFileManager : public ydBase::Element
    {
    public:
        typedef std::map<ydUtil::BlockIndex, osg::ref_ptr<GridBoxInfo> > GridBlockInfoMap;
        using MapToWorlFun = std::function< osg::Vec3 ( osg::Vec3 const& )>;
        TileFileManager( MapToWorlFun fun );
        ~TileFileManager();
    public:
        void setChildSize( osg::Vec3 const& childSize ){_childSize = childSize;};
        virtual void load( bool inThread ){};
        void addFile( std::string const& key, osg::Referenced *lpNode, FileProxy::FreeCallback cb );
        void stop();

        void activeOnce();

        void load(std::string const& sceneID, std::string const& filePath);
        osg::BoundingBox setCloudPoint( pcl::PointCloud<pcl::PointXYZI>const* cloud );

        GridBlockInfoMap getGridBoxInfo();

        osg::ref_ptr<GridBoxInfo> getGridBoxInfo( std::string const& key );//, int level, ydUtil::BlockIndex bi );
    protected:
        void dealThread();

        GridBoxInfo *getChild( ydUtil::BlockIndex const& localIndex );
    protected:
        typedef std::map< std::string, osg::ref_ptr<FileProxy> > FileProxyMap;
        FileProxyMap _fileProxy;

        osg::Vec3 _childSize;

        std::mutex _childMapMutex;
        GridBlockInfoMap _gridBlockInfoMap;
        std::mutex _fileProxyMutex;

        std::thread _dealThread;


        std::condition_variable _dealContidion;
        bool _running;
        MapToWorlFun _mapToWorldFun;

        osg::BoundingBox _validBoundBox;


    };
}


#endif // __SRC_BLOCK_SHOWER_TILE_FILE_MANAGER_H_FILE__
