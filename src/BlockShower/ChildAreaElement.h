#ifndef __BLOCKSHOWER_CHILD_AREA_ELEMENT_H_FILE__
#define __BLOCKSHOWER_CHILD_AREA_ELEMENT_H_FILE__
#include <BlockShower/export.h>
#include <ydBase/Element.h>
#include <osg/Switch>
#include <osg/ref_ptr>
#include <ydUtil/Block.h>
#include <mutex>
#include "BlockBody.h"
#include "LevelSize.h"
#include "GridBoxInfo.h"
#include "BoxElement.h"

namespace BlockShower
{
    class TileFileManager;
    class ChildAreaElement : public BoxElement
    {
    public:
        enum ShapeMode
        {
            SM_BLOCK = 0,
            SM_POINT
        };

        enum ColorMode
        {
            CM_ORIGIN = 0,
            CM_HEIGHT,
            CM_INTENSITY
        };

        META_Info(ChildAreaElement);
        ChildAreaElement( bool finish = false, int level = 0, int step = 1 );
        ~ChildAreaElement();

        virtual void load(bool inThread);
        void load2(bool inThread, TileFileManager *lpTileFileManager );

        static void setColorMode( unsigned int cm );
        static unsigned int getColorMode(){ return _colorMode; };

        static void setShapeMode( unsigned int sm );
        static unsigned int getShapeMode(){ return _shapeMode; };

        void setHeight( float v );
        float getHeight()const{ return _height; };

        void setFaceColor( osg::Vec4 const& c = osg::Vec4( 1.0, 1.0, 1.0, 0.5 ) );
        osg::Vec4 getFaceColor()const{ return _faceColor; };

        void setLineColor( osg::Vec4 const& c = osg::Vec4( 0.772f, 0.3529f, 0.067f, 1.0f ) );
        osg::Vec4 getLineColor()const{ return _lineColor; };

        void setPointSize( float ps );
        float getPointSize()const{return _pointSize;};

        ////////////////////////////////////////////////////////////////////////////
        // call by osg
        void traverse(osg::NodeVisitor& nv);

        void checkChild( int width, int height, float slope, osg::Polytope & frustum, osg::Vec3 const& cameraPos, TileFileManager *lpTileFileManager );

        void setupShader( );

        struct GridNode : public GridBoxInfo
        {
            GridNode( int level, int step );
            osg::ref_ptr<osg::Group> _gridRoot;
            osg::ref_ptr<osg::Node> _gridNode; // 当前节点的网格
            // 子节点
            osg::ref_ptr<osg::Node> _childNode;
        };
        std::map<ydUtil::BlockIndex, osg::ref_ptr<GridNode> > getChildMap(){ return _childMap; };

    protected:


        void showChild(GridNode *lpInfo, osg::BoundingBox const& childBB, TileFileManager *lpTileFileManager );
        void showSelf(GridNode *lpInfo, bool dirty, GridBoxInfo *lpMemoryBoxInfo);

        GridNode *getGridInfo( ydUtil::BlockIndex const& childIndex, osg::Vec3 const& globalCenter );


        std::string getFilePath( ydUtil::BlockIndex const& index, int level );

        void initGridBoxInfo( ydUtil::BlockIndex const &bi, BoxElement *lpInfo );

    protected:

        std::map<ydUtil::BlockIndex, osg::ref_ptr<GridNode> > _childMap;

        static unsigned int _colorMode;
        static unsigned int _shapeMode;

        float _height;
        float _pointSize;

        osg::Vec4 _lineColor;
        osg::Vec4 _faceColor;

        std::mutex _blockBodyMutex;
        // std::mutex _mutex;

        int _childIndex;

        static LevelSize _levelSize;

    };

}


#endif // __BLOCKSHOWER_CHILD_AREA_ELEMENT_H_FILE__
