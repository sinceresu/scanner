#include "BlockShower/ScanAreaElement.h"
#include "ChildAreaElement.h"
#include <ydBlockDB/BlockDB.h>
#include <ydUtil/log.h>
#include <ydUtil/console.h>
#include <osg/io_utils>
#include <ydUtil/log.h>
#include "BlockBody.h"
#include "TileFileManager.h"
#include "Config.h"
#include "CheckChildVisitor.h"

namespace BlockShower
{
    ScanAreaElement::ScanAreaElement( osg::Vec3 const &childSize )
    {
        TileFileManager *lpTFM = new TileFileManager([this](osg::Vec3 const pos ){
                                                   return this->mapToWorld( pos );
                                               });
        _tileFileManager = lpTFM;
        lpTFM->setChildSize( childSize );

        _validBoundBox.expandBy( osg::Vec3( 1, 1, 1 ));
        _childAreaElementDirty = false;
        _childSize = childSize;
        _height = -0xfffffff;
    }

    ScanAreaElement::~ScanAreaElement()
    {
        _tileFileManager = NULL;
    }

    void ScanAreaElement::load(bool inThread)
    {
        // 初始化时什么也不做
    }


    void ScanAreaElement::setStartPose( float x, float y, float z, float qx, float qy, float qz, float qw )
    {
        _localToWorld = osg::Matrix::rotate(osg::Quat(qx, qy, qz, qw)) * osg::Matrix::translate(osg::Vec3(x, y, z));
    }
    // ChildAreaElement *ScanAreaElement::getChild( ydUtil::BlockIndex const& localIndex )
    // {
    //     std::string id = get("id");
    //     std::lock_guard<std::mutex> l( _childMapMutex );

    //     ChildAreaElement *lpChild = NULL;

    //     auto it = _childMap.find( localIndex );

    //     if ( it == _childMap.end() )
    //     {
    //         osg::Vec3 childCenter = ydUtil::getPos( osg::Vec3( 0.0, 0.0, 0.0 ), localIndex, _childSize );
    //         osg::BoundingBox childBB( childCenter - _childSize / 2, childCenter + _childSize / 2 );
    //         // 找不到就创建
    //         // 取step为ROOT_0_STEP, 表示最多有7层, 在第0层,每一个格子的大小为ROOT_0_STEP * 显示精度, 比如显示精度为0.02, 则在第0层一个格子大小是2.56米.
    //         // 在距离较远时用大块显示也够了.
    //         lpChild = new ChildAreaElement( false, 0, ROOT_0_STEP );
    //         lpChild->set( "scanid", id );
    //         lpChild->set("cachepath", get("cachepath"));
    //         lpChild->set("blockindex", localIndex.toString());
    //         lpChild->setRange( childBB );
    //         lpChild->load( false );
    //         _childMap[localIndex] = lpChild;
    //         //TRACE("Create %d,%d,%d\n", localIndex._x, localIndex._y, localIndex._z );
    //     }else
    //     {
    //         lpChild = dynamic_cast<ChildAreaElement*>(it->second.get());
    //     }
    //     return lpChild;
    // }

    void ScanAreaElement::setCloudPoint(pcl::PointCloud<pcl::PointXYZI>const* cloud)
    {
        if ( !cloud )
        {
            return;
        }
        ydUtil::TimeTrace tt;
        TileFileManager *lpTFM = dynamic_cast<TileFileManager *>(_tileFileManager.get());
        lpTFM->set("scanid", get("id"));
        lpTFM->set("cachepath", get("cachepath"));

        osg::BoundingBox cloudBB = lpTFM->setCloudPoint( cloud );
        if ( cloudBB != _validBoundBox )
        {
            _childAreaElementDirty = true;
            _validBoundBox = cloudBB;
        }
        else
        {
            _childAreaElementDirty = false;
        }
        return;
    }


    osg::Vec3 ScanAreaElement::mapToWorld( osg::Vec3 const& pos )
    {
        return pos * _localToWorld;
    }
    std::string getGridKey( int level, ydUtil::BlockIndex const& bi );
    void ScanAreaElement::checkChild( int width, int height, float scope, osg::Polytope & frustum, osg::Vec3 const& cameraPos )
    {
        std::string id = get("id");

        TileFileManager *lpTileFileManager = dynamic_cast<TileFileManager*>(_tileFileManager.get() );
        // 检查某个子块是否存在对应的文件，如果存在，说明里面有数据，要加载进内存
        for (float x = _validBoundBox.xMin(); x <= _validBoundBox.xMax(); x += _childSize.x() )
        {
            for (float y = _validBoundBox.yMin(); y <= _validBoundBox.yMax(); y += _childSize.y() )
            {
                for (float z = _validBoundBox.zMin(); z <= _validBoundBox.zMax(); z += _childSize.z() )
                {
                    ydUtil::BlockIndex localIndex = ydUtil::getBlockIndex( osg::Vec3( 0.0, 0.0, 0.0 ), _childSize, osg::Vec3( x, y, z ) );
                    if ( _childMap.find( localIndex ) != _childMap.end() )
                    {
                        continue;
                    }

                    osg::ref_ptr<GridBoxInfo> memorygn;
                    if ( lpTileFileManager )
                    {
                        memorygn = lpTileFileManager->getGridBoxInfo( getGridKey( 0, localIndex ) );
                    }

                    std::string dbFile = getFilePath(get("cachepath"), localIndex, 0 );

                    // 检测文件是否存在, 一般来说只有增加，没有减少, 如果有减少的，也需要删除, 那么这一块就完全不显示了
                    if(!ydUtil::fileExist(dbFile.c_str()) && !memorygn )
                    {
                        removeChild( _childMap[localIndex] );
                        _childMap.erase( localIndex );
                        continue;
                    }

                    osg::Vec3 childCenter = ydUtil::getPos( osg::Vec3( 0.0, 0.0, 0.0 ), localIndex, _childSize );
                    osg::BoundingBox childBB( childCenter - _childSize / 2, childCenter + _childSize / 2 );

                    osg::ref_ptr<ChildAreaElement> lpChild = new ChildAreaElement( false, 0, ROOT_0_STEP );
                    lpChild->set("cachepath", get("cachepath"));
                    lpChild->set( "scanid", id );
                    lpChild->setRange( childBB );
                    lpChild->load2( false, lpTileFileManager );
                    _childMap[localIndex] = lpChild;
                    addChild( lpChild );
                }
            }
        }


        CheckChildVisitor ccv( dynamic_cast<TileFileManager*>(_tileFileManager.get()),
                               scope,
                               frustum, width, height, cameraPos );

        this->accept( ccv );

    }
    void ScanAreaElement::setBox(osg::Vec3 min, osg::Vec3 max)
    {
        _validBoundBox.set(min, max);
        _childAreaElementDirty = true;
    }
    std::string ScanAreaElement::getFilePath( std::string const& cachePath, ydUtil::BlockIndex const& index, int level )
    {
        std::string filePath = cachePath + "/" + std::to_string( level )
            + "_"  + index.toString() + ".cd";
        return filePath;
    }
    void ScanAreaElement::flush()
    {
        TileFileManager *lpTileFileManager = dynamic_cast<TileFileManager*>(_tileFileManager.get());
        if ( !lpTileFileManager )
        {
            return;
        }

        TileFileManager::GridBlockInfoMap gbm = lpTileFileManager->getGridBoxInfo();
        for ( auto g : gbm  )
        {
            g.second->flush();
        }
    }
}
