#include "BlockBody.h"
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Point>
#include <osg/CullFace>
#include <osg/MatrixTransform>
#include <osgUtil/SmoothingVisitor>
#include "ChainStack.h"
#include <stdlib.h>
#include <ydUtil/log.h>
#include "ChildAreaElement.h"
#include <ydUtil/DataReadWriter.h>
#include <fstream>
#include <ydUtil/console.h>
#include "TileFileManager.h"
#include <ydUtil/log.h>

namespace BlockShower
{
    void makeGridBox2(int idx,  osg::Vec3 const& minPos, osg::Vec3 const& maxPos, osg::Vec3Array *vertex, osg::DrawElementsUInt *ls, osg::DrawElementsUInt *qd )
    {
        vertex->push_back(minPos);
        vertex->push_back(osg::Vec3(maxPos.x(), minPos.y(), minPos.z()));
        vertex->push_back(osg::Vec3(maxPos.x(), maxPos.y(), minPos.z()));
        vertex->push_back(osg::Vec3(minPos.x(), maxPos.y(), minPos.z()));

        vertex->push_back(osg::Vec3(minPos.x(), minPos.y(), maxPos.z()));
        vertex->push_back(osg::Vec3(maxPos.x(), minPos.y(), maxPos.z()));
        vertex->push_back(maxPos);
        vertex->push_back(osg::Vec3(minPos.x(), maxPos.y(), maxPos.z()));


        ls->push_back(0 + idx);
        ls->push_back(1 + idx);

        ls->push_back(1 + idx);
        ls->push_back(2 + idx);

        ls->push_back(2 + idx);
        ls->push_back(3 + idx);

        ls->push_back(3 + idx);
        ls->push_back(0 + idx);

        ls->push_back(4 + idx);
        ls->push_back(5 + idx);

        ls->push_back(5 + idx);
        ls->push_back(6 + idx);

        ls->push_back(6 + idx);
        ls->push_back(7 + idx);

        ls->push_back(7 + idx);
        ls->push_back(4 + idx);
        ////

        ls->push_back(0 + idx);
        ls->push_back(4 + idx);

        ls->push_back(1 + idx);
        ls->push_back(5 + idx);

        ls->push_back(2 + idx);
        ls->push_back(6 + idx);

        ls->push_back(3 + idx);
        ls->push_back(7 + idx);
        ///

        qd->push_back(0 + idx);
        qd->push_back(1 + idx);
        qd->push_back(2 + idx);
        qd->push_back(3 + idx);

        qd->push_back(4 + idx);
        qd->push_back(5 + idx);
        qd->push_back(6 + idx);
        qd->push_back(7 + idx);

        qd->push_back(0 + idx);
        qd->push_back(1 + idx);
        qd->push_back(5 + idx);
        qd->push_back(4 + idx);

        qd->push_back(1 + idx);
        qd->push_back(2 + idx);
        qd->push_back(6 + idx);
        qd->push_back(5 + idx);

        qd->push_back(2 + idx);
        qd->push_back(3 + idx);
        qd->push_back(7 + idx);
        qd->push_back(6 + idx);

        qd->push_back(3 + idx);
        qd->push_back(0 + idx);
        qd->push_back(4 + idx);
        qd->push_back(7 + idx);
    }

    void makeGridBox1(int idx, osg::Vec3 const& minPos, osg::Vec3 const& maxPos, osg::Vec3Array* vertex, osg::Vec3Array *lpNormalVertex )
    {
        //osg::Vec3 center = ( maxPos + minPos ) / 2;

        std::vector<osg::Vec3> posList;
        posList.resize( 8 );
        osg::Vec3 p = minPos;
        posList[0] = (p);

        p = osg::Vec3(maxPos.x(), minPos.y(), minPos.z());
        posList[1] = ( p );

        p = osg::Vec3(maxPos.x(), maxPos.y(), minPos.z());
        posList[2] = ( p );

        p = osg::Vec3(minPos.x(), maxPos.y(), minPos.z());
        posList[3] = ( p );

        p = osg::Vec3(minPos.x(), minPos.y(), maxPos.z());
        posList[4] = ( p );

        p = osg::Vec3(maxPos.x(), minPos.y(), maxPos.z());
        posList[5] = ( p );

        p = maxPos;
        posList[6] = (p);

        p = osg::Vec3(minPos.x(), maxPos.y(), maxPos.z());
        posList[7] = ( p );

        // -z
        vertex->push_back(posList[3]);// +idx);
        lpNormalVertex->push_back(osg::Vec3(0, 0, -1));
        vertex->push_back(posList[2]);// 1 + idx);
        lpNormalVertex->push_back(osg::Vec3(0, 0, -1));
        vertex->push_back(posList[1]);// +idx);
        lpNormalVertex->push_back(osg::Vec3(0, 0, -1));
        vertex->push_back(posList[0]);// +idx);
        lpNormalVertex->push_back(osg::Vec3(0, 0, -1));

        // z
        vertex->push_back(posList[4]);// +idx);
        lpNormalVertex->push_back(osg::Vec3(0, 0, 1));
        vertex->push_back(posList[5]);// +idx);
        lpNormalVertex->push_back(osg::Vec3(0, 0, 1));
        vertex->push_back(posList[6]);// +idx);
        lpNormalVertex->push_back(osg::Vec3(0, 0, 1));
        vertex->push_back(posList[7]);// +idx);
        lpNormalVertex->push_back(osg::Vec3(0, 0, 1));

        // -y
        vertex->push_back(posList[0]);// +idx);
        lpNormalVertex->push_back(osg::Vec3(0, -1, 0));
        vertex->push_back(posList[1]);// +idx);
        lpNormalVertex->push_back(osg::Vec3(0, -1, 0));
        vertex->push_back(posList[5]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(0, -1, 0));
        vertex->push_back(posList[4]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(0, -1, 0));

        // y
        vertex->push_back(posList[2]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(0, 1, 0));
        vertex->push_back(posList[3]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(0, 1, 0));
        vertex->push_back(posList[7]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(0, 1, 0));
        vertex->push_back(posList[6]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(0, 1, 0));

        // x
        vertex->push_back(posList[1]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(1, 0, 0));
        vertex->push_back(posList[2]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(1, 0, 0));
        vertex->push_back(posList[6]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(1, 0, 0));
        vertex->push_back(posList[5]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(1, 0, 0));

        // -x
        vertex->push_back(posList[3]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(-1, 0, 0));
        vertex->push_back(posList[0]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(-1, 0, 0));
        vertex->push_back(posList[4]);// + idx);
        lpNormalVertex->push_back(osg::Vec3(-1, 0, 0));
        vertex->push_back(posList[7]);// +idx);
        lpNormalVertex->push_back(osg::Vec3(-1, 0, 0));
    }


    BlockChain::BlockChain( ydUtil::BlockIndex const& bi )
    {
        _index = bi;
        //_color = osg::Vec4( 1.0, 1.0, 0.0, 1.0 );
    }
    BlockChain::~BlockChain()
    {
    }

    void BlockChain::for_each(EachFun fun, int step )
    {
        if (fun(this))
        {
            return;
        }

        ChainStack cs;

        BlockChain *lpChain = this;
        BlockChain *lpStart = NULL;
        BlockChain *lpRefer = this;
        do
        {
            {
                BlockChain *lpTmpRefer = lpRefer;
                BlockChain *lpTmpChain = lpChain;
                BlockChain *lpTmpStart = lpStart;
                if ( lpTmpChain->_xn && lpTmpChain->_xn != lpTmpStart )
                {
                    if ( checkStep( lpTmpChain->_xn, lpTmpRefer, step ) )
                    {
                        if ( fun( lpTmpChain->_xn ) )
                        {
                            return;
                        }
                        cs.push( lpTmpChain->_xn, lpTmpChain, lpTmpChain->_xn );
                    }else
                    {
                        cs.push( lpTmpChain->_xn, lpTmpChain, lpTmpRefer );
                    }
                }
            }
            {
                BlockChain *lpTmpRefer = lpRefer;
                BlockChain *lpTmpChain = lpChain;
                BlockChain *lpTmpStart = lpStart;
                if ( lpTmpChain->_xp && lpTmpChain->_xp != lpTmpStart )
                {
                    if ( checkStep( lpTmpChain->_xp, lpTmpRefer, step ) )
                    {
                        if ( fun( lpTmpChain->_xp ) )
                        {
                            return;
                        }
                        cs.push( lpTmpChain->_xp, lpTmpChain, lpTmpChain->_xp );
                    }else
                    {
                        cs.push( lpTmpChain->_xp, lpTmpChain, lpTmpRefer );
                    }
                }
            }
            {
                BlockChain *lpTmpRefer = lpRefer;
                BlockChain *lpTmpChain = lpChain;
                BlockChain *lpTmpStart = lpStart;
                if ( lpTmpChain->_yn && lpTmpChain->_yn != lpTmpStart )
                {
                    if ( checkStep( lpTmpChain->_yn, lpTmpRefer, step ) )
                    {
                        if ( fun( lpTmpChain->_yn ) )
                        {
                            return;
                        }
                        cs.push( lpTmpChain->_yn, lpTmpChain, lpTmpChain->_yn );
                    }else
                    {
                        cs.push( lpChain->_yn, lpChain, lpRefer );
                    }
                }
            }
            {
                BlockChain *lpTmpRefer = lpRefer;
                BlockChain *lpTmpChain = lpChain;
                BlockChain *lpTmpStart = lpStart;
                if ( lpTmpChain->_yp && lpTmpChain->_yp != lpTmpStart )
                {
                    if ( checkStep( lpTmpChain->_yp, lpTmpRefer, step ) )
                    {
                        if ( fun( lpTmpChain->_yp ) )
                        {
                            return;
                        }
                        cs.push( lpTmpChain->_yp, lpTmpChain, lpTmpChain->_yp );
                    }else
                    {
                        cs.push( lpChain->_yp, lpChain, lpRefer );
                    }
                }
            }
            {
                BlockChain *lpTmpRefer = lpRefer;
                BlockChain *lpTmpChain = lpChain;
                BlockChain *lpTmpStart = lpStart;
                if ( lpTmpChain->_zn && lpTmpChain->_zn != lpTmpStart )
                {
                    if ( checkStep( lpTmpChain->_zn, lpTmpRefer, step ) )
                    {
                        if ( fun( lpTmpChain->_zn ) )
                        {
                            return;
                        }
                        cs.push( lpTmpChain->_zn, lpTmpChain, lpTmpChain->_zn );
                    }else
                    {
                        cs.push( lpChain->_zn, lpChain, lpRefer );
                    }
                }
            }
            {
                BlockChain *lpTmpRefer = lpRefer;
                BlockChain *lpTmpChain = lpChain;
                BlockChain *lpTmpStart = lpStart;
                if ( lpTmpChain->_zp && lpTmpChain->_zp != lpTmpStart )
                {
                    if ( checkStep( lpTmpChain->_zp, lpTmpRefer, step ) )
                    {
                        if ( fun( lpTmpChain->_zp ) )
                        {
                            return;
                        }
                        cs.push( lpTmpChain->_zp, lpTmpChain, lpTmpChain->_zp );
                    }else
                    {
                        cs.push( lpChain->_zp, lpChain, lpRefer );
                    }
                }
            }

            if ( cs.empty() )
            {
                break;
            }
            ChainStack::Item item = cs.pop();
            lpChain = item._lpChain;
            lpStart = item._lpStart;
            lpRefer = item._lpRefer;
        }
        while( true );
    }


    BlockChain *BlockChain::find( ydUtil::BlockIndex const& bi )
    {
        BlockChain *lpChain = this;
        if ( lpChain->_index == bi )
        {
            return lpChain;
        }

        while( lpChain )
        {
            if ( lpChain->_index._x < bi._x )
            {
                if ( lpChain->_xn && lpChain->_xn->_index._x > bi._x )
                {
                    return NULL;
                }
                lpChain = lpChain->_xn;
            }
            else if ( lpChain->_index._x > bi._x )
            {
                lpChain = lpChain->_xp;
            }else
            {
                if ( lpChain->_index == bi )
                {
                    return lpChain;
                }
                break;
            }
        }
        if ( !lpChain )
        {
            lpChain = this;
        }

        while( lpChain )
        {
            if ( lpChain->_index._y < bi._y )
            {
                if ( lpChain->_yn && lpChain->_yn->_index._y > bi._y )
                {
                    return NULL;
                }
                lpChain = lpChain->_yn;
            }else if ( lpChain->_index._y > bi._y )
            {
                lpChain = lpChain->_yp;
            }else
            {
                if ( lpChain->_index == bi )
                {
                    return lpChain;
                }
                break;
            }
        }
        if ( !lpChain )
        {
            lpChain = this;
        }

        while( lpChain )
        {
            if ( lpChain->_index._z < bi._z )
            {
                if ( lpChain->_zn && lpChain->_zn->_index._z > bi._z )
                {
                    return NULL;
                }
                lpChain = lpChain->_zn;
            }else if ( lpChain->_index._z > bi._z )
            {
                lpChain = lpChain->_zp;
            }else
            {
                if ( lpChain->_index == bi )
                {
                    return lpChain;
                }
                break;
            }
        }

        return NULL;
    }

    bool BlockChain::insert(ydUtil::BlockIndex const& bi, int gridSize )
    {
        osg::ref_ptr<BlockChain> lpbc = new BlockChain( bi );
        //lpbc->setColor( color );
        //BlockChain *lpChain = this;
        BlockChain *lpChain = _lastChain.get();
        if ( !lpChain )
        {
            lpChain = this;
        }

        while (lpChain)
        {
            if (lpChain->_index._x < bi._x)
            {
                if (lpChain->_xn )
                {
                    if (lpChain->_xn->_index._x > bi._x)
                    {
                        // not exist this x index, link
                        if ( checkLink ( lpChain, lpChain->_xn, lpbc, gridSize ) )
                        {
                            link( lpChain, lpChain->_xn, lpbc, X );
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        lpChain = lpChain->_xn;
                    }
                }
                else
                {
                    // no next value, so set lpbc to next, return
                    if ( checkLink ( lpChain, NULL, lpbc, gridSize ) )
                    {
                        link( lpChain, NULL, lpbc, X );
                        // lpChain->_xn = lpbc;
                        // lpbc->_xp = lpChain;
                        return true;
                    }

                    return false;
                }
            }
            else if (lpChain->_index._x == bi._x)
            {
                // x index find, to find y
                break;
            }
            else
            {
                if (!lpChain->_xp)
                {
                    if ( checkLink( NULL, lpChain, lpbc, gridSize ) )
                    {
                        link( NULL, lpChain, lpbc, X );
                        // lpChain->_xp = lpbc;
                        // lpbc->_xn = lpChain;
                        return true;
                    }
                    return false;
                }
                if (lpChain->_xp == this)
                {
                    break;
                }
                lpChain = lpChain->_xp;
            }
        }

        // not found
        if (!lpChain)
        {
            lpChain = this;
        }


        while (lpChain)
        {
            if (lpChain->_index._y < bi._y)
            {
                if (lpChain->_yn)
                {
                    if (lpChain->_yn->_index._y > bi._y)
                    {
                        // yes, it is
                        if ( checkLink( lpChain, lpChain->_yn, lpbc, gridSize ) )
                        {
                            link( lpChain, lpChain->_yn, lpbc, Y );
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        lpChain = lpChain->_yn;
                    }
                }
                else
                {
                    // no next value, so set lpbc to next, return
                    if ( checkLink( lpChain, NULL, lpbc, gridSize ) )
                    {
                        link( lpChain, NULL, lpbc, Y );
                        // lpChain->_yn = lpbc;
                        // lpbc->_yp = lpChain;
                        return true;
                    }
                    return false;
                }
            }
            else if (lpChain->_index._y == bi._y)
            {
                break;
            }
            else
            {
                if (!lpChain->_yp)
                {
                    if ( checkLink( NULL, lpChain, lpbc, gridSize ) )
                    {
                        link( NULL, lpChain, lpbc, Y );
                        // lpChain->_yp = lpbc;
                        // lpbc->_yn = lpChain;
                        return true;
                    }
                    return false;
                }

                if (lpChain->_yp == this)
                {
                    break;
                }
                lpChain = lpChain->_yp;
            }
        }
        // not found
        if (!lpChain)
        {
            lpChain = this;
        }

        while (lpChain)
        {
            if (lpChain->_index._z < bi._z)
            {
                if (lpChain->_zn)
                {
                    if (lpChain->_zn->_index._z > bi._z)
                    {
                        // yes, it is
                        if ( checkLink( lpChain, lpChain->_zn, lpbc, gridSize ) )
                        {
                            link( lpChain, lpChain->_zn, lpbc, Z );
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        lpChain = lpChain->_zn;
                    }
                }
                else
                {
                    // no next value, so set lpbc to next, return
                    if ( checkLink( lpChain, NULL, lpbc, gridSize ) )
                    {
                        link( lpChain, NULL, lpbc, Z );
                        // lpChain->_zn = lpbc;
                        // lpbc->_zp = lpChain;
                        return true;
                    }
                    return false;
                }
            }
            else if (lpChain->_index._z == bi._z)
            {
                if (lpChain->_index._x == bi._x && lpChain->_index._y == bi._y)
                {
                    // equal, retrun
                    return false;
                }
                break;
            }
            else
            {
                if (!lpChain->_zp)
                {
                    if ( checkLink( NULL, lpChain, lpbc, gridSize ) )
                    {
                        link( NULL, lpChain, lpbc, Z );
                        // lpChain->_zp = lpbc;
                        // lpbc->_zn = lpChain;
                        return true;
                    }
                    return false;
                }

                if (lpChain->_zp == this)
                {
                    if ( checkLink( lpChain->_zp, lpChain, lpbc, gridSize ) )
                    {
                        link( lpChain->_zp, lpChain, lpbc, Z );
                        return true;
                    }
                    //lpChain = lpChain->_zp;
                    return false;
                }
                lpChain = lpChain->_zp;
            }
        }
        return false;
    }
    void BlockChain::remove(ydUtil::BlockIndex const& bi)
    {
        osg::ref_ptr<BlockChain> lpChain = find( bi );
        if ( !lpChain )
        {
            return;
        }

        removeX( lpChain );
        removeY( lpChain );
        removeZ( lpChain );
    }

    void BlockChain::destory( BlockChain *lpBC )
    {
        if (_xn && _xn != lpBC )
        {
            _xn->destory( this );
            _xn = NULL;
        }
        if (_yn && _yn != lpBC )
        {
            _yn->destory( this );
            _yn = NULL;
        }
        if (_zn && _zn != lpBC )
        {
            _zn->destory( this );
            _zn = NULL;
        }

        if (_xp && _xp != lpBC )
        {
            _xp->destory( this );
            _xp = NULL;
        }
        if (_yp && _yp != lpBC )
        {
            _yp->destory( this );
            _yp = NULL;
        }
        if (_zp && _zp != lpBC )
        {
            _zp->destory( this );
            _zp = NULL;
        }
    }


///////////////////////////////////////////////////////////////////////
    BlockBody::BlockBody()
    {
        _loadCount = 0;
        _dirty = false;
        _unitGridSize = 0.04;
        _initFromFile = false;
    }
    BlockBody::~BlockBody()
    {
        if(_lpChain.valid())
        {
            if ( _dirty )
            {
                syncToFile();
                _dirty = false;
            }
            _lpChain->destory( _lpChain );
            _lpChain = NULL;
        }
    }

    osg::Vec4 getColorForIntensity(float const& intensity)
    {
        // printf("\n==============================%f==========================\n", intensity);
        osg::Vec4 color(1.0, 1.0, 0.0, 1.0);
        if (intensity < 0.0 || intensity > 255.0)
        {
            return color;
        }

        //---------红绿蓝颜色设置-----------
        int topColor[3];
        int midColor[3];
        int bottomColor[3];

        topColor[0] = 255; topColor[1] = 0; topColor[2] = 0;
        midColor[0] = 0;  midColor[1] = 255;  midColor[2] = 0;
        bottomColor[0] = 0; bottomColor[1] = 0; bottomColor[2] = 255;

        int r1 = midColor[0] - bottomColor[0];
        int g1 = midColor[1] - bottomColor[1];
        int b1 = midColor[2] - bottomColor[2];

        int r2 = topColor[0] - midColor[0];
        int g2 = topColor[1] - midColor[1];
        int b2 = topColor[2] - midColor[2];

        //--------获取当前点云高程极值---------
        float maxz = 255.0, minz = 0.0, midz = 127.5;

        if (intensity < midz) {
            float k1 = (intensity - minz) / (midz - minz);
            color.r() = (bottomColor[0] + r1 * k1) / 255.0;
            color.g() = (bottomColor[1] + g1 * k1) / 255.0;
            color.b() = (bottomColor[2] + b1 * k1) / 255.0;
        }
        else {
            float k2 = (intensity - midz) / (maxz - midz);
            color.r() = (midColor[0] + r2 * k2) / 255.0;
            color.g() = (midColor[1] + g2 * k2) / 255.0;
            color.b() = (midColor[2] + b2 * k2) / 255.0;
        }

        return color;
    }

#define USE_VERSION_1 1


    // todo : 现在只是创建了盒子的显示方式, 还要根据外部显示方式定义
    osg::Node *BlockBody::makeNode( int step, osg::Vec4 const& color )
    {
        step = 2;
        std::lock_guard<std::mutex> l ( _chainMutex );
        if ( !_lpChain )
        {
            // 先尝试从文件加载
            loadFromFile();
            if ( !_lpChain )
            {
                return NULL;
            }
        }

        osg::Vec3Array* lpVertexArray = new osg::Vec3Array;
        osg::Vec4Array* lpColorArray = new osg::Vec4Array;
        osg::Vec3Array* lpNoramlArray = new osg::Vec3Array;

        osg::ref_ptr<osg::DrawElementsUInt> ls = new osg::DrawElementsUInt(GL_LINES);
        osg::ref_ptr<osg::DrawElementsUInt> pd = new osg::DrawElementsUInt(GL_QUADS);

        float unitGridSize = _unitGridSize;

        osg::Vec3 pos ( _lpChain->_index._x * unitGridSize, _lpChain->_index._y * unitGridSize, _lpChain->_index._z * unitGridSize );
        //TRACE("%f,%f,%f\n", pos.x(), pos.y(), pos.z() );

        traverse([lpVertexArray, lpNoramlArray, lpColorArray, &ls, &pd, unitGridSize, step, pos](BlockChain* lpbc) {
                    std::pair< ChainPos, ChainPos> r = lpbc->getRange( step );
                    osg::Vec3 min = osg::Vec3(r.first._x, r.first._y, r.first._z) * unitGridSize - pos;
                    osg::Vec3 max = osg::Vec3(r.second._x, r.second._y, r.second._z) * unitGridSize - pos;
                    unsigned int oldSize = lpVertexArray->size();
                    if ( USE_VERSION_1 )
                    {
                        if( ChildAreaElement::getShapeMode() == ChildAreaElement::SM_POINT )
                        {
                            osg::Vec3 center = (min + max) * 0.5;
                            lpVertexArray->push_back(center);
                        }
                        else
                        {
                            // use normal
                            makeGridBox1(lpVertexArray->size(), min, max, lpVertexArray, lpNoramlArray);
                        }
                    }else
                    {
                        // no mormal
                        makeGridBox2(lpVertexArray->size(), min, max, lpVertexArray, ls, pd);
                    }

                    if ( ChildAreaElement::getColorMode() == ChildAreaElement::CM_INTENSITY )
                    {
                        osg::Vec4 color = getColorForIntensity(lpbc->_intensity);
                        lpColorArray->insert(lpColorArray->end(), lpVertexArray->size()-oldSize, color);
                    }

                    return false;
                }
            , step);

        osg::Geometry* lpGeometry = new osg::Geometry;
        if (lpVertexArray->size() > 0)
        {
            size_t mode = ChildAreaElement::getColorMode();
            if (mode == ChildAreaElement::CM_ORIGIN)
            {
                lpColorArray->push_back( color );
                lpGeometry->setColorArray(lpColorArray, osg::Array::BIND_OVERALL);
            }
            else if (mode == ChildAreaElement::CM_INTENSITY)
            {
                lpGeometry->setColorArray(lpColorArray, osg::Array::BIND_PER_VERTEX);
            }
            else if (mode == ChildAreaElement::CM_HEIGHT)
            {
                // code
            }

            lpGeometry->setVertexArray(lpVertexArray);
            if ( USE_VERSION_1 )
            {
                if( ChildAreaElement::getShapeMode() == ChildAreaElement::SM_POINT )
                {
                    lpGeometry->getOrCreateStateSet()->setAttributeAndModes( new osg::Point(2.0) );
                    lpGeometry->addPrimitiveSet(new osg::DrawArrays(GL_POINTS, 0, lpVertexArray->size()));
                }
                else
                {
                    lpGeometry->setNormalArray(lpNoramlArray, osg::Array::BIND_PER_VERTEX);
                    lpGeometry->addPrimitiveSet(new osg::DrawArrays(GL_QUADS, 0, lpVertexArray->size()));
                }
            }else
            {
                lpGeometry->addPrimitiveSet(pd);
                lpGeometry->addPrimitiveSet(ls);
            }
        }

        osg::Geode* lpGeode = new osg::Geode;
        lpGeode->addDrawable(lpGeometry);

        osg::MatrixTransform *lpMT = new osg::MatrixTransform;
        lpMT->addChild( lpGeode );
        lpMT->setMatrix( osg::Matrix::translate( pos ) );

        return lpMT;
    }

    bool BlockBody::addBlock( ydUtil::BlockIndex const& bi )
    {
        std::lock_guard<std::mutex> l ( _chainMutex );

        bool r = innerAddBlock( bi );
        if ( r && !_dirty )
        {
            _dirty = true;
        }
        return r;
    }

    void BlockBody::remove(ydUtil::BlockIndex const& bi)
    {
        if (!_lpChain)
        {
            return;
        }
        osg::ref_ptr<BlockChain> lpbc = _lpChain->find(bi);
        if (lpbc == _lpChain)
        {
            if (_lpChain->_xp)
            {
                _lpChain = _lpChain->_xp;
            }
            else if (_lpChain->_xn)
            {
                _lpChain = _lpChain->_xn;
            }
            else if (_lpChain->_yp)
            {
                _lpChain = _lpChain->_yp;
            }else if ( _lpChain->_yn )
            {
                _lpChain = _lpChain->_yn;
            }
            else if(_lpChain->_zp)
            {
                _lpChain = _lpChain->_zp;
            }
            else if (_lpChain->_zn)
            {
                _lpChain = _lpChain->_zn;
            }
        }

        _lpChain->removeX(lpbc);
        _lpChain->removeY(lpbc);
        _lpChain->removeZ(lpbc);

        _dirty = true;
    }


    BlockChain* BlockBody::find(ydUtil::BlockIndex const& bi)
    {
        if (!_lpChain)
        {
            return NULL;
        }

        return _lpChain->find(bi);
    }
    void BlockBody::syncToFile()
    {
        if ( _filePath.length() == 0 )
        {
            return;
        }

        std::ofstream ofs(_filePath, std::ios::out|std::ios::binary);
        if ( !_lpChain )
        {
            return;
        }

        ydUtil::DataReadWriter drw;
        traverse([&drw, &ofs](BlockChain* lpbc){
                     if ( !lpbc )
                     {
                         return false;
                     }

                     drw.write( ofs, lpbc->_index._x );
                     drw.write( ofs, lpbc->_index._y );
                     drw.write( ofs, lpbc->_index._z );
                     drw.write( ofs, lpbc->_intensity );

                     return false;

                 }
            , _gridSize);

        _dirty = false;
    }
    static int ChainLoadCount = 0;
    void BlockBody::freeChain()
    {
        std::lock_guard<std::mutex> l ( _chainMutex );
        if ( !_lpChain )
        {
            return;
        }
        if ( _dirty )
        {
            syncToFile();
        }

        _lpChain->destory( _lpChain );
        _lpChain = NULL;
        _initFromFile = false;
        _dirty = false;
        //TRACE("unload : %d\n", --ChainLoadCount);
    }

    void BlockBody::initChain()
    {
        if ( _initFromFile )
        {
            return;
        }
        std::lock_guard<std::mutex> l ( _chainMutex );
        loadFromFile();
        _dirty = false;
        //TRACE("load : %d\n", ++ ChainLoadCount);
    }

    void BlockBody::loadFromFile()
    {
        if ( _initFromFile )
        {
            return;
        }

        _initFromFile = true;
        if ( _filePath.length() == 0 )
        {
            return;
        }
        if ( !ydUtil::fileExist( _filePath.c_str() ) )
        {
            return;
        }


        std::ifstream ifs(_filePath, std::ios::out|std::ios::binary);
        if ( !ifs )
        {
            return;
        }

        if ( _lpChain )
        {
            _lpChain->destory( _lpChain );
            _lpChain = NULL;
        }

        _dirty = false;

        ydUtil::DataReadWriter drw;
        while( ( !ifs.eof() ) && ( ifs.peek() != EOF ) )
        {
            ydUtil::BlockIndex bi;
            drw.read( ifs, bi._x );
            drw.read( ifs, bi._y );
            drw.read( ifs, bi._z );

            float intensity;
            drw.read( ifs, intensity );

            innerAddBlock( bi );
            setIntensity( intensity );
        }


    }
    bool BlockBody::innerAddBlock( ydUtil::BlockIndex const& bi )
    {
        if ( !_lpChain )
        {
            _lpChain = new BlockChain( bi );
            //_lpChain->setColor( color );
            return true;
        }

        return _lpChain->insert( bi,  _gridSize );
    }

}
