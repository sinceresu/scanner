#include "UpdateChildElementHandler.h"
#include "ChildAreaElement.h"
#include <ydUtil/matrix_math.h>
#include <BlockShower/BlockShower.h>
#include <ydUtil/log.h>
#include <osg/Viewport>
#include <BlockShower/ScanAreaElement.h>
#include "ChildElementCache.h"

namespace BlockShower
{
    extern int G_LoadFileCount;
    UpdateChildElementHandler::UpdateChildElementHandler( BlockShower *lpBS )
        : _lpBlockShower( lpBS )
    {
        _lastViewMatrix = osg::Matrix( 0, 0, 0, 1,
                                       0, 0, 0, 1,
                                       0, 0, 0, 1,
                                       0, 0, 0, 1);
    }

    bool UpdateChildElementHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
    {
        if ( ea.getEventType() != osgGA::GUIEventAdapter::FRAME )
        {
            return false;
        }
        if ( !_lpBlockShower )
        {
            return false;
        }

        ydRender::SceneRender *sr = _lpBlockShower->getSceneRender();
        if ( !sr )
        {
            return false;
        }
        osgViewer::Viewer *lpViewer = sr->getViewer();
        if ( !lpViewer->getSceneData() )
        {
            return false;
        }

        osg::Matrix curViewMatrix = lpViewer->getCamera()->getViewMatrix();
        if ( curViewMatrix == _lastViewMatrix )
        {
            //return false;
        }
        osg::Matrix cameraWorldMatrix = osg::Matrix::inverse( curViewMatrix );
        _cameraPos = cameraWorldMatrix.getTrans();

        _lastViewMatrix = curViewMatrix;
        osg::Matrix projectionMatrix = lpViewer->getCamera()->getProjectionMatrix();
        double fovy, aspectRatio, znear, zfar;

        lpViewer->getCamera()->getProjectionMatrixAsPerspective( fovy, aspectRatio, znear, zfar );

        _fovy = fovy;

        osg::Polytope frustum;
        frustum.setToUnitFrustum(false, false);
        frustum.transformProvidingInverse(projectionMatrix);
        frustum.transformProvidingInverse(curViewMatrix);

        osg::Viewport* lpVp = lpViewer->getCamera()->getViewport();
        if ( lpVp )
        {
            _width = lpVp->width();
            _height =lpVp->height();
        }else
        {
            _width = ea.getWindowWidth();
            _height = ea.getWindowHeight();
        }
        ydUtil::TimeTrace tt;
        double slope = tan((_fovy * osg::PI) / 180 / 2);
        G_LoadFileCount = 0;
        updateElement( lpViewer->getSceneData()->asGroup(), lpViewer, frustum, slope );

        ChildElementCache::Instance()->checkToFree();

        return false;
    }

    void UpdateChildElementHandler::updateElement( osg::Group *lpRoot, osgViewer::Viewer *lpViewer, osg::Polytope &frustum, double slope )
    {
        if ( !lpRoot )
        {
            return;
        }

        ScanAreaElement *lpElement = dynamic_cast<ScanAreaElement*>( lpRoot );
        if ( lpElement )
        {
            osg::BoundingBox elementBB = lpElement->getBox();
            if(elementBB.valid())
            {
                if ( frustum.contains( lpElement->getBox() ) )
                {
                    lpElement->checkChild( _width, _height, slope, frustum, _cameraPos );
                }
            }
            return;
        }

        for ( unsigned int i = 0; i < lpRoot->getNumChildren(); i ++ )
        {
            osg::Node *lpNode = lpRoot->getChild( i );
            updateElement( lpNode->asGroup(), lpViewer, frustum, slope);
            // if ( updateElement( lpNode->asGroup(), lpViewer, frustum, slope) )
            // {
            //     return true;
            // }
        }
        // return false;
    }
}
