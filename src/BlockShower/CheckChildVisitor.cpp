#include "CheckChildVisitor.h"
#include "ChildAreaElement.h"
#include <ydUtil/log.h>

namespace BlockShower
{
    CheckChildVisitor::CheckChildVisitor( TileFileManager *lpTileFileManager, double slope
                       , osg::Polytope const& frustum, int width, int height
                       , osg::Vec3 const cameraPos )
        : osg::NodeVisitor( osg::NodeVisitor(TRAVERSE_ALL_CHILDREN) )
    {
        _lpTileFileManager = lpTileFileManager;
        _frustum = frustum;
        _width = width;
        _height = height;
        _cameraPos = cameraPos;
        _slope = slope;
    }

    void CheckChildVisitor::apply(osg::MatrixTransform &node)
    {
        ChildAreaElement* child = dynamic_cast<ChildAreaElement*>(&node);
        if(child)
        {   
            osg::BoundingBox elementBB = child->getBox();
            if(!elementBB.valid())
            {
                return;
            }
            
            if ( !_frustum.contains( elementBB) )
            {
                return;
            }

            child->checkChild( _width, _height, _slope, _frustum, _cameraPos, _lpTileFileManager );
        }


        traverse( node );
    }
}
