#ifndef __UPDATE_CHILD_ELELMENT_HANDLER_H_FILE__
#define __UPDATE_CHILD_ELELMENT_HANDLER_H_FILE__
#include <osgGA/GUIEventHandler>
#include <osg/Group>
#include <osg/Polytope>
#include <osgViewer/Viewer>


namespace BlockShower
{
    class BlockShower;
    class UpdateChildElementHandler : public osgGA::GUIEventHandler
    {
    public:
        using UpdateCallback = std::function<void()>;
        UpdateChildElementHandler( BlockShower *lpBS );

        virtual bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);

    protected:
        void updateElement( osg::Group *lpRoot, osgViewer::Viewer *lpViewer, osg::Polytope &frustum, double slope );
    protected:
        BlockShower *_lpBlockShower;

        osg::Matrix _lastViewMatrix;
        //osg::Matrix _mvp;

        float _fovy;
        int _width;
        int _height;
        osg::Vec3 _cameraPos;


        //CheckChildGridThread *_lpCheckThread;

    };
}



#endif // __UPDATE_CHILD_ELELMENT_HANDLER_H_FILE__
