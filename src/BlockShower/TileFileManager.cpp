#include "TileFileManager.h"
#include <ydUtil/log.h>
#include "Config.h"
#include <ydBlockDB/BlockDB.h>
#include "BlockShower/ScanAreaElement.h"
namespace BlockShower
{
    FileProxy::FileProxy( osg::Referenced *lpFile, FreeCallback callback )
    {
        _lpFile = lpFile;
        _freeCallback = callback;
        // 最多给10帧机会
        _frameCount = 10;

    };
    bool FileProxy::check()
    {
        _frameCount --;
        return _frameCount <= 0;
    }

    ///////////////////////////////////////////////////////////////////////////////
    // TileFileManager
    TileFileManager::TileFileManager( MapToWorlFun fun )
    {
        _mapToWorldFun = fun;
        _running = false;
        _dealThread = std::thread([this](){
                          this->dealThread();
                                  });
    }

    TileFileManager::~TileFileManager()
    {
        stop();
        GridBlockInfoMap gbm = getGridBoxInfo();
        for ( auto g : gbm  )
        {
            g.second->flush();
        }
    }

    void TileFileManager::addFile( std::string const& key, osg::Referenced *lpNode, FileProxy::FreeCallback cb )
    {
        FileProxyMap::iterator it = _fileProxy.find( key );
        if ( it == _fileProxy.end() )
        {
            FileProxy *lpProxy = new FileProxy( lpNode, cb );
            _fileProxy[key] = lpProxy;
            return;
        }

        FileProxy *lpProxy = it->second.get();
        lpProxy->active();
    }

    void TileFileManager::activeOnce()
    {
        // std::lock_guard<std::mutex> l( _fileProxyMutex );
        // for ( FileProxyMap::iterator it =  _fileProxy.begin(); it != _fileProxy.end();  )
        // {
        //     if ( !it->second->check() )
        //     {
        //         ( *( it->second.get() ) )();
        //         it->second->_lpFile = NULL;
        //         it = _fileProxy.erase( it );
        //         continue;
        //     }

        //     it ++;
        // }

        _dealContidion.notify_all();
    }

    void TileFileManager::dealThread()
    {
        _running = true;
        std::unique_lock<std::mutex> waitLock( _fileProxyMutex );
        while( _running )
        {
            for ( FileProxyMap::iterator it =  _fileProxy.begin(); it != _fileProxy.end();  )
            {
                if ( !it->second->check() )
                {
                    ( *( it->second.get() ) )();
                    it = _fileProxy.erase( it );
                    continue;
                }

                it ++;

            }
            _dealContidion.wait( waitLock );
        }
    }
    void TileFileManager::stop()
    {
        _running = false;
        _dealContidion.notify_all();
        if ( _dealThread.joinable() )
        {
            _dealThread.join();
        }
    }
    void TileFileManager::load(std::string const& sceneID, std::string const& filePath)
    {

    }

    GridBoxInfo *TileFileManager::getChild( ydUtil::BlockIndex const& localIndex )
    {
        std::string id = get("id");
        //std::lock_guard<std::mutex> l( _childMapMutex );

        GridBoxInfo *lpChild = NULL;

        auto it = _gridBlockInfoMap.find( localIndex );

        if ( it == _gridBlockInfoMap.end() )
        {
            osg::Vec3 childCenter = ydUtil::getPos( osg::Vec3( 0.0, 0.0, 0.0 ), localIndex, _childSize );
            osg::BoundingBox childBB( childCenter - _childSize / 2, childCenter + _childSize / 2 );
            // 找不到就创建
            // 取step为ROOT_0_STEP, 表示最多有7层, 在第0层,每一个格子的大小为ROOT_0_STEP * 显示精度, 比如显示精度为0.02, 则在第0层一个格子大小是2.56米.
            // 在距离较远时用大块显示也够了.
            lpChild = new GridBoxInfo( 0, ROOT_0_STEP );
            lpChild->setIndex(localIndex);
            lpChild->set( "scanid", id );
            lpChild->set("cachepath", get("cachepath"));
            lpChild->set("blockindex", localIndex.toString());
            lpChild->setRange( childBB );
            lpChild->load( false );
            lpChild->_blockBody->setFilePath( ScanAreaElement::getFilePath( get("cachepath"), localIndex, lpChild->getLevel() ) );

            _gridBlockInfoMap[localIndex] = lpChild;

            //TRACE("Create %d,%d,%d\n", localIndex._x, localIndex._y, localIndex._z );
        }else
        {
            lpChild = it->second.get();
        }
        return lpChild;
    }
    osg::BoundingBox TileFileManager::setCloudPoint(pcl::PointCloud<pcl::PointXYZI>const* cloud)
    {
        if ( !cloud )
        {
            return _validBoundBox;
        }

        std::string id = get("id");

        osg::BoundingBox cloudBB = _validBoundBox;

        std::map< ydUtil::BlockIndex, bool > indexMap;
        std::map<osg::ref_ptr<GridBoxInfo>, int> tmpRecorder;

        ydUtil::TimeTrace tt;
        for (size_t i = 0; i < (*cloud).size(); i++)
        {
            osg::Vec3 p;// = mapToWorld( osg::Vec3((*cloud)[i].x, (*cloud)[i].y, (*cloud)[i].z) );
            if ( _mapToWorldFun )
            {
                p = _mapToWorldFun( osg::Vec3((*cloud)[i].x, (*cloud)[i].y, (*cloud)[i].z));
            }else
            {
                p = osg::Vec3((*cloud)[i].x, (*cloud)[i].y, (*cloud)[i].z);
            }

            ydUtil::BlockIndex bi = ydBlockDB::get( p.x(), p.y(), p.z() );
            if ( indexMap.find( bi ) == indexMap.end() )
            {
                indexMap[bi] = false;
            }

            if ( indexMap[bi] )
            {
                continue;
            }else
            {
                indexMap[bi] = true;
            }

            ydUtil::BlockIndex localIndex = ydUtil::getBlockIndex( osg::Vec3( 0.0, 0.0, 0.0 ), _childSize, p );

            GridBoxInfo *lpInfo = getChild( localIndex );
            if ( !lpInfo )
            {
                continue;
            }

            osg::BoundingBox bb = lpInfo->getBox();
            if ( !bb.contains( p ) )
            {
                continue;
            }
            lpInfo->updateCloud( bi, p, tmpRecorder, (*cloud)[i].intensity );

            cloudBB.expandBy( p );
        }

        {
            std::lock_guard<std::mutex> l( _fileProxyMutex );

            for ( auto t : tmpRecorder )
            {
                osg::ref_ptr<GridBoxInfo> g = const_cast<GridBoxInfo*>( t.first.get() );

                addFile( std::to_string(t.first->getLevel()) + "_" + t.first->getIndex().toString(), t.first.get(), []( osg::Referenced *lpFile ){
                        GridBoxInfo *lpBoxInfo = dynamic_cast<GridBoxInfo*>(lpFile);
                        if ( lpBoxInfo )
                        {
                            lpBoxInfo->flush();
                        }

                    });
            }
        }
        activeOnce();

        // 判断是否需要增加新的ChildAreaElement, 判断的过程在osg的更新操作中
        if ( cloudBB != _validBoundBox )
        {
            //_childAreaElementDirty = true;
            _validBoundBox = cloudBB;
        }

        //TRACE( "use time : %d -> %f\n", (int)(*cloud).size(), tt.getUseTime() );
        return _validBoundBox;
    }
    TileFileManager::GridBlockInfoMap TileFileManager::getGridBoxInfo()
    {
        return _gridBlockInfoMap;
    }

    osg::ref_ptr<GridBoxInfo> TileFileManager::getGridBoxInfo( std::string const& key )//int level, ydUtil::BlockIndex bi )
    {
        //std::lock_guard<std::mutex> l( _fileProxyMutex );

        FileProxyMap::iterator it = _fileProxy.find( key );
        if ( it == _fileProxy.end() )
        {
            return NULL;
        }
        osg::ref_ptr<GridBoxInfo> boxInfo = dynamic_cast<GridBoxInfo*>((*it).second->_lpFile.get());
        return boxInfo;

        // for ( auto f : _fileProxy )
        // {
        //     osg::ref_ptr<GridBoxInfo> boxInfo = dynamic_cast<GridBoxInfo*>(f.second->_lpFile.get());
        //     if ( boxInfo.valid() )
        //     {
        //         if ( boxInfo->getIndex() == bi && boxInfo->getLevel() == level )
        //         {
        //             return boxInfo;
        //         }
        //     }
        // }
        // return NULL;
    }
}
