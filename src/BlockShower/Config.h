#ifndef __SRC_BLOCK_SHOWER_CONFIG_H_FILE__
#define __SRC_BLOCK_SHOWER_CONFIG_H_FILE__

namespace BlockShower
{

    #define COUNT_NAME "lidar_count"
    #define INDEX_NAME "lidar_geometry_index"
    #define COUNT_INDEX_ORDER 12


    #define CHILD_LAYER_COUNT 2
    #define FINISH_GRID_COUNT 8

    #define ROOT_0_STEP 16

    #define G_ChildAreaSize  16.0f
    #define Z_OFFSET -15.0f
}



#endif // __SRC_BLOCK_SHOWER_CONFIG_H_FILE__
