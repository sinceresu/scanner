#include "CameraTrackerManipulator.h"
#include <ydUtil/matrix_math.h>
#include "BlockShower/CameraTrackElement.h"

namespace BlockShower
{
    CameraTrackerManipulator::CameraTrackerManipulator()
    {
        setAllowThrow(false);
        // _trackerMode = osgGA::NodeTrackerManipulator::NODE_CENTER_AND_ROTATION;
        // setVerticalAxisFixed(false);
    }

    void CameraTrackerManipulator::setByMatrix(const osg::Matrixd& matrix)
    {
        osg::Vec3d eye,center,up;
        matrix.getLookAt(eye,center,up,_distance);
        computePosition(eye,center,up);
    }

    osg::Matrixd CameraTrackerManipulator::getMatrix() const
    {
        osg::Vec3d nodeCenter;
        osg::Quat nodeRotation;
        computeNodeCenterAndRotation(nodeCenter,nodeRotation);
        return osg::Matrixd::translate(0.0,0.0,_distance)*osg::Matrixd::rotate(_rotation)/*osg::Matrixd::rotate(nodeRotation)*/*osg::Matrix::translate(nodeCenter);
    }

    osg::Matrixd CameraTrackerManipulator::getInverseMatrix() const
    {
        osg::Vec3d nodeCenter;
        osg::Quat nodeRotation;
        computeNodeCenterAndRotation(nodeCenter,nodeRotation);
        return osg::Matrixd::translate(-nodeCenter)*osg::Matrixd::rotate(nodeRotation.inverse())*osg::Matrixd::translate(0.0,0.0,-_distance);
    }

    void CameraTrackerManipulator::setByInverseMatrix( const osg::Matrixd& matrix )
    {
        setByMatrix( osg::Matrixd::inverse(matrix));
    }

}
