#ifndef __SRC_BLOCK_SHOWER_GRID_BOX_INFO_H_FILE__
#define __SRC_BLOCK_SHOWER_GRID_BOX_INFO_H_FILE__
#include <BlockShower/export.h>
#include <osg/Referenced>
#include <osg/Node>
#include <osg/Group>
#include <osg/ref_ptr>
#include "BlockBody.h"
#include <map>
#include <ydBase/Element.h>
#include <functional>
#include "BoxElement.h"

namespace BlockShower
{
    class GridBoxInfo : public BoxElement
    {
    public:
        typedef std::map<ydUtil::BlockIndex, osg::ref_ptr<GridBoxInfo> > GridBoxInfoMap;
        GridBoxInfo( int level, int step);


        virtual void load( bool inThread );

        osg::ref_ptr<BlockBody> _blockBody;

        bool updateCloud( ydUtil::BlockIndex const& globalbi, osg::Vec3 const& globalCenter, std::map<osg::ref_ptr<GridBoxInfo>, int> &updateRecorder, float const& intensity );

        GridBoxInfoMap getGridBoxInfo(){ return _childMap; };

        void setShowChild( bool flag ){ _showChild = flag; };
        bool isShowChild()const{ return _showChild; };

        void flush();

        bool checkFileDirty();



        using ForeachFun = std::function<void( GridBoxInfo *)>;
        void for_each( ForeachFun fun );

        bool isUpdate(){ return _updateFlag; };

        bool gridDirty()const{ return _gridDirty;};
        void clearGridDirty(){_gridDirty = false;};
    protected:
        GridBoxInfo *getGridInfo( ydUtil::BlockIndex const& childIndex, osg::Vec3 const& globalCenter );
        virtual void initGridBoxInfo( ydUtil::BlockIndex const &bi, BoxElement *lpInfo );
    protected:
        GridBoxInfoMap _childMap;
        time_t _lastModifyTime;
        bool _showChild;

        bool _updateFlag;
        bool _updateChild;

        bool _gridDirty;

        int _updateCount;

    };

}

#endif // __SRC_BLOCK_SHOWER_GRID_BOX_INFO_H_FILE__
