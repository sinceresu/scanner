#include "CheckChildGridThread.h"
#include "ChildAreaElement.h"
#include <ydUtil/log.h>
#include "TileFileManager.h"

namespace BlockShower
{
    CheckChildGridThread::CheckChildGridThread( UpdateCallback fun )
    {
        _fun = fun;
        _down = false;
    }
    CheckChildGridThread::~CheckChildGridThread()
    {
        stop();
    }

    void CheckChildGridThread::work(std::vector<osg::observer_ptr<osg::Node> > checkNodes)
    {
        _down = false;
        TRACE("%d\n", checkNodes.size());
        for ( auto cn : checkNodes )
        {
            if ( _down )
            {
                break;
            }

            osg::ref_ptr<osg::Node> node = cn.get();
            ChildAreaElement *lpElement = dynamic_cast<ChildAreaElement *>( node.get() );
            if ( cn.valid() && lpElement )
            {
                lpElement->updateGrid();
            }
        }

        //TileFileManager::Instance()->Instance()->activeOnce();

        if ( _fun )
        {
            _fun();
        }

    }

    void CheckChildGridThread::stop()
    {
        _checkNodes.clear();
        _down = true;
        if ( _thread.joinable() )
        {
            _thread.join();
        }

    }
    void CheckChildGridThread::addCheckNode( osg::Node *lpNode )
    {
        _checkNodes.push_back( lpNode );
    }
    void CheckChildGridThread::clear()
    {
        _down = true;
        _checkNodes.clear();
    }
    void CheckChildGridThread::workOnce()
    {
        if ( _checkNodes.size() == 0 )
        {
            return;
        }
        std::vector<osg::observer_ptr<osg::Node> > checkNodes;
        checkNodes.swap( _checkNodes );

        _down = true;
        if ( _thread.joinable() )
        {
            _thread.join();
        }

        _thread = std::thread([this, checkNodes](){
                                   this->work( checkNodes );
                               });
    }
}
