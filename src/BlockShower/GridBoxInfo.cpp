#include "GridBoxInfo.h"
#include <ydBlockDB/BlockDB.h>
#include <atomic>
#include <fstream>
#include <ydUtil/DataReadWriter.h>
#include "BlockShower/ScanAreaElement.h"
#include "Config.h"
#include <sys/stat.h>
#include <ydUtil/log.h>
#include "Config.h"

namespace BlockShower
{
    extern std::atomic<float> G_Ratio;
    GridBoxInfo::GridBoxInfo( int level, int step )
        : BoxElement( level, step )
    {
        _gridDirty = false;
        _updateChild = false;
        _updateFlag = false;
        _blockBody = new BlockBody;

        _blockBody->setUnitGridSize( ydBlockDB::getAccuracy() );
        _blockBody->setGridSize( step * G_Ratio );
        _updateCount = 0;
    }

    bool GridBoxInfo::updateCloud( ydUtil::BlockIndex const& globalbi, osg::Vec3 const& globalCenter, std::map<osg::ref_ptr<GridBoxInfo>, int> &updateRecorder, float const& intensity )
    {
        GridBoxInfo *lpInfo = NULL;

        if( !_rangeBB.contains( globalCenter ) )
        {
            return false;
        }

        // 向自己内部添加
        _blockBody->initChain();
        _blockBody->setIntensity(intensity);
        if ( _blockBody->addBlock( globalbi ) )
        {
            if ( _updateCount > 200 )
            {
                if ( !_gridDirty )
                {
                    _gridDirty = true;
                }
                if ( !_updateFlag )
                {
                    _updateFlag = true;
                    updateRecorder[this] = 0;
                }
                _updateCount = 0;
            }
            _updateCount ++;

            return true;
        }

        ydUtil::BlockIndex childIndex = getChildIndex( globalCenter );
        lpInfo = getGridInfo( childIndex, globalCenter );

        if ( !lpInfo )
        {
            return false;
        }

        // 向子节点内添加
        if ( lpInfo->updateCloud( globalbi, globalCenter, updateRecorder, intensity ) )
        {
            _updateChild = true;
        }

        return false;
    }

    GridBoxInfo *GridBoxInfo::getGridInfo( ydUtil::BlockIndex const& childIndex, osg::Vec3 const& globalCenter )
    {
        GridBoxInfo *lpInfo = NULL;
        GridBoxInfoMap::iterator it;
        {
            it = _childMap.find( childIndex );
        }

        if ( it == _childMap.end() )
        {
            if (  _step >= CHILD_LAYER_COUNT )
            {
                GridBoxInfo *lpTmpInfo = new GridBoxInfo( _level + 1, _step / CHILD_LAYER_COUNT );

                _childMap[childIndex] = lpTmpInfo;

                lpInfo = lpTmpInfo;

                // 马上创建子节点
                initGridBoxInfo( childIndex, lpTmpInfo );

            }else
            {
                return NULL;
            }

        }else
        {
            lpInfo = it->second;
        }

        return lpInfo;
    }

    void GridBoxInfo::initGridBoxInfo( ydUtil::BlockIndex const &bi, BoxElement *lpInfo )
    {
        BoxElement::initGridBoxInfo( bi, lpInfo );

        GridBoxInfo *lpGridInfo = dynamic_cast<GridBoxInfo*>( lpInfo );
        if ( lpGridInfo )
        {
            lpGridInfo->_blockBody->setFilePath( ScanAreaElement::getFilePath( get("cachepath"), bi, lpInfo->getLevel() ) );
        }

    }

    void GridBoxInfo::flush()
    {
        if ( _updateFlag )
        {
            _blockBody->freeChain();
            _updateFlag = false;
            _updateCount = 0;
        }

        // 先向下遍历， 这里会把所有的子节点都遍历一次，速度会降低
        if ( _updateChild )
        {
            for ( auto g : _childMap )
            {
                g.second->flush();
            }
            _updateChild = false;
        }
    }
    bool GridBoxInfo::checkFileDirty()
    {
        struct stat stat_buf;
        if ( stat( _blockBody->getFilePath().c_str(), &stat_buf ) == 0 )
        {
            if ( _lastModifyTime != stat_buf.st_mtime )
            {
                _lastModifyTime = stat_buf.st_mtime;
                return true;
            }
            return false;
        }
        return false;
    }
    void GridBoxInfo::load( bool inThread )
    {
        _blockBody->initChain();
    }
    void GridBoxInfo::for_each( ForeachFun fun )
    {
        if ( !fun )
        {
            return;
        }
        fun(this);
        for ( auto g : _childMap )
        {
            g.second->for_each( fun );
        }
    }
}
