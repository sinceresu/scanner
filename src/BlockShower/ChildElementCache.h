#ifndef __SRC_BLOCK_SHOWER_CHILD_ELEMENT_CACHE_H_FILE__
#define __SRC_BLOCK_SHOWER_CHILD_ELEMENT_CACHE_H_FILE__
#include <ydUtil/singleton.h>
#include <map>
#include <osg/Node>
#include <osg/ref_ptr>
#include <mutex>

namespace BlockShower
{
    class ChildElementCache : public Singleton<ChildElementCache>
    {
    protected:
        ChildElementCache();
        ~ChildElementCache();
        friend class Singleton<ChildElementCache>;

    public:
        void add( std::string const& id, osg::Node *lpElement );
        osg::Node *get( std::string const& id );


        //void addUpdateItem();

        void checkToFree();
        void clear();

    protected:
        struct CacheItem : public osg::Referenced
        {
            osg::ref_ptr<osg::Node> _element;
            int _count;
        };
        std::map<std::string, osg::ref_ptr<CacheItem> > _cacheItems;

        std::mutex _cacheMutex;

        bool _running;
    };
}



#endif // __SRC_BLOCK_SHOWER_CHILD_ELEMENT_CACHE_H_FILE__


