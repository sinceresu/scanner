#include <osg/NodeVisitor>
#include "TileFileManager.h"
#include <osg/ref_ptr>
#include <osg/MatrixTransform>

namespace BlockShower
{
    class CheckChildVisitor : public osg::NodeVisitor
    {
    public:
        CheckChildVisitor( TileFileManager *lpTileFileManager, double slope
                           , osg::Polytope const& frustum, int width, int height
                           , osg::Vec3 const cameraPos );
        void apply(osg::MatrixTransform &node);

    protected:
        osg::ref_ptr<TileFileManager> _lpTileFileManager;
        osg::Polytope _frustum;
        double _slope;
        int _width;
        int _height;
        osg::Vec3 _cameraPos;
    };
}
