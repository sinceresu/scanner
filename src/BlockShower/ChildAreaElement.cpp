#include "ChildAreaElement.h"
#include <ydUtil/DataReadWriter.h>
#include <osg/Geometry>
#include <osg/Geode>
#include <osg/Point>
#include <ydBlockDB/BlockDB.h>
#include <ydUtil/StringUtils.h>
#include <osg/Shader>
#include <osg/Program>
#include <osgDB/ReadFile>
#include <osg/Uniform>
#include <ydUtil/log.h>
#include <osgViewer/Viewer>
#include <osgUtil/CullVisitor>
#include <osg/RenderInfo>
#include <ydUtil/console.h>
#include <osg/io_utils>
#include <assert.h>
#include <osg/LineWidth>
#include <ydUtil/geometry.h>
#include <ydUtil/DataReadWriter.h>
#include <fstream>
#include <atomic>
#include "BlockShower/ScanAreaElement.h"
#include "Config.h"
#include <ydUtil/geometry.h>
#include <thread>
#include "TileFileManager.h"
#include "ChildElementCache.h"

namespace BlockShower
{

    std::atomic<float> G_Ratio(1.0);
    // std::mutex G_IndexMutex;
    //int G_ChildAreaIndex(0);

    unsigned int ChildAreaElement::_shapeMode = 0;
    unsigned int ChildAreaElement::_colorMode = 0;
    LevelSize ChildAreaElement::_levelSize( 16 );

    const int G_FrameLoadFileCount = 30;
    int G_LoadFileCount = 0;


    int G_MaxLevel = 0;

    ChildAreaElement::GridNode::GridNode( int level, int step )
        : GridBoxInfo( level, step )
    {
        _gridRoot = new osg::Group;
    }



    //ELEMENT_FACTORY(ChildAreaElement);
    ChildAreaElement::ChildAreaElement( bool finish, int level, int step )
        : BoxElement( level, step )
    {
        G_MaxLevel = level > G_MaxLevel ? level : G_MaxLevel;

        setName( "ChildAreaElement" );

        _lineColor = osg::Vec4( 0.772f, 0.3529f, 0.067f, 1.0f );
        _faceColor = osg::Vec4( 1.0, 1.0, 0.0, 0.4);

        _height = 5.0f;

        getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);
        getOrCreateStateSet()->setMode(GL_BLEND, osg::StateAttribute::ON);
        getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);


    }
    ChildAreaElement::~ChildAreaElement()
    {
    }

    std::string getGridKey( int level, ydUtil::BlockIndex const& bi )
    {
        return std::to_string( level ) + "_" + bi.toString();
    }

    void ChildAreaElement::load2(bool inThread, TileFileManager *lpTileFileManager )
    {
        if ( _step < CHILD_LAYER_COUNT )
        {
            return;
        }
        std::string id = get("id");

        //_childMap.clear();
        osg::Vec3 childRange = getChildPartRange();
        for (float x = _rangeBB.xMin(); x <= _rangeBB.xMax(); x += childRange.x() )
        {
            for (float y = _rangeBB.yMin(); y <= _rangeBB.yMax(); y += childRange.y() )
            {
                for (float z = _rangeBB.zMin(); z <= _rangeBB.zMax(); z += childRange.z() )
                {
                    ydUtil::BlockIndex bi = getChildIndex( osg::Vec3( x, y, z ) );
                    std::string dbFile = getFilePath( bi, _level );

                    std::map<ydUtil::BlockIndex, osg::ref_ptr<GridNode> >::iterator it = _childMap.find( bi );
                    if(!ydUtil::fileExist(dbFile.c_str()) && ( !( lpTileFileManager->getGridBoxInfo( getGridKey( _level, bi ) ).get() ) ) )
                    {
                        // 如果文件不存在，就删除已经加载进来的节
                        if ( it != _childMap.end() )
                        {
                            GridNode *lpInfo = (*it).second;
                            if ( lpInfo )
                            {
                                removeChild( lpInfo->_gridRoot );
                                removeChild( lpInfo->_childNode );
                                _childMap.erase( bi );
                            }
                        }
                        continue;
                    }
                    if ( it != _childMap.end() )
                    {
                        continue;
                    }
                    //TRACE("load %d -> %s\n", _level, bi.toString().c_str() );

                    GridNode *lpInfo = getGridInfo( bi, osg::Vec3( x, y, z ) );
                }
            }
        }
    }
    void ChildAreaElement::load(bool inThread)
    {
        if ( _step <= 1 )
        {
            return;
        }
        std::string id = get("id");

        //_childMap.clear();
        osg::Vec3 childRange = getChildPartRange();
        for (float x = _rangeBB.xMin(); x <= _rangeBB.xMax(); x += childRange.x() )
        {
            for (float y = _rangeBB.yMin(); y <= _rangeBB.yMax(); y += childRange.y() )
            {
                for (float z = _rangeBB.zMin(); z <= _rangeBB.zMax(); z += childRange.z() )
                {
                    ydUtil::BlockIndex bi = getChildIndex( osg::Vec3( x, y, z ) );
                    std::string dbFile = getFilePath( bi, _level );

                    std::map<ydUtil::BlockIndex, osg::ref_ptr<GridNode> >::iterator it = _childMap.find( bi );
                    if(!ydUtil::fileExist(dbFile.c_str()) )
                    {
                        // 如果文件不存在，就删除已经加载进来的节
                        if ( it != _childMap.end() )
                        {
                            GridNode *lpInfo = (*it).second;
                            if ( lpInfo )
                            {
                                removeChild( lpInfo->_gridRoot );
                                removeChild( lpInfo->_childNode );
                                _childMap.erase( bi );
                            }
                        }
                        continue;
                    }
                    if ( it != _childMap.end() )
                    {
                        continue;
                    }

                    GridNode *lpInfo = getGridInfo( bi, osg::Vec3( x, y, z ) );
                }
            }
        }
    }
    void ChildAreaElement::setupShader()
    {
        osg::Shader *lpVertexShader = osgDB::readShaderFile( osg::Shader::VERTEX, "res/shader/lidardata.vert");
        osg::Shader *lpFragmentShader = osgDB::readShaderFile( osg::Shader::FRAGMENT, "res/shader/lidardata.frag");

        osg::Program *lpProgram = new osg::Program;
        lpProgram->addShader( lpVertexShader );
        lpProgram->addShader( lpFragmentShader );


        getOrCreateStateSet()->setAttributeAndModes( lpProgram, osg::StateAttribute::ON );

        getOrCreateStateSet()->addUniform( new osg::Uniform("maxHeight", 0.0f) );
        getOrCreateStateSet()->addUniform( new osg::Uniform("minHeight", 10.0f) );

        getOrCreateStateSet()->addUniform( new osg::Uniform("maxCount", 10) );
        getOrCreateStateSet()->addUniform( new osg::Uniform("showType", _colorMode) );
    }

    void ChildAreaElement::setHeight( float v )
    {
        _height = v;
    }
    void ChildAreaElement::setColorMode( unsigned int cm )
    {
        _colorMode = cm;
    }
    void ChildAreaElement::setShapeMode( unsigned int sm )
    {
        _shapeMode = sm;
    }

    void ChildAreaElement::setFaceColor( osg::Vec4 const& c )
    {
        _faceColor = c;
    }

    void ChildAreaElement::setLineColor( osg::Vec4 const& c )
    {
        _lineColor = c;
    }


    void ChildAreaElement::setPointSize( float ps )
    {
        _pointSize = ps;
        osg::Point *lpP = new osg::Point( ps );
        getOrCreateStateSet()->setAttributeAndModes( lpP );
    }

    void ChildAreaElement::traverse(osg::NodeVisitor& nv)
    {
        osgUtil::CullVisitor* cv = dynamic_cast<osgUtil::CullVisitor*>(&nv);
        if ( cv )
        {
            for ( auto g : _childMap )
            {
                if ( g.second->isShowChild() )
                {
                    if ( !containsNode( g.second->_gridRoot ) )
                    {
                        addChild( g.second->_gridRoot );
                    }
                    if ( g.second->_gridNode && !g.second->_gridRoot->containsNode( g.second->_gridNode ))
                    {
                        g.second->_gridRoot->removeChild( 0, g.second->_gridRoot->getNumChildren() );
                        g.second->_gridRoot->addChild( g.second->_gridNode );

                        // show box
                        // osg::Node* box = ydUtil::createBoundingBox(g.second->getBox() );
                        // g.second->_gridRoot->addChild( box );
                        // TRACE("show self grid: %d  ==> %s ==> %d\n", g.second->getLevel(), g.second->getIndex().toString().c_str(), g.second->getStep() );
                    }
                }
            }
        }

        ydBase::Element::traverse( nv );
    }
    void ChildAreaElement::showChild(GridNode *lpInfo, osg::BoundingBox const& childBB, TileFileManager *lpTileFileManager )
    {
        if ( !lpInfo->_childNode )
        {
            osg::ref_ptr<ChildAreaElement> lpChild = dynamic_cast<ChildAreaElement *>(ChildElementCache::Instance()->get( get("scanid") + std::to_string(lpInfo->getLevel()) + lpInfo->getIndex().toString()));
            if ( !lpChild )
            {
                 lpChild = new ChildAreaElement( false, _level + 1, _step / CHILD_LAYER_COUNT );

                lpChild->set("cachepath", get("cachepath"));
                lpChild->set("scanid", get("scanid"));
                lpChild->setRange( childBB );
                lpChild->load2( false, lpTileFileManager );
                if ( lpChild->getChildMap().size() == 0 )
                {
                    lpInfo->_childNode = NULL;
                    lpInfo->setShowChild( false );
                    return ;
                }
            }

            G_LoadFileCount ++;
            if ( G_LoadFileCount >= G_FrameLoadFileCount )
            {
                return;
            }
            lpInfo->_childNode = lpChild;
        }

        if ( !containsNode( lpInfo->_childNode ) )
        {
            addChild( lpInfo->_childNode );
        }

        lpInfo->setShowChild( true );
    }

    void ChildAreaElement::showSelf(GridNode *lpInfo, bool dirty, GridBoxInfo *lpMemoryBoxInfo )
    {
        if ( !lpInfo )
        {
            return;
        }
        // show self
        if ( lpInfo->_childNode )
        {
            ChildElementCache::Instance()->add( get("scanid") + std::to_string(lpInfo->getLevel()) + lpInfo->getIndex().toString(), lpInfo->_childNode);
            this->removeChild( lpInfo->_childNode );
            lpInfo->_childNode = NULL;
        }

        if ( !this->containsNode( lpInfo->_gridRoot ) )
        {
            this->addChild( lpInfo->_gridRoot );
            //TRACE("add self grid root : %d, %s\n", lpInfo->getLevel(), lpInfo->getIndex().toString().c_str() );
        }

        if ( dirty ||
             !lpInfo->_gridNode )
        {

            if ( G_LoadFileCount > G_FrameLoadFileCount )
            {
                return;
            }
            osg::ref_ptr<GridNode> gn( lpInfo );
            osg::ref_ptr<GridBoxInfo> memorygn = lpMemoryBoxInfo;//lpTileFileManager->getGridBoxInfo( lpInfo->getLevel(), lpInfo->getIndex () );
            std::thread t([this, dirty, gn, memorygn ](){
                              if ( memorygn )
                              {
                                  if( memorygn->gridDirty() )
                                  {
                                      gn->_gridNode = memorygn->_blockBody->makeNode( memorygn->getStep(), this->_faceColor );
                                      memorygn->clearGridDirty();

                                      G_LoadFileCount ++;
                                  }
                              }else
                              {
                                  if ( dirty )
                                  {
                                      gn->_blockBody->freeChain();
                                  }
                                  //TRACE("%d, %d : %s\n", gn->getStep(), gn->getLevel(), gn->getIndex().toString().c_str() );
                                  gn->_gridNode = gn->_blockBody->makeNode( gn->getStep(), this->_faceColor );
                                  gn->_blockBody->freeChain();
                                  G_LoadFileCount ++;
                              }
                          });
            t.detach();
        }

        lpInfo->setShowChild( true );
    }

    void ChildAreaElement::checkChild( int width, int height, float slope, osg::Polytope & frustum, osg::Vec3 const &cameraPos, TileFileManager *lpTileFileManager )
    {
        // if ( !frustum.contains( getBox() ) )
        // {
        //     return ;
        // }
        load2( false, lpTileFileManager );

        //removeChild( 0, getNumChildren() );
        float levelToLodRangeMax = _levelSize.get(_level);
        for ( auto g : _childMap )
        {
            osg::ref_ptr<GridBoxInfo> memorygn = lpTileFileManager->getGridBoxInfo( getGridKey( g.second->getLevel(), g.second->getIndex() ) );
            if ( _step > 1 )
            {
                osg::BoundingBox childBB = g.second->getBox();
                double projFactor = (0.5 * height) / (slope * ( childBB.center() - cameraPos ).length() );
                float screenPixelLen = childBB.radius() * projFactor;

                if ( screenPixelLen > levelToLodRangeMax )
                {
                    if ( !g.second->isShowChild() )
                    {
                        std::string dbFile = getFilePath( g.first, g.second->getLevel() );

                        if(ydUtil::fileExist(dbFile.c_str()) || memorygn )
                        {
                            showChild( g.second, childBB, lpTileFileManager );
                            if ( g.second->_childNode )
                            {
                                continue;
                            }
                        }
                    }
                    else
                    {
                        if ( !containsNode( g.second->_childNode ) )
                        {
                            if ( !g.second->_childNode )
                            {
                                std::string dbFile = getFilePath( g.first, g.second->getLevel() );

                                if(ydUtil::fileExist(dbFile.c_str())  || memorygn )
                                {
                                    showChild( g.second, childBB, lpTileFileManager);
                                    if ( g.second->_childNode )
                                    {
                                        continue;
                                    }
                                }
                            }else
                            {
                                showChild( g.second, childBB, lpTileFileManager );
                                continue;
                            }
                        }else
                        {
                            continue;
                        }
                    }
                }
            }

            bool dirty = g.second->checkFileDirty();
            if ( !dirty )
            {
                if ( memorygn && memorygn->gridDirty() )
                {
                    dirty = true;
                }
            }
            if ( ( g.second->isShowChild() ) ||
                 dirty )
            {
                showSelf(g.second, dirty, memorygn);
            }
        }
    }

    void ChildAreaElement::initGridBoxInfo( ydUtil::BlockIndex const &bi, BoxElement *lpInfo )
    {
        BoxElement::initGridBoxInfo( bi, lpInfo );

        GridBoxInfo *lpGridInfo = dynamic_cast<GridBoxInfo*>( lpInfo );
        if ( lpGridInfo )
        {
            lpGridInfo->_blockBody->setFilePath( ScanAreaElement::getFilePath( get("cachepath"), bi, lpInfo->getLevel() ) );
        }

    }

    ChildAreaElement::GridNode *ChildAreaElement::getGridInfo( ydUtil::BlockIndex const& childIndex, osg::Vec3 const& globalCenter )
    {
        GridNode *lpInfo = NULL;
        std::map<ydUtil::BlockIndex, osg::ref_ptr<GridNode> >::iterator it = _childMap.find( childIndex );

        if ( it == _childMap.end() )
        {
            GridNode *lpTmpInfo = new GridNode( _level, _step );

            _childMap[childIndex] = lpTmpInfo;
            lpInfo = lpTmpInfo;

            initGridBoxInfo( childIndex, lpTmpInfo );

        }else
        {
            lpInfo = it->second;
        }

        return lpInfo;
    }

    std::string ChildAreaElement::getFilePath( ydUtil::BlockIndex const& index, int level )
    {
        return ScanAreaElement::getFilePath( get("cachepath"), index, level );
    }
}
