#include "CustomModelManipulator.h"
#include <ydUtil/matrix_math.h>
#include "BlockShower/CameraTrackElement.h"

namespace BlockShower
{
    CustomModelManipulator::CustomModelManipulator()
    {
        setAllowThrow(false);
        // _trackerMode = osgGA::NodeTrackerManipulator::NODE_CENTER_AND_ROTATION;
        // setVerticalAxisFixed(false);
    }

    void CustomModelManipulator::setByMatrix(const osg::Matrixd& matrix)
    {
        osg::Vec3d eye,center,up;
        matrix.getLookAt(eye,center,up,_distance);
        
        osg::Matrixd lookat;
        lookat.makeLookAt(eye,center,up);

        _rotation = lookat.getRotate().inverse();
    }

    osg::Matrixd CustomModelManipulator::getMatrix() const
    {
        osg::Matrixd mat;
        CameraTrackElement* cte = dynamic_cast<CameraTrackElement*>(_node.get());
        if(!cte)
        {
            return mat;
        }

        mat = osg::Matrixd::rotate(cte->getRotate()) * osg::Matrixd::translate(cte->getPos());      //  + osg::Vec3(0.1, 0.1, 0.1)
        return mat;
    }

    osg::Matrixd CustomModelManipulator::getInverseMatrix() const
    {
        return osg::Matrixd::inverse(getMatrix());
    }


    void CustomModelManipulator::setTrackNode(osg::Node* node)
    {
        if (!node)
        {
            OSG_NOTICE<<"CustomModelManipulator::setTrackNode(Node*):  Unable to set tracked node due to null Node*"<<std::endl;
            return;
        }

        _node = node;
    }

}
